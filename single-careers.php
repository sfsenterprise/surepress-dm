<?php 
    use Surepress\Functions\Common as Common;
?>
<?php  get_header(); ?>
<?php
if ( have_posts() ) {
    while ( have_posts() ) : the_post(); ?> 
        <section class="hero-image career">
            <h1>
                Careers                        
            </h1>
            <?php do_action( 'after_hero_title'); ?> 
        </section>
        <?php $class_container = ( is_page('our-process') ? 'container-fluid' : 'container' ); ?>
        <div class="<?php echo $class_container; ?> ">
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                igmar
                <?php get_template_part('template-parts/careers/section', 'careers-single' ); ?>
            </div>
        </div> 
<?php
    endwhile;
}?>
<?php get_footer();?>