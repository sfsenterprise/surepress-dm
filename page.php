<?php 
    use Surepress\Functions\Common as Common;
?>
<?php  get_header(); ?>
<?php
if ( have_posts() ) {
    while ( have_posts() ) : the_post(); ?> 
      
        <?php if( is_front_page() || ( !Common\is_main() && is_front_page() ) ): ?>
            <?php get_template_part('template-parts/static', 'homepage' ); ?> 
        <?php elseif( is_page('thank-you') || is_page('reviews-thank-you')): ?>
            <?php get_template_part('template-parts/static', 'thank-you'); ?>
        <?php elseif( is_page('remodeling-guide-download')): ?>
            <?php get_template_part('template-parts/static', 'remodeling-guide-download'); ?>
        <?php elseif( get_post_meta(get_the_ID(), 'is_campaignsty', TRUE) == "1" ): ?>    
            <?php get_template_part('template-parts/static', 'campaigns-thank-you'); ?>
        <?php else : ?>
            <?php if(is_page('about')&&(get_current_blog_id() == 33)): ?>
                <section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/wp-content/uploads/2024/06/elizabethtown-about-banner-2.jpg');">>
            <?php else: ?>
                <section class="hero-image">
            <?php endif; ?>
                <h1>
                    <?php 
                    if(is_page('areas-we-serve')) {
                        $wp_query->nap->microsite_name = "";
                    }
                    ?>
                    <?php //echo( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').ucwords(get_the_title()); ?>
                    <?php
                    if(!empty($wp_query->nap->microsite_name) ){
                        if(!is_page('design-center-tour') && !is_page('resource-center')){
                            if(is_page('directions') && !empty( get_page_by_title('Areas We Serve') )){
                                echo $wp_query->nap->microsite_name.' - Service Areas';
                            } else {
                                echo $wp_query->nap->microsite_name.' - '.get_the_title();
                            }
                        }
                    }else{
                        if(!is_page('design-center-tour') && !is_page('resource-center')){
                            echo get_the_title();
                        }
                    }
                    ?>
                </h1>
                <?php do_action( 'after_hero_title'); ?> 
            </section>
            <?php $class_container = ( is_page('our-process') ? 'container-fluid' : 'container' ); ?>
            <div class="<?php echo $class_container; ?> ">
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                    <?php get_template_part('template-parts/static', $post->post_name ); ?>   
                    <?php
                        $the_content = get_the_content();
                        $the_content = do_shortcode($the_content);
                        $the_content = wpautop($the_content);

                        if( !is_page('our-process') && !is_page('contact-us')  && !is_page('directions') && !is_page('reviews')  ) {
                            if(is_page('Design Center Tour') && $the_content == ''){
                                echo '<img src="'.get_bloginfo('template_url').'/dist/images/design-center-placeholdercontent.jpg" title="Design Center Tour" style="width: 100%">';
                            }else{
                                echo $the_content;
                            }
                        }
                    ?>
                    <?php $vendors = get_post_meta( get_the_ID(), 'preferred_vendor', true); ?>
                    <?php if($vendors && count($vendors) > 0): ?>
                        <p>&nbsp;</p>
                        <hr />
                        <p>&nbsp;</p>
                        <ul style="margin: 0; padding: 0; list-style: none;">                        
                        <?php foreach($vendors as $vendor): ?>
                            <li>
                                <div class="row">
                                    <div class="col-md-3 col-xs-12" align="center">
                                        <?php if($vendor['vendor_link'] != ""): ?>
                                        <a href="<?php echo $vendor['vendor_link']; ?>" target="_blank">
                                        <?php endif; ?>    
                                            <?php echo wp_get_attachment_image($vendor['image'], 'full', false, array('alt' => $vendor['alt'], 'class' => 'img-responsive' ) ); ?>
                                        <?php if($vendor['vendor_link'] != ""): ?>
                                        </a>
                                        <?php endif; ?>
                                    </div>  
                                    <div class="col-md-9 col-xs-12">
                                        <h3>
                                            <?php if($vendor['vendor_link'] != ""): ?>
                                            <a href="<?php echo $vendor['vendor_link']; ?>" target="_blank">
                                            <?php endif; ?>    
                                                <?php echo $vendor['title']; ?>
                                            <?php if($vendor['vendor_link'] != ""): ?>    
                                            </a>
                                            <?php endif; ?>
                                        </h3>
                                        <?php echo $vendor['vendor_caption']; ?>
                                    </div>  
                                </div>
                                <hr class="clearfix" style="margin: 30px 0; float: none; clear: both;" />
                            </li>    
                        <?php endforeach; ?>   
                        </ul>                         
                    <?php endif; ?>

                </div>
            </div>                
        <?php endif; ?>    
    <?php endwhile;
} else {
    get_template_part('template-parts/content', 'none');
}
?>
<?php get_footer();?>