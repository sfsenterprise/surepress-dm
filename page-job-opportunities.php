<?php 
    use Surepress\Functions\Common as Common;
    $page_id = get_page_by_title('Job Opportunities')->ID;
    $background =  !empty(get_the_post_thumbnail_url($page_id)) ? get_the_post_thumbnail_url($page_id) : '/wp-content/themes/surepress-dm/dist/images/hero-galleries.jpg';
?>
<?php  get_header(); ?>
<?php
if ( have_posts() ) {
    while ( have_posts() ) : the_post(); ?> 
            <section class="hero-image" style="background: url('<?php echo $background; ?>') center no-repeat !important;background-size:cover !important;">
                <h1>
                    <?php 
                    if(is_page('areas-we-serve')) {
                        $wp_query->nap->microsite_name = "";
                    }
                    ?>
                    <?php echo( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').ucwords(get_the_title()); ?>                        
                </h1>
                <?php do_action( 'after_hero_title'); ?> 
            </section>
            <?php $class_container = ( is_page('our-process') ? 'container-fluid' : 'container' ); ?>
            <div class="<?php echo $class_container; ?> ">
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                    <input type="hidden" id="site-id" name="site-id" value="<?php echo get_current_blog_id(); ?>">
                    <?php get_template_part('template-parts/careers/section', 'careers' ); ?>
                </div>
            </div>                
    <?php endwhile;
} else {
    get_template_part('template-parts/content', 'none');
}
?>
<?php get_footer();?>