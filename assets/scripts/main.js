/*global yt_video_data*/
/** import external dependencies */
import 'jquery';
import 'bootstrap-sass';
import '@fancyapps/fancybox';
import 'slick-carousel';

// to detect if element is in viewport -Japs
(function($, win) {
  $.fn.inViewport = function(cb) {
     return this.each(function(i,el){
       function visPx(){
         var H = $(this).height(),
             r = el.getBoundingClientRect(), t=r.top, b=r.bottom;
         return cb.call(el, Math.max(0, t>0? H-t : (b<H?b:H)));  
       } visPx();
       $(win).on("resize scroll", visPx);
     });
  };
}(jQuery, window));


(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
	var Function = {
		get_case_study: function(IDs) {
			$.ajax({
				type:"POST",
				url: ajax_object.ajaxurl, // eslint-disable-line
				data: {
					action: 'get_case_studies',
					id: IDs,
				},
				dataType: 'html',
				success:function(response){
					$('.animated.bounceOutUp').remove();
					
					$('.case-studies-loading').fadeOut('fast', function(){
						$('.case-studies.panel-group').append(response);
					});
				},
				error: function(errorThrown){console.dir("error "+errorThrown.status);}, 
			}); 
		},

		getCookie: function(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		},

		fillEventSchedule: function(schedules){
			$('.events_dropdown select').empty();
			$(schedules).each(function(i){
				$(schedules[i].time).each(function(index){
					$('.events_dropdown select').append('<option>' + schedules[i].date + ' @ ' + schedules[i].time[index] + '</option>');
				});
			});
		},
	}

	var on_mobile = {  
		faq:function() {var append = "";
		$('.faq-categories li a').each(function(){
			var id = ($(this).data('id'));
			var title = $(this).text();
			var temp="";

			$('.panel.category-'+id).each(function(index){
				var panel_id = 'faq-accordion-mobile-inner-'+id;
				var content = $(this).find('.panel-body').text();
				var question = $(this).find('.panel-title').text();
				temp += '<div class="panel panel-default">'
							+'<a class="collapsed" data-toggle="collapse" data-parent="'+panel_id+'" href="#q'+id+index+'">'
								+'<div class="panel-heading">'
									+'<strong class="panel-title">'+question+'</strong>'
								+'</div><i></i>'
							+'</a>'
							+'<div id="q'+id+index+'" class="panel-collapse collapse">'
								+'<div class="panel-body">'+content+'</div>'
							+'</div>'
						+'</div>';
			});

			append += '<a class="collapsed parent-accordion" data-toggle="collapse" data-parent="#faqs-accordion-mobile" href="#faq-mobile-'+id+'">'
						+'<div class="panel-heading">'
							+'<strong class="panel-title">'+title+'</strong>'
						+'</div>'
					+'</a>'
					+'<div id="faq-mobile-'+id+'" class="panel-collapse collapse">'
						+'<div class="panel-group" id="faq-accordion-mobile-inner-'+id+'">'+temp+'</div>'
					+'</div>';
		}); 

		$('#faqs-accordion-mobile .panel').append(append);},

		services_inner: function(){
			var append="";
			$('.services-accordion-items li a').each(function(){
				var id = $(this).data('id');
				var title = $(this).html();
				var content = $('.services-accordion-items.tab-content #tab-'+id).html();

				append +='<a class="collapsed parent-accordion" data-toggle="collapse" data-parent="#faqs-accordion-mobile" href="#faq-mobile-'+id+'">'
						+'<div class="panel-heading">'
							+'<strong class="panel-title">'+title+'</strong>'
						+'</div>'
					+'</a>'
					+'<div id="faq-mobile-'+id+'" class="panel-collapse collapse">'
						+'<div class="panel-group" id="faq-accordion-mobile-inner-'+id+'">'+content+'</div>'
					+'</div>';
			});

			$('#services-inner-accordion-mobile .panel').append(append);
		},

		services_slider: function(){
			var slick_responsive_services= [{breakpoint: 1920,settings: "unslick" },{breakpoint: 768,settings: {slidesToShow: 1,slidesToScroll: 1}}];

			if( $( window ).width() >= 768 ){
				$('.services-slicker').slick("unslick");
			}else{
				$('.services-slicker').slick({
					prevArrow: $('.prev'),
					nextArrow: $('.next'),
					responsive: slick_responsive_services,
				});
			}
		},

		financing_offers: function(){
			var append="";
			$('.offer-categories li a').each(function(){
				var id = ($(this).data('id'));
				var title = $(this).text();
				var content=$('.offer-categories .tab-content #tab-'+id).html();

				append +='<a class="collapsed parent-accordion" data-toggle="collapse" data-parent="#financing-offers-accordion-mobile" href="#financing-offers-mobile-'+id+'">'
						+'<div class="panel-heading">'
							+'<strong class="panel-title">'+title+'</strong>'
						+'</div>'
					+'</a>'
					+'<div id="financing-offers-mobile-'+id+'" class="panel-collapse collapse">'
						+'<div class="panel-group" id="faq-accordion-mobile-inner-'+id+'">'+content+'</div>'
					+'</div>';
			});

			$('#financing-offers-accordion-mobile .panel').append(append);

			$('.nav-tabs a').on('click', function(){
				var tab = $(this).attr('data-id');

				$('#tab-'+tab).addClass('animated zoomIn', function(){
					$('.tab-pane.active').removeClass('animated zoomIn');
				});
			});
		},
  };

  

  var SP_THEME = {
    // All pages
    'common': {
		init: function() {

			$(".areas-we-serve h2").html("Jobsite Map");

			if (window.location.href.indexOf('?brochure=') > 0) {
				//window.open('https://www.dreammaker-remodel.com/wp-content/uploads/2020/05/5-Common-Remodeling-Mistakes-Home-Remodeling-Guide-Final-2020.pdf', '_blank');
				document.getElementById("dl").click();
			}
			// hide announcement if already clicked close
			if ( window.sessionStorage && window.sessionStorage.getItem('sfl-announcement-hide') !== null ) {
				$('section.announcements').css('display', 'none')
			}

			// close announcements
			$('#announcementClose').click(function(){
				$('section.announcements').hide('slow')
				$('.hero-image').removeAttr('css');
				sessionStorage.setItem('sfl-announcement-hide', true);
				return false;
			});

			
			if( window.sessionStorage && window.sessionStorage.getItem('sfl-announcement-hide') !== null && sessionStorage.getItem('sfl-sfl-announcement-hide') === true) {
				$('body').removeClass('hasAnnouncement')
			} else {
				$('body').addClass('hasAnnouncement')
			}


			// hide announcement if already clicked close
			if ( window.sessionStorage && window.sessionStorage.getItem('sfl-announcementFooter-hide') !== null ) {
				$('section.announcementsFooter').css('display', 'none')
			}
			// close announcements
			$('#announcementFooterClose').click(function(){
				$('section.announcementsFooter').hide('slow')
				sessionStorage.setItem('sfl-announcementFooter-hide', true);
				return false;
			});			


			//locations remove .collapse on mobile 
			$(window).on("resize", function () {
					$('.location_archive .panel').each(function(){
						var height = $(this).find('.panel-footer .collapse p').height();

						if(  ( $(window).width()  > 1199 && height > 65) ||
							( $(window).width()  > 991 && height > 55 ) ||
							( $(window).width()  <= 991 && height > 38 )){
							$(this).find('.panel-footer .btn-collapse').attr("data-toggle", 'collapse');
							$(this).find('.btn-collapse').addClass("see-more");
						}else{

							$(this).find('.panel-footer .btn-collapse').attr("data-toggle", '');
							$(this).find('.btn-collapse').removeClass("see-more");
						}		

					});
			}).resize();

			//direction link condition on mobile
			if( ($( window ).width()  < 768) && ($('.direction-url').length != 0)   ){
				var newHREF = "";
				var old = $('.direction-url').attr('href');
				newHREF = old.replace('maps/dir/?api=1&destination', '?&daddr' );
				$('.direction-url').attr("href", newHREF );
			}

			//Hide Show Buttons on Reviews
			if( $('.page.reviews .no-guild-reviews').length > 0 ){
				$('.page.reviews .guild-reviews').remove();
			}

			if( $('.page.reviews .guild-quality-map').length == 0){
				$('.page.reviews .feedback').remove();
			}
			

			require('./mobile/menu.js');

			//animations
			$(".home-category-thumbs ul, section.our-team, section.call-to-action, section.events-section, section.home-process ul").inViewport(function(px){ 
				if(px){ 
					$(this).addClass("animate") ; 
				}
			});	


			//SEARCH STUFFS START
			require('./map/geo-locate.js'); //USE MY LOCATION button @header

			//disable letters on zip code inputs
			$('.location_zip').keyup(function(){
				if (/\D/g.test(this.value))
				{ this.value = this.value.replace(/\D/g, ''); }
			});

			//LOCATION SEARCH STUFFS
			$('.location_zip').change(function(){
				$("#s, #t").val(this.value);
			});
			//SEARCH STUFFS END


			$('.fancybox, [data-fancybox="gallery"]').fancybox({
				idleTime: 0, 
			});			

			
			//scroll up/down detection
			var lastScrollTop = 0;
			$(window).scroll(function(){
				var st = $(this).scrollTop();
				if (st > lastScrollTop){
					$('body').removeClass('scrolledUp').addClass('scrolledDown');
				} else {
					$('body').removeClass('scrolledDown').addClass('scrolledUp');
				}
				lastScrollTop = st;
			});	

			// FAQ & Glossary Filter by Categories [Archive Page]  
			$(".faq-categories a , .glossary-categories a , .case-categories a ").click(function(){
				var keyword = $(this).data('id');

				$('.panel').addClass('hide').removeClass("show");
				$('.category-'+keyword).addClass("show");
			});

			//Case Studies add category class
			$('.post-type-archive-case-studies .panel').addClass(function(){
					return $(this).find('.hidden').text();
			});


			// [Mobile] Create Accordion on  FAQs Page 
			on_mobile.faq();
			on_mobile.services_inner();
			on_mobile.financing_offers();

			// Events set form title
			$('.post-type-archive-events .btn').click(function(){
				var title = $(this).data('title');
				$('.post-type-archive-events .modal-body h3').text(title);
			});

			//Events & Offers set captcha to left
			$('.post-type-archive-events #event1 .g-recaptcha, .post-type-archive-offers  #offer1 .g-recaptcha').closest('.field_description_below').addClass('gf_left_half');


			//Offers set form fields
			$('.post-type-archive-offers .view-offer').click(function(){
				var id = $(this).data('id');
				var classname =  '.post-type-archive-offers .modal-field.field-'+id;
				var title = $(classname+' .title').text();
				var header = $(classname+' .header-text').html();
				var offer = $(classname+' .offer-text').text();
				var offer_link = $(classname+' .offer-link').text();

				$('.post-type-archive-offers .modal-body h3').text(title);
				$('.post-type-archive-offers .modal-body .header-text').html(header);

				$('.hidden-field-id input:text').val(id);
				
				if(offer_link){
					$('.post-type-archive-offers .modal-footer .offer-text').text(offer);
					$('.post-type-archive-offers .modal-footer a').attr("href", offer_link);
				}else{
					$('.post-type-archive-offers .modal-footer').hide();
				}
				
			});

			//reload form on modal close after form submit
			$('.post-type-archive-events .modal, .post-type-archive-offers .modal').on('hidden.bs.modal', function() {
				if( $(this).find('.gform_confirmation_message').length == 1)
					window.location.reload();
			});


			var slick_responsive= [{breakpoint: 600,settings: {slidesToShow: 2,slidesToScroll: 2}},{breakpoint: 480,settings: {slidesToShow: 1,slidesToScroll: 1}}];
			//Slick Slider General Settings - used in case studies
			$('.case-image .slicker, .tax-portfolio_categories .slicker').slick({
				prevArrow: $('.prev'),
				nextArrow: $('.next'),
				responsive: slick_responsive,
			});

			on_mobile.services_slider();

			$( window ).resize(function(){
				on_mobile.services_slider();
			});


			//Popup Share Box
			$('.share_popup').click(function(e){
				e.preventDefault();
				window.open($(this).attr('href'),"_blank","width=500,height=500");
			});

			//Slider Form: pass checkbox data to popup on click
			$('.home-grey-categories form').submit(function(){

				var check_values = [];
				
				$(this).find("ul li input[type='checkbox']:checked").each(function(){
						check_values.push($(this).val());
				});

				$(".start-conversation-popup input[type='checkbox'] ").each(function(){
						if ( check_values.includes($(this).val()) ){
							$(this).prop("checked", true);
						}else{
							$(this).prop("checked", false);
						}
				});

				return false;
			});

			if($('.events-section .col-sm-4').length < 3){
				$('.events-section .col-sm-4').addClass('event-center');
			}
		},
		finalize: function() { },
	},
	// Blog Archive
	'home': {
		init: function() {

			//slider checkboxes
			$('.home-grey-categories label').click(function(){
				var checked = ( $('input[type=checkbox]:checked', this) ).length;
				
				if(checked !== 0) {
					$(this).addClass('active');	
				} else {
					$(this).removeClass('active');	
				}
				
			});	

			$('.moreReviewContent').click( function() {
					$('.home-reviews').addClass('more');
					$(this).hide('fast');
					return false;
			});

			
			$('#homeReviewCarousel').on('slide.bs.carousel', function () {
					$('.moreReviewContent').show('fast');
					$('.home-reviews').removeClass('more');			  	
			})			

		},
		finalize: function() { },
	},

	'page_template_template_locations': {
		init: function() {
			require('./map/map-multi.js');

			//LOCATION SEARCH STUFFS
			$('#location_state').change(function(){
				$("#s").val( $("option:selected", this ).val() ).parent(".searchform").submit();
			});		
	

		},
		finalize: function() { },
	},	

	'directions': {
		init: function() {
			require('./map/map-single.js');
		},
	},	

	'contact_us': {
		init: function() {
			require('./map/map-single.js');
		},
	},		

	'microsite_home': {
		init: function() {
			require('./map/map-single.js');
		},
	},		

	'reviews' : {
		init: function() {
			
			$( "#moreReviews" ).click(function() {
				let page    = $(this).data('currentpage');
				let perpage = $(this).data('perpage');
				let ajaxurl = $(this).data('ajaxurl');
				let total   = $(this).data('total');
				let form   = $(this).data('form');
				let count   = $('#moreReviewsCount').text();

				jQuery.ajax({
					type : "post",
					dataType : "json",
					url : ajaxurl,
					data : { action: "reviews", page: page++, perpage: perpage, total: total, form: form },
					beforeSend: function() {
						$('#reviewLoader').show('fast');
						$('#moreReviews').attr('disabled','disabled');
					},
					success: function(response) {
						if (response.success) {
							$('#moreReviewsCount').html( parseInt(count) + parseInt(response.data.count) );
							$('#moreReviews').data('currentpage', page++);
							$('#reviewLoop').append(response.data.html);
						} 

						if (response.data.last_page) {
							$('#moreReviews').hide('fast');
						}	
					},
					complete: function(){
						$('#reviewLoader').hide('fast');
						$('#moreReviews').removeAttr('disabled');
					},
				})
			});			

			$('.kv-ltr-theme-fa-star').rating({
				hoverOnClear: false,
                theme: 'krajee-fa',
				step: 1, 
            });
            function getUrlVars() {
				var vars = [], hash;
				var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
				for(var i = 0; i < hashes.length; i++) {
					hash = hashes[i].split('=');
					vars.push(hash[0]);
					vars[hash[0]] = hash[1];
				}
				return vars;
			}
			jQuery('#review-link').click(function(){
				$('html, body').animate({
					scrollTop: $('.create-review').offset().top - 380,
				}, 1000);
			});
			var submission = getUrlVars()["submission"];
			if(submission === 'success'){
				$('#create-review').html('<h6 id="review-message">Thank you for submitting a review! We hope everything met your expectations. Should a response be required we will get in touch with you soon.</h6>');
				$('#create-review').show();	
				$('html, body').animate({
					scrollTop: $('.create-review').offset().top - 300,
				}, 2000);
			}else if ($('#create-review').find('form .validation_message').length !== 0 ){
				$('#create-review').show();	
				location.href="#reviews-anchor";

			}
           
            var form_id = $('#create-review').find('form').attr('id');
            var arr = new Array();
            $('#create-review .gfield_contains_required input, #create-review .gfield_contains_required textarea').each(function() {
				arr.push(this.id);
			});
            $('#'+$('#create-review').find('input[type="submit"]').attr('id')).click(function(e){

				if(checkValid() && emailValid()){
					$('#'+form_id).submit();
				}else{
					e.preventDefault();
				}
				
            });

			var checkValid = function(){
				var val = true;
				$.each(arr, function(index,val){
					
					if( $('#'+val).val().length  === 0){
						$('#'+val).parent().closest('li').addClass('gfield_error');
						if(!$('#'+val).parent().closest('li').find('.gfield_description').length){
							$('#'+val).parent().closest('.ginput_container').after('<div class="gfield_description validation_message">This field is required.</div>');
						}
						console.log('checkValid failed');
						val = false;
					}else{
						$('#'+val).parent().closest('li').removeClass('gfield_error');
						if($('#'+val).parent().closest('li').find('.gfield_description').length){
							$('#'+val).parent().closest('li').find('.gfield_description').remove();
						}
					}
				});

				return val;
			}

			var emailValid = function(){
				var email = $('input[placeholder^="Email"]').val();
				var filter = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
				if(filter.test(email)){
					return true;
				}else{
					$('input[placeholder^="Email"]').parent().closest('li').addClass('gfield_error');
					if(!$('input[placeholder^="Email"').parent().closest('li').find('.gfield_description').length){
						$('input[placeholder^="Email"').parent().closest('.ginput_container').after('<div class="gfield_description validation_message">Invalid Email address.</div>');
					}
					console.log('emailValid failed');
					return false;
				}
			}

		},
		finalize: function() {
			$('.rating-input').on('change', function(){
				$('.rating-input').children().find('input').attr('value', $(this).val() );
			});
		},
	},
	// events archive
	'post_type_archive_events': {
		init: function(){

		},
		finalize: function(){
			let field_id = event_obj.event_field.field; // eslint-disable-line
			let form_id = event_obj.event_field.form_id; // eslint-disable-line

			$('.event-container .btn-info').on('click', function(){
				$('#input_' + form_id + '_' + field_id).val($(this).data('eventid'));
				var schedules = $(this).data('schedule');

				//store for page error
				document.cookie="event_schedid="+ $(this).data('eventid');
                Function.fillEventSchedule(schedules);
			});

			jQuery(document).on('gform_post_render', function(event, form_id, current_page){
				console.log(current_page); //ignore this
				var id = Function.getCookie("event_schedid");
				var sched2 = $('.btn-'+id).data('schedule');
				Function.fillEventSchedule(sched2);
				
			});

			$('.events article').each(function(){
				var parent = $(this).height(); 
				$(this).find('img').height(parent);
			});
	
			$(window).on("resize", function () {
				// Adjust event height on MD
				$(".events.type-events article").each(function(){
					var event_h = '100%';
					var img_h = '300'; 
					var w = $(this).find(".event-container").height();

					if( $( window ).width() >= 768){
						event_h = (w - 130);
						img_h = (w + 150); 	
					}

					$(this).find(".event-wrap").height(event_h);
					$(this).find("figure img").height(img_h);
				});

				
				if( $( window ).width() >= 768){
					jQuery(document).bind('gform_post_render', function(){
						$('.post-type-archive-events #event1 .g-recaptcha').closest('.gfield_error').addClass('gf_left_half');
					});
				}

			}).resize();
		},
	},

	'homepage': {
		init: function(){
			let field_id = event_obj.event_field.field; // eslint-disable-line
			let form_id = event_obj.event_field.form_id; // eslint-disable-line

			$('.event-center a').on('click', function(){
				console.log('#input_' + form_id + '_' + field_id);
				$('#input_' + form_id + '_' + field_id).val($(this).data('eventid'));

				$('.event-modal h3').text($(this).data('title'));

				$('.events_dropdown select').empty();

                var schedules = $(this).data('schedule');

                $(schedules).each(function(i){
                    $(schedules[i].time).each(function(index){
                        $('.events_dropdown select').append('<option>' + schedules[i].date + ' @ ' + schedules[i].time[index] + '</option>');
                    });
                });
			});
		},
		finalize: function(){},
	},

	//offers archive
	'post_type_archive_offers': {
		init: function(){
		},
		finalize: function(){
			$(window).on("resize", function () {
				if( $( window ).width() >= 768){
					jQuery(document).bind('gform_post_render', function(){
						$('.post-type-archive-offers #offer1 .g-recaptcha').closest('.gfield_error').addClass('gf_left_half');
					});
				}
				
			}).resize();
		},
	},

	'post_type_archive_case_studies': {
		init: function(){
			var site_id = $('.case-categories').data('id');

			var get_studies = function(category) {
				$.ajax({
					url: '/surefire/api/studies/filter/',
					method: 'GET',
					dataType: 'json',
					data: {
						site_id: site_id,
						category: category,
					},
					success: function(data) {
						//if(data.status == '200' && data.response.saved_case_cats.length > 0){
						if(data.status == '200' && Object.keys(data.response.saved_case_cats).length > 0){

							$('#case-studies-title').show();
							$('.case-categories .loading').fadeOut('normal', function(){
								$('.case-categories').append("<li class='animated slideInRight'><a href='#'>All</a></li>");
								/*
								$(data.response.saved_case_cats).each(function(i){
									if(data.response.saved_case_cats[i] == '-1'){
										$('.case-categories').append("<li class='animated slideInRight'><a href='#'>Other</a></li>");
									}else{
										$('.case-categories').append("<li class='animated slideInRight'><a href='#'>"+data.response.saved_case_cats[i]+"</a></li>");
									}
								});
								*/
								$.each(data.response.saved_case_cats, function(index,value){
									if(index < 0){
										$('.case-categories').append("<li class='animated slideInRight'><a href='#'>Other</a></li>");
									}else{
										$('.case-categories').append("<li class='animated slideInRight'><a href='#'>"+value+"</a></li>");
									}
								});
								$('.case-categories .loading').remove();
							});
						//} else if(data.status == '301' || data.response.saved_case_cats.length == 0) {
						} else if(data.status == '301' || Object.keys(data.response.saved_case_cats).length == 0) {
							$('#case-studies-title').hide();
							$('.case-categories').empty();

							$('.case-studies-loading').fadeOut('normal', function(){
								$('.case-studies-loading').remove();
								$('.case-studies.panel-group').html("<center class='animated pulse'>No case studies found</center>");
							})
						}
						
					},
				})
				.done(function(data){
					if(data.status == '200') {

						var result;
						var get_study_IDs = [];
						var primArr = [];
						if (site_id == '1') {
							result = Object.keys(data.response).map(function(key) {
								if(key != 'saved_case_cats') {
									return [Number(key), data.response[key]];
								}
							});

							$(result).each(function(index){
								if(result[index] !== undefined){
									primArr[result[index][0]] = [];
									$(result[index][1]).each(function(i, val){
										if(typeof val == 'object') {
											var cases = Object.keys(val).map(function(key) {
												return [val[key]];
											});

											$(cases).each(function(post){
												primArr[result[index][0]].push(cases[post][0]);
											});
										} else {
											primArr[result[index][0]].push(val);
										}
									});
									get_study_IDs.push({"postID" : primArr[result[index][0]] ,"blogID" : result[index][0],"site_id": site_id});
								}
							});
						} else {
							result = Object.keys(data.response).map(function(key) {
								if(key != 'saved_case_cats') {
									return [data.response[key]];
								}
							});

							$(result).each(function(index){
								if(result[index] !== undefined){
									primArr[site_id] = result[index][0];
									get_study_IDs.push({"postID" : result[index][0] ,"blogID" : site_id,"site_id": site_id });
								}
							});
							
						}

						Function.get_case_study(get_study_IDs);

					}
				});
			}

			get_studies();

			$(document).on('click', 'body .case-categories a', function(){
				event.preventDefault();

				let categoryName = $(this).text();

				$('.case-studies.panel-group > div').each(function(){
					$(this).removeClass('animated slideInUp').addClass('animated bounceOutUp');
				});

				$('.case-studies-loading').fadeIn('slow');

				(categoryName != 'Other') ? get_studies(categoryName) : get_studies('-1');
				//get_studies(categoryName);
				
			});
		},
		finalize: function(){
		},	
	},

	'post_type_archive_videos': {
		init: function(){
			$('#load_more_ytvids').on('click', function(){
				let page = $(this).data('page');
				let limit = $(this).data('limit');

				let paginated = Object.values(yt_video_data).slice(
					(parseInt(page) - 1) * parseInt(limit), 
					parseInt(page)*parseInt(limit)
				);
				
				$(this).data('page', parseInt(page)+1);

				// console.log(page, paginated, yt_video_data);

				var html = '';
				paginated.map( function(id) {
					html += '<li>';
					html += '<div class="embed-responsive embed-responsive-16by9">';
					html += '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'+id+'" allow="encrypted-media; gyroscope; modestbranding" allowfullscreen="" frameborder="0">';
					html += '</iframe>';
					html += '</div>';
					html += '</li>';
				});

				$("#yt_video_list").append(html);

				if(paginated.length < 1) {
					$(this).prop('disabled', true);
				}
			});			
		},
		finalize: function(){},		
	},

};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = SP_THEME;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		},
	};

	// Load Events
	$(document).ready(UTIL.loadEvents);

	$('.review-slider').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		infinite : true,
		prevArrow : '<div class="arrow prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
		nextArrow : '<div class="arrow next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
	});
	$(document).ready(function(){
		$.ajax({
			url: ajax_object.ajaxurl, // eslint-disable-line
				data: {
					type:"POST",
					action: 'getjobs',
					id: $('#site-id').val(),
				},
				dataType: 'html',
				success:function(response){
					$('.job select').html(response);
				},
		});
	});
})(jQuery); // Fully reference jQuery after this point.
