      jQuery('.myLocation').click(function(){
        initGeolocate();        
        return false;
      }); 


      function initGeolocate() {
        /************************************ 
        WE DONT NEED THIS DUE TO THIS IS DEPENDENT ON LAT/LNG ONLY
        var geocoder = new google.maps.Geocoder;
        GeoLocate(geocoder);
        ************************************/
        GeoLocate();
        
      }



      //function GeoLocate(geocoder) {
      function GeoLocate() {
        if (navigator.geolocation) { //if browser support geolocation

          //get latitude/longtitude
          navigator.geolocation.getCurrentPosition(function(position) { // eslint-disable-line no-unused-vars
            var geo_coords = {
              lat: parseFloat(position.coords.latitude),
              lng: parseFloat(position.coords.longitude),
            };

            //insert hidden fields and prefill with LAT/LNG input forms
            jQuery('<input>').attr({
                type: 'hidden',
                name: 'lat',
                value: geo_coords.lat,
            }).appendTo('.searchform');            

            jQuery('<input>').attr({
                type: 'hidden',
                name: 'lng',
                value: geo_coords.lng,
            }).appendTo('.searchform');
            //auto submit the form
            $("#s,#t").val("my_location").parent(".searchform").submit(); 

            /************************************
            WE DONT NEED THIS DUE TO THIS IS DEPENDENT ON LAT/LNG ONLY
            //start get zip  
              var j = 0;
              geocoder.geocode({'latLng': geo_coords}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                      if (results[0]) {
                          for (j = 0; j < results[0].address_components.length; j++) {
                              if (results[0].address_components[j].types[0] == 'postal_code'){
                                  //now that we have zip  
                                  //set the input to this zip code value
                                  //then submit the form
                                  $("#s").val( results[0].address_components[j].short_name ).parent(".searchform").submit();
                              }
                          }
                      }
                  } else {
                      alert("Geocoder failed due to: " + status);
                  }
              });
            //end get zip 
            ************************************/
          }, function() {
            handleLocationError(true, "success");
          });




        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, "fail");

        }
      }

      function handleLocationError(browserHasGeolocation, jstatus) { // eslint-disable-line no-unused-vars
        /* CATCH ALL ERRORS HERE!!!! */  
        //alert(jstatus);
      }




