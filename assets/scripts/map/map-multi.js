/*global google*/

/***********************************************************
CLEAN THIS UP... THIS IS NOT THE CORRECT WAY TO WRITE JS
THIS SHOULD BE WRITTEN IN OOP WAY -JAPZ
***********************************************************/

function initMapMulti() {
	jQuery(document).ready(function(){

		var sfl_container = jQuery("#sfl_map");
		var sfl_pin = sfl_container.attr('data-pin');

		var coordinates = [];

		//GET coordinates on <address> tag data attributes
		jQuery( ".location_archive address" ).each( function(){
			var data_arr = [];
			data_arr[0] = jQuery(this).attr('data-lat');
			data_arr[1] = jQuery(this).attr('data-lng');
			data_arr[2] = jQuery(this).attr('data-title');
			data_arr[3] = jQuery(this).attr('data-url');
			data_arr[4] = jQuery(this).attr('data-address');
			coordinates.push( data_arr );
		});
		console.log(  coordinates );

		var map = new google.maps.Map(
		document.getElementById('sfl_map'), {
			//maxZoom: 20,		
			styles: require('./map-style.json'),
		});


		var bounds  = new google.maps.LatLngBounds();//for centering
		var infowindow = new google.maps.InfoWindow(); //infowindow


		//Loop thru the coordinates AND generate markers/bounds
		for (var i in coordinates) {//array first level (STATE)

			var arr = [];	

			//push lat/lng to array so we can auto center map
			arr.push(new google.maps.LatLng(
				parseFloat(coordinates[i][0]),
				parseFloat(coordinates[i][1])
			));
			bounds.extend(arr[arr.length - 1])							

			//generate the markers  
			var marker = new google.maps.Marker({
				position: {lat: parseFloat(coordinates[i][0]), lng: parseFloat(coordinates[i][1]) }, 
				map: map,
				animation: google.maps.Animation.DROP,
				icon: sfl_pin,
				title: coordinates[i][2],
			});	


			//infowindow
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent( '<div id="sfl_container_infowindow"><p><a href="'+coordinates[i][3]+'">'+coordinates[i][2]+'</a><br>'+coordinates[i][4]+'</p></div>' );
					infowindow.open(map, marker);					
				}
			}) (marker, i));

			/*SUCKY ANIMATION WAHAHAA
			google.maps.event.addListener(marker, 'mouseover', (function() {
				return function() {
					if (this.getAnimation() !== null) {
						this.setAnimation(null);
					} else {
						this.setAnimation(google.maps.Animation.BOUNCE);
					}			
				}
			}) );
			*/	


			//center map set zoom
			if ( coordinates.length > 1 ){ //IF MULTIPLE MARKERS
				map.fitBounds(bounds);
			} else { // IF SINGLE MARKER ONLY
				map.setCenter( {lat: parseFloat(coordinates[i][0]), lng: parseFloat(coordinates[i][1]) } );
				map.setZoom(15);				
			}

		}



	}); 	  
}



jQuery(document).ready(function(){
	google.maps.event.addDomListener(window, 'load', initMapMulti);
}); 