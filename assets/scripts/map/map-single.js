/*global google*/

/***********************************************************
CLEAN THIS UP... THIS IS NOT THE CORRECT WAY TO WRITE JS
THIS SHOULD BE WRITTEN IN OOP WAY -JAPZ
***********************************************************/

function initMap() {
	jQuery(document).ready(function(){

		var sfl_container = jQuery("#sfl_map");
		var sfl_lat = parseFloat( sfl_container.attr('data-lat'));
		var sfl_lng = parseFloat(sfl_container.attr('data-lng'));
		var sfl_title = sfl_container.attr('data-title');
		var sfl_address = sfl_container.attr('data-address');
		var sfl_phone = sfl_container.attr('data-phone');
		var sfl_pin = sfl_container.attr('data-pin');
		
		var uluru = {lat: sfl_lat, lng: sfl_lng};

		var contentString = '<div id="sfl_container_infowindow">'+ '<p>'+ '<strong>'+sfl_title+'</strong><br>'+ sfl_address+'<br>'+ sfl_phone+ '</p>'+ '</div>';

		var infowindow = new google.maps.InfoWindow({content: contentString});

		var map = new google.maps.Map(document.getElementById('sfl_map'), {
		zoom: 15, 
		center: uluru,
		styles: require('./map-style.json'),
		});

		var marker = new google.maps.Marker({
						position: uluru,
						map: map,
						title: sfl_title, 				
						animation: google.maps.Animation.DROP,
						icon: sfl_pin,
					}); 

		infowindow.open(map, marker); //open info by default
		marker.addListener('click', function() {infowindow.open(map, marker);});
	}); 	
}

jQuery(document).ready(function(){
	google.maps.event.addDomListener(window, 'load', initMap);
}); 