var on_mobile = {  // eslint-disable-line no-unused-vars
		faq:function() {var append = "";
		$('.faq-categories li a').each(function(){
			var id = ($(this).data('id'));
			var title = $(this).text();
			var temp="";

			$('.panel.category-'+id).each(function(index){
				var panel_id = 'faq-accordion-mobile-inner-'+id;
				var content = $(this).find('.panel-body').text();
				var question = $(this).find('.panel-title').text();
				temp += '<div class="panel panel-default">'
							+'<a class="collapsed" data-toggle="collapse" data-parent="'+panel_id+'" href="#q'+id+index+'">'
								+'<div class="panel-heading">'
									+'<strong class="panel-title">'+question+'</strong>'
								+'</div><i></i>'
							+'</a>'
							+'<div id="q'+id+index+'" class="panel-collapse collapse">'
								+'<div class="panel-body">'+content+'</div>'
							+'</div>'
						+'</div>';
			});

			append += '<a class="collapsed parent-accordion" data-toggle="collapse" data-parent="#faqs-accordion-mobile" href="#faq-mobile-'+id+'">'
						+'<div class="panel-heading">'
							+'<strong class="panel-title">'+title+'</strong>'
						+'</div>'
					+'</a>'
					+'<div id="faq-mobile-'+id+'" class="panel-collapse collapse">'
						+'<div class="panel-group" id="faq-accordion-mobile-inner-'+id+'">'+temp+'</div>'
					+'</div>';
		}); 

		$('#faqs-accordion-mobile .panel').append(append);},

		services_inner: function(){
			var append="";
			$('.services-accordion-items li a').each(function(){
				var id = $(this).data('id');
				var title = $(this).html();
				var content = $('.services-accordion-items.tab-content #tab-'+id).html();

				append +='<a class="collapsed parent-accordion" data-toggle="collapse" data-parent="#faqs-accordion-mobile" href="#faq-mobile-'+id+'">'
						+'<div class="panel-heading">'
							+'<strong class="panel-title">'+title+'</strong>'
						+'</div>'
					+'</a>'
					+'<div id="faq-mobile-'+id+'" class="panel-collapse collapse">'
						+'<div class="panel-group" id="faq-accordion-mobile-inner-'+id+'">'+content+'</div>'
					+'</div>';
			});

			$('#services-inner-accordion-mobile .panel').append(append);
		},

		services_slider: function(){
			var slick_responsive_services= [{breakpoint: 1920,settings: "unslick" },{breakpoint: 768,settings: {slidesToShow: 1,slidesToScroll: 1}}];

			if( $( window ).width() >= 768 ){
				$('.services-slicker').slick("unslick");
			}else{
				$('.services-slicker').slick({
					prevArrow: $('.prev'),
					nextArrow: $('.next'),
					responsive: slick_responsive_services,
				});
			}
		},

		financing_offers: function(){
			var append="";
			$('.offer-categories li a').each(function(){
				var id = ($(this).data('id'));
				var title = $(this).text();
				var content=$('.offer-categories .tab-content #tab-'+id).html();

				append +='<a class="collapsed parent-accordion" data-toggle="collapse" data-parent="#financing-offers-accordion-mobile" href="#financing-offers-mobile-'+id+'">'
						+'<div class="panel-heading">'
							+'<strong class="panel-title">'+title+'</strong>'
						+'</div>'
					+'</a>'
					+'<div id="financing-offers-mobile-'+id+'" class="panel-collapse collapse">'
						+'<div class="panel-group" id="faq-accordion-mobile-inner-'+id+'">'+content+'</div>'
					+'</div>';
			});

			$('#financing-offers-accordion-mobile .panel').append(append);
		},
  };