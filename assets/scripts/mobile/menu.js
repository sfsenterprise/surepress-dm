/******************************************
this is for mobile menu dropdown (768px below)
this will enable the dropdowns on click using 
click as event listener --- Japol
*******************************************/

//add class and data attr for reference
$('#navbar > #menu-primary-menu > li:has(> ul)').addClass("mobileDropDown").attr("data-toggle", "closed");

//listener for click to open menu
$('#navbar > #menu-primary-menu > li.mobileDropDown').on("click", function(e) {

	var target = $(e.target);

	//listener for click to close menu
	if( target.is( "#navbar > #menu-primary-menu  li .menu_close" ) ){
		$('li.mobileDropDown').attr("data-toggle", "closed");
		$('span.menu_close').remove();
		return;
	}

	$('li.mobileDropDown').attr("data-toggle", "closed");
	$('span.menu_close').remove();	
	$(this).attr("data-toggle", "open").append('<span class="menu_close">&#xe088;</span>');

	// var link_item = $('> a',this);
	// $('ul li#inserted', this).remove();
	// $('ul', this).prepend( '<li id="inserted"><a href="'+link_item.attr("href")+'">'+link_item.html()+'</a></li>');

	if( target.is('#navbar > #menu-primary-menu > li.mobileDropDown > a') && $(window).innerWidth() <= 768 ){
		return false; //do not follow href
	} else {
		/*do nothing .. follow href*/
	}

});
/********the rest is on the styles/layouts/header.scss***************/