<?php

//GET/LOAD ALL FUNCTION FILES @ 'functions' DIRECTORY
$files = glob( get_template_directory().'/functions/' . "*.php");
foreach($files as $file): 
	require $file; 
endforeach;


/*****************************************/
/***************************************/
	//default WP theme setup
	add_action('after_setup_theme', 'Surepress\\Functions\\Common\\default_wp');
	//theme css-js
	add_action('wp_enqueue_scripts', 'Surepress\\Functions\\Assets\\theme_assets', 100);
	//Page titles
	add_filter( 'document_title_parts', 'Surepress\\Functions\\Titles\\title', 100, 2 );	
	//filter microsite menus
	add_filter( 'wp_get_nav_menu_items', 'Surepress\\Functions\\Menu\\MicrositeMenu', 99, 3 );
	//filter special page
	add_filter( 'wp_nav_menu_objects', 'Surepress\\Functions\\Menu\\CustomMenuObject', 10, 2 );
	//generate post types
	add_action( 'init', 'Surepress\\Functions\\PostTypes\\GeneratePostTypes',10);
	//post type taxonomies
	add_action( 'init', 'Surepress\\Functions\\Taxonomies\\GenerateTaxonomy', 1 ); //ALways run taxonomy earlier.... dont change 1
	
	//post type custom fields
	add_action('acf/init', 'Surepress\\Functions\\ACF\\GenerateAcfFields',99);

	//post type services exclude children
	add_action( 'pre_get_posts', 'Surepress\\Functions\\Services\\GetServicesLanding' );
	//reorder posts service archive page
	add_filter( 'posts_orderby' , 'Surepress\\Functions\\Services\\archive_order' );	


	//attach nap
	add_action( 'parse_query', array(Surepress\Functions\Microsites\Microsite::class,'attach_nap') );

	// ACTIVATE surepress-dm theme for all microsites
	add_action('after_switch_theme', 'Surepress\\Functions\\Common\\ActivateTheme');

	// Redirect non exsiting singular pages
	add_action( 'template_redirect','Surepress\\Functions\\Common\\RedirectSingularPages' );

	//Sort Glossary by title
	//add_action( 'pre_get_posts', 'Surepress\\Functions\\Glossary\\GetGlossary' );

	//Sort FAQs by title
	add_action( 'pre_get_posts', 'Surepress\\Functions\\FAQ\\GetFAQs' );

	//Sort Team by menu_order
	add_action( 'pre_get_posts', 'Surepress\\Functions\\Team\\GetTeam' );	

	//Set gravity form services checkbox dynamically
	add_filter("gform_pre_render", 'Surepress\\Functions\\Common\\populate_services_checkbox');
	add_filter( 'gform_pre_validation', 'Surepress\\Functions\\Common\\populate_services_checkbox' );
	add_filter( 'gform_pre_submission_filter', 'Surepress\\Functions\\Common\\populate_services_checkbox' );
	add_filter("gform_admin_pre_render", 'Surepress\\Functions\\Common\\populate_services_checkbox');

	//Set gravity form services dropdown dynamically
	add_filter("gform_pre_render", 'Surepress\\Functions\\Common\\populate_services_dropdown');
	add_filter( 'gform_pre_validation', 'Surepress\\Functions\\Common\\populate_services_dropdown' );
	add_filter( 'gform_pre_submission_filter', 'Surepress\\Functions\\Common\\populate_services_dropdown' );
	add_filter("gform_admin_pre_render", 'Surepress\\Functions\\Common\\populate_services_dropdown');
	
	
	//set wp_query data from sfl_microsite data in relation with wp_posts state/city post ID and blog status
	add_action( 'parse_query', 'Surepress\\Functions\\Locations\\archive', 1 );
	//alter locations archive query as defined by parse_query filter
	add_action( 'pre_get_posts', 'Surepress\\Functions\\Locations\\get_states', 12 );

	//custom title tags
	add_filter( 'wpseo_title', 'Surepress\\Functions\\Common\\custom_title_tags', 10, 2 );

	//add_filter( 'gform_pre_render', 'Surepress\\Functions\\Gravity\\populate_posts' );
	//filter for Review form to set newly added review as "static"
	add_filter( 'gform_entry_post_save', 'Surepress\\Functions\\Gravity\\hide_new_reviews', 10, 2);

	//filter to remove hentry
	add_filter( 'post_class', 'Surepress\\Functions\\Common\\ja_remove_hentry', 10, 2 );

	//scripts and gtm
	add_action('wp_head','Surepress\\Functions\\Scripts\\header_scripts', 99);
	add_action('wp_footer','Surepress\\Functions\\Scripts\\footer_scripts', 99);

	//Custom form email recipients
	add_filter("gform_notification", 'Surepress\\Functions\\Common\\custom_email_notification', 10, 3);

	//Set gravity form events dropdown dynamically
	add_filter("gform_pre_render", 'Surepress\\Functions\\Common\\populate_events_dropdown');
	add_filter( 'gform_pre_validation', 'Surepress\\Functions\\Common\\populate_events_dropdown' );
	add_filter( 'gform_pre_submission_filter', 'Surepress\\Functions\\Common\\populate_events_dropdown' );
	add_filter("gform_admin_pre_render", 'Surepress\\Functions\\Common\\populate_events_dropdown');
	

	// Campaign stuff
	// redirect to thank you page
	add_filter( 'gform_confirmation', 'Surepress\\Functions\\Gravity\\campaignThankyou', 10, 4 );
	// use campaign template
	add_filter( 'template_include', 'Surepress\\Functions\\Campaigns\\campaignTemplate', 99 );

	add_action( 'load-post.php', 'Surepress\\Functions\\CampaignsTy\\metabox' );
	add_action( 'load-post-new.php', 'Surepress\\Functions\\CampaignsTy\\metabox' );	

	add_action( 'after_hero_title', 'Surepress\\Functions\\Titles\\afterHerocontent' );

	add_post_type_support('page', 'excerpt');

	//action to Render the "Field ID" property for Gravity Form fields under advanced tabs
	add_action( 'gform_field_advanced_settings', 'Surepress\\Functions\\Gravity\\render_field_id_setting', 10, 5 );
	add_action( 'gform_editor_js', 'Surepress\\Functions\\Gravity\\field_id_editor_script' );
	//filter function to replace gravity form field id if Field ID in advanced settings has value
	add_filter( 'gform_field_content','Surepress\\Functions\\Gravity\\gform_replace__fieldid', 10,5);

	//add youtube videos data as js variable for "Load More Videos" button
	add_action('wp_print_scripts','Surepress\\Functions\\Videos\\header_scripts', 99);	
	add_filter('https_ssl_verify', '__return_false');
	
function getMicrositeExtraFields($microsite_id){
    global $wpdb; 
    $sql = "SELECT microsite_fields FROM sfl_microsites WHERE microsite_blog_id  = $microsite_id";
    $results = $wpdb->get_results($sql);
    if(count($results) > 0){
        return maybe_unserialize($results[0]->microsite_fields);
    }
}
function getMicrositeLatLng($microsite_id){
    global $wpdb; 
    $sql = "SELECT microsite_lat, microsite_lng FROM sfl_microsites WHERE microsite_blog_id  = $microsite_id";
    $results = $wpdb->get_results($sql);
    if(count($results) > 0){
        return $results;
    }
}
function date_compare($a, $b)
{
    $t1 = strtotime($a['post_date']);
    $t2 = strtotime($b['post_date']);
    return $t2 - $t1;
}
function sfl_get_distance($lat1, $lon1, $lat2, $lon2) {
	if (($lat1 == $lat2) && ($lon1 == $lon2)) {
		return 0;
	}
	else {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;

		return $miles;

	}
}