<?php get_header( get_post_type() == 'campaigns' ? 'campaigns' : '' ); ?>
<?php
if ( have_posts() ) :
    while ( have_posts() ) : the_post();
        $post_label =  get_post_type_object( get_post_type() );
        $hero_title = $post_label->label;
        
         if( get_post_type() == 'services'){
            $hero_title =  get_the_title();
        }else if(get_post_type() == 'post'){
            $hero_title =  "Blog"; 
        }else if( get_post_type() != 'campaigns'){  ?>
            <section class="hero-image">
                <h1><?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').ucwords($hero_title); ?></h1>
            </section>
        <?php } ?>     
        <div class=" <?php echo get_post_type() != 'campaigns'?  'container' : ''; ?>">
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
               <?php get_template_part('template-parts/single', get_post_type() ); ?>   
            </div>
        </div>                
<?php endwhile; ?>
<?php else :
	    get_template_part('template-parts/content', 'none');
endif;	
?>


<?php get_footer( get_post_type() == 'campaigns' ? 'campaigns' : '' ); ?>
