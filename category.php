<?php use Surepress\Functions\Common as Common; ?>
<?php get_header(); ?>
<section class="hero-image">
    <h1>
    	<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ')." Blog "; ?>
    </h1>
</section>
<?php 
	$current_category = ( !empty(get_the_category()) ? get_the_category() : '');
	$current_category = ( !empty($current_category[0]) ? $current_category[0] : '' );
	 ?>
<div class="container">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	    <h2>DreamMaker Remodeling Tips</h2>
	    <h3>Category: <strong><?php echo ($current_category->name); ?></strong></h3>
	    
	    <?php if ( have_posts() ) : ?>
		    <?php while ( have_posts() ) : the_post(); ?>
		    	<div class="posts-wrapper">
			        <?php if(has_post_thumbnail()){ 
						the_post_thumbnail();
					}else{
						Common\default_thumbnail();
					}?>
					<div class="post-content">
						<h6><?php the_title(); ?></h6>
						<hr>
						<p><?php echo wp_trim_words( get_the_content(), 70,'[...]' ); ?></p>
						<a href="<?php echo get_the_permalink();?>">Read More </a>
					</div>
				</div>
		    <?php endwhile; ?>
		<?php endif; ?>

		<div class="pagination">
			<?php 
			$args = array(
				'prev_next' => true,
				'prev_text'          => __('«'),
				'next_text'          => __('»')
			);?>
			<?php echo paginate_links($args); ?>
		</div>
		
   	</div>
</div>	
	

<?php get_footer(); ?>