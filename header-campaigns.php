<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Google as Google;
use Surepress\Functions\Scripts as Scripts;

$header_scripts = get_post_meta(get_the_ID(), 'campaign_header_script', TRUE );
?>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="<?php echo Assets\asset_path('images/favicon.png') ?>" type="image/x-icon" />
      <link rel="shortcut icon" href="<?php echo Assets\asset_path('images/favicon.png') ?>" type="image/x-icon" />   
      <?php wp_head(); ?>
      <?php $leads =  get_blog_option(get_current_blog_ID(), 'dm_settings'); ?>
      <?php if( isset($leads['slmcleads_key'][0]) && !empty($leads['slmcleads_key'][0]) ):?>
          <script>var pAuth = '<?php echo $leads['slmcleads_key'][0]; ?>';</script>
          <script type='text/javascript' src='https://libs.sfs.io/public/lead-script-gf.js?v=2.1.00'></script>
      <?php endif; ?> 
      <!-- Google Tag Manager -->
      <?php if (!empty($wp_query->nap->microsite_gtm_ga)): ?>
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','<?php echo $wp_query->nap->microsite_gtm_ga; ?>');</script>
      <?php endif; ?>
      <!-- End Google Tag Manager -->

      <?php echo $header_scripts; ?>
    </head>

    <body <?php body_class(); ?>>       
        <?php if (!empty($wp_query->nap->microsite_gtm_ga)): ?>
          <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $wp_query->nap->microsite_gtm_ga; ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
          <!-- End Google Tag Manager (noscript) -->
        <?php endif; ?>
        <?php echo Scripts\body_scripts(); ?>

        <header id="masthead" class="site-header" role="banner">
            <?php $template = get_post_meta( get_the_ID(), 'campaign_template', true ); ?>
            <?php
              if( in_array($template, ['version2_b', 'version2_k']) ) {
                get_template_part('template-parts/campaigns/header', 'version2');
              }
              elseif( $template == 'kitchen_bath' ){
                get_template_part('template-parts/campaigns/header', 'kitchen-bath');
              } 
              else {
                get_template_part('template-parts/campaigns/header', 'main'); 
              }
            ?>
        </header>
        
        <main class="main-campaign awtsutus">