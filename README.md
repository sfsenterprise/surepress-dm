# Dream Maker Remodel (DreamMaker-Remodel.com) #

### Requirements ###

* Node V12.5.0 or higher (node -v)
* NPM V6.9.0 or higher (npm -v)
* Yarn V1.19.0 or higher (yarn -v)
* Local URL: https://www.dreammaker-remodel.com

### Installation ###

To install:
```
	-- yarn
```

To Watch Locally
```
  -- yarn start
```

To Build for Production/Live
```
  -- yarn build:production
```
