<?php
//load microsite class
require_once ('classes/class_microsite.php');

//load helpers
require_once('helpers/microsite.php');

//load shortcodes
require_once('helpers/shortcodes.php');