<?php

use Surefire\Surepress\Microsite;

function get_microsites($state = null, $city = null, $blog_id = null)
{
	$location =& Locations::get_instance();

	$_microsites = $location->get_microsites();

	if ( ! $_microsites ) {
		return null;
	}

	$_microsites = apply_filters( 'get_microsites', $_microsites );

	if ($state) {
		foreach($_microsites as $key => $microsite) {
			$turf_state = $microsite->microsite_turf_state_province ?: $microsite->microsite_state_province;
			$turf_city = $microsite->microsite_turf_city ?: $microsite->microsite_city;
			
			if ($city) {
				if ($blog_id && $blog_id !== $microsite->microsite_blog_id) {
					unset($_microsites[$key]);
				}
				else if ($state !== state_decode($turf_state) || ucwords(strtolower($city)) !== ucwords(strtolower($turf_city))) {
					unset($_microsites[$key]);
				}
			} else {
				if ($state !== state_decode($turf_state)) {
					unset($_microsites[$key]);
				}
			}
		}
		sort($_microsites);
	}

	return $_microsites;
}

function get_microsite( $microsite = null ) {
	if ( empty( $microsite ) ) {
		$microsite = get_current_blog_id();
	}

	if ( $microsite instanceof Microsite ) {
		$_microsite = $microsite;
	} elseif ( is_object( $microsite ) ) {
		$_microsite = new Microsite( $microsite );
	} else {
		$_microsite = Microsite::get_instance( $microsite );
	}

	if ( ! $_microsite ) {
		return null;
	}

	$_microsite = apply_filters('get_microsite', $_microsite);

	return $_microsite;
}

function get_microsite_field($site, $metakey)
{
	if (!is_object($site)) $site = get_microsite($site);

	if (!isset($site->microsite_fields->{$metakey})) return null;

	if ($metakey === 'associated_zips') {
		if ($site->microsite_fields->{$metakey}) {
			$associated_zips = array_map('trim', explode(',', $site->microsite_fields->{$metakey}));
			return $associated_zips;
		}
		return null;
	}

	return $site->microsite_fields->{$metakey};
}

function get_microsite_by_zip($zip) {
	$zips = array();
	$microsites = get_microsites();

	foreach($microsites as $microsite) {
		$associated_zips = get_microsite_field($microsite, 'associated_zips');
		if (empty($associated_zips) && $microsite->microsite_zip_postal === '00000' && !is_array($associated_zips) ) continue;

		foreach($associated_zips as $associated_zip) {
			if ($associated_zip === $zip) {
				return $microsite;
			}
		}
	}
}

function get_microsite_nap($blog_id) {
	global $wpdb;
	$microsite = get_microsite($blog_id);
	if (!$microsite) return null;

	return array(
		'name'                 => $microsite->microsite_name,
		'microsite_blog_id'    => $blog_id,
		'permalink'            => $microsite->microsite_domain,
		'state_province'       => $microsite->microsite_state_province,
		'city'                 => $microsite->microsite_city,
		'street'               => sprintf('%1$s%2$s', $microsite->microsite_street, ($microsite->microsite_street2 ? ', ' . $microsite->microsite_street2 : '')),
		'zip_postal'           => $microsite->microsite_zip_postal,
		'lat'                  => $microsite->microsite_lat,
		'lng'                  => $microsite->microsite_lng,
	);
}

function get_microsites_napjson($state = null, $city = null, $blog_id = null) {
	$naps = array();
	$microsites = get_microsites(state_decode($state));
	if (!$microsites) return null;

	$states = array();
	foreach($microsites as $microsite) {
		if ( $blog_id ) {
			if ($microsite->microsite_blog_id !== $blog_id) continue;

			$naps[] = get_microsite_nap($microsite->microsite_blog_id);
			return json_encode($naps);
		} else {
			$naps[] = get_microsite_nap($microsite->microsite_blog_id);
		}
	}
	return json_encode($naps);
}

function get_map_style() {
	return json_decode(file_get_contents(get_stylesheet_directory() . '/framework/geo/mapstyles.json'));
}

function get_microsite_ids() {
	$location =& Locations::get_instance();

	return $location->get_microsite_ids();
}