<?php

class Dreammaker_Shortcodes
{
    static $instance;

    function __construct()
    {

        self::$instance =& $this;

        add_shortcode('location_full_address', array($this, 'redwood_shortcode_full_address'));
        add_shortcode('location_city', array($this, 'redwood_shortcode_location_city'));
        add_shortcode('location_name', array($this, 'redwood_shortcode_location_name'));
        add_shortcode('location_street_address', array($this, 'redwood_shortcode_street_address'));
        add_shortcode('location_state', array($this, 'redwood_shortcode_location_state'));
        add_shortcode('location_zip', array($this, 'redwood_shortcode_location_zip'));
        add_shortcode('operator_name', array($this, 'redwood_shortcode_operator_name'));
        add_shortcode('operator_image', array($this, 'redwood_shortcode_operator_image'));
        add_shortcode('phone_number', array($this, 'redwood_shortcode_phone_number'));
        add_shortcode('email_address', array($this, 'redwood_shortcode_email_address'));
        add_shortcode('area_served_1', array($this, 'redwood_shortcode_area_served_1'));
        add_shortcode('area_served_2', array($this, 'redwood_shortcode_area_served_2'));
        add_shortcode('license_number', array($this, 'redwood_shortcode_license_number'));
        add_shortcode('url_careerplug', array(&$this, 'redwood_shortcode_careerplug_link'));
        add_shortcode('location-search-results', array(&$this, 'redwood_location_search_results'));
        add_shortcode('craftsman_verify', array($this, 'redwood_shortcode_craftsman_verify'));


        // These things should not be here.
        add_action('wp_head', array($this, 'redwood_render_header_scripts'));
        add_action('wp_footer', array($this, 'redwood_render_footer_scripts'));
        add_action('wp_afterbody', array($this, 'redwood_render_afterbody_scripts'));
        add_action('admin_init', array($this, 'add_shortcode_button'));
        add_action('admin_enqueue_scripts', array($this, 'get_shortcodes'));
        add_action('wp_ajax_craftsman_verify', array($this, 'craftsman_verify'));
        add_action('wp_ajax_nopriv_craftsman_verify', array($this, 'craftsman_verify'));

        add_action('wp_afterbody', array($this, 'render_gtm_afterbody'));
    }

    /**
     * Shortcode to display the full address.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_full_address($atts)
    {
        $settings = get_option('redwood_location_settings', array());

        $address_string = (!empty($settings['street_address'])) ? str_replace(' ', '+', $settings['street_address']) : '';
        $address_string .= (!empty($settings['city'])) ? '+' . str_replace(' ',  '+', $settings['city']) : '';
        $address_string .= (!empty($settings['state'])) ? '+' . str_replace(' ', '+', $settings['state']) : '';
        $address_string .= (!empty($settings['zip'])) ? '+' . $settings['zip'] : '';

        ob_start();
        ?>
        <a href="https://maps.google.com?daddr={$address_string}" target="_blank">
            <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <?php
                if (!empty($settings['street_address'])) :
                    echo $settings['street_address'] . '<br />';
        endif;
        if (!empty($settings['city'])) :
                    echo $settings['city'] . ', ';
        endif;
        echo $settings['state'] . ' ';
        echo $settings['zip'];
        ?>
            </address>
        </a>
        <?php
        return ob_get_clean();
    }

    /**
     * Shortcode to display the location's name.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_location_name($atts)
    {
        $settings = get_option('redwood_location_settings', array());

        $class = 'location_name';
        $a = shortcode_atts(array(
            'html_before' => "<span itemprop='name' class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        $value = !empty($settings['location_name']) ? $settings['location_name'] : get_option('blogname');

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's city.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_location_city($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['city'])) {
            return '';
        }

        $class = 'location_city';
        $a = shortcode_atts(array(
            'html_before' => "<span itemprop='addressLocality' class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's street address.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_street_address($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['street_address'])) {
            return '';
        }

        $class = 'location_street_address';
        $a = shortcode_atts(array(
            'html_before' => "<span itemprop='streetAddress' class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's state.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_location_state($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['state'])) {
            return '';
        }

        $class = 'location_state';
        $a = shortcode_atts(array(
            'html_before' => "<span itemprop='addressRegion' class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's zip.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_location_zip($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['zip'])) {
            return '';
        }

        $class = 'location_zip';
        $a = shortcode_atts(array(
            'html_before' => "<span itemprop='postalCode' class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's owner name.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_operator_name($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['operator_name'])) {
            return '';
        }

        $class = 'operator_name';
        $a = shortcode_atts(array(
            'html_before' => "<span class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's owner name.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_operator_image($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['operator_image'])) {
            return '';
        }

        $a = shortcode_atts(array(
            'class' => 'attachment-headshot',
            'id' => '',
            'size' => 'headshot',
        ), $atts);

        return wp_get_attachment_image($value, $a['size'], false, $a);
    }

    /**
     * Shortcode to display the location's phone number.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_phone_number($atts)
    {
        $settings = get_option('redwood_location_settings', array());

        if (empty($value = $settings['phone_number'])) {
            return '';
        }

        $class = 'location_phone_number';
        $new_value = @$_COOKIE['ppc_user']=='true' ? '814-240-1047' : $value;
        $shore_tracking = '781-318-8181';
        $lakeway_tracking = '512-598-1800';

        if( ( isset($settings['mongoose_id']) && $settings['mongoose_id'] ) && ( isset($settings['mongoose_phone']) && $settings['mongoose_phone']) ){
            $value = $settings['mongoose_phone'];
        }

        $a = shortcode_atts(array(
            'html_before' => "<a itemprop='telephone' href='tel:" . alphanum_decode($value) . "' class='" . $class . "'>",
            'html_after'  => '</a>',
            'bare'        => false,
        ), $atts);

        if ($settings['location_id'] == 802) {
            $a['html_before'] = '<a itemprop="telephone" onclick="ga(\'trimarkview.send\',\'event\',\'Phone Call Tracking\',\'Click/Touch\',\'Number\');" href="tel:'.alphanum_decode($new_value).'"  class="'.$class. '">';
        }

        return $a['bare'] == false ? $a['html_before'] . phone_number_format($value) . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's email address.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_email_address($atts)
    {
        $settings = get_option('redwood_location_settings', array());

        $class = 'location_email';
        $a = shortcode_atts(array(
            'html_before' => "<span itemprop='email' class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        $value = !empty($settings['email']) ? $settings['email'] : get_option('admin_email');

        return (!empty($email)) ? $a['html_before'] . $value . $a['html_after'] : '';
    }

    /**
     * Shortcode to display the location's area serverd 1.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_area_served_1($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['area_served_1'])) {
            return '';
        }

        $class = 'location_area_served_1';
        $a = shortcode_atts(array(
            'html_before' => "<span class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's area serverd 2.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_area_served_2($atts)
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['area_served_2'])) {
            return '';
        }

        $class = 'location_area_served_2';
        $a = shortcode_atts(array(
            'html_before' => "<span class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    /**
     * Shortcode to display the location's area serverd 2.
     *
     * @since 0.8.0
     */
    public function redwood_shortcode_license_number()
    {
        $settings = get_option('redwood_location_settings', array());
        if (empty($value = $settings['license_number'])) {
            return '';
        }

        $class = 'location_license_number';
        $a = shortcode_atts(array(
            'html_before' => "<span class='" . $class . "'>",
            'html_after'  => '</span>',
            'bare'        => false,
        ), $atts);

        return $a['bare'] == false ? $a['html_before'] . $value . $a['html_after'] : $value;
    }

    public function redwood_shortcode_careerplug_link($atts, $content, $tag)
    {
        $settings = get_option('redwood_hmc_careerplug', '') ?: array();

        return ( ! empty($settings['url_careerplug'] ) ? $settings['url_careerplug'] : network_site_url('/become-a-craftsman/'));
    }

    public static $states = array(0 => '-- Select state --', 'AL' => 'Alabama', 'AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas', 'CA' => 'California', 'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware', 'DC' => 'District of Columbia', 'FL' => 'Florida', 'GA' => 'Georgia', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois', 'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana', 'ME' => 'Maine', 'MD' => 'Maryland', 'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota', 'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana', 'NE' => 'Nebraska', 'NV' => 'Nevada', 'NH' => 'New Hampshire', 'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York', 'NC' => 'North Carolina', 'ND' => 'North Dakota', 'OH' => 'Ohio', 'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania', 'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota', 'TN' => 'Tennessee', 'TX' => 'Texas', 'UT' => 'Utah', 'VT' => 'Vermont', 'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia', 'WI' => 'Wisconsin', 'WY' => 'Wyoming', 'AB' => 'Alberta', 'BC' => 'British Columbia', 'MB' => 'Manitoba', 'NB' => 'New Brunswick', 'NL' => 'Newfoundland', 'NS' => 'Nova Scotia', 'NT' => 'Northwest Territories', 'NU' => 'Nunavut', 'ON' => 'Ontario', 'PE' => 'Prince Edward Island', 'QC' => 'Quebec', 'SK' => 'Saskatchewan', 'YT' => 'Yukon');

	public static function get_all_locations_by_state() {
		$states = self::$states;
		unset( $states[0] );
		// $states = array_flip($states);
		$locations = get_microsites();
		foreach ( $states as $state => $code ) {
			$state_locations = array();
			foreach ( $locations as $location ) {
				if ( $location->microsite_state_province === $code ) {
					$state_locations[] = $location;
				}
			}
			$states[ $state ] = $state_locations;
		}
		return $states;
	}

	public function redwood_location_search_results( $atts ) {
        /* NOT NEEDED ... THIS FUNCTION HAS BEEN OVERIDDEN BY /surepress-hmc/framework/helpers/search-locations.php --JAPOL
		if ( isset($_GET['search-location']) ) {

			$search_zip = $_GET['search-location'];
			$locations = get_microsites();
			$state_locations = array();

			foreach ( $locations as $location ) {
				if ( $location->microsite_zip_postal === $search_zip ) {
					$state_locations[] = $location;
				}
			}

			$tmpl = '<div class="row">';
			$loop_count = 0;
			foreach ($state_locations as $loc) {
				$loop_count++;
				$tmpl .= '<div class="redwood-location col-sm-4" data-limit="-1" data-location-id="'. $loc->microsite_blog_id .'" itemscope itemtype="http://schema.org/LocalBusiness">';
				$tmpl .= '<a href="'. $loc->microsite_vanity_url .'" target="_blank" itemprop="url">';
				$tmpl .= '<h4 class="location-name" itemprop="name">'. $loc->microsite_name .'</h4>';
				$tmpl .= '</a>';
				$tmpl .= '<p class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
				$tmpl .= '<span itemprop="streetAddress">'. $loc->microsite_street .'</span><br>';
				$tmpl .= '<span itemprop="addressLocality">'. $loc->microsite_city .'</span>, ';
				$tmpl .= '<span itemprop="addressRegion">'. $loc->microsite_state_province .'</span> ';
				$tmpl .= '<span itemprop="postalCode">'. $loc->microsite_zip_postal .'</span>';
				if( $loc->microsite_phone !== '' ) {
					$tmpl .= '<br><a href="tel:'. $loc->microsite_phone .'" class="phone-number" itemprop="telephone">'. $loc->microsite_phone .'</a>';
				}
				$tmpl .= '</p>';
				$tmpl .= '<p>';
				$tmpl .= '<a href="'. $loc->microsite_domain .'" target="_blank" class="visit-location" itemprop="url">Visit Location</a>';
				$tmpl .= '<span> | </span>';
				$tmpl .= '<a href="'. $loc->microsite_domain .'request-free-estimate" target="_blank">Request Free Estimate</a>';
				$tmpl .= '</p>';
				$tmpl .= '</div>';

				if ( $loop_count === 3 ) {
					$tmpl .= '<div class="clearfix hidden-xs"></div>';
				}
			} //end $location foreach
			$tmpl .= '</div>';
		} else {

			$results = self::get_all_locations_by_state();
			$states = array_filter($results);

			// echo '<pre>: '. print_r( $states, true ) .'</pre>';

			$tmpl = '';
			foreach ($states as $state => $location) {
				$tmpl .= '<div class="row">';
				$tmpl .= '<div class="col-sm-12">';
				$tmpl .= '<h3>' . state_decode($state) . '</h3>';
				$tmpl .= '</div>';

				$loop_count = 0;

				foreach ($location as $loc) {
					$loop_count++;
					$tmpl .= '<div class="redwood-location col-sm-4" data-limit="-1" data-location-id="'. $loc->microsite_blog_id .'" itemscope itemtype="http://schema.org/LocalBusiness">';
					$tmpl .= '<a href="'. $loc->microsite_vanity_url .'" target="_blank" itemprop="url">';
					$tmpl .= '<h4 class="location-name" itemprop="name">'. $loc->microsite_name .'</h4>';
					$tmpl .= '</a>';
					$tmpl .= '<p class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
					$tmpl .= '<span itemprop="streetAddress">'. $loc->microsite_street .'</span><br>';
					$tmpl .= '<span itemprop="addressLocality">'. $loc->microsite_city .'</span>, ';
					$tmpl .= '<span itemprop="addressRegion">'. $loc->microsite_state_province .'</span> ';
					$tmpl .= '<span itemprop="postalCode">'. $loc->microsite_zip_postal .'</span>';
					if( $loc->microsite_phone !== '' ) {
						$tmpl .= '<br><a href="tel:'. $loc->microsite_phone .'" class="phone-number" itemprop="telephone">'. $loc->microsite_phone .'</a>';
					}
					$tmpl .= '</p>';
					$tmpl .= '<p>';
					$tmpl .= '<a href="'. $loc->microsite_domain .'" target="_blank" class="visit-location" itemprop="url">Visit Location</a>';
					$tmpl .= '<span> | </span>';
					$tmpl .= '<a href="'. $loc->microsite_domain .'request-free-estimate" target="_blank">Request Free Estimate</a>';
					$tmpl .= '</p>';
					$tmpl .= '</div>';

					if ( $loop_count === 3 ) {
						$tmpl .= '<div class="clearfix hidden-xs"></div>';
					}
				} //end $location foreach
				$tmpl .= '</div>';
			} //end $states foreach

		}
        
        
		return $tmpl;
        */
	}

	    public function redwood_shortcode_craftsman_verify()
	    {
	        return
<<<HTML
<form class="form-inline" data-craftsman-verify>
<input type="text" class="form-control" name="zip" placeholder="Zip/Postal Code">
<button type="submit" class="btn btn-primary"><strong>Go!</strong></button>
</form>
HTML;
	    }

    /**
     * Action to output header scripts.
     *
     * @since 0.9.0
     */
    public function redwood_render_header_scripts()
    {
        $settings = get_option('redwood_location_settings', array());
        $scripts = !empty($settings['header_scripts']) ? $settings['header_scripts'] : '';

        echo do_shortcode($scripts);

        $protocol = isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
        ?>
        <script type="text/javascript">
            window.ajaxurl = window.ajaxurl || '<?php echo admin_url('admin-ajax.php', $protocol); ?>';
        </script>
        <?php
        $gtm = !empty($settings['gtm_gta']) ? $settings['gtm_gta'] : '';
        if (isset($settings['gtm_gta']) && $settings['gtm_gta']) :
        ?>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
        <!-- End Google Tag Manager -->
        <?php
        endif;

        $main_settings = get_blog_option(1, 'redwood_location_settings');
        $main_gtm = !empty($main_settings['gtm_gta']) ? $main_settings['gtm_gta'] : '';

        if( ! is_main_site() && $main_gtm ) :
        ?>
        <!-- Google Tag Manager GLOBAL -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?php echo $main_gtm; ?>');</script>
        <!-- End Google Tag Manager GLOBAL -->
        <?php
        endif;
    }

    /**
     * Action to output footer scripts.
     *
     * @since 0.9.0
     */
    public function redwood_render_footer_scripts()
    {
        $settings = get_option('redwood_location_settings', array());
        $scripts = !empty($settings['footer_scripts']) ? $settings['footer_scripts'] : '';

        echo do_shortcode($scripts);
    }

    public function redwood_render_afterbody_scripts()
    {
        $settings = get_option('redwood_location_settings', array());
        $scripts = !empty($settings['afterbody_scripts']) ? $settings['afterbody_scripts'] : '';

        echo do_shortcode($scripts);
    }

    public function render_gtm_afterbody()
    {

        $settings = get_option('redwood_location_settings', array());
        $gtm = !empty($settings['gtm_gta']) ? $settings['gtm_gta'] : '';
        if (isset($settings['gtm_gta']) && $settings['gtm_gta']) :
        ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <?php
        endif;

        $main_settings = get_blog_option(1, 'redwood_location_settings');
        $main_gtm = !empty($main_settings['gtm_gta']) ? $main_settings['gtm_gta'] : '';

        if( ! is_main_site() && $main_gtm ) :
        ?>
        <!-- Google Tag Manager (noscript) GLOBAL -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $main_gtm; ?>"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) GLOBAL -->
        <?php
        endif;
    }

    public function add_shortcode_button()
    {
        if (current_user_can('edit_posts') && current_user_can('edit_pages')) {
            add_filter('mce_external_plugins', array($this, 'add_buttons'));
            add_filter('mce_buttons', array($this, 'register_buttons'));
        }
    }

    public function add_buttons($plugin_array)
    {
        // NOTE: add this also. Check the shortcode js in the old plugin.

        // $plugin_array['redwood_shortcodes'] = plugin_dir_url(dirname(__FILE__)) . 'js/shortcode-tinymce-button.js';

        return $plugin_array ;
    }

    public function register_buttons($buttons)
    {
        array_push($buttons, 'separator', 'redwood_shortcodes');

        return $buttons;
    }

    public function get_shortcodes()
    {
        // wp_enqueue_style('tinymce-fix', plugin_dir_url(dirname(__FILE__)) . 'css/tinymce-fix.css');
        wp_localize_script('jquery', 'redwood_shortcodes', $this->get_shortcodes_localization());
    }

    /**
     * Gather all shortcodes
     */
    public function get_shortcodes_localization()
    {
        $shortcodes = array(
            'location_full_address',
            'location_city',
            'location_name',
            'location_street_address',
            'location_state',
            'location_zip',
            'operator_name',
            'phone_number',
            'email_address',
            'area_served_1',
            'area_served_2',
        );

        $shortcodes = apply_filters('redwood_shortcodes_list', $shortcodes);

        return $shortcodes;
    }

    public static function get_location_table()
    {
        global $wpdb;

        return $wpdb->base_prefix . 'redwood_locations';
    }

    private static $setting_name = 'redwood_hmc_careerplug';

    public static function get_site_careerplug_link($site_id)
    {
        switch_to_blog($site_id);
        $settings = get_option(self::$setting_name, array());
        restore_current_blog();
        return (!empty($settings['url_careerplug']) ? $settings['url_careerplug'] : false);
    }

    public static function find_associated_zip($zip, $single = false)
    {
        global $wpdb;
        if (preg_match('/\d{5}([\-]\d{4})?/', $zip)) {
            list($zip) = explode('-', $zip);
        } elseif (preg_match('/[a-zA-Z0-9]{3}(\s?[a-zA-Z0-9]{3})?/', $zip)) {
            list($zip) = explode(' ', $zip);
            $zip = substr($zip, 0, 3);
        }
        $results = $wpdb->get_results('SELECT * from ' . self::get_location_table() . " WHERE associated_zips LIKE '%" . $zip . "%'", ARRAY_A);

        return ($single && isset($results[0]) ? $results[0] : $results);
    }

    public function craftsman_verify()
    {
        $response = array(
            'status'  => 'error',
            'message' => 'Invalid AJAX call',
        );

        if (defined('DOING_AJAX') && DOING_AJAX) {
            $response['message'] = 'No zip provided';
            if (isset($_POST['zip']) && $_POST['zip']) {
                $response['message'] = 'No location found';
                $location = self::find_associated_zip($_POST['zip'], true);
                if ($location) {
                    if ($careerplug_link = self::get_site_careerplug_link($location['site_id'])) {
                        $location['careerplug_link'] = $careerplug_link;
                    }
                    $response['status'] = 'success';
                    $response['message'] = $location;
                }
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public static function &get_instance()
    {
        if ( ! isset( self::$instance ) )
            self::$instance = new self;

        return self::$instance;
    }
}

Dreammaker_Shortcodes::get_instance();