<?php

namespace Surefire\Surepress;

final class Microsite {

	public $ID;
	public $microsite_blog_id = '0';
    public $microsite_post_id = '0';
	public $microsite_date = '0000-00-00 00:00:00';
	public $microsite_updated = '0000-00-00 00:00:00';
    public $microsite_domain = '';
    public $microsite_slug = '';
    public $microsite_turf_city = '';
    public $microsite_turf_state_province = '';
    public $mongoose_id = '';
    public $mongoose_phone = '';

	public static function get_instance( $blog_id ) {
		global $wpdb;

		$blog_id = (int) $blog_id;
		if ( ! $blog_id ) {
			return false;
		}

		$_microsite = wp_cache_get( $blog_id, 'microsites' );

		if ( ! $_microsite ) {
			$_microsite = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM sfl_microsites WHERE microsite_blog_id = %d LIMIT 1", $blog_id ) );

			if ( empty( $_microsite ) || is_wp_error( $_microsite ) ) {
				return false;
			}

            switch_to_blog($blog_id);
            $_microsite->microsite_domain = home_url('/');
            restore_current_blog();
            $_microsite->microsite_fields = (object) maybe_unserialize($_microsite->microsite_fields);
            if (isset($_microsite->microsite_fields->market_city) && $_microsite->microsite_fields->market_city) {
            	$_microsite->microsite_turf_city = $_microsite->microsite_fields->market_city;
            }

            if (isset($_microsite->microsite_fields->market_state) && $_microsite->microsite_fields->market_state) {
            	$_microsite->microsite_turf_state_province = $_microsite->microsite_fields->market_state;
            }

            if (isset($_microsite->microsite_fields->mongoose_id) && $_microsite->microsite_fields->mongoose_id) {
                $_microsite->mongoose_id = $_microsite->microsite_fields->mongoose_id;
            }

            if (isset($_microsite->microsite_fields->mongoose_phone) && $_microsite->microsite_fields->mongoose_phone) {
                $_microsite->mongoose_phone = $_microsite->microsite_fields->mongoose_phone;
            }

            wp_cache_set( $_microsite->microsite_blog_id, $_microsite->microsite_fields, 'microsite-fields' );
			wp_cache_add( $blog_id, $_microsite, 'microsites' );
		}

		return new Microsite( $_microsite );
	}

	public function __construct( $microsite ) {
		foreach( get_object_vars( $microsite ) as $key => $value ) {
			$this->$key = $value;
		}
	}

	public function to_array() {
		return get_object_vars( $this );
	}

	public function __get( $key ) {
        switch ( $key ) {
			case 'ID':
				return (int) $this->ID;
			case 'microsite_blog_id':
				return (int) $this->microsite_blog_id;
			case 'microsite_post_id':
                return (int) $this->microsite_post_id;
			default:
				$fields = $this->get_fields();
				if ( isset( $fields->$key ) ) {
					return $fields->$key;
				}
		}

		return null;
	}

	public function __isset( $key ) {
		return $this->$key;
	}

	public function __set( $key, $value ) {
		$this->$key = $value;
	}

    private function get_fields() {
		$fields = wp_cache_get( $this->microsite_blog_id, 'microsite-fields' );

		if ( false === $fields ) {

			$fields = (object) maybe_unserialize($this->microsite_fields);

			wp_cache_set( $this->microsite_blog_id, $fields, 'microsite-fields' );
		}

		$fields = apply_filters( 'microsite_fields', $fields );

		return $fields;
	}
}