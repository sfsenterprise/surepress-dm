<?php
namespace Surepress\Functions\Taxonomies;

function GenerateTaxonomy() {
	//GET/LOAD ALL FUNCTION FILES @ 'taxonomies' DIRECTORY
	$files = glob( get_template_directory().'/functions/taxonomies/' . "*.php");
	foreach($files as $file): 
		require $file; 
	endforeach;
}	