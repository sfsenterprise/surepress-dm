<?php
namespace Surepress\Functions\Campaigns;

function campaignTemplate ($j_template) {

    $template['campaign'] = get_post_meta( get_the_ID(), 'campaign_template', true );           
    $template['campaignty'] = get_post_meta( get_the_ID(), 'campaignsty_template', true );

    if (in_array('landing', $template)) {
		$j_template = get_template_directory() . '/template-landing.php';
	}
	return $j_template;	
}