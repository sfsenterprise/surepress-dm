<?php
use Surepress\Functions\Assets as Assets;

add_action("wp_ajax_reviews", "reviews");
add_action("wp_ajax_nopriv_reviews", "reviews");
add_action("wp_ajax_getjobs", "getjobs");
add_action("wp_ajax_nopriv_getjobs", "getjobs");

function reviews() {
	$search_criteria = $paging = array();
	$search_criteria['status'] = 'active';
	$paging          = array( 'offset' => $_REQUEST['page'] * $_REQUEST['perpage'], 'page_size' => $_REQUEST['perpage'] );
	$sorting         = array( 'key' => 'date_created', 'direction' => 'DESC' );
	$reviews         = GFAPI::get_entries($_REQUEST['form'], $search_criteria, $sorting, $paging, $_REQUEST['total']);

	$return = array();

	foreach($reviews as $review) {
		if($review['status'] === 'inactive') continue;
		$r = null;

		$r .= '<div class="col-xs-12 entry">';
		$r .= 	'<div class="col-xs-12 col-sm-2">';
		$r .= 		'<img class="col-lg-6" src="'. Assets\asset_path('images/review-quote.png') .'" alt="Review Quote"/>';
		$r .= 				'<span>';
		$r .= 				(isset($review['date_created']) ? date("M j, Y", strtotime($review['date_created'])) : '');
		$r .= 				'</span>';		
		
		$r .= 		'<span class="stars">';		

		if( isset($review[4])  ) { 
			for($i = 0; $i < $review[4]; $i++) {
				$r .= '<i class="fa fa-star"></i>';
			}	
		}					
		$r .= 		'</span>';

		$r .= 	'</div>';
		$r .= 	'<div class="col-xs-12 col-sm-10">';
		$r .= 		'<i>'.stripslashes_deep($review[5]).'</i><br>';
		$r .= 		'<strong>- ';
		$r .= 		($review[6]) ? $review[6] : '';
		$r .= 		' ';
		$r .= 		($review[7]) ? $review[7] : '';
		$r .= 		' ';
		$r .= 		' | '; 
		$r .= 		($review[8]) ? $review[8].', ' : '';
		$r .= 		($review[9]) ? $review[9] : '';
		$r .= 		'</strong>';
		$r .= 	'</div>';
		$r .= '</div>';

		array_push($return, $r);
	}

	wp_send_json_success( array(
		'count'     => count($reviews),
		'html'      => implode(' ', $return),
		'last_page' => ( ($_REQUEST['page'] + 1) * $_REQUEST['perpage'] >= $_REQUEST['total'] ) ? true : false
		)
	);
	die();
}
//ajax function to fetch jobs list on current site
function getJobs(){
	if(isset($_REQUEST['id'])){
		switch_to_blog($_REQUEST['id']);
		$joblist = '<option value="">* Choose Job</option>';
		$args = array(
		    'posts_per_page' => -1,
		    'post_type' => 'job-opportunities',
		    'post_status' => 'publish'
		);
		$careers = new WP_Query( $args );
		if ( $careers->have_posts() ) {
		    while ( $careers->have_posts() ) : $careers->the_post();
		    	$joblist .= '<option value="'.get_the_title().'">'.get_the_title().'</option>';
		    endwhile;
		}
		restore_current_blog();
		echo $joblist;
		die();
	}
}