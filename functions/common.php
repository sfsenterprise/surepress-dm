<?php
namespace Surepress\Functions\Common;
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Microsites\Microsite as Microsite; 
use Surepress\Functions\Locations as Locations; 

function populate_event_schedules($eventid)
{
    switch_to_blog(get_current_blog_id());
        $arr_schedules = array();

        $schedules = get_field( 'schedule', $eventid );

        for($i = 0; $i < $schedules; $i++) :

            if(strtotime(date('M d, Y')) > strtotime(date_format(date_create(get_field( 'schedule_'.$i.'_date', $eventid )), 'M d, Y'))) continue;

            $arr_schedules[$i]['date'] = date_format(date_create(get_field( 'schedule_'.$i.'_date', $eventid )), 'M d, Y');

            $times = get_field('schedule_'.$i.'_time', $eventid);

            for( $x = 0; $x < $times; $x++) :
                $arr_schedules[$i]['time'][$x] = date_format(date_create(get_field('schedule_'.$i.'_time_'.$x.'_start', $eventid)), 'h:i a') . ' to ' . date_format(date_create(get_field('schedule_'.$i.'_time_'.$x.'_end', $eventid)), 'h:i a');
            endfor;

        endfor;

        return iterator($arr_schedules);
    restore_current_blog();
}

function iterator($arr)
{
    $output = array();

    foreach ($arr as $key => $value)
    {
        $output[] = $value;
    }

    return $output;
}

function is_main()
{
    if( get_current_blog_id() === 1){
        return true;
    }else{
        return false;
    }   
}


function default_wp(){
    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' ); 
    add_theme_support( 'title-tag' );   

    add_image_size( '320x295', 320, 295, true ); //team archive thumb
    add_image_size('events-archive', 400, 600, true);
    add_image_size('services-archive', 400, 400, true);
    add_image_size('post-single', 900, 500, true);
    add_image_size('421x461', 421, 461, true); //before and after gallery
    add_image_size('1100x565', 1100, 565, true); //Service Page Single (Child pages section)
    add_image_size('282x122', 282, 122, true); //Service Page Single (Child pages section :: View Gallery)
    add_image_size('120x100', 120, 100, true); //Service Page Single (Child pages section :: Tab Navs)
    add_image_size('services-intro', 900, 600, true);
}


/* fow wp-admin use only*/
function get_cpt() {
  global $post, $typenow, $current_screen;

  if ( $post && $post->post_type ) {
    return $post->post_type;
  }
  elseif ( $typenow ) {
    return $typenow;
  }
  elseif ( $current_screen && $current_screen->post_type ) {
    return $current_screen->post_type;
  }
  elseif ( isset( $_REQUEST['post_type'] ) ) {
    return sanitize_key( $_REQUEST['post_type'] );
  }
  elseif ( isset( $_REQUEST['post'] ) ) {
    return get_post_type( $_REQUEST['post'] );
  }
  return null;
}

function get_portfolios($tax, $term){
        $args_gallery = array(
                        'posts_per_page'   => -1,
                        'numberposts'      => -1,
                        'orderby'          => 'date',
                        'order'            => 'DESC',
                        'post_type'        => 'portfolio',
                        'post_status'      => 'publish',
                        'suppress_filters' => false,

                        'tax_query' => array(
                            array(
                                'taxonomy' => $tax,
                                'field' => 'slug',
                                'terms' => $term,
                                //'include_children' => true,
                            )
                        ),                      
                        
                    );
    $gallery_array = get_posts( $args_gallery );    
    return $gallery_array;
} 

function get_portfolio_categories($tax, $s = false){
    $args = array(
            'hide_empty'    => ($s == true ? true : false),
            'taxonomy'      => $tax,
            'orderby'       => 'meta_value_num',
            'order'         => 'ASC',
            'parent'        => 0,
            'meta_query' => [[
                    'key' => 'gallery_order',
                    'type' => 'NUMERIC',
                  ]], 
        );
    

    /*if($tax == 'portfolio_categories' && is_main()){
        $args['order'] = 'ASC';
        $args['meta_key'] = 'category_sort';
        $args['orderby']   = 'meta_value';
    }*/

    $categories = get_terms($args);

    return $categories;
}

function get_portfolio_cover($id, $size = 'full'){
    $cat_name = get_term_by('id', $id, 'portfolio_categories');
    $cat_cover = (isset($cat_name->term_id) && !empty($cat_name->term_id) ? get_term_meta($cat_name->term_id, 'img_cover', true) : '');

    if(isset($cat_cover) && !empty($cat_cover)){
        return wp_get_attachment_image($cat_cover, $size);
    }else{

        $img_url = Assets\asset_path('images/portfolio-traditional-kitchen.jpg');
            $alt = 'Traditional Kitchen';
        if(!empty($cat_name->slug)){
            if( $cat_name->slug == 'traditional-kitchens'){
                $img_url = Assets\asset_path('images/portfolio-traditional-kitchen.jpg');
                $alt = 'Traditional Kitchen';
            }
            if( $cat_name->slug == 'transitional-kitchens'){
                $img_url = Assets\asset_path('images/portfolio-transitional-kitchen.jpg');
                $alt = 'Transitional Kitchens';
            }
            if( $cat_name->slug == 'contemporary-kitchen'){
                $img_url = Assets\asset_path('images/portfolio-contemporary-kitchen.jpg');
                $alt = 'Contemporary Kitchens';
            }
            if( $cat_name->slug == 'master-bath'){
                $img_url = Assets\asset_path('images/portfolio-master-bathroom.jpg');
                $alt = 'Master Bathroom';
            }
            if( $cat_name->slug == 'standard-baths'){
                $img_url = Assets\asset_path('images/portfolio-standard-bathroom.jpg');
                $alt = 'Standard Bathroom';
            }
            if( $cat_name->slug == 'interior-remodeling'){
                $img_url = Assets\asset_path('images/portfolio-interior-remodeling.jpg');
                $alt = 'Interior Remodeling in Millen, GA';
            }
            if( $cat_name->slug == 'safety-mobility'){
                $img_url = Assets\asset_path('images/portfolio-safety-mobility.jpg');
                $alt = 'Safety and Mobility';
            }
            if( $cat_name->slug == 'cabinet-refacing'){
                $img_url = Assets\asset_path('images/portfolio-cabinet-refacing.jpg');
                $alt = 'Cabinet Refacing';
            }
        }
        $cover_img = '<img src="'.$img_url.'" class="attachment-services-archive size-services-archive" alt="'.$alt.'" />';
    }

    return $cover_img;
}

function get_posts_by_custom_post($post_type, $tax, $term, $count = NULL, $field = 'slug'){
	$args = array(
			'posts_per_page' => ($count != NULL ? $count : -1),
			'post_type' => $post_type,
			'tax_query' => array(
						    array(
						        'taxonomy' => $tax,
						        'field' => $field,
						        'terms' => $term,
							)
			),
		);
	$posts = get_posts( $args );
	if($post_type == 'portfolio'){
		$cat = get_term_by($field, $term, $tax);
        if( isset( $cat->term_id ) ) :
    		$img_order = get_blog_option(get_current_blog_id(), 'gallery_img_ordering_'.$cat->term_id);
    		if(isset($img_order) && $img_order != FALSE) {
    			$order = array();
    			foreach ($img_order as $ordr) {
                    foreach ($posts as $key => $value) {
                       if($ordr == $value->ID){
                            $order[] = $value;
                       }
                    }
                }

                foreach ($posts as $key => $val) {
                    if(!in_array($val, $order)) {
                        $order[] = $val;
                    }
                }
                return $order;
    		}else{
    			return $posts;
    		}
        endif;
	}
	
	return $posts;
}

function get_related_case($id){

        if ( get_post_type() ==="services"){
            $meta_key = 'case_services';
        } else if( get_post_type() === "portfolio" ){
            $meta_key = 'case_gallery';
        }       

        $args_case = array(
                        'posts_per_page'   => 2,
                        'numberposts'      => 2,
                        'orderby'          => 'date',
                        'order'            => 'DESC',
                        'post_type'        => 'case-studies',
                        'post_status'      => 'publish',
                        'suppress_filters' => false,

                        
                        'meta_query' => array(
                            array(
                                'key' => $meta_key,
                                'value' => $id,
                                'compare' => 'LIKE'
                            )
                        )
                                            
                        
                    );

    $case_array = get_posts( $args_case );  
    return $case_array; 
}

//temporarily set for  events
function default_thumbnail($title=null){
    $thumbs = ((get_post_type() == 'post') ?  'post-single' : 'events-archive'); 

    if( has_post_thumbnail() ) {
        echo the_post_thumbnail($thumbs);
    } else {
        
            $slug =  untrailingslashit (  str_replace(get_site_url().'/services/', "", get_permalink() )  );

            switch_to_blog(1);
                $main_service = get_page_by_path( $slug, OBJECT, 'services' );
                if($main_service)  $main_thumb = get_the_post_thumbnail( $main_service->ID );
            restore_current_blog();

            if( isset($main_thumb) ){
                echo $main_thumb;
            } else {
                echo '<img src="'.Assets\asset_path('images/dream-kitchen.jpg').'"  />';
            }
    }
}


function get_service_children($parent){
    
    global $post;


    if (  !is_admin()  && is_singular('services')  ) :

        $args = array(
            'posts_per_page'   => -1,
            'orderby'          => 'menu_order',
            'order'            => 'ASC',
            'post_type'        => 'services',
            'post_parent'      => $parent,
            'post_status'      => 'publish',
            'suppress_filters' => true,
        );

        $children = get_posts( $args );
        return $children;
    endif;

}



function get_blog_posts($num){

        $args = array(
            'posts_per_page'   => $num,
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'post',
            'post_status'      => 'publish',
            'suppress_filters' => true,
        );

        $data = get_posts( $args );
        return $data;
}




//to active microsite theme to use surepress-dm
function ActivateTheme(){

    if( get_current_blog_id() != 1 ) return;

    $sites = get_sites( array('site__not_in' => array(1) ) );
    foreach($sites as $s){
        update_blog_option( $s->blog_id, 'current_theme', 'Dream Maker');
        update_blog_option( $s->blog_id, 'template', 'surepress-dm');   
        update_blog_option( $s->blog_id, 'stylesheet', 'surepress-dm');     
    }   
}

function RedirectSingularPages(){
    global $post;  
    if ( (is_singular('offers') && !isset($_GET['preview'])) || 
         is_singular('events') || 
         is_singular('glossary') ||
         is_singular('faqs') ||
         is_singular('team') ||
         (is_singular('services') && $post->post_parent ) ) { 

        wp_redirect( home_url(), 302 );
        exit;
  }
}

function get_events(){

    $args = array(
        'posts_per_page'   => -1,
        'orderby'          => 'order',
        'order'            => 'ASC',
        'post_type'        => 'events',
        'post_status'      => 'publish',
        'suppress_filters' => true,
        'meta_key'         => 'on_homepage',
        'meta_value'       => 1
    );

    $data = get_posts( $args );
    return $data;
}

function get_parent_services($limit){
    $args = array(
            'posts_per_page'   => $limit,
            'orderby'          => 'menu_order',
            'order'            => 'ASC',
            'post_type'        => 'services',
            'post_status'      => 'publish',
            'post_parent'      => 0,
            'suppress_filters' => true,
        );

    $services = get_posts( $args );

    return $services;
}
 

function populate_services_checkbox($form){
    if(!$form)
        return;

    $i = 1; 

    foreach ( $form['fields'] as &$field ) {
        
        if ( $field->type != 'checkbox' || strpos( $field->cssClass, 'services_checkbox' ) === false ) {
            continue;
        }

        $limit = (strpos( $field->cssClass, 'limit_4' ) === false) ? -1 : 4;

        $services = get_parent_services($limit);
        $choices = array();
        $inputs = array();
        
        foreach ( $services as $service ) {

            if ( $i % 10 == 0 ) {
                $i++;
            }

            //$field_id_final = $field->id."_".$i;
            $choices[] = array( 'text' => $service->post_title, 'value' => $service->post_title );
            $inputs[] = array( "label" =>$service->post_title, "id" => "{$field->id}.{$i}" );
            //array_push($choices, array("text" => $service->post_title, "value" => $service->post_title));
            //array_push($inputs, array("label" =>$service->post_title, "id" => $field_id_final));
                $i++;
        }

        $field->choices = $choices;
        $field->inputs = $inputs;
    }

    return $form;

}

function populate_services_dropdown($form){
    if(!$form)
        return;

    
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'services_dropdown' ) === false ) {
            continue;
        }
 
        $services = get_parent_services(-1);
        $choices = array();
 
        foreach ( $services as $service ) {
            $choices[] = array( 'text' => $service->post_title, 'value' => $service->post_title );
        }
 
        $field->placeholder = 'Interested In';
        $field->choices = $choices;
 
    }
 
    return $form;
}

function formatPhone($phone){
    if($phone === "") return;
    $output = sprintf("(%s) %s-%s",
              substr($phone, 0, 3),
              substr($phone, 3, 3),
              substr($phone, 6, 4));
    return $output;
    //return $phone;
}



function FeaturedServices(){
    return get_blog_option( get_current_blog_id(), 'featured_services' );
}

function custom_title_tags($title){
    global $wp_query;
    $new_title = ( !empty($wp_query->nap->microsite_name) ? " | ".$wp_query->nap->microsite_name : "" )." | DreamMaker Bath & Kitchen "; 

    // HOMEPAGE: DreamMaker Bath & Kitchen | <location>
    if(is_front_page()){
        return "DreamMaker Bath & Kitchen ".( !empty($wp_query->nap->microsite_name) ? " | ".$wp_query->nap->microsite_name : "" );
    }

    // PAGE: <Page name> | <location> | DreamMaker Bath and Kitchen
    if(is_page()){
        return get_the_title().$new_title; 
    }
    
    if(is_archive()){
        //ARCHIVE: <service name> | <location> | DreamMaker Bath and Kitchen
        if(get_post_type() == 'portfolio'){
            return "Gallery ".$new_title;
        }
        if(is_tax('portfolio_categories')){
            $term = get_term(get_queried_object()->term_id);
            return $term->name." | Gallery".$new_title;
        }
        else{
            return ucwords(preg_replace( '/[^a-z]/i', " ", get_post_type())).$new_title;
        }  
    }

    if(is_singular()){
        return get_the_title()." | ".ucwords(preg_replace( '/[^a-z]/i', " ", get_post_type())).$new_title;
    }

    if ( !is_front_page() && is_home() ) {
        return  "Blog ".$new_title;
    }   

    else{
        return $title;
    }
}


function MicrositeAddress($multi_line = false){
    global $wp_query;

    if(!$wp_query->nap) return;
    $output = null;
    
    if( $wp_query->nap->microsite_street ){

        $separator = ($multi_line) ? '' : ', ';

        $output .= $wp_query->nap->microsite_street;
        $output .= ($wp_query->nap->microsite_street2) ? ' '.$wp_query->nap->microsite_street2.$separator : $separator; 

        if($multi_line) $output .= '<br>';
    }    

    $output .= $wp_query->nap->microsite_city . ', ';
    $output .= $wp_query->nap->microsite_state_province . ' ';
    $output .= $wp_query->nap->microsite_zip_postal;

    return $output;
}


function isCustomReviewsMicrosite(){
    global $wp_query;
    /*St. Louis Park - 48
    Livonia - 65
    Wilmington - 75 - removed DRS24
    Greenville - 35 - removed DRS-213
    Rapid City - 44
    Canal Winchester - 28
    Lansing - 38
    Pittsburgh - 43
    Southeast Wisconsin - */

    $custom_list = [65,44,28,38,43,71];
    if( in_array($wp_query->nap->microsite_blog_id, $custom_list )  ){
        return true;
    }

    return false;
}

function ja_remove_hentry( $class ) {
    $class = array_diff( $class, array( 'hentry' ) );   
    return $class;
}

function custom_email_notification($notification, $form, $entry){
    global $wp_query;

    if(!$form)
        return;

    $values = unserialize($wp_query->nap->microsite_fields);

    $forms = array(
        array('Start A Conversation Popup', 'start_conversation_email' ),
        array('Start A Conversation', 'start_conversation_email' ),
        array('Contact Us', 'contact_us_email'),
        array('Offers','special_offers_email'),
        array('Events', 'events_email'),
        array('Reviews Form','reviews_email' ),
        array('Start A Conversation Popup - Campaign', 'start_conversation_campaign_email' ),
        array('Start A Conversation - Campaign', 'start_conversation_campaign_email' ),
        array('Download Remodeling Guide', 'download_remodeling_guide' ),
    );

    foreach($forms as $f){
        if($form['title'] == $f[0]){
            if( $form['title'] == 'Start A Conversation Popup - Campaign' || $form['title'] == 'Start A Conversation - Campaign'  ) {
               $email = get_post_meta( get_the_ID(), 'campaign_email');
               if(isset($email[0])){
                $email = $email[0];
               } else {
                return $notification;
               }
            }else{
                $email = (isset($values['start_conversation_email']) && !empty($values['start_conversation_email']) ) ?  $values[ $f[1] ] : $wp_query->nap->microsite_admin_email ;
            }  
        } else {
           $email = $wp_query->nap->microsite_admin_email ; 
        }
    }


    $notification['to'] = $email;
    return $notification;
}


function populate_events_dropdown($form){

    if(!$form)
        return;

    
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'events_dropdown' ) === false ) {
            continue;
        }

         if(!isset($_COOKIE['event_sched']) )
             continue;

        
        $text = $_COOKIE['event_sched']; 
        $services = explode(",", $text);
        $choices = array();
 
        foreach ( $services as $service ) {
            if($service == 'null')
                $service = "No Schedule";

            $choices[] = array( 'text' => $service, 'value' => $service );
        }
 
        $field->placeholder = '* Date & Time';
        $field->choices = $choices;
    }
 
    return $form;
}

function has_current_events(){

    wp_reset_query();      
    $args = array( 'post_type' => 'events', 'posts_per_page' => -1 );
    $myposts = get_posts( $args );
    $date_now = new \DateTime(); 
    $date_now = $date_now->format('m/j/y');
    $date = [];
    foreach ( $myposts as $b_post ) { 
        $schedule = get_field( 'schedule', $b_post->ID );

        if( !empty( $schedule ) ) :

            for( $i = 0; $i < $schedule; $i++ ) :
                $date[] = date('m/d/y', strtotime(get_field( 'schedule_'.$i.'_date', $b_post->ID )));
            endfor;
        endif;
        
        foreach ( $date as $value ) :
            if( strtotime( $date_now ) <= strtotime( $value ) ) :
                return true;
            endif;
        endforeach;
    } 

    wp_reset_postdata();

    return false;
}

function sfl_get_nearest_microsite($zip){
	$nearest = null;
	$sites = get_sites( ['site__not_in'=>array(1), 'archived'=>0, 'deleted'=>0, 'fields'=> 'ids'] );
	$site_assoc_zips = array();
	$in_assoc = false;
	foreach($sites as $site){
		$microsite_fields = getMicrositeExtraFields($site);
		$site_assoc_zips[$site] = explode( ",", $microsite_fields['associated_zips'] );
		foreach($site_assoc_zips as $site_key => $assoc){
			if(in_array($zip, $assoc)){
				$nearest = $site_key;
				$in_assoc = true;
			}
		}
	}
	if(!$in_assoc){
		$coordinates = Locations\get_latlng($zip);
		$lat = $coordinates['results'][0]['geometry']['location']['lat'];
		$lng = $coordinates['results'][0]['geometry']['location']['lng'];
		$xy = array();
		foreach($sites as $site) {
			switch_to_blog($site);
				$xy[$site] = sfl_get_distance(getMicrositeLatLng($site)[0]->microsite_lat, getMicrositeLatLng($site)[0]->microsite_lng, $lat, $lng);
			restore_current_blog();
		}
		$nearest = array_keys($xy, min($xy))[0];
	}
	return $nearest;
}