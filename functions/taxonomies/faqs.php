<?php

    register_taxonomy(
        'faqs-category',
        'faqs',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'faqs-category' ),
            'hierarchical' => true,
        )
    );