<?php

    register_taxonomy(
        'services-category',
        'services',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'service-category' ),
            'hierarchical' => true,
        )
    );