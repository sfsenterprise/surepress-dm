<?php

    register_taxonomy(
        'portfolio_categories',
        'portfolio',
        array(
            'label' => __( 'Categories' ),
            'rewrite' => array( 'slug' => 'galleries', 'with_front' => false),
            'hierarchical' => true,
        )
    );