<?php

    register_taxonomy(
        'glossary-category',
        'glossary',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'glossary-category' ),
            'hierarchical' => true,
        )
    );