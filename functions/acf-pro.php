<?php
namespace Surepress\Functions\ACF;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Services as Services;

function GenerateAcfFields() {

	if ( !is_admin() ) return;
	if( !taxonomy_exists( 'portfolio_categories' ) ) die( "Too early Jose" );

	$post_type = Common\get_cpt(); 
	$parent_posts = array();
	$gallery_posts = array();		
	$categories = array();
	$services_categories = array();

	/////////////////CLEAN ME START	
	if( $post_type == "case-studies") {

		$posts_array = Services\get_services();	

		foreach($posts_array as $k => $p){
			if($p->post_parent == 0) { 
				$parent_posts[$p->ID] = $p->post_title;

			}
		}

		//$gallery_array = Common\get_portfolios('portfolio_categories', 'case-studies');
		$gallery_array =  Common\get_portfolio_categories('portfolio_categories');

		
		foreach($gallery_array as $val){
			//if($g->post_parent == 0) { 
				//$gallery_posts[$g->ID] = $g->post_title .'<br>'. get_the_post_thumbnail($g->ID, 'thumbnail');
				$gallery_posts[$val->term_id] = $val->name;
			//}
		}

	} else if( $post_type == "services"){

		$gallery_array = Common\get_portfolios('portfolio_categories', 'services');
	
		foreach($gallery_array as $k => $g){
			if($g->post_parent == 0) { 
				$gallery_posts[$g->ID] = $g->post_title .'<br>'. get_the_post_thumbnail($g->ID, 'thumbnail');
			}
		}

	} else {
		//do nothing
	}
	/////////////////CLEAN ME END

	$categories = 	get_terms(['taxonomy'=>'services-category', 'hide_empty'=>false]);
	
	foreach ($categories as $category) {
		$name = get_term_by('id', $category->term_id, 'services-category');
		$services_categories[$category->term_id] = $name->name;
	}

	//GET/LOAD ALL FUNCTION FILES @ 'taxonomies' DIRECTORY
	$files = glob( get_template_directory().'/functions/acf/' . "*.php");
	foreach($files as $file): 
		require $file; 
	endforeach;
}