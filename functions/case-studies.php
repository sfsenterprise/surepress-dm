<?php
	namespace Surepress\Functions\CaseStudies;

	add_action('wp_ajax_get_case_studies', __NAMESPACE__.'\\get_case_studies');
    	add_action('wp_ajax_nopriv_get_case_studies', __NAMESPACE__.'\\get_case_studies');
	function get_case_studies(){
			$posted = $_POST["id"]; 
			$studies_html = "";
            $site_id = $_POST["site_id"]; 
			foreach($posted as $p){
				$siteid = $p["blogID"];
                $site_id = $p["site_id"];
				if( !empty($p["postID"]) ){
                    //query for the corporate site case studies
                    if($site_id == 1){
					       switch_to_blog($siteid);
						foreach( $p["postID"] as $case ){
                            //print_r($case); die();
							$study = get_post((int) $case);
							$meta = get_post_meta($study->ID); 
							$gallery = isset($meta['gallery']) ? format_gallery2(maybe_unserialize($meta['gallery'][0]), $siteid):'';
							$cat = ($meta['category'][0] == -1) ? 'Uncategorized' : $meta['category'][0]; 
							$term = _get_term($cat);
							$cat_name = isset($term->term_id) ? $term->name:$cat;
							$studies_html .= '<div class="animated slideInUp panel panel-default"><div class="panel-body"><table class="col-lg-9">';
							$studies_html .= '<tr><td><strong>Project Name</strong></td>';
							$studies_html .= '<td>'.$study->post_title.'</td>';
							$studies_html .= '</tr><tr><td><strong>Location</strong></td>';
							$studies_html .= '<td>'.$meta['city'][0].', '.$meta['state'][0].'</td></tr>';

							if(!empty( $study->post_excerpt )):
								$studies_html .= '<tr><td><strong>Project Summary</strong></td>';
								$studies_html .= '<td>'.$study->post_excerpt.'</td></tr>';
							endif;
							
							$studies_html .= '<tr><td><strong>Project Category</strong></td><td>'.$cat_name.'</td></tr>';

							if(!empty( $meta['testimonials'][0] )):
								$studies_html .= '<tr><td><strong>Customer Testimonials</strong></td><td>'.$meta['testimonials'][0].'</td></tr>';
							endif;

							$studies_html .= '</table><span class="col-lg-3"><div class="row">';
							$studies_html .= '<div class="col-lg-12"><p><a class="btn btn-info" href="'.get_the_permalink($study->ID).'">View this case study </a></p>';
							$studies_html .= '</div></div><div class="row"><div class="col-lg-12">'.$gallery.'</div></div></span></div></div>';
						}
					       restore_current_blog();
                    }else{ //query for the microsites case studies
                        switch_to_blog($site_id);
                        $study = get_post((int) $p["postID"]);
                        $meta = get_post_meta($study->ID); 

                        // DRS-650 ( Ogden causes error when using format_gallery2 )
                        if($site_id == 41){
                            $gallery = isset($meta['gallery']) ? format_gallery(maybe_unserialize($meta['gallery'][0]), $siteid):'';
                        } else {
                            $gallery = isset($meta['gallery']) ? format_gallery2(maybe_unserialize($meta['gallery'][0]), $siteid):'';
                        }
                        
                        $cat = ($meta['category'][0] == -1) ? 'Uncategorized' : $meta['category'][0]; 
                        $term = _get_term($cat);
                        $cat_name = isset($term->term_id) ? $term->name:$cat;
                        $studies_html .= '<div class="animated slideInUp panel panel-default"><div class="panel-body"><table class="col-lg-9">';
                        $studies_html .= '<tr><td><strong>Project Name</strong></td>';
                        $studies_html .= '<td>'.$study->post_title.'</td>';
                        $studies_html .= '</tr><tr><td><strong>Location</strong></td>';
                        $studies_html .= '<td>'.$meta['city'][0].', '.$meta['state'][0].'</td></tr>';

                        if(!empty( $study->post_excerpt )):
                            $studies_html .= '<tr><td><strong>Project Summary</strong></td>';
                            $studies_html .= '<td>'.$study->post_excerpt.'</td></tr>';
                        endif;
                        
                        $studies_html .= '<tr><td><strong>Project Category</strong></td><td>'.$cat_name.'</td></tr>';

                        if(!empty( $meta['testimonials'][0] )):
                            $studies_html .= '<tr><td><strong>Customer Testimonials</strong></td><td>'.$meta['testimonials'][0].'</td></tr>';
                        endif;

                        $studies_html .= '</table><span class="col-lg-3"><div class="row">';
                        $studies_html .= '<div class="col-lg-12"><p><a class="btn btn-info" href="'.get_the_permalink($study->ID).'">View this case study </a></p>';
                        $studies_html .= '</div></div><div class="row"><div class="col-lg-12">'.$gallery.'</div></div></span></div></div>';
                        restore_current_blog();
                    }
				}
			}
            //echo $siteid;
			//print_r($posted);
            echo $studies_html;
			wp_die();
	}

    function _get_term($term_id)
    {
        switch_to_blog(1);
            $term = get_term((int)$term_id, 'project_categories');
        restore_current_blog();

        return $term;
    }

    function format_gallery($gallery, $siteid)
    {
        $output = '';

        switch_to_blog($siteid);
            $blog_name = get_bloginfo('name');
            $uploads_dir = wp_upload_dir();
        restore_current_blog();

        $output .= '<ul class="case-gallery">';
        foreach ($gallery as $key => $value)
        {
            $output .= '<li><img src="'.$uploads_dir['baseurl'] . '/gallery/' . $value['image'].'" alt="'.$blog_name.' - '.$value['image'].'" /></li>';
        }
        $output .= '</ul>';

        return $output;
    }
    function format_gallery2($gallery, $siteid)
    {
        $output = '';
        $imgfileext = '';
        $imgfilenoext = '';
        switch_to_blog($siteid);
            $blog_name = get_bloginfo('name');
            $uploads_dir = wp_upload_dir();
        restore_current_blog();
        
        if( is_array($gallery) && count($gallery) > 0 ) :
            $output .= '<ul class="case-gallery">';
            foreach ($gallery as $key => $value)
            {
                // $imgfile = $uploads_dir['basedir']. '/gallery/' .$value['image'];

                // if(file_exists($imgfile)){
                //     //only create resized file if desired dimension does not exist
                //     if(!file_exists(wp_get_image_editor( $imgfile )->generate_filename('75x75'))){
                //         $editor = wp_get_image_editor( $imgfile, array() );
                //         $resultimage = $editor->resize(75, 75, true);
                //         if (!is_wp_error($resultimage)) {
                //             $resizedimage = $editor->save($editor->generate_filename('75x75'));
                //             $value['image'] = $resizedimage['file'];
                //         }

                //     }else{
                //         $imgfileext = preg_match('/\./', $value['image']) ? preg_replace('/^.*\./', '', $value['image']) : '';
                //         $imgfilenoext = preg_replace('/.[^.]*$/', '', $value['image']); //get filename without file extension
                //         $value['image'] = $imgfilenoext.'-75x75.'.strtolower($imgfileext);
                //     }
                // }

                $output .= '<li>';
                // $output .= '<img src="'.$uploads_dir['baseurl'] . '/gallery/' . $value['image'].'" alt="'.$blog_name.' - '.$value['image'].'" />';
                $thumb = get_gallery_thumb($value['image'], $siteid);
                if ($thumb):
                    $output .= '<img src="'.$thumb.'" alt="'.$blog_name.' - '.$value['image'].'" />';
                else:
                   
                   $old_thumb = explode('.', $value['image']);

                   if (count($old_thumb) > 2 ) {
                        $old_thumb[ count($old_thumb) - 2 ] = $old_thumb[ count($old_thumb) - 2 ] . '-75x75';
                        $old_thumb[ count($old_thumb) - 1 ] = strtolower($old_thumb[ count($old_thumb) - 1 ]);
                        $new_thumb = implode('.', $old_thumb);
                   } else {
                        $old_thumb[ count($old_thumb) - 1 ] = strtolower($old_thumb[ count($old_thumb) - 1 ]);
                        $new_thumb = implode('-75x75.', $old_thumb);
                   }
                   

                   $output .= '<img src="'.$uploads_dir['baseurl'] . '/gallery/' . $new_thumb .'" alt="'.$blog_name.' - '.$value['image'].'" />';    
                endif;    
                $output .= '</li>';
            }
            $output .= '</ul>';
        endif;    

        return $output;
    }  


    function get_gallery_thumb($filename, $siteid) {
        switch_to_blog($siteid);
            global $wpdb;
            $sql = $wpdb->prepare("SELECT * FROM  $wpdb->posts WHERE  post_type = 'attachment' and guid like %s order by post_date desc", "%$filename");
            $attachments = $wpdb->get_results($sql, OBJECT);
            $img = $attachments[0]->ID ? wp_get_attachment_image_url( $attachments[0]->ID,  'thumbnail') : null;
        restore_current_blog();   
        return $img;
    }      
 ?>
