<?php
//namespace Surepress\Functions\Walker;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Services as Services;

class HeaderServiceSubNav extends Walker_Nav_Menu {
	
	function end_el(&$output, $item, $depth=0, $args=array()) {

	    if( $item->title === "Services" ){
			$output .= '<ul class="sub-menu">';
			foreach ( Services\get_services(0) as $service ) {
				$output .= '<li><a href="'.get_permalink($service->ID).'">'.$service->post_title.'</a></li>';
			}
			$output .= '</ul>';
		}	

	}

}