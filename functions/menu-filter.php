<?php
namespace Surepress\Functions\Menu;
use Surepress\Functions\Common as Common;

function MicrositeMenu( $items, $menu, $args ) {
		//return $items;
		restore_current_blog();
		if( !Common\is_main() ) {
				//microsite
			    foreach ( $items as $key => $item ) {
		            if(!empty($item->url)) {
		            	//Hide Locations and Directions page on Microsite
		            	if( ($item->title === "Directions") || ($item->title === "Locations") ) unset($items[$key]);

						//check if page is existing on the microsite by checking if slug [including parent i.e "/about/XXXX"] exists
						if($item->object === "page" ){
							if( get_page_by_path( Get_Slug($item->url), OBJECT, array('page') ) === null ){
								 unset($items[$key]);
							}
						}

						// $microsite_menu = array('41', '48', '92', '25', '96');
						// if (!in_array(get_current_blog_id(), $microsite_menu) && $item->title === "Local Remodeling Projects" ) {
						// 	unset($items[$key]);
						// }
						//unset the dropdown link under galleries if page does not exist
						if($item->title === "Local Remodeling Projects" && !get_page_by_title('Areas We Serve')){
							unset($items[$key]);
						}
						//change the url by microsite
		            	if( substr($item->url,0,1) === '/'){ //for items using relative url
		                	$item->url = get_site_url().$item->url;
		            	} else { //for items using absolute url
		            		$item->url = str_replace( network_site_url(), get_site_url()."/" , $item->url);
						}

		            }
				}

		} else {
			//main site
			foreach ( $items as $key => $item ) {
				//Hide Directions page on Main
				if( ($item->title === "Directions") || ($item->title === "Our Team") || ($item->title === "Local Remodeling Projects") || ($item->title === "Resource Center") ) unset($items[$key]); continue;
			}
		}

		return $items;
		switch_to_blog(1);

}

function CustomMenuObject( $sorted_menu_items, $args ) {
	restore_current_blog();
	$aboutID = 0;
	$aboutURL = '';
	$serviceID=0;
	$directionId=0;
	$serviceURL='';
	$theme_location = "Primary Menu";
	$addtomenu = array();

	if ( isset($args->menu->name) && ($theme_location !== $args->menu->name) ) {
        return $sorted_menu_items;
    }

	foreach ( $sorted_menu_items as $key => $item ) {
		if($item->title == 'About'){
			$aboutID = $item->ID;
			$aboutURL = $item->url;
		}
		if($item->title == 'Services'){
			$serviceID = $item->ID;
			$serviceURL = $item->url;
		}
		if ($item->title == 'Directions') {
			$directionId = $item->ID;
		}
		if ($item->title == 'Resource Center') {
			$resourcecenterId = $item->ID;
			$item = array(
	            'title'            => 'Preferred Vendors',
	            'menu_item_parent' => $resourcecenterId,
	            'ID'               => $s->post_name,
	            'db_id'            => rand ( 10000 , 99999 ),
	            'url'              => get_site_url().'/preferred-vendors/',
        	);	
        	if(get_page_by_title( 'Preferred Vendors' )){
        		$sorted_menu_items[] = (object) $item;
        	}
        			
			$item = array(
	            'title'            => 'Design Center Tour',
	            'menu_item_parent' => $resourcecenterId,
	            'ID'               => $s->post_name,
	            'db_id'            => rand ( 10000 , 99999 ),
	            'url'              => get_site_url().'/design-center-tour/',
        	);
        	if(get_page_by_title( 'Design Center Tour' )){
        		$sorted_menu_items[] = (object) $item;
        	}
			$item = array(
	            'title'            => 'Helpful Videos',
	            'menu_item_parent' => $resourcecenterId,
	            'ID'               => $s->post_name,
	            'db_id'            => rand ( 10000 , 99999 ),
	            'url'              => get_site_url().'/helpful-videos/',
        	);
			
    		$sorted_menu_items[] = (object) $item;
    		$item = array(
	            'title'            => 'Remodeling Tips',
	            'menu_item_parent' => $resourcecenterId,
	            'ID'               => $s->post_name,
	            'db_id'            => rand ( 10000 , 99999 ),
	            'url'              => get_site_url().'/blog/',
        	);
        	$sorted_menu_items[] = (object) $item;
			
		}
	}
	$current_microsite = get_current_blog_id();
	$microsite_fields = getMicrositeExtraFields($current_microsite);

	/*  REMOVED AS PER: https://surefiresocial.atlassian.net/browse/DRS-166 --Japz
	if( $current_microsite == "92"){
		$addtomenu[] = array('name' => "Photo Contest", 'url' => "photo-contest/");
	}
	*/

	if( $current_microsite == "67" || $current_microsite == "94" ){
		$addtomenu[] = array('name' => "Preferred Vendors", 'url' => "preferred-vendors/");
	}

	if( $current_microsite == "48"){
		$addtomenu[] = array('name' => "Awards", 'url' => "awards/");
		$addtomenu[] = array('name' => "Media", 'url' => "media/");
	}

    //if( !Common\is_main()){ if(Common\has_current_events()){ $addtomenu[] = array('name' => "Events",'url' => "events/"); }}
    if( !Common\is_main()){ if((Common\has_current_events()) && $microsite_fields['microsite_show_events']){ $addtomenu[] = array('name' => "Events",'url' => "events/"); }}

    if( ( has_financing_option() ||	!empty( get_blog_option( $current_microsite, 'corporate_financing' ) ) ) && $microsite_fields['microsite_show_financing'] )
	{
		$addtomenu[] = array('name' => "Financing Options", 'url' => "financing-options/");
	}

    if( has_special_offers() && $microsite_fields['microsite_show_special_offers']){ $addtomenu[] = array( 'name' => "Special Offers", 'url' => "special-offers/");}

    /** services menu **/
    $services = Common\get_parent_services(-1);
     if($services){
		foreach($services as $s){
			$item = array(
	            'title'            => $s->post_title,
	            'menu_item_parent' => $serviceID,
	            'ID'               => $s->post_name,
	            'db_id'            => rand ( 10000 , 99999 ),
	            'url'              => get_permalink($s->ID),
        	);

    		$sorted_menu_items[] = (object) $item;
		}
     }
     //redirecting Directions page
	$direction_name = ( !empty( get_page_by_title('Areas We Serve') ) && !Common\is_main() ) ? 'Service Areas' : 'Directions'; 
    $addtomenu[] = array('name' => $direction_name, 'url' => "directions/");

	// Add Buildertrend
	//if( Common\is_main() ){ $addtomenu[] = array('name' => "Client Online Access", 'url' => "client-online-access/"); };

    if(isset($addtomenu)){
    	    foreach($addtomenu as $a){
    	    	$item = array(
    		            'title'            => $a['name'],
    		            'menu_item_parent' => $aboutID,
    		            'ID'               => ucwords($a['name']).'id',
    		            'db_id'            => rand ( 10000 , 99999 ),
    		            'url'              => (!empty($aboutURL) ? $aboutURL : get_site_url()).$a['url'],
    	        	);

    	    	$sorted_menu_items[] = (object) $item;
    	    }
	}

	$submenus = array();
	$parent_menus = array();
	foreach ( $sorted_menu_items as $key => $item ) {
		if($item->menu_item_parent == 0){
			$parent_menus[$item->ID] = array(
				'ID'	=> $item->ID,
				'title'	=> $item->title,
				'url'	=> $item->url,
				'count'	=> 1
			);
		} else {
			if(array_key_exists($item->menu_item_parent, $parent_menus)){
				$parent_menus[$item->menu_item_parent]['count']++;
			}
		}
	}
	foreach ( $parent_menus as $key => $menu ) {
		if($menu['count'] > 1){
			$item = array(
				'title'            => $menu['title'],
				'menu_item_parent' => $menu['ID'],
				'ID'               => $menu['ID'],
				'db_id'            => rand ( 10000 , 99999 ),
				'url'              => $menu['url']
			);
			$submenus[] = (object) $item;
		}
	}
	if(!empty($submenus)){	
		array_splice( $sorted_menu_items, 1, 0, $submenus );
	}

	/* removed the condition to permanently add Local Remodeling Projects under galleriies */
	/**  remove areas we serve under directions menu **/
	// $microsite_menu = array('41', '48', '92', '25', '96');
	// if (!in_array(get_current_blog_id(), $microsite_menu)) {
	// 	$areas = get_page_by_path('areas-we-serve');
	// 	if ($areas) {
	// 		$items = array(
	// 			'title'				=> 'Areas We Serve',
	// 			'menu_item_parent'	=> $directionId,
	// 			'ID'				=> $areas->ID,
	// 			'db_id'				=> rand ( 10000 , 99999 ),
	// 			'url'				=> get_permalink($areas->ID)
	// 		);
	// 		$sorted_menu_items[] = (object) $items;
	// 	}
	// }
	
    return $sorted_menu_items;
    switch_to_blog(1);
};


function Get_Slug($url){
	$slug =  untrailingslashit (
				str_replace(network_site_url(), "", $url)
			 );

	return $slug;
 }


 function has_financing_option(){
 	$today = new \DateTime();
 	$today = $today->format('m/d/Y');

 	$options = get_posts(['post_type'=>'financial-options', 'post_status'=>'any', 'posts_per_page'=>-1]);

 	if($options){
 		foreach ( $options as $o ){
 			$metas = get_post_meta($o->ID, 'options', true);
 			if(!empty( $metas['show']) &&  $metas['show'] == 1 && strtotime($metas['expiration']) >= strtotime($today)){
 				return true;
 			}
 		}
 	}

 	return false;
 }


 function has_special_offers(){
 	$offers = get_posts(['post_type'=>'offers', 'post_status'=>'publish', 'posts_per_page'=>-1]);
 	if($offers){
 		foreach ($offers as $offer) {
 			$expiration = get_post_meta($offer->ID, 'offers_expiration', true);
 			$date_now = date("Y/m/d");

 			if( strtotime( $expiration ) >= strtotime( $date_now ) ):
 				return true;
 			endif;
 		}
 	}
 	return false;
 }

 function AddToMenu($details){


 }
 /*
 function getMicrositeExtraFields($microsite_id){
 	global $wpdb;
 	$sql = "SELECT microsite_fields FROM sfl_microsites WHERE microsite_blog_id  = $microsite_id";
 	$results = $wpdb->get_results($sql);
 	if(count($results) > 0){
 		return maybe_unserialize($results[0]->microsite_fields);
 	}
 }
*/
