<?php
namespace Surepress\Functions\Videos;

use Surepress\Functions\Common as Common;


function header_scripts () {

	if( get_post_type() == 'videos' ) {
		?>
			<script> 
				var yt_video_data = <?php echo json_encode( get() ); ?>;
			</script>
		<?php	
	}
}

function get() {
	wp_reset_postdata();

	$data = array();
	$videos_main = array();
	$videos_microsite = array();

	// get main site videos
	switch_to_blog(1);
	$args = get_posts([
		'post_type' => 'videos',
		'post_status' => 'publish',
		'numberposts' => -1,
		'orderby'  => 'date',
		'order'    => 'DESC'
	]);

	$main_arrs = new \WP_Query($args);

	if(!empty($main_arrs->query)) {
		foreach ($main_arrs->query as $r) {
			$videos_main[$r->post_date] = get_field('youtube_id', $r->ID);
		}		
	}
	restore_current_blog();

	// get microsite videos
	if( !Common\is_main() ) {
		$args = get_posts([
			'post_type' => 'videos',
			'post_status' => 'publish',
			'numberposts' => -1,
			'orderby'  => 'date',
			'order'    => 'DESC'
		]);
			
		$microsite_arrs = new \WP_Query($args);

		if(!empty($microsite_arrs->query)) {
			foreach ($microsite_arrs->query as $m) {
				$videos_microsite[$m->post_date] = get_field('youtube_id', $m->ID);
			}		
		}		

		$data = array_merge($videos_main, $videos_microsite);		
	} else {
		$data = $videos_main;
	}

	krsort($data);
	return $data;
}


function content() {
	global $wp_query;
	$content = array();
	$fields = maybe_unserialize($wp_query->nap->microsite_fields);

	if( !Common\is_main() && isset($fields['video_page_content']) && $fields['video_page_content'] !== "" ) {
		$content = $fields;	
	} else {
		global $wpdb;
	  	$global = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE microsite_blog_id = '1'");
		$content = maybe_unserialize($global->microsite_fields);
	}

	return $content;

}