<?php
	use Surepress\Functions\Locations as Locations; 
	use Surepress\Functions\Common as Common;

add_filter("gform_confirmation", "confirm_change", 10, 4);
function confirm_change($confirmation, $form, $lead, $ajax){

	if($form['title'] == 'Reviews Form'){
		$url = '?submission=success';
		 $confirmation = array('redirect' => $url);
	}

	 if( $form['title'] == 'Start A Conversation - Campaign' || $form['title'] == 'Start A Conversation Popup - Campaign' ){
		 $redirect = get_post_meta( get_the_ID(), 'campaign_thank_you_id');
		 
		 if( isset( $redirect[0] ) && !empty($redirect[0]) ){
			switch_to_blog(get_current_blog_id());
			$confirmation = array('redirect' => get_permalink($redirect[0]));
			restore_current_blog();
		 }
	}

	return $confirmation;
    
}

add_action('gform_after_submission', 'download_pdf_campaign', 10, 2);
function download_pdf_campaign($entry, $form) {
	if ($form['title'] == 'Download Remodeling Guide') {
		//echo '<script> document.getElementById("dlremodel").click(); </script>';
		$pdf_url = 'https://www.dreammaker-remodel.com/wp-content/uploads/pdf/5-Common-Remodeling-Mistakes-Home-Remodeling-Guide-Final-2020.pdf';
    	echo "<script>window.onload=function(){const a=document.createElement('a');a.href='{$pdf_url}';a.download='';document.body.appendChild(a);a.click();document.body.removeChild(a);};</script>";
	}
	
	$zip = null;
	switch ($form['title']) {
		case "Start A Conversation Popup":
      		$zip = rgar($entry, '12');
			break;
		case "Contact Us":
			$zip = rgar($entry, '8');
			break;
		case "Download Remodeling Guide":
		case "Events":
		case "Talk to Us Today About Your Remodeling Dreams":
		case "Perfect Kitchen and Bathroom":
			$zip = rgar($entry, '6');
			break;
		case "Job Opportunity":
		case "Start A Conversation":
			$zip = rgar($entry, '14');
			break;
    default:
      $zip = null;
	}

	if(!empty($zip)){
		$microsite_id = Common\sfl_get_nearest_microsite($zip);
		
		if($microsite_id !== get_current_blog_id()){
			switch_to_blog($microsite_id);
				$form_id = RGFormsModel::get_form_id($form['title']);
				unset($entry['id']);
				$entry['form_id'] = $form_id;
				$result = GFAPI::add_entry( $entry );
			restore_current_blog();
		}

		if(isset($_GET['test-location'])){
			switch_to_blog($microsite_id);
				echo 'Form Title: '. $form['title'] .'<br>';
				echo 'Microsite ID: '. $microsite_id .'<br>';
				echo 'Location Sent: '. get_bloginfo('name') .'<br>';
				echo 'Form ID: '. $form_id .'<br>';
			restore_current_blog();
			exit;
		}
	}
}


add_filter( 'gform_pre_send_email', 'set_from_email', 10, 4 );
function set_from_email( $email, $message_format, $notification, $entry ) {
	global $wp_query; 
	$email['headers']['From'] = 'From: "noreply@formleads.surepulse.com" <noreply@formleads.surepulse.com>';

  $zip = null;
  $location = null;
  $microsite_form = null;
  $form = GFAPI::get_form( $entry['form_id'] );
	switch ($form['title']) {
		case "Start A Conversation Popup":
      $zip = rgar($entry, '12');
			break;
		case "Contact Us":
			$zip = rgar($entry, '8');
			break;
		case "Download Remodeling Guide":
		case "Events":
		case "Talk to Us Today About Your Remodeling Dreams":
		case "Perfect Kitchen and Bathroom":
			$zip = rgar($entry, '6');
			break;
		case "Job Opportunity":
		case "Start A Conversation":
			$zip = rgar($entry, '14');
			break;
    default:
      $zip = null;
	}
  
	if(!empty($zip)){
		$microsite_id = Common\sfl_get_nearest_microsite($zip);
		switch_to_blog($microsite_id);
			$form_id = RGFormsModel::get_form_id($form['title']);
			$microsite_form = GFAPI::get_form( $form_id );

			$email['to'] = get_bloginfo('admin_email');
			foreach($microsite_form['notifications'] as $notif){
				if($notif['name'] == 'Admin Notification'){
					$email['to'] = str_replace( '{admin_email}', get_bloginfo('admin_email'), $notif['to'] );
				}
			}

			$location = get_bloginfo('name');
		restore_current_blog();

		if($microsite_id == 60){
			$email['to'] .= ',sherry@dreammakeraiken.com';
		}

		// West Collin County
		if($microsite_id == 96){
			$email['to'] .= ',erica@dmbkwcc.com,igmarf@surefirelocal.com';
		}

		// Chester County
		if($microsite_id == 131){
			$email['to'] .= ',maureen@surefirelocal.com,benrandiherndon@gmail.com,igmarf@surefirelocal.com';
		}

		// Hollywood
		if($microsite_id == 92){
			$email['to'] .= ',maureen@surefirelocal.com,igmarf@surefirelocal.com, neil@dreammakerhollywoodfl.com, gianb@surefirelocal.com';
		}
	}

	$cc_email = unserialize($wp_query->nap->microsite_fields)['microsite_cc_email'];
	if(!empty($cc_email)){
		$email['to'] .= ',' . $cc_email;
	}

	if(get_current_blog_id() == 60){
		$email['to'] .= ',sherry@dreammakeraiken.com';
	}
	// West Collin County
	if(get_current_blog_id() == 96){
		$email['to'] .= ',erica@dmbkwcc.com,igmarf@surefirelocal.com';
	}
	// Chester County
	if(get_current_blog_id() == 131){
		$email['to'] .= ',maureen@surefirelocal.com,benrandiherndon@gmail.com,igmarf@surefirelocal.com';
	}
	$email['to'] .= ", marketing@dreammakerbk.com";
	$email['message'] .= "<p> Location: ".( empty($location) ? "Corporate Site" : $location." (Microsite)" )." </p> ";

	if(isset($_GET['test-email'])){
		$email['abort_email'] = true;
		echo 'Zip: '. $zip .'<br>';
		echo 'Location Sent: '. $location .'<br>';
		echo 'Email to Send: '. $email['to'];
		exit;
	}

	return $email;
}

//add_filter( 'wpseo_sitemap_exclude_post_type', 'sitemap_exclude_post_type', 10, 2 );

function sitemap_exclude_post_type( $value, $post_type ) {
	global $wp_query;

	$post_type_to_exclude = array();
	if( !isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_events']) || maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_events'] != 1) :
		$post_type_to_exclude[] = 'events';
	endif;

	if( !isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_case_studies']) || maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_case_studies'] != 1) :
		$post_type_to_exclude[] = 'case-studies';
	endif;

	if( !isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_special_offers']) || maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_special_offers'] != 1) :
		$post_type_to_exclude[] = 'offers';
	endif;

	// if( !isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_financing']) || maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_financing'] != 1) :
	// 	$post_type_to_exclude[] = 'financing';
	// endif;
	if( in_array( $post_type, $post_type_to_exclude ) ) 
	return true;
}

add_filter( 'wpseo_exclude_from_sitemap_by_post_ids', 'sitemap_exclude_page', 10, 2 );

function sitemap_exclude_page( ) {
	global $wp_query;
	if( !isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_financing']) || maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_financing'] != 1) :
	 	$post_id = get_page_by_path('financing-options');
		 if(!empty($post_id))
		 	return $post_id->ID;
	endif;
}

add_filter('wpseo_enable_xml_sitemap_transient_caching', '__return_false');

function hide_menu_items( $items, $args ) {
	global $wp_query;

	$arr = array( 
			'microsite_show_events' 		=> 'Events',
			'microsite_show_case_studies' 	=> 'Case Studies',
			'microsite_show_special_offers'	=> 'Special Offers',
			'microsite_show_financing'		=> 'Financing Options',
		);

	foreach ($arr as $k => $val) {

		if( !isset(maybe_unserialize($wp_query->nap->microsite_fields)[$k]) || maybe_unserialize($wp_query->nap->microsite_fields)[$k] != 1) :

			foreach ($items as $key => $value) {
				if($value->title === $val):
					unset($items[$key]);
				endif;
			}

		endif;

	}

	return $items;
}

add_filter('wp_nav_menu_objects', 'hide_menu_items', 10, 2);

add_action( 'wp', 'force_404_events' );

function force_404_events() {

	if(!is_post_type_archive('events')) return;
	
	if ( have_posts() ) :
		$args = ['post_type' => 'events', 'posts_per_page' => '-1' ];
		$events = get_posts( $args );
		$date_now = date("Y-m-d"); 
		$valid = false;

		foreach($events as $event):
			$sched = get_field( 'schedule', $event->ID );

			for($i = 0; $i < $sched; $i++):
				$date = get_field( 'schedule_'.$i.'_date', $event->ID );
				
				if(strtotime($date) >= strtotime($date_now)):
					$valid = true;
				endif;
			endfor;
		endforeach;

		if($valid == false):
			force_404();
		endif;
	else:
		force_404();
	endif;
}

function force_404(){
	header("Status: 404 Not Found");
	$GLOBALS['wp_query']->set_404();
	status_header(404);
	nocache_headers();
}

add_action( 'wp', 'force_404_financial_options' );

function force_404_financial_options(){
	if(!is_page('financing-options')) return;

	$args = ['post_type' => 'financial-options', 'post_status'=>'any', 'posts_per_page' => '-1' ];
	$financing = get_posts( $args );
	$date_now = date("Y-m-d"); 
	$valid = false;
	$corporate_financing = get_option('corporate_financing');

	foreach( $financing as $finance):
		$sched = get_post_meta($finance->ID, 'options', true);

		if(strtotime($sched['expiration']) >= strtotime($date_now)):
			$valid = true;
		endif;
	endforeach;

	if(isset($corporate_financing) && !empty($corporate_financing)):
		switch_to_blog(1);
			$corporate_args = ['post_type' => 'financial-options', 'post__in' => $corporate_financing, 'post_status'=>'any', 'posts_per_page' => '-1' ];
			$corporate_posts = get_posts($corporate_args);
			foreach( $corporate_posts as $key => $value):
				$corporate_sched = get_post_meta($value->ID, 'options', true);

				if(strtotime($corporate_sched['expiration']) >= strtotime($date_now)):
					$valid = true;
				endif;
			endforeach;
		restore_current_blog();
	endif;

	if($valid == false):
		force_404();
	endif;
}

function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' )
    )
  );
}

add_action( 'init', 'register_my_menus' );