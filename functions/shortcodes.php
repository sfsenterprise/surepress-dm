<?php 

namespace Surepress\Functions\Shortcode;

/**
* Register here all shortcodes in this theme
**/
class Shortcodes {

	static $instance;
	public $microsite;

	public function __construct()
	{

		self::$instance =& $this;

		$this->microsite = $this->get_microsite();

		if( is_array($this->microsite) && ! empty($this->microsite) ){
			$this->microsite->microsite_address = '';
			
			foreach ($this->microsite as $field => $value) {
				add_shortcode($field, array($this, $field));			
			}
		}
		
	}

	public function get_microsite()
	{
		global $wpdb;

		$microsite = $wpdb->get_results( "SELECT * FROM sfl_microsites WHERE microsite_blog_id = " . get_current_blog_id(), OBJECT );

		return isset($microsite[0]) ? $microsite[0]:'';
	}

	public function microsite_tae($atts)
	{
		return "MALAKING TAE NI GIAN";
	}	

	public function microsite_phone($atts)
	{
		return $this->microsite->microsite_phone;
	}

	public function microsite_name($atts)
	{
		return $this->microsite->microsite_name;
	}

	public function microsite_admin_email($atts)
	{
		return $this->microsite->microsite_admin_email?:get_bloginfo('admin_email');
	}

	public function microsite_street($atts)
	{
		return $this->microsite->microsite_street;
	}

	public function microsite_city($atts)
	{
		return $this->microsite->microsite_city;
	}

	public function microsite_state_province($atts)
	{
		return $this->microsite->microsite_state_province;
	}

	public function microsite_zip_postal($atts)
	{
		return $this->microsite->microsite_zip_postal;
	}

	public function microsite_address()
	{
		return $this->microsite->microsite_street . ', ' . $this->microsite->microsite_city . ', ' . $this->microsite->microsite_state_province . ' ' . $this->microsite->microsite_zip_postal;
	}

	public function microsite_lat($atts)
	{
		return $this->microsite->microsite_lat;
	}

	public function microsite_lng($atts)
	{
		return $this->microsite->microsite_lng;
	}

	public function microsite_gtm_ga($atts)
	{
		return $this->microsite->microsite_gtm_ga;
	}

	public function microsite_owner($atts)
	{
		return $this->microsite->microsite_owner;
	}

	public function microsite_fields($atts)
	{
		$microsite_fields = maybe_unserialize($this->microsite->microsite_fields);

		$field = $atts['field'];

		return $microsite_fields[$field];
	}

	public static function &get_instance()
    {
        if ( ! isset( self::$instance ) )
            self::$instance = new self;

        return self::$instance;
    }

}
Shortcodes::get_instance();