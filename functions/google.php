<?php
namespace Surepress\Functions\Google;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Microsites\Microsite as Microsite; 

function json_ld(){
    global $wp_query;

    $social = namespace\GetSocialMedia();  

    $name = ( Common\is_main() ) ? "DreamMaker Bath & Kitchen" :  "DreamMaker of " . $wp_query->nap->microsite_name ;

    //if($wp_query->nap->microsite_blog_id == 63){
        $name = "DreamMaker Bath & Kitchen " . $wp_query->nap->microsite_name ;
        $newhours = array();
        foreach( get_blog_option($wp_query->nap->microsite_blog_id, "operating_hours") as $day => $hours ){
            $operatinghours = ucwords($day) . " " . date('g:iA', strtotime($hours['opening'])).' - '.date('h:iA', strtotime($hours['closing']));
            if(!array_key_exists("closed", $hours) ){
                array_push($newhours, $operatinghours);
            }
        }
    //}

    //This is the common format for json_ld
    $data = array(
        "@context" => "http://schema.org",
        "@type" => "HomeAndConstructionBusiness",
        "name" => $name,
        "Description" => $name.": Enhancing Lives, Improving Homes.",
        "url" =>  get_site_url(),
        "image" => get_site_url().'/wp-content/themes/surepress-dm/dist/images/main-logo-v1.png',
        "priceRange" => "Get a free quote",
        "Address" => array(
                        array(
                                "@type" => "PostalAddress",
                                "addressCountry" => "United States",                
                                "addressLocality" => $wp_query->nap->microsite_city,
                                "addressRegion" => $wp_query->nap->microsite_state_province,
                                "postalCode" => $wp_query->nap->microsite_zip_postal,
                                "streetAddress" => $wp_query->nap->microsite_street,
                                "telephone" => $wp_query->nap->microsite_phone
                           )
                        ),
        "openingHours" => $newhours
     );

    //For microsites.. lets insert areas we serve in the $data array
    if( !Common\is_main() ) {
        $fields = !empty($wp_query->nap->microsite_fields) ? unserialize($wp_query->nap->microsite_fields) : "";
        $service_areas = !empty($fields) ? $fields["area_served_1"] : "" ; 

        if( $service_areas || $service_areas != "") {
            $data["areaServed"] = array(
                "@type" => "Place",
                "containsPlace" => array(
                    "@type" => "Place", 
                    "address" => array(
                        "@type" => "PostalAddress",
                        "addressCountry" => "United States",
                        "addressRegion" => $wp_query->nap->microsite_state_province,
                        "addressLocality" => array($service_areas)
                    )                
                 )
            );
        }    
    }


    //insert social media url in $data if its not empty
    if ( !empty($social) ) {
        $data["sameAs"] = $social;
    }


    //unset $data due to blog schema uses different microsite format
    //dirty solution.. revise this! --J
    if ( (!is_front_page() && is_home()) || (is_single() && get_post_type() == 'post' ) ) {
        // Blog PAges NOT FINAL
            $data =  array(
                 "@context" => "http://schema.org",
                 "name" => $name,
                 "@type" => "Blog",
                 "description" => "Visit our resource section for remodeling tips, ideas and inspiration from the experts at DreamMaker Bath & Kitchen. For related inquiries, feel free to reach us through our contact form or call (800) 583-2133",
                 "author" => "DreamMaker Bath & Kitchen",      
                 "Publisher" => "DreamMaker Bath & Kitchen",
                 "url" => get_site_url().'/blog/'
            );
    }

    if(get_post_type() == 'events' ){
        
        global $post;
        $myposts = get_posts(  array('post_type' => 'events') );

        $evs = [];
        foreach( $myposts as $post ) : 
       
            $location_full = explode(",",get_field("event_location"));


           $temp["@type"] = "Event";
           $temp["name"] = get_the_title();
           $temp["startDate"] = "";
           $temp["location"]["@type"] = "Place";
           $temp["location"]["address"]["@type"] = "PostalAddress";
           // $temp["location"]["address"]["streetAddress"] = $location_full[0];
           // $temp["location"]["address"]["addressLocality"] = $location_full[1];
           // $temp["location"]["address"]["postalCode"] = $location_full[2];
           // $temp["location"]["address"]["addressRegion"] = $location_full[3];
           // $temp["location"]["address"]["addressCountry"] = $location_full[4];
           $temp["image"] = [];
           $temp["description"] = "";
           $temp["endDate"] = "";
           array_push($evs,$temp );
        endforeach; 
        wp_reset_postdata();

          $data = array(
            "@context" => "http://schema.org",
            "@graph" => $evs
        );
    }


    return json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT );
}   


function GetSocialMedia(){

    $social = array();
    if( isset( Microsite::social()['social'] ) ){
        foreach( Microsite::social()['social'] as $key => $value ){
            if($value){
                array_push($social, $value);
            } 
        }
    } 

    return $social;

}