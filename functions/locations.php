<?php
namespace Surepress\Functions\Locations;
use Surepress\Functions\Locations as Locations;

function archive( $wp_query ) {

  global $wp_the_query, $wpdb, $post;	
  
  if (isset($wp_query->query['pagename']) != "locations") return;
  
  if ( !is_admin() && $wp_query->is_main_query() && $wp_the_query === $wp_query ) :

          $misc_select = "";
          $misc_where = "";

          if( isset($_GET['search']) ){


                $search_term = trim($_GET['search']);     
              
                if( is_numeric($search_term) || $search_term === "my_location"  ){
                    
                      if( $search_term === "my_location" && isset($_GET['lat']) && isset($_GET['lng']) ){
                          //if "use my location" button.. do approximate zip search    
                          $search_lat = trim($_GET['lat']);
                          $search_lng = trim($_GET['lng']);  

                          $formula = Locations\get_distance($search_lat, $search_lng);                                          
                            $misc_select = ", {$formula} AS Nap_Distance";                          
                            $misc_where = " AND ( {$formula} <= 300 )";

                      } else {
                            $r = Locations\get_latlng($search_term);

                            $search_lat = $r['results'][0]['geometry']['location']['lat'];
                            $search_lng = $r['results'][0]['geometry']['location']['lng'];
                            
                            $formula = Locations\get_distance($search_lat, $search_lng); 

                            //if VALID zip code was entered ... do approximate zip search 
                            $result = $wpdb->get_row( "SELECT microsite_zip_postal, microsite_lat, microsite_lng FROM sfl_microsites WHERE microsite_zip_postal = {$search_term} OR microsite_fields LIKE '%{$search_term}%'", OBJECT );

                            if($result){
                              if($result->microsite_lng !== "0.000000" && $result->microsite_lat !== "0.000000"){
                                $misc_select = ", {$formula} AS Nap_Distance";
                              }
                              $misc_where = $result->microsite_zip_postal == $search_term ? " AND NAP.microsite_zip_postal = '{$search_term}'" : " AND NAP.microsite_fields LIKE '%{$search_term}%'";
                            }
                            else{
                              //using google map api 

                              $misc_select = ", {$formula} AS Nap_Distance";
                              $misc_where = " AND ( {$formula} <= 300 OR NAP.microsite_fields LIKE '%{$search_term}%' )";
                            }
                      }

              } else {
                $search_term = "%".str_replace( array(" ", "+"), array("%", "%"), $search_term)."%";                
                $misc_where = " AND NAP.microsite_state_province LIKE '{$search_term}'";
              }

         }// $wp_query->is_search()

          $custom_query = "SELECT 
                                NAP.microsite_blog_id AS Nap_Blog,      
                                NAP.microsite_name AS Nap_Name,
                                NAP.microsite_admin_email AS Nap_Email,
                                NAP.microsite_street AS Nap_Street,
                                NAP.microsite_street2 AS Nap_Street2,
                                NAP.microsite_city AS Nap_City,
                                NAP.microsite_state_province AS Nap_State,
                                NAP.microsite_slug AS Nap_slug,
                                NAP.microsite_zip_postal AS Nap_Zip,
                                NAP.microsite_phone AS Nap_Phone,
                                NAP.microsite_lat AS Nap_lat,
                                NAP.microsite_lng AS Nap_lng,
                                NAP.microsite_fields AS Nap_fields
                                {$misc_select}

                              FROM sfl_microsites AS NAP
                                      LEFT JOIN wp_blogs AS SITE ON SITE.blog_id = NAP.microsite_blog_id
                              
                              WHERE 
                                      NAP.microsite_blog_id NOT IN (1,4,59)
                                        AND SITE.public = '1' 
                                        AND SITE.archived != '1' 
                                        AND SITE.deleted != '1'
                                        {$misc_where}

                              ORDER BY Nap_State ASC, Nap_Name ASC";


                             
                              
          $all_nap = $wpdb->get_results( $custom_query, OBJECT );
          
          //print_r($all_nap); exit;  /* DEBUG */
          //echo $custom_query; exit; /* DEBUG */

          $state_nap = array();
          foreach($all_nap as $s){
           
            /****FILTER OUT MICROSITES WITH RADIUS LIMIT*****/
            $fields = ($s->Nap_fields) ? unserialize($s->Nap_fields) : array();

            $max_radius = ( array_key_exists('microsite_search_radius', $fields) ) ? (float) $fields['microsite_search_radius'] : 50;  
            $mongoose_phone = ( array_key_exists('mongoose_phone', $fields) ) ? $fields['mongoose_phone'] : '';
            $distance = isset($s->Nap_Distance) ? (float) round( $s->Nap_Distance, 2)  : null; 
            if( $distance >  $max_radius ) continue;           
            /****FILTER end*****/
           
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Name'] = $s->Nap_Name;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Email'] = $s->Nap_Email;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Street'] = $s->Nap_Street;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Street2'] = $s->Nap_Street2;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_City'] = $s->Nap_City;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_State'] = $s->Nap_State;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_slug'] = $s->Nap_slug;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Zip'] = $s->Nap_Zip;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Phone'] = $s->Nap_Phone;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Mongoose_Phone'] = $mongoose_phone;

            /***********************************************************************************************
              FOR MICROSITES WHO DONT WANT TO PUT THEIR STREET (THAT'S WHY WE DON'T HAVE THEIR COORDINATES)
              WE WILL USE GOOGLE TO OBTAIN THEIR LAT/LNG
            ***********************************************************************************************/          
            if( $s->Nap_lat === "0.000000" && $s->Nap_lng === "0.000000"){
              
              $coords = get_latlng($s->Nap_Zip); 
              $latitude = $coords['results'][0]['geometry']['location']['lat'];
              $longtitude = $coords['results'][0]['geometry']['location']['lng'];              
              
            } else {
              $latitude = $s->Nap_lat;
              $longtitude = $s->Nap_lng;
            }


            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_lat'] = $latitude;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_lng'] = $longtitude;

            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_fields'] = $s->Nap_fields;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Distance'] = $distance;
            $state_nap[$s->Nap_State][$s->Nap_City][$s->Nap_Blog]['Nap_Radius'] = $max_radius;

          }
          //print_r( $state_nap ); exit;
          //Set wp_query vars
          $wp_query->locations_nap = $state_nap;        
          $wp_query->locations_states = array_keys($state_nap); 

          $wp_query->locations_states_list_divider = ceil( count($wp_query->locations_states) / 3) ; 

  endif;

  return $wp_query;
}


function get_states( $query ) {

    global $wp_the_query, $wp_query, $wpdb, $location_nap; 

    if ( !is_admin() && $query->is_main_query() && $wp_the_query === $query && is_post_type_archive('location') ) :
        
        $query->set( 'posts_per_page' , -1 );
        $query->set( 'orderby' , 'title' );
        $query->set( 'order' , 'ASC' );
        $query->set('post__in', $wp_query->locations_states);

        if( $query->is_search() ){
          $query->set('found_posts', array_sum(array_map("count", $wp_query->locations_nap)) );
        }

    endif;
}    

function active_states(){
  /****************************************************************
  this should be optimized to due fuction archive() is running the 
  same query less the "where" (for search) clause ---------- J
  *****************************************************************/
  global $wpdb;

  $custom_query =     "SELECT 
                              NAP.microsite_blog_id AS Nap_Blog,      
                              NAP.microsite_city AS Nap_City,
                              NAP.microsite_state_province AS Nap_State       

                      FROM sfl_microsites AS NAP
                              LEFT JOIN {$wpdb->prefix}blogs AS SITE ON SITE.blog_id = NAP.microsite_blog_id

                      WHERE 
                          NAP.microsite_blog_id NOT IN (1,4,59)
                            AND SITE.public = '1' 
                            AND SITE.archived != '1' 
                            AND SITE.deleted != '1'

                      GROUP BY Nap_State          
                      ORDER BY Nap_State ASC";

                  
  $active_states = $wpdb->get_results( $custom_query, OBJECT );  
  return $active_states;
}


function get_distance($lat, $lng){
    /*This is the formula in getting the Radius using lat/lng info */                 
    //DON NOT EDIT THIS :: THIS IS AN EXACT SCIENCE BASED ON https://en.wikipedia.org/wiki/Haversine_formula  
    $distance = "( 3958*3.1415926*sqrt((NAP.microsite_lat -'{$lat}') * (NAP.microsite_lat -'{$lat}') +  cos(NAP.microsite_lat/57.29578)*cos('{$lat}'/57.29578)*(NAP.microsite_lng-'{$lng}')*(NAP.microsite_lng - '{$lng}')) /180)";
    return $distance;
}

function get_latlng($search_term){
      $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$search_term}&key=AIzaSyDAZzkjfo5i1kHQUok7i7jsTsceTGlWyN8";
      $response_json = file_get_contents($url);
      $r = json_decode($response_json, true); 

      if( empty($r['results'])){ //fallback if the above api returns zero result
          $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$search_term},US&key=AIzaSyDAZzkjfo5i1kHQUok7i7jsTsceTGlWyN8";
          $response_json = file_get_contents($url);
          $r = json_decode($response_json, true);                          
      }  
      return $r;
}

// Count the sub array of locations_nap

function nap_count($arr, $depth=2) { 
  if (!is_array($arr) || !$depth) return 0; 
           
    $result_nap = count($arr);
    
    foreach ($arr as $nap_city){
      $result_nap+=nap_count($nap_city, $depth-1); 
    } 
        
    return $result_nap;
}