<?php

  register_post_type( 'videos',
    array(
      'labels' => array(
        'name' => __( 'Videos' ),
        'singular_name' => __( 'Video' ),
        'add_new_item' =>  __( 'Add Video' ),
		'edit_item' => __( 'Edit Video' ),
		'new_item' => __( 'New Video' ),
		'view_item' => __( 'View Video' ),
		'view_items' => __( 'View Videos' ),
		'search_items' => __( 'Search Video' ),
		'not_found' => __( 'No Videos found' ),
		'not_found_in_trash' => __( 'No Video found in trash' ),
		'all_items' => __( 'All Videos' ),
		'archives' => __( 'Videos Archive' ),
		'attributes' => __( 'Video Attributes' ),
		'insert_into_item' => __( 'Insert in Video' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Video' ),
		'menu_name'  => __( 'Videos' ), //wp-admin sidebar label
  		),

		'public' => true,
		'has_archive' => 'helpful-videos',
		'exclude_from_search' => true,
		// 'publicly_queryable' => false,

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_video', 
			'read_post'          => 'read_video', 
			'delete_post'        => 'delete_video', 
            'delete_posts'       => 'delete_videos',
			'edit_posts'         => 'edit_videos', 
			'edit_others_posts'  => 'edit_others_videos', 
			'publish_posts'      => 'publish_videos',       
			'read_private_posts' => 'read_private_videos', 
			'create_posts'       => 'edit_videos', 
		),
		'rewrite' => array(
	      'slug' => 'videos',
	      'with_front' => false,
        ),
		'supports' => array(
			'title',
			// 'editor',
			// 'thumbnail',
			'page-attributes'
		),
		'hierarchical' => true,
		// 'taxonomies' => array('faqs-category')
    )
  );
