<?php

  register_post_type( 'case-studies',
    array(
      'labels' => array(
        'name' => __( 'Remodeling Case Studies' ),
        'singular_name' => __( 'Case Studies' ),
        'add_new_item' =>  __( 'Add Case' ),
		'edit_item' => __( 'Edit Case' ),
		'new_item' => __( 'New Case' ),
		'view_item' => __( 'View Case' ),
		'view_items' => __( 'View Case' ),
		'search_items' => __( 'Search Case Studies' ),
		'not_found' => __( 'No Case Study found' ),
		'not_found_in_trash' => __( 'No Case Study found in trash' ),
		'all_items' => __( 'All Case Studies' ),
		'archives' => __( 'Case Studies Archive' ),
		'attributes' => __( 'Case Attributes' ),
		'insert_into_item' => __( 'Insert in Case' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Case' ),
		//'featured_image' => __( 'Profile Photo' ),
		//'set_featured_image'  => __( 'Set Profile Photo' ),
		//'remove_featured_image'  => __( 'Remove Profile Photo' ),
		//'use_featured_image'  => __( 'Use as Profile Photo' ),
		'menu_name'  => __( 'Case Studies' ), //wp-admin sidebar label
  		),

      	'taxonomies' => array('post_tag, case_category'),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,
		'rewrite' => array(
						'slug' => 'about/case-studies',
						'with_front' => false,
		             ),			

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_case', 
			'read_post'          => 'read_case', 
			'delete_post'        => 'delete_case', 
			'delete_posts'       => 'delete_cases',
			'edit_posts'         => 'edit_cases', 
			'edit_others_posts'  => 'edit_others_cases', 
			'publish_posts'      => 'publish_cases',       
			'read_private_posts' => 'read_private_cases', 
			'create_posts'       => 'edit_cases', 
		),
     

		'supports' => array(
			'title',
			'editor',
			//'thumbnail',
			//'custom-fields',	
			'page-attributes',
			'excerpt'
		)

    )
  );
