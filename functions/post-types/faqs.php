<?php

  register_post_type( 'faqs',
    array(
      'labels' => array(
        'name' => __( 'Frequently Asked Questions' ),
        'singular_name' => __( 'FAQ' ),
        'add_new_item' =>  __( 'Add FAQ' ),
		'edit_item' => __( 'Edit FAQ' ),
		'new_item' => __( 'New FAQ' ),
		'view_item' => __( 'View FAQ' ),
		'view_items' => __( 'View FAQs' ),
		'search_items' => __( 'Search FAQ' ),
		'not_found' => __( 'No FAQs found' ),
		'not_found_in_trash' => __( 'No FAQ found in trash' ),
		'all_items' => __( 'All FAQs' ),
		'archives' => __( 'FAQs Archive' ),
		'attributes' => __( 'FAQ Attributes' ),
		'insert_into_item' => __( 'Insert in FAQ' ),
		'uploaded_to_this_item' => __( 'Uploaded to this FAQ' ),
		'menu_name'  => __( 'FAQs' ), //wp-admin sidebar label
  		),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_faq', 
			'read_post'          => 'read_faq', 
			'delete_post'        => 'delete_faq', 
            'delete_posts'       => 'delete_faqs',
			'edit_posts'         => 'edit_faqs', 
			'edit_others_posts'  => 'edit_others_faqs', 
			'publish_posts'      => 'publish_faqs',       
			'read_private_posts' => 'read_private_faqs', 
			'create_posts'       => 'edit_faqs', 
		),
		'rewrite' => array(
	      'slug' => 'faq',
	      'with_front' => false,
        ),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'page-attributes'
		),
		'hierarchical' => true,
		'taxonomies' => array('faqs-category')
    )
  );
