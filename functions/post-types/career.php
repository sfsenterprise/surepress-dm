<?php

  // register_post_type( 'careers',
  //   array(
  //     'labels' => array(
  //       'name' => __( 'Careers' ),
  //       'singular_name' => __( 'Item' ),
  //       'add_new_item' =>  __( 'Add Item' ),
		// 'edit_item' => __( 'Edit Item' ),
		// 'new_item' => __( 'New Item' ),
		// 'view_item' => __( 'View Item' ),
		// 'view_items' => __( 'View Items' ),
		// 'search_items' => __( 'Search Item' ),
		// 'not_found' => __( 'No Items found' ),
		// 'not_found_in_trash' => __( 'No Item found in trash' ),
		// 'all_items' => __( 'All Items' ),
		// 'archives' => __( 'Items Archive' ),
		// 'menu_name'  => __( 'Careers' ), //wp-admin sidebar label
  // 		),

		// 'public' => true,
		// 'has_archive' => false,
		// 'exclude_from_search' => true,

		// 'show_ui' => true, //show of hide in wp-admin
		// 'menu_icon' => 'dashicons-clipboard',

		// 'capabilities' => array(
		// 	'edit_post'          => 'edit_career_item', 
		// 	'read_post'          => 'read_career_item', 
		// 	'delete_post'        => 'delete_career_item', 
  //           'delete_posts'       => 'delete_career_items',
		// 	'edit_posts'         => 'edit_career_items', 
		// 	'edit_others_posts'  => 'edit_others_career_items', 
		// 	'publish_posts'      => 'publish_career_items',       
		// 	'read_private_posts' => 'read_private_career_items', 
		// 	'create_posts'       => 'edit__items', 
		// ),
		// 'rewrite' => array(
	 //      //'slug' => '/',
	 //      'with_front' => false,
  //       ),
		// 'supports' => array(
		// 	'title',
		// 	'editor',
		// 	'thumbnail',
		// 	//'custom-fields',
		// 	//'page-attributes'
		// ),
		// 'hierarchical' => false,
		// 'taxonomies' => array('career-category')

  //   )
  // );
register_post_type( 'job-opportunities',
    array(
      'labels' => array(
        'name' => __( 'Careers' ),
        'singular_name' => __( 'Item' ),
        'add_new_item' =>  __( 'Add Item' ),
		'edit_item' => __( 'Edit Item' ),
		'new_item' => __( 'New Item' ),
		'view_item' => __( 'View Item' ),
		'view_items' => __( 'View Items' ),
		'search_items' => __( 'Search Item' ),
		'not_found' => __( 'No Items found' ),
		'not_found_in_trash' => __( 'No Item found in trash' ),
		'all_items' => __( 'All Items' ),
		'archives' => __( 'Items Archive' ),
		'menu_name'  => __( 'Careers' ), //wp-admin sidebar label
  		),

		'public' => true,
		'has_archive' => false,
		'exclude_from_search' => true,

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-clipboard',

		'capabilities' => array(
			'edit_post'          => 'edit_career_item', 
			'read_post'          => 'read_career_item', 
			'delete_post'        => 'delete_career_item', 
            'delete_posts'       => 'delete_career_items',
			'edit_posts'         => 'edit_career_items', 
			'edit_others_posts'  => 'edit_others_career_items', 
			'publish_posts'      => 'publish_career_items',       
			'read_private_posts' => 'read_private_career_items', 
			'create_posts'       => 'edit__items', 
		),
		'rewrite' => array(
	      //'slug' => '/',
	      'with_front' => false,
        ),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			//'custom-fields',
			//'page-attributes'
		),
		'hierarchical' => false,
		'taxonomies' => array('career-category')

    )
  );