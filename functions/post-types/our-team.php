<?php

  register_post_type( 'team',
    array(
      'labels' => array(
        'name' => __( 'Our Team' ),
        'singular_name' => __( 'Our Team' ),
        'add_new_item' =>  __( 'Add Team Member' ),
		'edit_item' => __( 'Edit Team Member' ),
		'new_item' => __( 'New Team Member' ),
		'view_item' => __( 'View Team Member' ),
		'view_items' => __( 'View Team Members' ),
		'search_items' => __( 'Search Team' ),
		'not_found' => __( 'No Team Members found' ),
		'not_found_in_trash' => __( 'No Team Member found in trash' ),
		'all_items' => __( 'All Team Members' ),
		'archives' => __( 'Our Team Archive' ),
		'attributes' => __( 'Team Member Attributes' ),
		'insert_into_item' => __( 'Insert in Team Member' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Team Member' ),
		'featured_image' => __( 'Profile Photo' ),
		'set_featured_image'  => __( 'Set Profile Photo' ),
		'remove_featured_image'  => __( 'Remove Profile Photo' ),
		'use_featured_image'  => __( 'Use as Profile Photo' ),
		'menu_name'  => __( 'Our Team' ), //wp-admin sidebar label
  		),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,
		'rewrite' => array(
						'slug' => 'about/our-team',
						'with_front' => false,
		             ),	

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_team', 
			'read_post'          => 'read_team', 
			'delete_post'        => 'delete_team', 
			'delete_posts'		 => 'delete_teams',
			'edit_posts'         => 'edit_teams', 
			'edit_others_posts'  => 'edit_others_teams', 
			'publish_posts'      => 'publish_teams',       
			'read_private_posts' => 'read_private_teams', 
			'create_posts'       => 'edit_teams', 
		),
     

		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			//'custom-fields',
			'page-attributes'
		)

    )
  );
