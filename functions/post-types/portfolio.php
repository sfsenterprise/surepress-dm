<?php

  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name' => __( 'Galleries' ),
        'singular_name' => __( 'Portfolio' ),
        'add_new_item' =>  __( 'Add Portfolio' ),
		'edit_item' => __( 'Edit Portfolio' ),
		'new_item' => __( 'New Portfolio' ),
		'view_item' => __( 'View Portfolio' ),
		'view_items' => __( 'View Portfolio' ),
		'search_items' => __( 'Search Portfolio' ),
		'not_found' => __( 'No Portfolio found' ),
		'not_found_in_trash' => __( 'No Portfolio found in trash' ),
		'all_items' => __( 'All Portfolios' ),
		'archives' => __( 'Our Portfolio Archive' ),
		'attributes' => __( 'Portfolio Attributes' ),
		'insert_into_item' => __( 'Insert in Portfolio' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Portfolio' ),
		'featured_image' => __( 'Gallery Cover' ),
		'set_featured_image'  => __( 'Set Gallery Cover' ),
		'remove_featured_image'  => __( 'Remove Gallery Cover' ),
		'use_featured_image'  => __( 'Use as Gallery Cover' ),
		'menu_name'  => __( 'Portfolio' ), //wp-admin sidebar label
  		),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,
		'rewrite' => array(
						'slug' => 'galleries',
						'with_front' => false,
		             ),	

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_portfolio', 
			'read_post'          => 'read_portfolio', 
			'delete_post'        => 'delete_portfolio', 
			'delete_posts'		 => 'delete_portfolios',
			'edit_posts'         => 'edit_portfolio', 
			'edit_others_posts'  => 'edit_others_portfolio', 
			'publish_posts'      => 'publish_portfolio',       
			'read_private_posts' => 'read_private_portfolio', 
			'create_posts'       => 'edit_portfolio', 
		),
     

		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			//'custom-fields',
			'page-attributes'
		)

    )
  );
