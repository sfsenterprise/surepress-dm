<?php

  register_post_type( 'glossary',
    array(
      'labels' => array(
        'name' => __( 'Glossary' ),
        'singular_name' => __( 'Item' ),
        'add_new_item' =>  __( 'Add Item' ),
		'edit_item' => __( 'Edit Item' ),
		'new_item' => __( 'New Item' ),
		'view_item' => __( 'View Item' ),
		'view_items' => __( 'View Items' ),
		'search_items' => __( 'Search Item' ),
		'not_found' => __( 'No Items found' ),
		'not_found_in_trash' => __( 'No Item found in trash' ),
		'all_items' => __( 'All Items' ),
		'archives' => __( 'Items Archive' ),
		'attributes' => __( 'Item Attributes' ),
		'menu_name'  => __( 'Glossary' ), //wp-admin sidebar label
  		),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_glossary_item', 
			'read_post'          => 'read_glossary_item', 
			'delete_post'        => 'delete_glossary_item', 
            'delete_posts'       => 'delete_glossary_items',
			'edit_posts'         => 'edit_glossary_items', 
			'edit_others_posts'  => 'edit_others_glossary_items', 
			'publish_posts'      => 'publish_glossary_items',       
			'read_private_posts' => 'read_private_glossary_items', 
			'create_posts'       => 'edit__items', 
		),
		'rewrite' => array(
	      //'slug' => 'slug',
	      'with_front' => false,
        ),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			//'custom-fields',
			'page-attributes'
		),
		'hierarchical' => true,
		'taxonomies' => array('glossary-category')

    )
  );
