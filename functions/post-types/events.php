<?php 
	register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'Event' ),
        'add_new_item' =>  __( 'Add Event' ),
		'edit_item' => __( 'Edit Event' ),
		'new_item' => __( 'New Event' ),
		'view_item' => __( 'View Event' ),
		'view_items' => __( 'View Events' ),
		'search_items' => __( 'Search Event' ),
		'not_found' => __( 'No Events found' ),
		'not_found_in_trash' => __( 'No Events found in trash' ),
		'all_items' => __( 'All Events' ),
		'archives' => __( 'Events Archive' ),
		'attributes' => __( 'Event Attributes' ),
		'insert_into_item' => __( 'Insert into Events' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Event' ),
		'featured_image' => __( 'Event Image' ),
		'set_featured_image'  => __( 'Set Event Image' ),
		'remove_featured_image'  => __( 'Remove Event Image' ),
		'use_featured_image'  => __( 'Use as Event Image' ),
		'menu_name'  => __( 'Events' ), 
  		),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,

		'show_ui' => true,
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_event', 
			'read_post'          => 'read_event', 
			'delete_post'        => 'delete_event',
			'delete_posts'       => 'delete_events', 
			'edit_posts'         => 'edit_events', 
			'edit_others_posts'  => 'edit_others_events', 
			'publish_posts'      => 'publish_events',       
			'read_private_posts' => 'read_private_events', 
			'create_posts'       => 'edit_events', 
		),
	    'rewrite' => array(
	      'slug' => 'about/events',
	      'with_front' => false,
        ),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			//'custom-fields',
			'page-attributes'
		)

    )
  );

?>