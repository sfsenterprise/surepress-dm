<?php 
	register_post_type( 'offers',
    array(
      'labels' => array(
        'name' => __( 'Special Offers' ),
        'singular_name' => __( 'Offer' ),
        'add_new_item' =>  __( 'Add Offer' ),
		'edit_item' => __( 'Edit Offer' ),
		'new_item' => __( 'New Offer' ),
		'view_item' => __( 'View Offer' ),
		'view_items' => __( 'View Offers' ),
		'search_items' => __( 'Search Offer' ),
		'not_found' => __( 'No Offers found' ),
		'not_found_in_trash' => __( 'No Offers found in trash' ),
		'all_items' => __( 'All Offers' ),
		'archives' => __( 'Offers Archive' ),
		'attributes' => __( 'Offer Attributes' ),
		'insert_into_item' => __( 'Insert into Offers' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Offer' ),
		'menu_name'  => __( 'Offers' ),
  		),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_offer', 
			'read_post'          => 'read_offer', 
			'delete_post'        => 'delete_offer',
			'delete_posts'		 => 'delete_offers', 
			'edit_posts'         => 'edit_offers', 
			'edit_others_posts'  => 'edit_others_offers', 
			'publish_posts'      => 'publish_offers',       
			'read_private_posts' => 'read_private_offers', 
			'create_posts'       => 'edit_offers', 
		),
	    'rewrite' => array(
	      'slug' => 'about/special-offers',
	      'with_front' => false,
        ),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			//'custom-fields',
			'page-attributes'
		)

    )
  );

?>