<?php

  register_post_type( 'services',
    array(
      'labels' => array(
        'name' => __( 'Services' ),
        'singular_name' => __( 'Services' ),
        'add_new_item' =>  __( 'Add Service' ),
		'edit_item' => __( 'Edit Service' ),
		'new_item' => __( 'New Service' ),
		'view_item' => __( 'View Service' ),
		'view_items' => __( 'View Services' ),
		'search_items' => __( 'Search Service' ),
		'not_found' => __( 'No Services found' ),
		'not_found_in_trash' => __( 'No Service found in trash' ),
		'all_items' => __( 'All Services' ),
		'archives' => __( 'Services Archive' ),
		'attributes' => __( 'Service Attributes' ),
		'insert_into_item' => __( 'Insert in Service' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Service' ),
		'featured_image' => __( 'Service Image' ),
		'set_featured_image'  => __( 'Set Service Image' ),
		'remove_featured_image'  => __( 'Remove Service Image' ),
		'use_featured_image'  => __( 'Use as Service Image' ),
		'menu_name'  => __( 'Services' ), //wp-admin sidebar label
  		),

		'public' => true,
		'has_archive' => true,
		'exclude_from_search' => true,

		'show_ui' => true, //show of hide in wp-admin
		'menu_icon' => 'dashicons-location',

		'capabilities' => array(
			'edit_post'          => 'edit_service', 
			'read_post'          => 'read_service', 
			'delete_post'        => 'delete_service', 
			'delete_posts'		 => 'delete_services',
			'edit_posts'         => 'edit_services', 
			'edit_others_posts'  => 'edit_others_services', 
			'publish_posts'      => 'publish_services',       
			'read_private_posts' => 'read_private_services', 
			'create_posts'       => 'edit_services', 
		),
		'rewrite' => array( /*'slug' => 'slug',*/ 'with_front' => false, ),
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			//'custom-fields',
			'page-attributes'
		),
		'hierarchical' => true,
		'taxonomies' => array('service-category')

    )
  );
