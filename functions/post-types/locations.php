<?php
/*
   register_post_type( 'location',
     array(
       'labels' => array(
         'name' => __( 'Locations' ),
         'singular_name' => __( 'Location' ),
         'add_new_item' =>  __( 'Add New' ),
         'edit_item' => __( 'Edit Location' ),
         'new_item' => __( 'New Location' ),
         'view_item' => __( 'View Location' ),
         'view_items' => __( 'View Locations' ),
         'search_items' => __( 'Search Locations' ),
         'not_found' => __( 'No Location found' ),
         'not_found_in_trash' => __( 'No Location found in trash' ),
         'all_items' => __( 'All Locations' ),
         'archives' => __( 'Locations Archive' ),
         'attributes' => __( 'Location Attributes' ),
         'insert_into_item' => __( 'Insert in Location' ),
         'uploaded_to_this_item' => __( 'Uploaded to this Location' ),
         'menu_name'  => __( 'Locations' ), //wp-admin sidebar label
         ),

        'public' => true,
        'has_archive' => true,
        'hierarchical'    => true,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'publicly_queryable'  => true,
        'query_var'           => false,         

         'show_ui' => false, //show of hide in wp-admin
         'menu_icon' => 'dashicons-location',

         'capabilities' => array(
             'edit_post'          => 'edit_location', 
             'read_post'          => 'read_location', 
             'delete_post'        => 'delete_location', 
             'delete_posts'       => 'delete_locations',
             'edit_posts'         => 'edit_locations', 
             'edit_others_posts'  => 'edit_others_locations', 
             'publish_posts'      => 'publish_locations',       
             'read_private_posts' => 'read_private_locations', 
             'create_posts'       => 'edit_locations', 
         ),

         'rewrite' => array( 'slug' => 'locations', 'with_front' => false, ),
     
         'supports' => array(
             'title',
             'editor',
             'thumbnail',
             'custom-fields',
             'page-attributes'
         )

     )
   );

*/   