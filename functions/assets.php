<?php
namespace Surepress\Functions\Assets;
use Surepress\Functions\Microsites;

if ( ! function_exists('asset_path') ) :
    function asset_path($asset) {
        if ( file_exists( $manifest = get_template_directory_uri() . '/dist/assets.json' ) ) {
            
            $manifest = json_decode(file_get_contents($manifest), true);

            return get_stylesheet_directory_uri() . '/dist/' . $manifest[$asset];
        }

        return get_template_directory_uri() . '/dist/' . $asset;
    }
endif;

/**
 * Theme assets
 */
function theme_assets() {
    //wp_enqueue_style('surepress-hmc/fonts', asset_path('styles/google-fonts.css'), false, null);

    if(!is_page('surefire')) {
        wp_enqueue_style('surepress-dm/css', asset_path('styles/main.css'), false, null);
        wp_enqueue_script('surepress-dm/js', asset_path('scripts/main.js'), array('jquery'), null, true);
    }
    
    wp_enqueue_script('surepress-dm-fonts', asset_path('scripts/google-fonts.js'), null, null, true);
    wp_localize_script( 'surepress-dm/js', 'ajax_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
    )); 
    
    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if( is_front_page() || is_singular('location') || is_archive('location') || is_page(array('directions', 'contact-us', 'locations') ) ){ 
          wp_enqueue_script('google-maps', '//maps.google.com/maps/api/js?key=AIzaSyDAZzkjfo5i1kHQUok7i7jsTsceTGlWyN8', ['jquery'], null, true);
    }

    if(is_page('reviews')){ 
        wp_enqueue_style('star-rating-css', get_template_directory_uri().'/vendor/rating/css/star-rating.min.css', false, null);
        wp_enqueue_style('krajee-fa-theme-css', get_template_directory_uri().'/vendor/rating/themes/krajee-fa/theme.min.css', false, null);
        wp_enqueue_script('star-rating-js', get_template_directory_uri().'/vendor/rating/js/star-rating.js', array('jquery'), null, true);
        wp_enqueue_script('krajee-fa-theme-js', get_template_directory_uri().'/vendor/rating/themes/krajee-fa/theme.min.js', array('jquery'), null, true);
    }

    // dynamically get and set Events form object
    $event_field = \Surepress\Functions\Microsites\Microsite::get_form('Events');

    wp_localize_script( 'surepress-dm/js', 'event_obj', [ 'event_field'=>$event_field ] );
    
    switch_to_blog(1);
    wp_localize_script( 'surepress-dm/js', 'ajax_object', [ 'ajaxurl' => admin_url( 'admin-ajax.php' ) ] );
    restore_current_blog();

    wp_enqueue_script( 'surepress-dm/js' );
}