<?php

	acf_add_local_field_group(array(
		'key' => 'group_offer',
		'title' => 'Popup Fields',
		'fields' => array (
			/*array (
				'key' => 'header_text',
				'name' => 'header_text',
				'type' => 'wysiwyg',
				'label' => 'Header Text'
			),*/
			array (
				'key' => 'offer_text',
				'name' => 'offer_text',
				'type' => 'text',
				'label' => 'Download Instructions'
			),
			array (
				'key' => 'offer_link',
				'name' => 'offer_link',
				'type' => 'file',
				'label' => 'Download File'
			),
			array (
				'key' => 'offers_expiration',
				'label' => 'Expiration',
				'type' => 'date_picker',
				'name' => 'offers_expiration'
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'offers',
				),
			),
		),
		'position' => 'normal'
	));
