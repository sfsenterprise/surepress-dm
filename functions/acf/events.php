<?php

acf_add_local_field_group(
	array(
		'key' => 'group_event_1',
		'title' => 'Event Info',
		'fields' => array (
			array (
				'key' => 'event_location',
				'label' => 'Location',
				'type' => 'text',
				'name' => 'event_location'
			),
			array(
				'key' => 'field_5c194504627f7',
				'label' => 'Schedule',
				'name' => 'schedule',
				'type' => 'repeater',
				'instructions' => 'Add Schedule',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => 'Add Schedule',
				'sub_fields' => array(
					array(
						'key' => 'field_5c194635dcd7a',
						'label' => 'Date',
						'name' => 'date',
						'type' => 'date_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'display_format' => 'F j, Y',
						'return_format' => 'd/m/Y',
						'first_day' => 1,
					),
					array(
						'key' => 'field_5c19470089805',
						'label' => 'Time',
						'name' => 'time',
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => 0,
						'max' => 0,
						'layout' => 'table',
						'button_label' => 'Add Time',
						'sub_fields' => array(
							array(
								'key' => 'field_5c19471989806',
								'label' => 'Start Time',
								'name' => 'start',
								'type' => 'time_picker',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'display_format' => 'g:i a',
								'return_format' => 'g:i a',
							),
							array(
								'key' => 'field_5c19473989807',
								'label' => 'End Time',
								'name' => 'end',
								'type' => 'time_picker',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array(
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'display_format' => 'g:i a',
								'return_format' => 'g:i a',
							),
						),
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'events',
				),
			),
		),
		'position' => 'acf_after_title'
	)
);