<?php

	acf_add_local_field_group(array(
		'key' => 'team',
		'title' => 'Details',
		'fields' => array (
			array (
				'key' => 'team_position',
				'name' => 'team_position',
				'type' => 'text',
				'label' => 'Position'
			),
			array (
				'key' => 'team_phone',
				'name' => 'team_phone',
				'type' => 'text',
				'label' => 'Phone'
			),
			array (
				'key' => 'team_email',
				'name' => 'team_email',
				'type' => 'email',
				'label' => 'Email'
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'team',
				),
			),
		),
		'position' => 'acf_after_title'
	));
