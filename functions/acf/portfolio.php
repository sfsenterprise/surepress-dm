<?php

///////////////////////////////////////////
// Gallery
//////////////////////////////////////////

	acf_add_local_field_group(array(
		'key' => 'gallery_after',
		'title' => 'Gallery (After)',
		'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'portfolio',
							),
						),
					),
		'fields' => array (
			array (
				'key' => 'portfolio_after',
				'type' => 'image',
				'name' => 'portfolio_after',
				'label' => 'This is the `latest/default` gallery output',
			)
		),		
		'position' => 'normal',
	));	

/* for gallery archive sorting */
		acf_add_local_field_group(array(
		'key' => 'category_sort',
		'title' => '',

		'fields' => array (
			array (
				'key' => 'category_sort',
				'name' => 'category_sort',				
				'type' => 'number',
				'label' => 'Category order',
				'default_value' => 99
			)		


		),

		'location' => array (
						array (
							array (
								'param' => 'taxonomy',
								'operator' => '==',
								'value' => 'portfolio_categories',
							),
						),
					),
		'position' => 'side'
	));	