<?php
	acf_add_local_field_group(array(
		'key' => 'case_misc_info',
		'title' => 'Case Study Info',


		'fields' => array (
			array (
				'key' => 'case_location',
				'name' => 'case_location',
				'label' => 'Location',
				'instructions' => 'City, State',
				'type' => 'text'
			),
			array (
				'key' => 'case_story',
				'name' => 'case_story',
				'label' => 'Project Story',
				'type' => 'textarea'
			),			
			array (
				'key' => 'case_reason',
				'name' => 'case_reason',
				'label' => 'Why did client Go With DreamMaker?',
				'type' => 'textarea'
			),
			array (
				'key' => 'case_testimonial',
				'name' => 'case_testimonial',				
				'label' => 'How Does The Client Feel About Their Remodel?',
				'type' => 'textarea'
			),
			array (
				'key' => 'case_products',
				'name' => 'case_products',				
				'label' => 'Products Used',
				'type' => 'text'
			),
			array (
				'key' => 'case_experts',
				'name' => 'case_experts',				
				'label' => 'Experts Who Made This Dream a Reality',
				'type' => 'text'
			)			


		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'case-studies',
				),
			),
		),
		'position' => 'normal'
	));	


///////////////////////////////////////////
// CASE in connection with Services
//////////////////////////////////////////
	acf_add_local_field_group(array(
		'key' => 'case_services_sidebar',
		'title' => 'Services Completed	',

		'fields' => array (
			array (
				'key' => 'case_services',
				'name' => 'case_services',				
				'type' => 'checkbox',
				'choices' => $parent_posts,	
				'layout' => 1,			
			)		


		),

		'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'case-studies',
							),
						),
					),
		'position' => 'side'
	));		


///////////////////////////////////////////
// Gallery
//////////////////////////////////////////
/*	
	acf_add_local_field_group(array(
		'key' => 'case_studies_gallery_before',
		'title' => 'Gallery (Before)',

		'fields' => array (
			array (
				'key' => 'gallery_img_before',
				'name' => 'gallery_img_before',				
				'type' => 'checkbox',
				'choices' => $gallery_posts,	
				'layout' => 'horizontal',
				'wrapper-width' => '20%'
			)		


		),

		'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'case-studies',
							),
						),
					),
		'position' => 'normal'
	));		
*/

	acf_add_local_field_group(array(
		'key' => 'case_gallery_group',
		'title' => 'Gallery',

		'fields' => array (
			array (
				'key' => 'case_gallery',
				'name' => 'case_gallery',				
				'type' => 'select',
				'choices' => $gallery_posts,	
				'layout' => 'horizontal',
				'wrapper-width' => '20%'
			)		


		),
		'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'case-studies',
							),
						),
					),
		'position' => 'normal'
	));			

	acf_add_local_field_group(array(
		'key' => 'case_category_sidebar',
		'title' => 'Services Category	',

		'fields' => array (
			array (
				'key' => 'case_category',
				'name' => 'case_category',				
				'type' => 'checkbox',
				'choices' => $services_categories,	
				'layout' => 1,			
			)		


		),

		'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'case-studies',
							),
						),
					),
		'position' => 'side'
	));