<?php
acf_add_local_field_group(array(
	'key' => 'service_gallery',
	'title' => 'Gallery',

	'fields' => array (
		array (
			'key' => 'service_gallery_imgs',
			'name' => 'service_gallery_imgs',				
			'type' => 'checkbox',
			'choices' => $gallery_posts,	
			'layout' => 'horizontal',
			'wrapper-width' => '20%'
		)		


	),
	'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'services',
						),
						array (
							'param' => 'page_type',
							'operator' => '!=',
							'value' => 'parent',
						),
					),
				),
	'position' => 'normal'
));				