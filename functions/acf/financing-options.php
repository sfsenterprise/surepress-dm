<?php

$page = get_page_by_path( 'about/financing-options' );

if(isset($page)):
	if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5bec4409bd223',
		'title' => 'Financing Options',
		'fields' => array(
			array(
				'key' => 'field_5bec444e42cb8',
				'label' => 'Disclaimer',
				'name' => 'disclaimer',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '* Projects shown are for example purposes only. Assumes a minimum purchase amount as indicated, professional installation included. DreamMaker Bath and Kitchen by Worldwide ("DreamMaker") is neither a broker nor a lender. Financing is provided by third-party lenders unaffiliated with DreamMaker, under terms and conditions arranged directly between the customer and such lender, all subject to credit requirements and satisfactory completion of finance documents. Any finance terms advertised are estimates only. DreamMaker does not assist with, counsel or negotiate financing, other than providing customers an introduction to lenders interested in financing DreamMaker customers. Estimated advertised payments assume special third-party financing to well qualified buyers on approved credit. Not all buyers may qualify. Higher rates and payment amounts apply for buyers with lower credit ratings. Some conditions may apply. Room must be in suitable condition for remodeling without repair work (e.g., no water, mold, structural damage, etc.). Any waterproofing, remediation, lead safe work practices or other repairs necessary for proper installation (as deemed by DreamMaker or its designees in their sole discretion) must be performed at owner’s expense prior to installation. Installation labor and quoted pricing does not include painting, product upgrades, variances due to room size or structural differences or lead paint testing. Offer valid only for participating dealers in geographical areas served by DreamMaker or its designees. Liability for any applicable permitting fees, taxes and/or other fees that may apply are the sole responsibility of the customer. Prices are subject to change without notice and may vary by location. Some conditions may apply. Each franchise location is independently owned and operated.',
				'placeholder' => 'Disclaimer',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
			),
			array(
				'key' => 'field_5bec449342cb9',
				'label' => 'Financing Options',
				'name' => 'financing_options',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array(
					array(
						'key' => 'field_5bec44ac42cba',
						'label' => 'Title',
						'name' => 'title',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'Title',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5bec44c042cbb',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'Image',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5bf2d2a08fcff',
						'label' => 'Show',
						'name' => 'show',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 1,
						'ui' => 0,
						'ui_on_text' => '',
						'ui_off_text' => '',
					),
					array(
						'key' => 'field_5bec44e642cbc',
						'label' => 'Details',
						'name' => 'details',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'new_lines' => 'br',
					),
					array (
						'key' => 'expiration',
						'label' => 'Expiration',
						'type' => 'date_picker',
						'name' => 'expiration'
					),  
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'page',
					'operator' => '==',
					'value' => $page->ID,
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

	endif;
endif;