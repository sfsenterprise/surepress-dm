<?php
 
$id = (!empty( $_GET['post'] ) && (get_the_title($_GET['post']) == 'Directions') ) ? $_GET['post'] : "no-id";

	acf_add_local_field_group(array(
		'key' => 'group_direction',
		'title' => 'Directions',
		'fields' => array (
						array (
							'key' => 'direction_text',
							'name' => 'direction_text',
							'type' => 'wysiwyg',
							'label' => 'Direction'
						),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
				array (
					'param' => 'post',
					'operator' => '==',
					'value' => $id ,
				),
			),
		),
		'position' => 'normal'
	));

	