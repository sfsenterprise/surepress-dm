<?php 

	namespace Surepress\Functions\Glossary;

	function GetGlossary( $query ) {
		if( !is_admin() && $query->is_main_query() && is_post_type_archive( 'glossary' ) ) {
			$query->set( 'posts_per_page', -1 );
	        $query->set( 'order', 'ASC' );
	        $query->set( 'orderby', 'post_title' );
		}
	}

?>