<?php
namespace Surepress\Functions\Scripts;

function header_scripts(){
    global $wp_query;
    $script = unserialize($wp_query->nap->microsite_fields);

    if( $script["header_scripts"] || $script["header_scripts"] != null ){
        echo $script["header_scripts"];
    }

}    


function footer_scripts(){
    global $wp_query;
    $script = unserialize($wp_query->nap->microsite_fields);

    if( $script["footer_scripts"] || $script["footer_scripts"] != null ){
        echo $script["footer_scripts"];
    }
}


function body_scripts(){
    global $wp_query;
    $script = unserialize($wp_query->nap->microsite_fields);

    if( $script["afterbody_scripts"] || $script["afterbody_scripts"] != null ){
        return $script["afterbody_scripts"];
    }
}