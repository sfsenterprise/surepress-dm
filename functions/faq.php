<?php
	namespace Surepress\Functions\FAQ;

	function GetFAQs( $query ) {
		if( !is_admin() && $query->is_main_query() && is_post_type_archive( 'faqs' ) ) {
			$query->set( 'post_type', 'faqs' );
	        $query->set( 'order', 'ASC' );
	        $query->set( 'orderby', 'post_title' );
		}
	}

 ?>
