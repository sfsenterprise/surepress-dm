<?php
namespace Surepress\Functions\CampaignsTy;

function metabox ($j_template) {
    add_action( 'add_meta_boxes', __NAMESPACE__ . '\\wpadminMeta' );
	add_action( 'save_post', __NAMESPACE__ . '\\saveMeta', 10, 2 );    
}

function wpadminMeta() {
  add_meta_box(
    'smashing-post-class',      // Unique ID
    esc_html__( 'Post Class', 'example' ),    // Title
    __NAMESPACE__ . '\\form',   // Callback function
    'page',         // Admin page (or post type)
    'side',         // Context
    'default'         // Priority
  );
}

function form( $post ) { ?>

  <?php wp_nonce_field( basename( __FILE__ ), 'is_campaignsty_nonce' ); ?>
  <?php $is_campaignsty = get_post_meta( $post->ID, 'is_campaignsty', true ); ?>
  <?php $checked =  ($is_campaignsty === '1') ? 'checked="checked"' : ''; ?>
  <p>
    <label for="is_campaignsty">Campaign Thank You page?</label>
    <br />
    <input type="hidden" name="is_campaignsty" value="0">        
    <input class="widefat" type="checkbox" id="is_campaignsty" name="is_campaignsty" value="1" <?php echo $checked; ?> />
  </p>
<?php }


function saveMeta( $post_id, $post ) {
  if ( 
  	   !isset( $_POST['is_campaignsty_nonce'] ) || 
  	   !wp_verify_nonce( $_POST['is_campaignsty_nonce'], basename( __FILE__ ) ) 
  	)
    return $post_id;

  $new_meta_value = $_POST['is_campaignsty'];
  $meta_key = 'is_campaignsty';
  $meta_value = get_post_meta( $post_id, $meta_key, true );
  
  update_post_meta( $post_id, $meta_key, $new_meta_value );
}