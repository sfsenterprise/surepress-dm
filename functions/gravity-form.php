<?php
namespace Surepress\Functions\Gravity;
use Surepress\Functions\Common as Common; 

function populate_posts( $form ) {

    if( "Start A Conversation" === $form['title'] || "Start A Conversation Popup" === $form['title']  ){

            foreach ( $form['fields'] as &$field ) {
                
                if ( "Start A Conversation" === $form['title'] && $field->type != 'checkbox' && $field->id != 11 ) continue;

                if ( "Start A Conversation Popup" === $form['title'] && $field->type != 'checkbox' && $field->id != 6 ) continue;

                $limit = (strpos( $field->cssClass, 'limit_4' ) === false) ? -1 : 4;
                
                $posts = Common\get_parent_services($limit);
                
                $choices = array();
         
                foreach ( $posts as $post ) {
                    $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
                }
                $choices = array_map("unserialize", array_unique(array_map("serialize", $choices)));

                $field->choices = $choices;
            }        
    }

    return $form;
}


function hide_new_reviews( $entry, $form ) {

    if( $form['title'] === 'Reviews Form' ) {
        $entry['status'] = 'inactive';
        \GFAPI::update_entry_property( $entry['id'], 'status', $entry['status'] );
    }

     return $entry;
} 

function campaignThankyou( $confirmation, $form, $entry, $ajax ) {
    $template = get_post_meta( get_the_ID(), 'campaign_template', true );
    $redirect = get_post_meta( get_the_ID(), 'campaign_thank_you_id', true );
    if( is_singular('campaigns') && $template === "landing" && $redirect > 0) {
        $confirmation = array( 'redirect' => ( get_permalink($redirect)) );
    }
    return $confirmation;
}

//function for rendering the Field ID in the advanced tab on gravity form edit panel
function render_field_id_setting( $position, $form_id ) {
    if ( 50 !== $position ) {
        return;
    }
    ?>
    <li class="field_id_setting field_setting">
        <label for="field_field_id" class="section_label">
        <?php echo esc_html_x( 'Field ID', 'label for Gravity Forms field ID input', 'growella' ); ?>
        </label>
        <input id="field_field_id" type="text" onchange="SetFieldProperty('fieldID', this.value);" />
    </li>
<?php
}

//to make sure the new Field ID will appear in the advanced tab on gravity form edit panel
function field_id_editor_script() {
?>

    <script type="text/javascript">
    /*
        * Add .field_id_setting onto the end of each field
        * type's properties.
        */
    jQuery.map(fieldSettings, function (el, i) {
        fieldSettings[i] += ', .field_id_setting';
    });

    // Populate field settings on initialization.
    jQuery(document).on('gform_load_field_settings', function(ev, field){
        jQuery(document.getElementById('field_field_id'))
        .val(field.fieldID || '');
        });
    </script>
<?php
}
//function to replace gravity form field id if Field ID in advanced settings has value
function gform_replace__fieldid(  $content, $field, $value, $lead_id, $form_id ){
	if(isset($field->fieldID) && $field->fieldID != ''){
		return str_replace( "id='input_".$form_id."_".$field->id."'", "id='$field->fieldID'", $content );
	}
    return $content;
}