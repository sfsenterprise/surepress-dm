<?php
namespace Surepress\Functions\BodyClass;
use Surepress\Functions\Common as Common; 

function body_class( $classes ) {
    global $post;
  
    if(!$post){
        return $classes;
    }

    if( !Common\is_main() && is_front_page() ){
    	$classes[] = "microsite_home";
    }


    $template['campaign'] = get_post_meta( get_the_ID(), 'campaign_template', true );           
    $template['campaignty'] = get_post_meta( get_the_ID(), 'campaignsty_template', true );
    if (in_array('landing', $template)) {
        $classes[] = 'template-campaign-landing';
    }    

    if(is_singular('campaigns')) {
        $template = get_post_meta( get_the_ID(), 'campaign_template', true );
        $campaignClass = ($template) ? 'template-campaign-'.$template : 'template-campaign-default';
        $classes[] = $campaignClass;
    }

    $classes[] = $post->post_name;

    $classes = array_diff($classes, ["media"]);

    $classes[] =  Common\is_main() ? "main" : "microsite";

    return $classes;
}

add_filter( 'body_class', __NAMESPACE__.'\\body_class', 109);

