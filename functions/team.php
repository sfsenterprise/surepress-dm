<?php
namespace Surepress\Functions\Team;


function GetTeam( $query ) {
	if( !is_admin() && $query->is_main_query() && is_post_type_archive( 'team' ) ) {
		$query->set( 'posts_per_page', -1 );
        $query->set( 'order', 'ASC' );
        $query->set( 'orderby', 'menu_order' );
	}
}


function get_team_member(){

		$args = array(
			'posts_per_page'   => 1,
			'orderby'          => 'modified',
			'order'            => 'DESC',
			'post_type'        => 'team',
			'meta_key'         => 'on_homepage',
			'meta_value'       => '1',
			'post_status'      => 'publish',
			'suppress_filters' => true,
		);

		$first_team = array(
			'posts_per_page'   => 1,
			'orderby'          => 'modified',
			'order'            => 'DESC',
			'post_type'        => 'team',
			'post_status'      => 'publish',
			'suppress_filters' => true,
		);

		$featured = get_posts( $args );

		return ( is_array($featured) && empty($featured) ) ? get_posts( $first_team ):$featured;
}