<?php 

namespace Surepress\Functions\Microsites;

/**
* Add here all about Microsite
**/
class Microsite {

	public static function attach_nap( $wp_query )
	{
		global $wpdb, $nap;

        if ( $blog_id = get_current_blog_id() ) {

            $nap = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE microsite_blog_id = {$blog_id}");

            $wp_query->nap = $nap;
        }
        return $wp_query;
	}


	public static function social(){
	 	$data = get_blog_option( get_current_blog_id(), 'dm_settings' );	
	 	return $data;
	}

    /*
    * @params string:form_title, string:field_label
    */
    public static function get_form($form_title=null, $field_label=null)
    {
        $res = array();

        $res['field'] = self::_get_form_field($form_title, 'Event ID', 'id'); // @params string:form_name, string:field_name, string:field_to_return
        $res['form_id'] = self::_get_form_field($form_title, '', 'form_id');

        return $res;
    }

    private static function _get_form_field($form_title = null, $label = null, $return)
    {
        global $wpdb;

        switch_to_blog(get_current_blog_id());
            $form = $wpdb->get_row( "SELECT id FROM {$wpdb->prefix}gf_form WHERE title = '{$form_title}'" );
            
            $metas = isset($form->id) ? $wpdb->get_row( "SELECT display_meta FROM {$wpdb->prefix}gf_form_meta WHERE form_id = '{$form->id}'" ) : NULL;
            $metas = isset($form->id) ? json_decode(maybe_unserialize( $metas->display_meta )) : NULL;
        restore_current_blog();

        if( $return === 'form_id' && isset($form->id) ) return $form->id;

        if( !empty($metas->fields) ){
            foreach ($metas->fields as $field) {
                $field = (array)$field;

                if( $field['label'] === $label ){
                    $output = $field[$return];
                } 
            }
        }

        if( isset($output) ) return $output;

        return false;
    }

}