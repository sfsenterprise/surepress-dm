<?php
namespace Surepress\Functions\Services;



function get_services($parent = null){

		$args_services = array(
			'posts_per_page'   => -1,
			'orderby'          => 'menu_order',
			'order'            => 'ASC',
			'post_type'        => 'services',
			'post_status'      => 'publish',
			'post_parent'      => $parent,
			'suppress_filters' => true,
	 
			'meta_query' => array(
			   'relation' => 'OR',
			    array(
			     'key' => 'hide_in_menu',
			     'compare' => 'NOT EXISTS',
			     'value' => '' // This is ignored, but is necessary...
			    ),
			    array(
			     'key' => 'hide_in_menu',
			     'value' => '1',
			     'compare'   => '!='
			    )
			)		    
		);

		$posts_array = get_posts( $args_services );
		return $posts_array;
}		

function archive_order( $orderby ) {
	global $wpdb;
	
	// Check if the query is for an archive
	if ( is_archive() && get_query_var("post_type") == "services" ) {
		// Query was for archive, then set order
		return "$wpdb->posts.menu_order ASC";
	}
	
	return $orderby;
}


function GetServicesLanding( $query ) {
	if( !is_admin() && $query->is_main_query() && is_post_type_archive( 'services' ) ) {
		//print_r($query); exit;
        $query->set( 'post_parent', 0 );

	}
}