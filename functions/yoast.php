<?php

namespace Surefire\Theme\Addons;

class Yoast_Integration {

  static $instance;

  protected function __construct() {
    self::$instance =& $this;

    add_shortcode('location_name', array($this, 'location_name'), 10, 1);
    add_shortcode('location_city', array($this, 'location_city'), 10, 1);
    add_shortcode('location_state', array($this, 'location_state'), 10, 1);
    add_shortcode('phone_number', array($this, 'phone_number'), 10, 1);
    add_shortcode('address', array($this, 'address'), 10, 1);
    add_shortcode('area_served_1', array($this, 'area_served_1'), 10, 1);
    add_shortcode('keyword', array($this, 'keyword'), 10, 1);
    add_shortcode('blog_post_title', array($this, 'blog_post_title'), 10, 1);
  }

  public function apply_filters() {
    add_filter( 'wpseo_title', [$this, 'filter_wpseo_title'], 99, 1); 
    add_filter( 'wpseo_opengraph_title', [$this, 'filter_wpseo_title'], 99, 1); 
    add_filter( 'wpseo_metadesc', [$this, 'filter_wpseo_description'], 99, 1);
    add_filter( 'wpseo_opengraph_desc', [$this, 'filter_wpseo_description'], 99, 1);
  }

  public function add_variables() {
    add_action('wpseo_register_extra_replacements', array(&$this, 'register_custom_yoast_variables'));
  }

  public function register_custom_yoast_variables() {
    wpseo_register_var_replacement('location_name', array(&$this, 'custom_yoast_variable_name'), 'advanced', 'Location name');
    wpseo_register_var_replacement('location_city', array(&$this, 'custom_yoast_variable_city'), 'advanced', 'Location city');
    wpseo_register_var_replacement('location_state', array(&$this, 'custom_yoast_variable_state'), 'advanced', 'Location state');
  }

  public function location_name($attrs) {
    $of = (get_current_blog_id() != '1') ? 'of ':'';
    return 'DreamMaker Bath & Kitchen ' . $of . $this->custom_yoast_variable_name();
  }

  public function location_city($attrs) {
    return $this->custom_yoast_variable_city();
  }

  public function location_state($attrs) {
    return $this->custom_yoast_variable_state();
  }

  public function phone_number($attrs) {
    $phone = preg_replace("/[^0-9]/", "", $this->get_nap('microsite_phone'));
    $length = strlen($phone);

    switch($length) {
      case 7:
        return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
      break;
      case 8:
        return preg_replace("/([0-9]{3})([0-9]{5})/", "$1-$2", $phone);
      break;
      case 10:
        return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
      break;
      case 11:
        return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
      break;
      default:
        return $phone;
      break;
    }
  }

  public function area_served_1($attrs) {
    $fields = $this->get_nap('microsite_fields');
    $fields = maybe_unserialize($fields);

    return $fields['area_served_1'];
  }

  public function address($attrs) {
    $address = $this->get_nap('microsite_street') . ' ' . $this->get_nap('microsite_city') . ', ' . $this->get_nap('microsite_state_province') . ' ' . $this->get_nap('microsite_zip_postal');
    return $address;
  }

  public function keyword($attrs) {
    global $wp_query;
    $keywords = get_post_meta($wp_query->queried_object->ID, '_yoast_wpseo_focuskw_text_input', true);
    return $keywords;
  }

  public function blog_post_title($attrs) {  
    global $post;
    return  (!is_404() ? !empty($post->post_title) ? $post->post_title : "Default Blog title" : 'Page Not Found') ;
  }

  public function custom_yoast_variable_name() {
    return $this->get_nap('microsite_name');
  }

  public function custom_yoast_variable_city() {
    return $this->get_nap('microsite_city');
  }

  public function custom_yoast_variable_state() {
    if ($state = $this->get_nap('microsite_state_province')) {
        return $state;
    }
  }

  public function custom_yoast_replacements( $replacements, $args ) {
    var_dump($replacements);
    return $replacements;
  }

  private function get_nap($yoast_variable) {
    global $wp_query;

    if (isset($wp_query->nap)) {
        if (isset($wp_query->nap->{$yoast_variable})) {
            return $wp_query->nap->{$yoast_variable};
        }
    }
  }

   public function filter_wpseo_title($title)
   { 

       global $wp_query;

       $wpseo_archive_titles = get_option('wpseo_titles', true);

       if( is_category()){
          return single_cat_title('',false)." | ".do_shortcode('[location_name]');
       }
        if( is_archive() ){     
        
          if( isset($wp_query->query['post_type']) && isset($wpseo_archive_titles["title-ptarchive-{$wp_query->query['post_type']}"]) && ! $this->strposa($wpseo_archive_titles["title-ptarchive-{$wp_query->query['post_type']}"], ['%'])){
            return do_shortcode($wpseo_archive_titles["title-ptarchive-{$wp_query->query['post_type']}"]);
          }
          elseif(isset($wpseo_archive_titles[$title]) && ! $this->strposa($wpseo_archive_titles[$title], ['%'])){
            $title = 'title-' . $wp_query->query['post_type'];
            return do_shortcode($wpseo_archive_titles[$title]);
          }
          elseif( isset($wp_query->query['post_type']) && isset($wpseo_archive_titles['title-'.$wp_query->query['post_type']]) && ! $this->strposa($wpseo_archive_titles['title-'.$wp_query->query['post_type']], ['%']) ){
            return do_shortcode($wpseo_archive_titles['title-'.$wp_query->query['post_type']]);
          }
          else{
            if( isset($wp_query->queried_object->taxonomy) && $wp_query->queried_object->taxonomy == 'portfolio_categories' && get_current_blog_id() != 1 ){
              return do_shortcode($wp_query->queried_object->name . ' Gallery | [location_name] | [location_city], [location_state]');
            }

            return $title;
          }
        }

        if( isset($wp_query->queried_object->ID) ){
          $yoast_title = get_post_meta($wp_query->queried_object->ID, '_yoast_wpseo_title', true);
        }
       
        if ( isset($yoast_title) && $yoast_title ){
          $title = do_shortcode($yoast_title);
        }
        elseif( isset($wpseo_archive_titles['title-post']) && ! $this->strposa($wpseo_archive_titles['title-post'], ['%']) ){
          
          
          if( isset($wp_query->query['pagename']) ){
              if($wp_query->query['pagename'] == 'blog'){
                  $title = "Blog | ".do_shortcode('[location_name]');
              }
          }else{
            $title = do_shortcode($wpseo_archive_titles['title-post']);
          }
          
        }
        else{
          $title = $title;
        }

        return $title;

   }

   public function filter_wpseo_description($metadesc)
   {
      global $wp_query;

      $wpseo_archive_desc = get_option('wpseo_titles', true);

      if( is_archive() ){

        if(isset($wp_query->query['post_type']) && isset($wpseo_archive_desc["metadesc-ptarchive-{$wp_query->query['post_type']}"])){
            return do_shortcode($wpseo_archive_desc["metadesc-ptarchive-{$wp_query->query['post_type']}"]);
        }
        elseif( isset( $wp_query->query['portfolio_categories']) ){
          /*$term_meta = get_term_meta($wp_query->queried_object->term_id);
          return do_shortcode($wp_query->queried_object->description);*/

          return do_shortcode($metadesc);
        }
        elseif( isset( $wp_query->query['post_type'] ) ){
          if( isset($wpseo_archive_desc['metadesc-'.$wp_query->query['post_type']]) ){
            return do_shortcode($wpseo_archive_desc['metadesc-'.$wp_query->query['post_type']]);
          }
          else{
            $desc = 'metadesc-' . $wp_query->query['post_type'];
            return do_shortcode($wpseo_archive_desc[$desc]);
          }
        }
      
      }
      
      if( isset($wp_query->queried_object->ID) ){
        if( isset($wp_query->query['pagename']) && $wp_query->query['pagename'] == 'blog' && 
            isset($wpseo_archive_desc['metadesc-post']) && ! $this->strposa($wpseo_archive_desc['metadesc-post'], ['%']) ){
            
            $metadesc = do_shortcode($wpseo_archive_desc['metadesc-post']);
        }
        else{
          $metadesc = get_post_meta($wp_query->queried_object->ID, '_yoast_wpseo_metadesc', true);

          $metadesc = wp_trim_words( do_shortcode($metadesc), 54, '...' );
          $metadesc = ( strlen($metadesc) > 320 ) ? wp_trim_words( do_shortcode($metadesc), 50, '...' ):$metadesc;
          $metadesc = ( strlen($metadesc) > 320 ) ? substr($metadesc, 0, 315) . '...':$metadesc;
          $metadesc = ( strlen($metadesc) === 0 ) ? substr($wp_query->queried_object->post_content, 0, 315) . '...':$metadesc;
        }
      }
      
      return $metadesc;
   }

   private function strposa($haystack, $needles=array(), $offset=0) {
       $chr = array();
       foreach($needles as $needle) {
           $res = strpos($haystack, $needle, $offset);
           if ($res !== false) $chr[$needle] = $res;
       }

       if(empty($chr)) return false;

       return true;
   }

    public static function &get_instance()
    {
        if ( ! isset(self::$instance) )
            self::$instance = new self;

        return self::$instance;
    }
}

$yoast_integration =& Yoast_Integration::get_instance();
$yoast_integration->add_variables();
$yoast_integration->apply_filters();