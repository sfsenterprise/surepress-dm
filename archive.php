<?php 
$current_microsite = get_current_blog_id();
$microsite_fields = getMicrositeExtraFields($current_microsite);
//redirect to 404 if events, case studies financial options and special offers are disabled
if(($microsite_fields['microsite_show_events'] == 0 && get_post_type() == 'events') || ($microsite_fields['microsite_show_case_studies'] == 0 && $wp_query->query['post_type'] == 'case-studies') || ($microsite_fields['microsite_show_special_offers'] == 0 && get_post_type() == 'offers')){
	
	wp_redirect(get_home_url().'/404');
}
?>
<?php get_header(); ?> 

<!-- beaverton ourteam custom hero image --->
<?php if((get_post_type() == 'team')&&(get_current_blog_id() == 25)): ?>

	<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/beaverton/wp-content/uploads/sites/25/2022/03/web-staff-pic.jpg');">
		<h1>
			<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
		</h1>
	</section>

<!-- greater rockwall ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 116)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/greater-rockwall/wp-content/uploads/sites/116/2022/05/rockwall-ourteam.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- tyler ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 58)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/tyler/wp-content/uploads/sites/58/2024/04/tyler-new-team-banner.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- ogden ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 41)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/ogden/wp-content/uploads/sites/41/2024/01/our-team-banner-ogden-optimized.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- nwa ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 109)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/nwa/wp-content/uploads/sites/109/2023/02/nwa-our-team.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- east georgia ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 63)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/east-georgia/wp-content/uploads/sites/63/2024/10/east-georgia-our-team-banner-2.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- ann arbor ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 23)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/ann-arbor/wp-content/uploads/sites/23/2023/08/ann-arbor-our-team.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- reno ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 90)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/wp-content/uploads/2024/06/elizabethtown-about-banner-2.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- southernri ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 108)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/southernri/wp-content/uploads/sites/108/2024/01/our-team-banner-southernri.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- coachella ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 133)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/coachella/wp-content/uploads/sites/133/2023/12/Coachella-Team.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- wilmington ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 75)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/wilmington/wp-content/uploads/sites/75/2024/04/wilmington-team-banner.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- larimerco ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 115)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/larimerco/wp-content/uploads/sites/115/2024/05/larimerco-new-team-banner.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- elizabethtown ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 33)): ?>

<section class="hero-image" style="align-items:flex-end; background-image:url('https://www.dreammaker-remodel.com/elizabethtown/wp-content/uploads/sites/33/2024/06/our-team-elizabethtown-1.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- amarillo ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 22)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/amarillo/wp-content/uploads/sites/22/2024/07/amarillo-our-team.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- southern-lakes ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 68)): ?>

<section class="hero-image" style="background-image:url('https://www.dreammaker-remodel.com/southern-lakes/wp-content/uploads/sites/68/2024/09/southern-lakes-our-team-new.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- south charlotte ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 135)): ?>

<section class="hero-image" style="align-items:flex-end; background-image:url('https://www.dreammaker-remodel.com/southcharlotte/wp-content/uploads/sites/135/2024/12/south-charlotte-our-team-banner.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<!-- aiken ourteam custom hero image --->
<?php elseif((get_post_type() == 'team')&&(get_current_blog_id() == 60)): ?>

<section class="hero-image" style="align-items:flex-end; background-image:url('https://www.dreammaker-remodel.com/aiken/wp-content/uploads/sites/60/2025/01/aiken-our-team-page-banner.jpg');">
	<h1>
		<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
	</h1>
</section>

<?php else: ?>

	<section class="hero-image">
		<h1>
			<?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').(( get_post_type() ) ? (get_post_type_object( get_post_type() )->label == 'Services' ? "Remodeling Services" : get_post_type_object( get_post_type() )->label ) : ucwords($wp_query->queried_object->label)); ?>
		</h1>
	</section>

<?php endif; ?>

<div class="container">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >   
		<?php
		if ( is_search() ) {  		    
			get_template_part('template-parts/search/search', $wp_query->query['post_type'] );
		}else if ( have_posts()  && !is_404() ) {   
		    get_template_part('template-parts/archives/archive', get_post_type());
		} else if( is_archive('faqs') || is_archive('glossary') ) {  
				get_template_part('template-parts/archives/archive', $wp_query->query['post_type'] );		
		} else { ;?>
		   <center><h6> Archive Not Found! </center></h6>
		<?php }
		?>
	</div>
</div>			
<?php get_footer(); ?>