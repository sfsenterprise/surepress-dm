<?php
     /**
     * Template Name: Locations template
     */

    get_header(); 
?>
<section class="hero-image">
    <h1>Locations</h1>

</section>
<div class="container">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >   
        <?php
            use Surepress\Functions\Common as Common;
            use Surepress\Functions\Assets as Assets;
        ?>
        <?php get_template_part('template-parts/partials/search', 'locations'); ?>

        <?php if ( have_posts() ) : ?>
                <?php if( isset($_GET['search']) ) : ?>
                    <?php get_template_part('template-parts/search/search', 'location'); ?>
                <?php else : ?>    
                    <div id="sfl_map" style="width: 100%; height: 500px;" data-pin="<?php echo Assets\asset_path('images/map-icon.png') ?>"></div>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part('template-parts/partials/location', 'city'); ?>
                    <?php endwhile; ?>  
                <?php endif; ?>
        <?php else : ?>
            No locations found. 
        <?php endif; ?> 
    </div>
</div>     
<?php get_footer(); ?>