<?php 
	use Surepress\Functions\Assets as Assets;
	use Surepress\Functions\Common as Common;
	use Surepress\Functions\Services as Services;

	$template =  get_post_meta( get_the_ID(), 'campaign_template', true ); 
  $campaign_footer_script = get_post_meta(get_the_ID(), 'campaign_footer_script', TRUE );
?>
		</main>

        <?php
          if( in_array($template, ['version2_b', 'version2_k']) ) {
            get_template_part('template-parts/campaigns/footer', 'version2');
          }
          elseif( $template == 'kitchen_bath' ){
            get_template_part('template-parts/campaigns/footer', 'kitchen-bath');
          } 
          else {
            get_template_part('template-parts/campaigns/footer', 'main'); 
          }
        ?>

		<?php wp_footer(); ?>
    <?php echo $campaign_footer_script; ?>
	</body>
</html>