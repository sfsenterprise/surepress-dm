<?php
	/****************************************************************
	WHOEVER CREATED THIS FILE
	
	TO DO :::: 
		create taxonomy.php .. 
		move this inside template-parts ....
						--- xxxoooxxx MAMA J

	****************************************************************/
?>
<?php use Surepress\Functions\Common as Common; ?>
<?php get_header(); ?>
<?php 
	$category = get_queried_object(); 
	$posts = Common\get_posts_by_custom_post('portfolio', 'portfolio_categories', $category->slug);
?>
<section class="hero-image">
	<h1><?php echo $wp_query->nap->microsite_name.' '.ucwords($category->name); ?></h1>
</section>
<div class="container">
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
		<h6 align="center"><?php echo ucwords($category->name); ?></h6>
		<div class="gallery_container slicker" data-slick='{"slidesToShow": 3, "slidesToScroll": 3}'>
			<?php if($posts) : ?>
				<?php foreach ($posts as $post) : ?>
					<?php $thumbnail_id = get_post_thumbnail_id( $post->ID );?>
					<div class="img-container">
						<a href="<?php echo get_the_post_thumbnail_url($post->ID); ?>" data-fancybox="gallery" data-caption="<?php echo get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); ?>" title="<?php echo get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); ?>">
							<?php echo wp_get_attachment_image( $thumbnail_id  , 'medium' ); ?>
						</a>
					</div>
					<?php $get_after_img = get_field('portfolio_after', $post->ID); ?>
					<?php  if($get_after_img) : ?>
						<div class="img-container">
							<a href="<?php echo wp_get_attachment_image_src($get_after_img)[0]; ?>" data-fancybox="gallery" data-caption="<?php echo get_post_meta($get_after_img, '_wp_attachment_image_alt', true); ?>" title="<?php echo get_post_meta($get_after_img, '_wp_attachment_image_alt', true); ?>">
								<?php echo wp_get_attachment_image( $get_after_img  , 'medium' ); ?>
							</a>
						</div>
					<?php endif; ?>
				<?php endforeach ?>
			<?php else : ?>	
				No images found.
			<?php endif; ?>
		</div>	
		<div class="arrows">	
			<a class="prev"><i></i></a>
			<a class="next"><i></i></a>
		</div>
		<?php if( isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_case_studies']) && maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_case_studies'] == 1) :?>
			<?php get_template_part('template-parts/section/section', 'gallery-case-studies'); ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>