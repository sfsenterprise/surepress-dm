<?php 
	use Surepress\Functions\Assets as Assets;
	use Surepress\Functions\Common as Common;
	use Surepress\Functions\Services as Services;
?>
		</main>
		<?php if (!is_page_template(array('template-landing.php'))) : ?>
			<?php get_template_part('template-parts/section/section', 'start-a-conversation' ); ?>
		<?php endif; ?>
		<?php get_template_part('template-parts/section/section', 'badges' ); ?>
		<?php get_template_part('template-parts/section/section','announcements-footer'); ?>   
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container-fluid">
				
				<div class="row">	
						<figure>
							<a href="<?php echo site_url('/'); ?>">

								<?php if( Common\is_main() ) : ?>
									<img src="<?php echo Assets\asset_path('images/dreammaker-logo-franchises.png')?>" alt="DreamMaker Bath & Kitchen"/>
								<?php else : ?>
									<img src="<?php echo Assets\asset_path('images/dreammaker-logo-franchise.png')?>" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/>
								<?php endif; ?>	
							</a>
							<?php if( !Common\is_main() ) : ?>	
								<figcaption><?php echo $wp_query->nap->microsite_name; ?></figcaption>			
							<?php endif; ?>		
						</figure>

						<div class="newsletter">
							<?php if( Common\is_main() ) : ?>
								<p>DreamMaker's bottom line is people. We believe if we treat people with respect and integrity, success will follow.</p>
								<p><a class="btn btn-link btn-lg" data-toggle="modal" data-target="#newsletter-form">Get Our Newsletter</a></p>
							<?php else : ?>
								<p>The Code of Values informs the way DreamMaker treats you, our customer. We hope you will see the difference.</p>
								<p><a class="btn btn-link btn-lg" data-toggle="modal" data-target="#newsletter-form">Get Our Newsletter</a></p>
							<?php endif; ?>	
						</div>	


						<div class="new-grid">
							<div class="footer-nav">
								<h4 class="title">Services</h4>
								<ul>
								<?php foreach( Services\get_services(0) as $service ) :  ?>
									<li><a href="<?php echo get_permalink($service->ID); ?>"><?php echo $service->post_title; ?></a></li>
								<?php endforeach; ?>
								</ul>
							</div>							
							<div class="footer-nav">
								<h4 class="title">Resources</h4>
								<ul id="menu-resources-menu">
									<?php if( !Common\is_main() ) : ?>
										<li><a href="<?php echo get_site_url(); ?>/directions/">Directions</a></li>
										<li><a href="<?php echo get_site_url(); ?>/faq/">FAQ</a></li>
										<li><a href="<?php echo get_site_url(); ?>/glossary/">Glossary</a></li>
										<li><a href="<?php echo get_site_url(); ?>/blog/">Remodeling Tips</a></li>
										<li><a href="<?php echo get_site_url(); ?>/sitemap/">Sitemap</a></li>
										<li><a href="<?php echo get_site_url(); ?>/privacy-policy/">Privacy Policy</a></li>
									<?php else: ?>
										<li><a href="/locations/">Locations</a></li>
										<li><a href="/faq/">FAQ</a></li>
										<li><a href="/glossary/">Glossary</a></li>
										<li><a href="/blog/">Remodeling Tips</a></li>
										<li><a href="/sitemap/">Sitemap</a></li>
										<li><a href="/privacy-policy/">Privacy Policy</a></li>
									<?php endif; ?>
								</ul>
								
							</div>

							<address>
								<h4>
									<?php echo "<a href='".get_site_url()."/contact-us/'>Contact Us</a> ";	?>
								</h4>
								<p>
									<?php $mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($wp_query->nap->microsite_fields)['mongoose_phone']);?>
									<strong><a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $wp_query->nap->microsite_phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $wp_query->nap->microsite_phone); ?></a></strong><br>
									<?php if ( !empty(unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']) ): ?>
										<a href="mailto:<?php echo unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']; ?>"><?php echo unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']; ?></a>
									<?php endif; ?>
									<?php if( !Common\is_main() ) : ?>
										<?php if ( !empty(($wp_query->nap->microsite_owner)) ): ?>
											<p>This location is independently owned and locally operated by <?php echo $wp_query->nap->microsite_owner; ?> under license from DreamMaker Bath & Kitchen.</p>
										<?php endif; ?>
									<?php endif; ?>	
									<?php if ( !empty(unserialize($wp_query->nap->microsite_fields)['microsite_contractor_license']) ): ?>
										<p>License: <?php echo unserialize($wp_query->nap->microsite_fields)['microsite_contractor_license']; ?></p>
									<?php endif; ?>
								</p>						
							</address>
						</div>	

						<div class="footer-social">
							<h4>Connect with Us</h4>
							<?php if( !Common\is_main() ) : ?>
								<a href="<?php echo get_site_url(get_current_blog_ID()); ?>/about/our-team/">Meet The Team</a>
							<?php endif; ?>
							<?php get_template_part('template-parts/widgets/widget','social-media');  ?>
							<p><a href="https://www.dreammakerfranchise.com/" target="_blank" class="btn btn-link btn-lg smaller">Own a Franchise</a></p>
						</div>
						<div class="clearfix"></div>

				</div>
			<!-- end row -->
				<!-- start constant-contact-modal -->
				<div class="modal fade start-conversation-popup newsletter-popup" id="newsletter-form" tabindex="-1" role="dialog" aria-labelledby="newsletter-form" aria-hidden="true" style="z-index:99999;">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
						<span class="border"></span>
						<div class="modal-header"> 
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="fa fa-times"></i></span>
							</button></div>
						<div class="modal-body">
							<style>
								.newsletter-popup .g-recaptcha {
									display:none;
								}
								.newsletter-popup h2.ctct-form-header {
									font-family: 'Neuton' !important;
									font-weight: 700 !important;
									font-size: 2.188em !important;
								}
								.ctct-form-embed.form_0 .ctct-form-defaults {
									background: #432a71 !important;
								}
								.ctct-form-embed.form_0 .ctct-form-defaults .ctct-form-header{
									color: #fff;
								}
								.ctct-form-embed.form_0 .ctct-form-defaults .ctct-form-text,
								.ctct-form-embed.form_0 .ctct-form-custom .ctct-form-label,
								.ctct-form-embed.form_0 .ctct-form-defaults .ctct-gdpr-text,
								.ctct-form-embed.form_0 .ctct-form-defaults .ctct-gdpr-text .ctct-form-footer-link {
									color: #fff;
									font-weight: normal;
									font-size: 15px;
								}
								.ctct-form-embed.form_0 .ctct-form-defaults .ctct-gdpr-text,
								.ctct-form-embed.form_0 .ctct-form-defaults .ctct-gdpr-text .ctct-form-footer-link {
									font: inherit;
									font-size: 11px;
								}
								.ctct-form-embed.form_0 .ctct-form-custom .ctct-form-button {
									background-color: #ffde4c;
									color: #332e30;
								}
								.ctct-form-embed.form_0 .ctct-form-custom .ctct-form-button:hover {
									color: #fff;
									background-color: #1e1332;
									border-color: #010101;
								}
							</style>
							<?php if( Common\is_main() ) : ?>
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="6d9dd65d-3cd3-4649-8e13-4637d558b1e2"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 60): ?> <!--- aiken -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="6840c50a-0f1c-4cdd-a40e-1b87d3aa9348"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 22): ?> <!--- amarillo -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="b9b1b2c3-2765-410b-b6c4-bcb4226c0059"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 23): ?> <!--- ann arbor --> 	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="3b92682a-6da3-4ef2-8232-31a27a129030"></div>
								<!-- End Constant Contact Inline Form Code -->								
							<?php elseif(get_current_blog_id() == 24): ?> <!--- bakersfield -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="c3570407-a00a-477d-be39-4872c8fe4c9b"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 25): ?> <!--- beaverton -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="a0ac52d2-c3f9-4bb1-8ce6-4e52f92864e1"></div>
								<!-- End Constant Contact Inline Form Code -->					
							<?php elseif(get_current_blog_id() == 114): ?> <!--- burlington -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="412017c9-0121-410f-b0fb-13a734f84a53"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 30): ?> <!--- colorado springs -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="d55b3374-12c2-4695-95a4-586f7685e4b2"></div>
								<!-- End Constant Contact Inline Form Code -->	
							<?php elseif(get_current_blog_id() == 133): ?> <!--- coachella -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="61778ad0-3b60-4bd1-b4e5-1d9addf092a3"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 131): ?> <!--- chester -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="fd4e570f-1788-4db7-ba07-cdf46073c037"></div>
								<!-- End Constant Contact Inline Form Code -->								
							<?php elseif(get_current_blog_id() == 63): ?> <!--- east georgia -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="8378439b-26a5-40e7-bb85-2f75ecede94d"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 33): ?> <!--- elizabethtown -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="7061f494-d000-469d-b554-861b86b3bd6b"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 34): ?> <!--- fredericksburg -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="1685427c-7c0d-42e8-8cab-c46f23976763"></div>
								<!-- End Constant Contact Inline Form Code -->	
							<?php elseif(get_current_blog_id() == 116): ?> <!--- rockwall -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="6a363761-e106-422c-8535-a3241cb32131"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 118): ?> <!--- greensboro -->		
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="d6c43c70-7dc4-4c25-b13a-7953196ed952"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 35): ?> <!--- greenville -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="684ad287-2522-4dd1-8b63-b8166672d735"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 92): ?> <!--- hollywood -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="3208c589-1792-49cd-b98f-7cfeef708f63"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 36): ?> <!--- huntsville -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="8690e726-62cc-41bf-b87e-79acda2e0159"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 74): ?> <!--- jupiter -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="8ce50238-bd88-4aa0-acf5-ee202b658ff6"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 115): ?> <!--- larimerco -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="f2e34b48-024b-4381-88f1-3c4d79557240"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 39): ?> <!--- lubbock -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="84b01c2a-69f1-44d7-944d-0852a85acdd9"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 94): ?> <!--- madison -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="297053f8-80c2-46e4-9292-3d205863b6f8"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 40): ?> <!--- newington -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="18a66608-ca54-4e6a-824d-f05471a52848"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 109): ?> <!--- nwa -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="b404bbbc-2c58-49fb-acb7-1d51b2de6ce7"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 110): ?> <!--- nwdc -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="211d1455-f835-4c31-a772-722adca08b40"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 41): ?> <!--- Ogden -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="ef896b1d-fbb6-4582-83d9-2b908d7e8338"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 117): ?> <!--- Omaha -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="0ccdc8d7-d61b-4632-8a74-b8f0a3dcb570"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 72): ?> <!--- Orland -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="de9b5b0a-7847-4c17-8fb4-daabd81dad72"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 43): ?> <!--- Pittsburgh -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="ea1a2ef4-0f12-4632-bc33-8197ef26c734"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 90): ?> <!--- Reno -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="f6625bfc-0483-4772-b081-94eae4f026eb"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 45): ?> <!--- Schaumburg -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="b49072b8-d3b5-4477-a5fe-419c00f38e32"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 70): ?> <!--- Southeast Florida -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="7d870028-3caf-488d-b6e7-dc7882539340"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 111): ?> <!--- South Valley -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="3390bb4b-2c7e-4071-ad60-9b1858696fff"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 68): ?> <!--- Southern Lakes -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="cc23ec32-e27b-4d23-8725-9f01409c975c"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 108): ?> <!--- Southern Rhode Island -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="4b243391-04aa-4d94-975e-71034eaed890"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 47): ?> <!--- Springfield -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="92912fa3-b7fd-494c-bfea-bfdf32d59977"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 48): ?> <!--- St. Louis Park -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="68c29f1f-f418-43cb-80bb-53cbbb1e1b46"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 107): ?> <!--- Woodlands -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="31ecca04-77d9-4445-92a2-242da5715599"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 58): ?> <!--- Tyler -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="0e15eb36-ccb5-486b-b737-915e948cb515"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 53): ?> <!--- Waco / Central Texas -->	
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="5183775a-a1ea-4e79-95a4-11c6db4050c7"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 96): ?> <!--- Wcc / West Collin County -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="c9b9e147-90b6-48d7-a8f5-83b8e4c25fed"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 132): ?> <!--- Williamsburg -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="c5ad324c-7ab1-4c77-b7cf-20c6672521a5"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php elseif(get_current_blog_id() == 75): ?> <!--- Wilmington -->
								<!-- Begin Constant Contact Inline Form Code -->
								<div class="ctct-inline-form" data-form-id="98f61cec-7cc0-46fc-90ca-f21a8648e1cc"></div>
								<!-- End Constant Contact Inline Form Code -->
							<?php endif; ?>
							
						</div>
						</div>
					</div>
				</div>
				<!-- end constant-contact-modal -->

			</div>			
			<?php $option = get_blog_option(get_current_blog_ID(), 'dm_settings');
				$ccorp_text = (isset($option['footer_ccorp'][0]) && !empty($option['footer_ccorp'][0])) ? $option['footer_ccorp'][0] : "&copy; 2005-" . date('Y') . " Worldwide Refinishing Systems Inc. dba DreamMaker Bath & Kitchen. Independently Owned and Operated Franchises."; ?>
			<p class="copyright"><?php echo $ccorp_text; ?></p>			
		</footer>
		<?php wp_footer(); ?>
		<?php if( !Common\is_main() ) : ?>
			<script> (function(){ var s = document.createElement('script'); var h = document.querySelector('head') || document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, disableBgProcess : false, language : 'en', position : 'left', leadColor : '#146FF8', triggerColor : '#146FF8', triggerRadius : '50%', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerIcon : 'people', triggerSize : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerOffsetX : 20, triggerOffsetY : 20, triggerRadius : '20' } }); }; h.appendChild(s); })();</script>
		<?php endif; ?>
	</body>
</html>