<?php 
    use Surepress\Functions\Common as Common;
?>
<?php  get_header(); ?>
<?php
if ( have_posts() ) {
    while ( have_posts() ) : the_post(); ?> 
            <section class="hero-image career">
                <h1>
                    <?php 
                    if(is_page('areas-we-serve')) {
                        $wp_query->nap->microsite_name = "";
                    }
                    ?>
                    <?php echo( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').ucwords(get_the_title()); ?>                        
                </h1>
                <?php do_action( 'after_hero_title'); ?> 
            </section>
            <?php $class_container = ( is_page('our-process') ? 'container-fluid' : 'container' ); ?>
            <div class="<?php echo $class_container; ?> ">
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                    <!-- <?php get_template_part('template-parts/static', $post->post_name ); ?>   
                    <?php if( !is_page('our-process') && !is_page('contact-us')  && !is_page('directions') && !is_page('reviews')  ) : ?>
                        <?php the_content(); ?>
                    <?php endif; ?> -->
                    <?php get_template_part('template-parts/careers/section', 'careers' ); ?>
                </div>
            </div>                
    <?php endwhile;
} else {
    get_template_part('template-parts/content', 'none');
}
?>
<?php get_footer();?>