<?php get_header(); ?>
	<?php if( is_front_page() ): ?>
		<?php get_template_part('template-parts/static', 'homepage' ); ?>
	<?php else : ?>	
		<section class="hero-image">
            <h1><?php echo ( empty($wp_query->nap->microsite_name) ? '' : $wp_query->nap->microsite_name.' - ').' Blog' ?></h1>
		</section>
		<div class="container">
		<h2>DreamMaker Remodeling Tips</h2>
		<?php if ( have_posts() ) : ?>
		    <?php while ( have_posts() ) : the_post(); ?>
		        <?php get_template_part('template-parts/content', get_post_type()); ?>
		    <?php endwhile; ?>
		<?php else : ?>
		    <?php get_template_part('template-parts/content', 'none'); ?>
		<?php endif; ?>

		<div class="pagination">
			<?php 
			$args = array(
				'prev_next' => true,
				'prev_text'          => __('«'),
				'next_text'          => __('»')
			);?>
			<?php echo paginate_links($args); ?>
		</div>

		</div>	

	<?php endif; ?>	
<?php get_footer(); ?>