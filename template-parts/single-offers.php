<div class="modal fade in" id="offer1" tabindex="-1" role="dialog" aria-labelledby="offer1" aria-hidden="false" style="display: block">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header"> 
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Close</span>
          </button>
        </div>
        <div class="modal-body">
            <h3 id="offer-title"><?php the_title(); ?></h3>
            <div class="header-text" id="offer-text">
            	<?php the_content(); ?>
            </div>
    		    <?php  gravity_form( 'Offers',false, false,false, false,true, 100, true );    ?>
        </div>
    </div>
  </div>
</div>  

<script type="text/javascript">

function updateContent(){
	let data = JSON.parse(localStorage.getItem('sfl_preview'));
	jQuery('#offer-title').html(data.title);
	jQuery('#offer-text').html(data.content);
}

jQuery(document).ready(function() {	
	var params = {};location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){params[k]=v})

	window.addEventListener("storage", updateContent); 
	if (localStorage.getItem("sfl_preview") !== null && params.preview === "true" && params.new === "true") {
		updateContent();
	}
});	
</script>