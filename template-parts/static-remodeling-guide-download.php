<section class="hero-image">
    <h1><?php the_title(); ?></h1>
</section>

<div class="container content">

    <div class="wp-block-columns">
        <div class="wp-block-column" style="flex-basis:66.66%">

            <?php the_content(); ?>
            <p>&nbsp;</p><p>&nbsp;</p>

            <?php   
                $form_id = RGFormsModel::get_form_id("Download Remodeling Guide");
                gravity_form( $form_id,  false,  false,  false,  null,  true,  1,  true ); 
            ?>

        </div>

        <div class="wp-block-column" style="flex-basis:33.33%">
            <?php
                $post_meta = get_post_meta(get_the_ID(), 'campaign_subphoto', true);
                $img = ($post_meta != "") ? wp_get_attachment_image_src($post_meta, 'full')[0] : 'https://www.dreammaker-remodel.com/wp-content/uploads/2020/08/remodeling-guide-graphic-template-1.png';
            ?>
            <figure class="wp-block-image size-large"><img src="<?php echo $img; ?>"></figure>
        </div>
    </div>

    <p></p>
    <p style="float:left"> </p>
    <p></p>
</div>

<a id="dlremodel" target="_blank" style="display:none" href="http://www.dreammaker-remodel.com/wp-content/uploads/pdf/5-Common-Remodeling-Mistakes-Home-Remodeling-Guide-Final-2020.pdf" download>remodel</a>
<div class="landing-reviews">
    <?php get_template_part('template-parts/static-landing-reviews'); ?>
</div>
