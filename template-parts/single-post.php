<?php use Surepress\Functions\Common as Common;?>
<?php use Surepress\Functions\Assets as Assets;?>
<?php the_title( '<h2>', '</h2>' ); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

	<div class="entry-content">
		<hr>
		<div class="featured-img">
			<?php if(has_post_thumbnail()){ 
				the_post_thumbnail('post-single');
			}else{
				Common\default_thumbnail();
			}?>
		</div><hr>
		<div class="content">
			<?php the_content();?>
		</div>
	</div>
	<footer class="entry-footer">
		<span class="cat-tags-links">
			<span class="cat-links"><i class="fa fa-folder-open"></i><?php echo get_the_category_list(", ", 'single', ''); ?></span>
		</span>
		<span class="blog-share">
			SHARE THIS POST:
			<ul>
				<li><a class="share_popup"  href="http://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>"><i class="fa fa-facebook"></i></a></li>
				<li><a class="share_popup"  href="https://twitter.com/share?url=<?php echo get_the_title(); ?>&text=<?php echo get_the_title(); ?>&hashtags=DreamMakerRemodeling"><i class="fa fa-twitter"></i></a></li>
				<li><a class="share_popup" href="https://pinterest.com/pin/create/bookmarklet/?media=<?php echo ( (has_post_thumbnail()) ? get_the_post_thumbnail_url() : Assets\asset_path('images/dream-kitchen.jpg') ); ?>&url=<?php echo get_the_permalink(); ?>&description=<?php echo get_the_title(); ?>"><i class="fa fa-pinterest"></i></a></li>
			</ul>
		</span>
	</footer>
	<nav class="post-links">
		<?php previous_post_link('<span class="prev"><i class="fa fa-angle-left"></i> %link</span>', 'Previous Post'); ?>
		<?php next_post_link('<span class="next">%link <i class="fa fa-angle-right"></i></span>', 'Next Post'); ?> 
	</nav>
</article>
	<?php get_template_part('template-parts/section/section', 'blog-recent-posts' ); ?>
	 
