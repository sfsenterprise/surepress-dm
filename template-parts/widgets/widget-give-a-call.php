<?php use Surepress\Functions\Common as Common; ?>
<?php $mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($wp_query->nap->microsite_fields)['mongoose_phone']);?>
<?php $microsite_phone = preg_replace('/[^0-9]/', '', $wp_query->nap->microsite_phone);?>

<p class="give-a-call"><span>Give Us a Call! </span><a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $microsite_phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num)) ? Common\formatPhone($mongoose_num):$wp_query->nap->microsite_phone; ?></a></p>

