<ul class="social-media-case-study">
	<li><a class="fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( get_permalink() ); ?>" target="_blank"></a></li>
	<li><a class="pn" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode( get_permalink() ); ?>&media=<?php echo urlencode( get_the_post_thumbnail_url( get_the_ID(),'full') ); ?>&description=<?php echo urlencode(strip_tags( get_the_excerpt() )); ?>" target="_blank"></a></li>
	<!--<li><a class="hz" href=""></a></li>-->
	<li><a class="li" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode( get_permalink() ); ?>&title=<?php echo urlencode(substr(get_the_title(), 0, 200)); ?>&summary=<?php echo urlencode( substr(strip_tags(get_the_excerpt()),0,255) ); ?>&source=<?php echo urlencode(get_bloginfo('name')); ?>" target="_blank"></a></li>
</ul>