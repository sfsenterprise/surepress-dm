<?php use Surepress\Functions\Microsites\Microsite as Microsite; ?>
<ul class="social-media"> 
	<?php if( isset( Microsite::social()['social'] ) ) : ?>
		<?php foreach ( Microsite::social()['social'] as $key => $value ) :  ?>
			<?php if($value) : ?>			
				<li>
					<a target="_blank" class="fa <?php echo str_replace('url_', 'fa-', $key); ?>" href="<?php echo $value; ?>" title="<?php echo str_replace('url_', '', $key); ?>"><span><?php echo str_replace('url_', '', $key); ?></span></a>
				</li>
			<?php endif; ?>			
		<?php endforeach; ?>
	<?php endif; ?>	
</ul>