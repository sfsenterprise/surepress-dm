<?php  use Surepress\Functions\Common as Common; ?>

<div class="intro">
    <?php /*
	<h2><?php the_title(); ?></h2>
    <?php //the_post_thumbnail('services-intro', array( 'class' => 'alignright' )); ?>
    */ ?>
	<?php the_content(); ?>
</div>
<p>&nbsp;</p>
<div class="services-container mobile  hidden-md hidden-lg hidden-xl">
    <div class="panel-group" id="services-inner-accordion-mobile">
        <div class="panel panel-default">
        </div>
    </div>
</div>


 <ul class="nav nav-tabs services-accordion-items hidden-xs hidden-sm nav-justified">
<?php if ( count( Common\get_service_children( get_the_ID() ) ) > 0  ) : ?>
    <?php foreach ( Common\get_service_children(get_the_ID()) as $k => $child ) :  ?>  
            <li class="<?php echo ($k < 1) ? 'active' : ''; ?>">
                <a href="#tab-<?php echo $child->ID; ?>" data-toggle="tab" data-id="<?php echo $child->ID; ?>" > 
                    <?php echo get_the_post_thumbnail( $child->ID, 'thumbnail' ); ?>
                    <?php echo $child->post_title;  ?>
                </a>
            </li>
    <?php endforeach; ?>
<?php endif; ?>
</ul>

<?php if ( count( Common\get_service_children( get_the_ID() ) ) > 0  ) : ?>
     <div class="tab-content services-accordion-items hidden-xs hidden-sm"> 
    <?php foreach ( Common\get_service_children(get_the_ID()) as $k => $child ) :  ?>  
        <div class="tab-pane <?php echo ($k < 1) ? 'active' : ''; ?>" id="tab-<?php echo $child->ID; ?>">
            
            <?php /*
            <figure>
                <?php echo get_the_post_thumbnail($child->ID, array(1100,565)); ?>
                 probably not needed due to live data has this on the_content - Japol 
                <?php if( !empty(get_the_post_thumbnail_caption( $child->ID)) ): ?>  
                    <figcaption><i><?php echo get_the_post_thumbnail_caption( $child->ID); ?></i></figcaption> 
                <?php endif; ?>
                
            </figure>
            

             <?php if(get_field('service_gallery_imgs',$child->ID) ): ?>
                <a href="<?php echo get_permalink( get_field('service_gallery_imgs',$child->ID)[0]) ; ?>" class="pull-right">
                <?php echo get_the_post_thumbnail( get_field('service_gallery_imgs', $child->ID )[0], array(282,122) ); ?> 
                <span>View Gallery</span>
            </a>
            <?php endif; ?>
            */ ?>
            
            <article>
                <?php echo  wpautop($child->post_content); ?>
            </article>

            <?php 
                $cat_id = (get_field('service_gallery', $child->ID) ? get_field('service_gallery', $child->ID) : '');
                $cover_img = Common\get_portfolio_cover($cat_id,'thumbnail');
            ?>
            <?php if($cat_id): ?>
                <div class="clearfix" align="right">
                    <a href="<?php echo get_category_link($cat_id); ?>"class="child_gallery">
                        <?php echo $cover_img;?>
                        <span>View Gallery</span>
                    </a>
                </div>    
            <?php endif;?>

        </div>
    <?php endforeach; ?>  
    </div>
<?php endif; ?>


<?php 
    //$service_gallery = ( get_term_by( 'slug', $post->post_name, 'portfolio_categories' ) ) ? get_term_by( 'slug', $post->post_name, 'portfolio_categories' )->term_id : '';
    $service_gallery = (get_field('service_gallery', get_the_ID()) ? get_field('service_gallery', get_the_ID()) : '');
    $service_cover_img = Common\get_portfolio_cover($service_gallery,'thumbnail');
?>
<?php if($service_gallery && count( Common\get_service_children( get_the_ID() ) ) < 1 ): ?>
    <div class="clearfix">
        <a href="<?php echo get_category_link($service_gallery); ?>" class="child_gallery">
            <?php echo $service_cover_img;?>
            <span>View Gallery</span>
        </a>
    </div>    
<?php endif;?>  
<?php if( isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_case_studies']) && maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_case_studies'] == 1) :?>
    <?php 
        //get_template_part('template-parts/section/section', 'related-case-studies'); 
        get_template_part('template-parts/section/section', 'services-case-studies'); 
    ?>
<?php endif; ?>