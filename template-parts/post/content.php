<?php  get_template_part('template-parts/section/section', 'blog-search');  ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<header class="entry-header">
		<?php if ( is_single() ) {
				the_title( '<h6 class="entry-title">', '</h6>' );
			}
		?>
	</header>
	
	<div class="entry-content">
		<hr><?php the_post_thumbnail(); ?><hr>
		<?php the_content();?>
	</div>

	<footer class="entry-footer">
	<?php 
		$categories_list = get_the_category_list(", ", 'single', '');
			if ( 'post' === get_post_type() ) {
				if ( ( $categories_list ) || $tags_list ) {
					echo '<span class="cat-tags-links">';
						if ( $categories_list ) {
							echo '<span class="cat-links"><i class="fa fa-folder-open"></i>' . $categories_list . '</span>';
						}
					echo '</span>';
			?>
				<span class="blog-share">
					Share this post:
					<ul>
						<li><a class="#" href=""><i class="fa fa-facebook"></i></a></li>
						<li><a class="#" href=""><i class="fa fa-twitter"></i></a></li>
						<li><a class="#" href=""><i class="fa fa-pinterest"></i></a></li>
						<li><a class="#" href=""><i class="fa fa-youtube"></i></a></li>
					</ul>
				</span>
			<?php 
				}
			}
		?>
	</footer>
</article>
