<?php 
	use Surepress\Functions\Common as Common;
?>
<div class="container-fluid">
	<div class="row">

		<div class="home_top_slide">
			<?php get_template_part('template-parts/section/section', 'home-grey-categories' ); ?>
			<?php get_template_part('template-parts/section/section', 'home-slider' ); ?>
		</div>	

		<?php get_template_part('template-parts/section/section', 'home-category-thumbs' ); ?>
	</div>

	<?php if( !Common\is_main() ) : ?>	
		<p>&nbsp;</p>
		<div class="row">	
			<?php get_template_part('template-parts/section/section', 'our-team' ); //FOR MICROSITES ?>
			<?php get_template_part('template-parts/section/section', 'home-reviews' ); ?>
		</div>		
	<?php endif; ?>

	<?php get_template_part('template-parts/section/section', 'purple-call-to-action' ); ?>

	<?php if( !Common\is_main() ) : ?>
			<?php if( isset(maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_events']) && maybe_unserialize($wp_query->nap->microsite_fields)['microsite_show_events'] == 1) :?>
				<?php get_template_part('template-parts/section/section', 'events' ); //FOR MICROSITES ?>
			<?php endif; ?>
	<?php endif; ?>			

	<?php if( Common\is_main() ) : ?>
	<div class="row">	
		<?php get_template_part('template-parts/section/section', 'home-values' ); ?>
		<?php get_template_part('template-parts/section/section', 'home-reviews' ); ?>
	</div>	
	<?php endif; ?>			

	<?php get_template_part('template-parts/section/section', 'home-process' ); ?>

	<?php if( !Common\is_main() ) : ?>
	<div class="row" id="hours_map">	
		<?php get_template_part('template-parts/section/section', 'home-hours' ); ?>
		<?php get_template_part('template-parts/section/section', 'home-map' ); ?>
	</div>	
	<?php endif; ?>		

	<?php get_template_part('template-parts/section/section', 'blog' ); ?>
		
</div>
