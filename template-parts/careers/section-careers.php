<?php
// $args = array(
//     'posts_per_page' => -1,
//     'post_type' => 'careers',
// );
$args = array(
    'posts_per_page' => -1,
    'post_type' => 'job-opportunities',
);
$careers = new WP_Query( $args );
if ( $careers->have_posts() ) {
    while ( $careers->have_posts() ) : $careers->the_post();
        $careermeta = get_post_meta(get_the_ID());
?>
<div class="case-studies panel-group">
    <div class="animated slideInUp panel panel-default">
        <div class="panel-body">
            <table class="col-lg-12 table">
                <tbody>
                    <tr>
                        <td class="col-lg-3"  scope="row"><strong>Position</strong></td>
                        <td class="col-lg-9"><?php the_title(); ?></td>
                    </tr>
                    <?php if(isset($careermeta['salaryrange'][0]) && $careermeta['salaryrange'][0] != '') : ?>
                    <tr>
                        <td class="col-lg-3"  scope="row"><strong>Salary</strong></td>
                        <td class="col-lg-9"><?php echo $careermeta['salaryrange'][0]; ?></td>
                    </tr>
                <?php endif; ?>
                    <tr>
                        <td class="col-lg-3"  scope="row"><strong>Essential Duties and Responsibilities</strong></td>
                        
                        <td class="col-lg-9">
                        <?php
                        if(isset($careermeta['responsibility'][0])){
                            if(strpos($careermeta['responsibility'][0],'<!--more-->') > 0){
                                echo substr($careermeta['responsibility'][0], 0, strpos($careermeta['responsibility'][0],'<!--more-->')).'(To see more click View Details)';
                            }else{
                                echo $careermeta['responsibility'][0];
                            }
                        }
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3"  scope="row"><strong>Education and Experience</strong></td>
                        <td class="col-lg-9">
                            <?php
                            if(isset($careermeta['educ_exp'][0])){
                                if(strpos($careermeta['educ_exp'][0],'<!--more-->') > 0){
                                    echo substr($careermeta['educ_exp'][0], 0, strpos($careermeta['educ_exp'][0],'<!--more-->')).'(To see more click View Details)';
                                }else{
                                    echo $careermeta['educ_exp'][0];
                                }
                            }
                            ?>        
                        </td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">&nbsp;</td>
                        <td class="col-lg-9">
                            <br>
                            <a class="btn btn-info" href="<?php the_permalink(); ?>">View Details </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
    endwhile;
}else{
    //echo 'No Jobs Posted';
?>
<div class="row">
    <div class="col-lg-6">
        <img src="<?php bloginfo('template_url') ?>/dist/images/Screenshot_1.png">
    </div>
    <div class="col-lg-6">
        <?php
        if(get_current_blog_id() == 1){
        ?>
        <p>Thank you for visiting our Career Page at DreamMaker Bath &amp; Kitchen. Unfortunately, we do not have any job openings at this time. We appreciate your interest in joining the team and encourage you to check back with us for available career opportunities in the future.</p>
        <p>If you’re interested in owning a DreamMaker franchise, click <a href="https://www.dreammakerfranchise.com/" target="_blank" style="font-weight: bold;">here</a> to learn more</p>
        <?php
        }else{
        ?>
        <p>Thank you for visiting our Career Page at DreamMaker Bath &amp; Kitchen of <?php bloginfo('blogname') ?>. Unfortunately, we do not have any job openings at this time. We appreciate your interest in joining the team and encourage you to check back with us for available career opportunities in the future.</p>
        <p>If you’re interested in owning a DreamMaker franchise, click <a href="https://www.dreammakerfranchise.com/" target="_blank" style="font-weight: bold;">here</a> to learn more</p>
        <?php
        }
        ?>
    </div>
</div>
<?php
}