<?php
$careermeta = get_post_meta(get_the_ID()); 
?>
<div id="career-single">
	<h2><?php the_title(); ?></h2>
	<?php 
	if(isset($careermeta['salaryrange'][0]) && $careermeta['salaryrange'][0] != ''):
	?>
	<div class="salary" style="font-size: 18px; font-weight: bold; margin-bottom: 20px;">
		Salary: <?php echo $careermeta['salaryrange'][0]; ?>
	</div>
	<?php endif; ?>
	<div class="description">
		<?php the_content(); ?>
	</div>
	<ul class="nav nav-tabs services-accordion-items hidden-xs hidden-sm nav-justified">
	    <li class="active">
	        <a href="#tab-1000" data-toggle="tab" data-id="1000">Essential Duties and Responsibilities</a>
	    </li>

	    <li class="">
	        <a href="#tab-1002" data-toggle="tab" data-id="1002">Education and Experience</a>
	    </li>
	    <li class="">
	        <a href="#tab-1005" data-toggle="tab" data-id="1005">Other Qualifications</a>
	    </li>
	    <li class="">
	        <a href="#tab-1006" data-toggle="tab" data-id="1006">Who Do You Report To?</a>
	    </li>
	</ul>
	<div class="tab-content services-accordion-items hidden-xs hidden-sm"> 
	    <div class="tab-pane active" id="tab-1000">         
	        <article>
	            <?php echo isset($careermeta['responsibility'][0]) ? $careermeta['responsibility'][0] : '' ?>
	        </article>
	    </div>
	    <div class="tab-pane " id="tab-1002">
	        <article>
	            <?php echo  isset($careermeta['educ_exp'][0]) ? $careermeta['educ_exp'][0] : '' ?>
	        </article>
	    </div>
	    <div class="tab-pane " id="tab-1005">          
	        <article>
	            <?php echo  isset($careermeta['qualifications'][0]) ? $careermeta['qualifications'][0] : '' ?>
	        </article>  
	    </div>
	    <div class="tab-pane " id="tab-1006">          
	        <article>
	            <?php echo  isset($careermeta['reportto'][0]) ? $careermeta['reportto'][0] : '' ?>
	        </article>  
	    </div>
	</div>
</div>