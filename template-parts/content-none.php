
<div class="container">
	<span class="title">404</span><br>
	<span class="subtitle">page was not found</span><br>
	<span class="text">You may have mistyped the address or the page may have moved.</span>
	<span class="buttons">
		<a class="btn btn-info" href="<?php echo get_site_url(); ?>">Back to homepage</a>
	<a class="btn btn-primary" href="<?php echo get_site_url(); ?>/contact-us">Contact us</a>
	</span>
	<span class="subtitle"> Or </span>
	<form  role="search" method="get" action="<?php echo get_site_url(); ?>/">
		<fieldset class="form-group">
			<input type="text" name="s" class="form-control" id="FirstName"  placeholder="Search this site">
			<button type="submit" class=""><i class="fa fa-search"></i></button>
		</fieldset>

	</form>
</div>