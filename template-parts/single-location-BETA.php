<?php 
	use Surepress\Functions\Locations as Locations; 
	use Surepress\Functions\Common as Common; 
?>
<h1><?php the_title(); ?></h1>
<article>
	<?php the_content(); ?>
</article>


<section class="single">
	<div class="row">	

		<div class="cities">
			<?php /*this needs to be run @ 1 loop only --Japol TODO */ ?>
			<?php /*
			<div class="panel-group">			
			<?php foreach($wp_query->locations_nap[ get_the_ID() ]  as  $key => $city) : ?>
				<?php if( !is_numeric($key) ) continue; ?>
				<?php foreach($city as $blogId => $microsite) : ?>
					<div class="panel panel-default">
						<div class="panel-body">
							<strong><?php echo $microsite['Nap_Name']; ?></strong>
							<address>
								<i class="fa fa-map-marker"></i><?php echo $microsite['Nap_Street']; ?> <?php echo $microsite['Nap_Street2']; ?><br>
								<i class="fa fa-map-marker"></i><?php echo $microsite['Nap_City']; ?>, <?php echo $microsite['Nap_State']; ?> <?php echo $microsite['Nap_Zip']; ?> <br>
								<i class="fa fa-phone"></i><a href="tel:<?php echo $microsite['Nap_Phone']; ?>"><?php echo Common\formatPhone($microsite['Nap_Phone']); ?></a><br>
								<i class="fa fa-globe"></i><a href="<?php echo get_site_url( $blogId ); ?>">Visit Website</a>
							</address>
					    </div>
						<div class="panel-footer">
					    	<span class="col-sm-9">
					    		<strong>Service Areas</strong>
					    		<p><?php echo (unserialize($microsite['Nap_fields'])['area_served_1'] !=  "") ?  unserialize($microsite['Nap_fields'])['area_served_1'] : "N/A"; ?> </p>
					    	</span>
					    	<a class="col-sm-3 btn btn-link">View All</a>
					    	<div class="clearfix"></div>
					    </div>
					</div>			
				<?php endforeach; ?>	
			<?php endforeach; ?>	
			</div>
		</div>
		*/ ?>
		<?php //echo Locations\google_map_data(); ?>
		<div class="map">
			<div id="sfl_map" style="width: 100%; height: 500px;"></div>
		</div>

	</div>	
</section>	



<script src="http://d3js.org/topojson.v1.min.js"></script>

  <script>


   var geoJsonObject;
   var thejson;

   jQuery(document).ready(function(){



  var mapOptions = {
    zoom: 5,
    center: new google.maps.LatLng(38.284335, -120.833818),
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map=new google.maps.Map( jQuery('#sfl_map')[0], mapOptions);

//county.json is a topojson file

  jQuery.getJSON("http://www.dreammaker-remodel.com/wp-content/themes/surepress-dm/functions/geo/state_province.geo.json", function(data){
        geoJsonObject = topojson.feature(data, data.features.type)
        map.data.addGeoJson(geoJsonObject); 
      }); 



  });//end document ready


 </script>

