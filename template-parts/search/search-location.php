<?php
	use Surepress\Functions\Common as Common;
	use Surepress\Functions\Assets as Assets;
	use Surepress\Functions\Locations as Locations;
	//$results_num = count($wp_query->locations_nap);
	$results_num = Locations\nap_count($wp_query->locations_nap) - count($wp_query->locations_nap);
?>
<?php //get_template_part('template-parts/partials/search', 'locations'); ?>

<?php if ( $results_num > 0 ) : ?>
	<div id="sfl_map" style="width: 100%; height: 500px;" data-pin="<?php echo Assets\asset_path('images/map-icon.png') ?>"></div>
	<p>
		<em>
		You're viewing (<?php echo $results_num; ?>) 
		result<?php echo ( $results_num > 1) ? 's' : ''; ?> 
		for your search “<?php echo ($_GET['search'] === "my_location") ? "using your location": $_GET['search']; ?>”
		</em>
	</p>
	<?php  include(locate_template('template-parts/partials/location-city.php')); ?>
<?php else : ?>
	<p><em>No locations found.	</em></p>
<?php endif; ?>	
