<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Microsites;

global $post;
$option = get_option("dm_settings"); 
$url = 'http://www.guildquality.com/ws/wpwidget.php?mp=' . (isset($option['guild_quality']['guild_member']) ? $option['guild_quality']['guild_member'] : '');
$url_get_contents_return = wp_remote_get($url);
$json = wp_remote_retrieve_body($url_get_contents_return);
$data = json_decode($json, TRUE);
$v3_api = (isset($option['guild_quality']['review_code']) && !empty($option['guild_quality']['review_code'])) ? true : false;
$v3_api_class = ($v3_api) ? 'col-12' : 'col-xs-12 col-sm-6 col-sm-pull-6';
?>
<span class="col-xs-12">

<h2><?php echo isset($option['guild_quality']['review_title']) ? $option['guild_quality']['review_title'] : '' ;?></h2>	
</span>

<ul>
	<li class="guild-reviews"><a class="btn" href="#testimonial1">Guild Quality Reviews</a></li>
	<li class="feedback"><a class="btn" href="#testimonial2">Feedback Map</a></li>
	<li><a class="btn " href="#testimonial3">Customer Testimonials</a></li>
	<?php 
		$leads =  get_blog_option(get_current_blog_ID(), 'dm_settings'); 
		if( isset($leads['yext_key']['firstparty']) && !empty($leads['yext_key']['firstparty']) ): ?>
			<li><a class="btn" href="#testimonial-4">1st Party Reviews</a></li>
	<?php endif; ?>
</ul>
<section class="guild">
	<a  class="hidden-anchor" id="testimonial1"></a>
	<?php if(!$v3_api) { ?>	
		<figure> 
			<img src=" <?php echo Assets\asset_path('images/GuildQuality_Logo.png'); ?> " alt="Guildquality Member"  />
			<figcaption>
				<span class="stars">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
				</span>
				<h6><?php echo (isset($data['Name']) ? $data['Name'] : 'DreamMaker Bath & Kitchen'); ?></h6>
			</figcaption>
		</figure>
	<?php } ?>
	<p class="<?php echo $v3_api_class; ?>">
		<?php if( Common\isCustomReviewsMicrosite() ): ?>
			At DreamMaker, we work hard to provide an exceptional experience for each of our clients that pursues a remodeling project with us.  As we regularly evaluate and measure our performance, client reviews provide the necessary feedback that both encourage our team in the things we are doing well and  forces us to address the things that need tweaked.  We welcome reviews via Guild Quality, a third party review company, Google, Facebook and on our website.<br></br>
			If you are an existing client, thank you for being a part of our ongoing mission. If you are just researching us for the first time, we hope the reviews below offer you a better understanding of the DreamMaker experience.
		<?php elseif( $post->post_content ) : ?>
			<?php echo $post->post_content; ?>
		<?php else : ?>
			Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.
		<?php endif; ?>
	</p>

	<?php if($v3_api) { ?>	
		<h1>&nbsp;</h1>
		<?php echo $option['guild_quality']['review_code'];?>
	<?php } elseif(isset($data['Feedback'])) {

			if(array_search('review', array_column($data['Feedback'], 'type')) !== FALSE && isset($option['guild_quality']['show_review']) && $option['guild_quality']['show_review'] == 1) {

				foreach ($data['Feedback'] as $key => $value) {  ?>
					<?php if($value['type'] == 'review'): ?>
						<div class="col-xs-12 entry">
							<div class="col-xs-6">
								<strong><?php echo (isset($value['reviewer']) ? $value['reviewer'] : '');?></strong><br>
								<i> <?php echo (isset($value['city']) ? $value['city'] : '');?>, <?php echo (isset($value['state']) ? $value['state'] : '');?></i>
							</div>
							<div class="col-xs-6">
								<span class="stars">
									<?php if(isset($value['rating'])) : ?>
										<?php for($i = 0;$i < $value['rating'];$i++) { ?>
											<i class="fa fa-star"></i>
										<?php } ?>
									<?php endif; ?>
								</span>
							</div>
							<div class="col-xs-12">
								<p><i class="quote">&quot;</i><?php echo (isset($value['review']) ? $value['review'] : '');?></p>
								<a href="<?php echo $value['url'];?>" target="_blank" class="btn">Read More</a>
							</div>
						</div>
					<?php endif; ?>
				<?php }
			}
			if(array_search('comment', array_column($data['Feedback'], 'type')) !== FALSE && isset($option['guild_quality']['show_comments']) && $option['guild_quality']['show_comments'] == 1) {
				foreach ($data['Feedback'] as $key => $value) {  ?>
					<?php if($value['type'] == 'comment'): ?>
						<div class="col-xs-12 entry">
							<div class="col-xs-6">
								<strong><?php echo (isset($value['question']) ? $value['question'] : '');?></strong><br>
								<i> <?php echo (isset($value['city']) ? $value['city'] : '');?>, <?php echo (isset($value['state']) ? $value['state'] : '');?></i>
							</div>
							<div class="col-xs-12">
								<p><i class="quote">&quot;</i><?php echo (isset($value['comment']) ? $value['comment'] : '');?></p>
								<a href="<?php echo $value['url'];?>" target="_blank" class="btn">Read More</a>
							</div>
						</div>
					<?php endif; ?>
				<?php }
			}

		?>	
		<?php if(isset($option['guild_quality']['guild_member']) && !empty($option['guild_quality']['guild_member'])) : ?>	
			<?php if(array_search('review', array_column($data['Feedback'], 'type')) !== FALSE && isset($option['guild_quality']['show_review']) && $option['guild_quality']['show_review'] == 1 || array_search('comment', array_column($data['Feedback'], 'type')) !== FALSE && isset($option['guild_quality']['show_comments']) && $option['guild_quality']['show_comments'] == 1) :  ?>
				<span class="col-xs-12"><a target="_blank" href="<?php echo $option['guild_quality']['guild_member'];?>&tab=reviews#more-reviews" class="btn read-more">Read more reviews & comments <i class="fa fa-arrow-right"></i></a></span>
			<?php else: ?>
				<span class="col-xs-12 no-guild-reviews">
					<h3>Guild Quality Reviews coming soon</h3>
				</span>
			<?php endif; ?>
		<?php else: ?>
			<span class="col-xs-12 no-guild-reviews">
				<h3>Guild Quality Reviews coming soon</h3>
			</span>
		<?php endif; ?>

		<?php }else{?>
			<span class="col-xs-12 no-guild-reviews">
				<h3>Guild Quality Reviews coming soon</h3>
			</span>
		<?php }	?>
</section>

<section class="feedback">
	<a class="hidden-anchor" id="testimonial2"></a>
	
	<?php if(isset($option['guild_quality']['map_code']) && !empty($option['guild_quality']['map_code'])): ?>
		<?php if( Common\isCustomReviewsMicrosite() ): ?>
			<h6>DreamMaker is Trusted Nationally</h6>
			<p>This map displays recent work and reviews from DreamMaker locations across the country. Zoom in and out to see different cities and neighborhoods of the map. Click on gray pins to see the location of recent work, and blue pins to see reviews and comments from DreamMaker customers.</p>
		<?php elseif( isset($option['guild_quality']['map_text']) && !empty($option['guild_quality']['map_text']) ) : ?>
			<?php echo $option['guild_quality']['map_text']; ?>
		<?php else : ?>
			<?php $main_option = get_blog_option(1, "dm_settings");  ?>
			<?php if( !empty($main_option['guild_quality']['map_text']) ) : ?>
				<?php echo $main_option['guild_quality']['map_text']; ?>
			<?php else : ?>
				<h6>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h6>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>
			<?php endif; ?>
		<?php endif; ?>

		<div class="guild-quality-map">
			<?php echo $option['guild_quality']['map_code'];?>
		</div>
	<?php endif; ?>
	<hr>
</section>
<section class="wordpress" >

	<a class="btn create-review" data-toggle="collapse" data-target="#create-review" <?php echo (isset($_GET['submission'])) ? 'href="'.get_permalink( get_the_ID() ).'"' : '' ;?>>Leave a Testimonial</a>
	<a class="hidden-anchor" id="reviews-anchor"></a>
	<div id="create-review" class="collapse">
		<h6>Submit your review at DreamMaker Bath & Kitchen</h6>
		<?php $form = \Surepress\Functions\Microsites\Microsite::get_form('Reviews Form'); ?>
		<?php echo do_shortcode('[gravityform id="'.$form['form_id'].'" title="false" description="false" ajax="false" tabindex=100]'); ?>
	</div>
	<a class="hidden-anchor" id="testimonial3"></a>
	<?php
		$current_page = 1;
		$per_page = 10;

		$fields = RGFormsModel::get_form_meta( $form['form_id'] );
		if(isset($fields) && !empty($fields)) :
			$response       = $search_criteria = $paging = array();
			$search_criteria['status'] = 'active';
        	$total_count    = GFAPI::count_entries( $form['form_id'], $search_criteria );
        	$paging         = array( 'offset' => $current_page - 1, 'page_size' => $per_page );
			$sorting = array( 'key' => 'date_created', 'direction' => 'DESC' );
			//$reviews = GFAPI::get_entries($form['form_id'], array(), $sorting);
			$reviews = GFAPI::get_entries($form['form_id'], $search_criteria, $sorting, $paging, $total_count);
			$arr = array();
			if(isset($reviews) && !empty($reviews)) :?>
				<h6><?php echo (isset($data['Name']) ? $data['Name'] : 'DreamMaker Bath & Kitchen'); ?></h6>
			<?php 
				foreach($reviews as $review):
					if($review["status"] != "inactive"):
						foreach ($fields['fields'] as $key => $value) :
							if($value['label'] == 'Rating'){
								$arr['rating'] = $review[$value['id']];
							}else if($value['label'] == 'Review'){
								$arr['review'] = $review[$value['id']];
							}else if($value['label'] == 'First Name'){
								$arr['first_name'] = $review[$value['id']];
							}else if($value['label'] == 'Last Name'){
								$arr['last_name'] = $review[$value['id']];
							}else if($value['label'] == 'City'){
								$arr['city'] = $review[$value['id']];
							}else if($value['label'] == 'State'){
								$arr['state'] = $review[$value['id']];
							}
						endforeach;
					?>
					<div class="col-xs-12 entry">
						<div class="col-xs-12 col-sm-2">
							<img class="col-lg-6" src="<?php echo Assets\asset_path('images/review-quote.png') ?>" alt="Review Quote"/>
							<span><?php echo (isset($review['date_created']) ? date("M j, Y", strtotime($review['date_created'])) : ''); ?></span>
							<span class="stars">
								<?php if(isset($arr['rating']) && is_numeric($arr['rating'])) : for($i =0; $i < $arr['rating']; $i++) : ?>
									<i class="fa fa-star"></i>
								<?php endfor; endif; ?>
							</span>
						</div>
						<div class="col-xs-12 col-sm-10">
							<i><?php echo (isset($arr['review']) ? stripslashes_deep($arr['review']) : '');?></i><br>
							<strong>- <?php echo (isset($arr['first_name']) ? $arr['first_name'] : '');?> <?php echo (isset($arr['last_name']) ? $arr['last_name'][0] : '');?>. | <?php echo (isset($arr['city']) ? $arr['city'] : '');?>, <?php echo (isset($arr['state']) ? $arr['state'] : '');?></strong>
						</div>
					</div>
					<?php	
					endif;
				endforeach;
			endif; 
		endif;?>

		<div id="reviewLoop"></div>

		<?php if ($total_count >= ($per_page * $current_page) ) : ?>
			<p>&nbsp;</p>
			<p align="center">
				<button class="btn btn-default" id="moreReviews" data-form="<?php echo $form['form_id']; ?>" data-ajaxurl="<?php echo admin_url('admin-ajax.php'); ?>" data-currentpage="<?php echo $current_page; ?>" data-perpage="<?php echo $per_page; ?>" data-total="<?php echo $total_count; ?>">Show More Testimonials</button> <img src="/wp-admin/images/loading.gif" id="reviewLoader" style="display: none" /> <br>
				<small>Showing <span id="moreReviewsCount"><?php echo $current_page * $per_page; ?></span> of <?php echo $total_count; ?> reviews.</small>
			</p>
		<?php endif; ?>	
</section>
<?php if( isset($leads['yext_key']['firstparty']) && !empty($leads['yext_key']['firstparty']) ): ?>
<section class="yextWrap">
	<a class="hidden-anchor" id="testimonial-4"></a>
	<hr>
	<h6>1st Party Reviews</h6>
	<?php 	
		$leads =  get_blog_option(get_current_blog_ID(), 'dm_settings'); 
		if( isset($leads['yext_key']['firstparty']) && !empty($leads['yext_key']['firstparty']) ):
			echo $leads['yext_key']['firstparty'];
		endif; 
	?>
</section>
<?php endif; ?>
