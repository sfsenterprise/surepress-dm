<h3><?php the_title(); ?> test</h3>
<div class="gallery_container">	
	<?php if(get_field('gallery_after') ): ?>
			<?php foreach(get_field('gallery_after', 0) as $image) : ?>
					<div class="col-xs-12 col-sm-4">
						<a href="<?php echo wp_get_attachment_url($image); ?>" data-fancybox="gallery" data-caption="<?php echo get_post_meta($image, '_wp_attachment_image_alt', true); ?>" title="<?php echo get_post_meta($image, '_wp_attachment_image_alt', true); ?>">
							<?php echo wp_get_attachment_image( $image  , array(421,461) ); ?>
						</a>	
					</div>
			<?php endforeach; ?>
	<?php else : ?>
			No images found.
	<?php endif; ?>
</div>	

<?php //get_template_part('template-parts/section/section', 'related-case-studies'); ?>

