<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
?>
	<div class="posts-wrapper">
		<?php if(has_post_thumbnail()){ 
			the_post_thumbnail();
		}else{
			Common\default_thumbnail();
		}?>
		<div class="post-content">
			<h6><?php the_title(); ?></h6>
			<hr>
			<p><?php echo wp_trim_words( get_the_content(), 70,'[...]' ); ?></p>
			<a href="<?php echo get_the_permalink();?>">Read More </a>
		</div>
		<div>
		</div>
	</div>

