<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common; ?>
<h2><?php the_title(); ?></h2>
<?php $meta = get_post_meta($post->ID);  ?>

<div class="case">
	<?php if(!empty($post->post_title)):?>
		<div class="left"><strong>Project Name</strong></div>
		<div class="right"><?php echo $post->post_title; ?></div>
	<?php endif; ?>

	<?php if(!empty($post->post_excerpt)):?>
	<div class="left"><strong>Project Summary</strong></div>
	<div class="right"><?php echo $post->post_excerpt; ?></div>
	<?php endif; ?>

	<?php if(!empty($post->post_content)):?>
	<div class="left"><strong>Project Story</strong></div>
	<div class="right"><?php echo $post->post_content;  ?></div>
	<?php endif; ?>


	<div class="left"><strong>Project Category</strong></div>
	<div class="right"><?php echo ($meta['category'][0] == -1) ? 'Uncategorized' : $meta['category'][0]; ?></div>

	<div class="left"><strong>Job Category</strong></div>
	<div class="right"><?php  echo (isset($meta['job_categories'][0])) ? implode(', ', unserialize($meta['job_categories'][0])) : 'Uncategorized'; ?></div>

	<div class="left"><strong>Services Performed</strong></div>
	<div class="right"><?php  echo (isset($meta['services'][0])) ? implode(', ', unserialize($meta['services'][0])) : 'Uncategorized'; ?></div>

	<?php if(!empty($meta['products_used'][0])):?>
	<div class="left"><strong>Products Used</strong></div>
	<div class="right"><?php echo $meta['products_used'][0]; ?></div>
	<?php endif; ?>

	<?php if(!empty($meta['why'][0])):?>
		<div class="left"><strong>Why DreamMaker</strong></div>
		<div class="right"><?php echo $meta['why'][0]; ?></div>
	<?php endif; ?>

	<?php if(!empty($meta['how_remodel'][0])):?>
	<div class="left"><strong>How does the remodel enhance the client's life</strong></div>
	<div class="right"><?php echo $meta['how_remodel'][0]; ?></div>
	<?php endif; ?>

	<?php if(!empty($meta['testimonials'][0])):?>
	<div class="left"><strong>Customer Testimonials</strong></div>
	<div class="right"><?php echo $meta['testimonials'][0]; ?></div>
	<?php endif; ?>

	<?php if(!empty($meta['city'][0])):?>
	<div class="left"><strong>City</strong></div>
	<div class="right"><?php echo $meta['city'][0]; ?></div>
	<?php endif; ?>

	<?php if(!empty($meta['state'][0])):?>
	<div class="left"><strong>State</strong></div>
	<div class="right"><?php echo $meta['state'][0]; ?></div>
	<?php endif; ?>

	<?php if(!empty($meta['who_did'][0])):?>
	<div class="left"><strong>Who did the work</strong></div>
	<div class="right"><?php echo $meta['who_did'][0]; ?></div>
	<?php endif; ?>
</div>

<div class="case-image">
	<?php $blog_name = get_bloginfo('name'); ?>
    <?php $uploads_dir = wp_upload_dir(); ?>
    <div class="slicker" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>
	    <?php if(isset($meta['gallery'])) : ?>
			<?php foreach(maybe_unserialize($meta['gallery'][0]) as $key => $value) : ?>
				<figure>
					<?php if( isset($value["url"]) ):?>
						<img src="<?php echo $value['url']; ?>" alt="<?php echo $blog_name; ?> - <?php echo $value['image']; ?>" />
					<?php else: ?>
						<img src="<?php echo $uploads_dir['baseurl'].'/gallery/'.$value['image']; ?>" alt="<?php echo $blog_name; ?> - <?php echo $value['image']; ?>" />
					<?php endif; ?>
					<?php if(!empty($value['description'])): ?>
						<figcaption><?php echo $value['description']; ?></figcaption>
					<?php endif;?> 
					<?php if(!empty($value['title'])): ?>
					<span class="pin"><div class="inner"><?php echo $value['title']; ?></div></span>
					<?php endif;?> 
				</figure>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<ul class="arrows">
		<li><a class="prev"><i></i></a></li>
		<li><a class="next"><i></i></a></li>
	</ul>

</div>

<h5>Share this project:</h5>
 <?php get_template_part('template-parts/widgets/widget','social-media-case-study'); ?>


