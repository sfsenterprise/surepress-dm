<?php use Surepress\Functions\Common as Common; ?>

<?php 
	$sitemap = array( 
		array( 'Homepage' , get_page_by_title('Homepage'), 'page' ),
		array( 'Contact Us' , get_page_by_title('Contact Us'), 'page' ),
		array( 'Locations' , get_page_by_title('Locations'), 'page' ),
		array( 'About' , get_page_by_title('About'), 'page' ),
		array( 'Our History' , get_page_by_title('Our History'), 'page' ),
		array( 'Our Process' , get_page_by_title('Our Process'), 'page' ),
		array( 'Reviews' , get_page_by_title('Reviews'), 'page' ),
		array( 'Case Studies' , get_post_type_object('case-studies'), 'post-type' ),
		array( 'Our Team' , get_page_by_path('about/our-team'), 'page' ),
		array( 'Offers' , get_post_type_object('offers'), 'post-type' ),
		array( 'Events' , get_post_type_object('events'), 'post-type' ),
		array( 'Code of Values' , get_page_by_title('Code of Values'), 'page' ),
		array( 'Services' , get_post_type_object('services'), 'post-type' ),
		array( 'Galleries' ,get_post_type_object('portfolio'), 'post-type' ),
		array( 'Directions' , get_page_by_title('Directions'), 'page' ),
		array( 'FAQ' , get_post_type_object('faqs'), 'post-type' ),
		array( 'Glossary' ,get_post_type_object('glossary'), 'post-type' ),
		array( 'Blog' ,get_page_by_title('Blog'), 'page' ),
		array( 'Financing Options' ,get_page_by_title('Financing Options'), 'page' ),
		array( 'Service Areas' ,get_page_by_title('Service Areas'), 'page' ),
		array( 'Privacy Policy' ,get_page_by_title('Privacy Policy'), 'page' ),
	);

?>
	<ul>
		<?php foreach($sitemap as $key => $value): ?>
			<?php if( ($sitemap[$key][0] != 'Case Studies' && $sitemap[$key][0] != 'Events' && $sitemap[$key][0] != 'Offers') ):  // exclude hidden post types?> 
				<?php  if ( $sitemap[$key][2] == 'page'): ?>
						<?php if( $key == 0 ): ?>
							 <li><a href="<?php echo get_site_url(); ?>/"><?php echo $sitemap[$key][0]; ?></a></li>
						<?php else: ?>
							<?php if ( !empty($sitemap[$key][1]->post_status) && $sitemap[$key][1]->post_status == 'publish'): ?>
								<li><a href="<?php echo get_permalink( $sitemap[$key][1]->ID ); ?>"><?php echo $sitemap[$key][0]; ?></a></li>
							<?php endif; ?>
						<?php endif; ?>
					
				<?php else: ?>
					<?php if ($sitemap[$key][1]->name =='faqs'): ?>
						<li><a href="<?php echo get_site_url().'/faq/'; ?>"><?php echo $sitemap[$key][0]; ?></a></li>
					<?php elseif ($sitemap[$key][1]->name =='portfolio'): ?>
						<li><a href="<?php echo get_site_url().'/galleries/'; ?>"><?php echo $sitemap[$key][0]; ?></a></li>
					<?php else: ?>	
						<li><a href="<?php echo get_site_url().'/'.$sitemap[$key][1]->name.'/'; ?>"><?php echo $sitemap[$key][0]; ?></a></li>
					<?php endif; ?>
				<?php endif; ?>
					<?php if($key == 12) : ?>
						<ul>
							<?php 	$services = new WP_Query( array( 'post_type' => 'Services', 'order' => 'ASC' , 'orderby' => 'menu_order', 'post_parent'=> 0   ) ); ?>
							<?php if( $services->have_posts() ):	?>
								<?php while ( $services->have_posts() ) :  $services->the_post(); ?>
									<li><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>	
								<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_query(); ?>
						</ul>
					<?php endif; ?>
					<?php if($key == 13) : ?>
						<?php $categories = Common\get_portfolio_categories('portfolio_categories', true); ?> 
						<ul>
						<?php if( !empty($categories)): ?>
							<?php foreach($categories as $category): ?>
										<li><a href="<?php echo get_post_type_archive_link('portfolio').''.$category->slug.'/'; ?>"><?php echo $category->name; ?></a></li>	
							<?php endforeach; ?>
						<?php endif; ?>
						</ul>
					<?php endif; ?>	

			<?php endif; ?>
		<?php endforeach; ?>
	</ul>

<?php 
