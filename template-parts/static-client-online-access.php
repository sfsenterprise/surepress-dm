<!-- Login Page -->
<div style="color:#333;margin:1em auto;text-align:center;">
<div style="display:inline-block;max-width:355px;padding-right:1em;text-align:left;">
<h1>Online Access</h1>
Our simple online system makes it possible to access your project from any computer, tablet, or smartphone.<br>
<iframe src="https://buildertrend.net/NewLoginFrame.aspx?builderID=59977" style="border:0px;height:180px;width:100%;margin-top:1em;" frameborder="0"></iframe>
</div>
<div style="display:inline-block;width:100%;max-width:500px;vertical-align:top;padding-top:20px;">
<div style="position:relative;padding-bottom:51.25%;padding-top:25px;height: 0;">
<iframe src="https://www.youtube.com/embed/dPaczHBe75c?rel=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
</div>
</div>
<div style="color:#999;font-size:.8em;font-style:italic;margin-top:1em;width:100%;">
To access your account please contact Buildertrend at <span style="white-space:nowrap;">1-877-309-0368.</span>
</div>
</div>
<!-- /Login Page -->

<!-- Form -->
<!-- <div class="buildertrend">
    <script type="text/javascript" src="https://buildertrend.net/leads/contactforms/js/btClientContactForm.js"></script>
    <iframe src="https://buildertrend.net/leads/contactforms/ContactFormFrame.aspx?builderID=80122" scrolling="no" id="btIframe" style="background:#c5c5c5;border:0px;margin:0 auto;width:100%;"></iframe>
</div> -->
<!-- /Form -->