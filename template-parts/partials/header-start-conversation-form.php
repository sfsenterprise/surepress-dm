<div class="modal fade start-conversation-popup" id="start1" tabindex="-1" role="dialog" aria-labelledby="start1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <span class="border"></span>
      <div class="modal-header"> 
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-times"></i></span>
        </button></div>
      <div class="modal-body">
      <h2> Design Your Space With the Help of Our Experts</h2>
       <?php gravity_form( 'Start A Conversation Popup',false, false,false, false,true, 1, true );    ?> 
      </div>
    </div>
  </div>
</div>