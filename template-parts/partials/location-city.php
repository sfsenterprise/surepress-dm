<?php
	use Surepress\Functions\Common as Common;
	/**********************************************************
	this is being used by /locations?s=35801 AND /locations
	**********************************************************/
	$state_key = ( get_the_ID() != "" ) ? get_the_ID() : $key; //MESSY CLEAN THIS UP J
	$state_name = ( get_the_ID() != "" ) ? get_the_title() : get_the_title($key); //MESSY CLEAN THIS UP J
?>
<?php foreach($wp_query->locations_nap as $state => $city ) : ?>		
		<div class="panel-group location_archive">
		<?php $state_name = ( strlen($state) > 2 ) ?  $state : $GLOBALS['states_abbr'][$state]; ?>
		<h6><?php echo $state_name; ?></h6>

		<?php foreach( $city as $k => $city) : ?>		
			<?php foreach($city as $m => $microsite) : ?>
				<?php $mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($microsite['Nap_Mongoose_Phone']));?>	
				<div class="panel panel-default">
					<div class="panel-body">
							<strong>
									<?php echo $microsite['Nap_Name']; ?> Location 
									<?php if(  isset($_GET['search']) && ( $_GET['search'] === "my_location" || strlen( $_GET['search']) === 5) ) : ?>
									<sup><?php echo ($microsite['Nap_Distance']) ?  $microsite['Nap_Distance']. ' mi away' : '' ;?></sup>
									<?php endif; ?>
							</strong>
							<div class="contents">
								<address
									data-url="/<?php echo $microsite['Nap_slug']; ?>" 
									data-title="<?php echo 'DreamMaker of '.$microsite['Nap_Name']; ?>" 
									data-lat="<?php echo $microsite['Nap_lat']; ?>" 
									data-lng="<?php echo $microsite['Nap_lng']; ?>" 
									data-address="<?php echo htmlentities($microsite['Nap_Street'].' '.$microsite['Nap_Street2']).'<br><strong>'.$microsite['Nap_City'].', '.$state_name.' '.$microsite['Nap_Zip'].'</strong>'; ?>"
								>
									<i class="fa fa-map-marker"></i><?php echo $microsite['Nap_Street']; ?><?php echo ($microsite['Nap_Street2'] !== '' ? '<br>&nbsp;&nbsp;&nbsp;' : ''); ?> <?php echo $microsite['Nap_Street2']; ?><br>
									<i class="fa fa-map-marker"></i><?php echo $microsite['Nap_City']; ?>, <?php echo $state_name; ?> <br>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $microsite['Nap_Zip']; ?> <br>
									<i class="fa fa-phone"></i><a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $microsite['Nap_Phone']); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $microsite['Nap_Phone']); ?></a><br>
									
								</address>
								<div class="ylwbtn"><a href="/<?php echo $microsite['Nap_slug']; ?>/" class="btn btn-primary">Visit Website</a></div>
							</div>
							
							

				    </div>
				    <?php $s_areas = (unserialize($microsite['Nap_fields'])['area_served_1'] !=  "") ?  unserialize($microsite['Nap_fields'])['area_served_1'] : "Coming Soon!"; ?>
					<div class="panel-footer">
				    	<span class="col-sm-12 collapse" id="<?php echo sanitize_title($microsite['Nap_Name']); ?>">
				    		<strong>Service Areas</strong>
				    		<p><?php echo $s_areas; ?></p>
				    	</span>
				    	<a class="btn-collapse collapsed" data-toggle="collapse" data-target="#<?php echo sanitize_title($microsite['Nap_Name']); ?>"><i class=" fa arrow"></i></a>
				    </div>
				    
				</div>
			<?php endforeach; ?>
		<?php endforeach; ?>			

		<?php if ( isset($_GET['lat']) && isset($_GET['lng']) ) : /*FOR LOCATIONS SEARCH TO SHOW YOUR CURRENT LOCATION*/ ?>
			<address data-url="#" data-title="You are here" data-lat="<?php echo $_GET['lat']; ?>" data-lng="<?php echo $_GET['lng']; ?>" data-address="Your current location"></address>
		<?php endif; ?>

		</div>
<?php endforeach; ?>


			