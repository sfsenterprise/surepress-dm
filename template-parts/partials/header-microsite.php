<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
?>
    <?php get_template_part('template-parts/section/section','announcements'); ?>   
                <!--- MICROSITE -->
                <div id="topmost_microsite" class="container-fluid">
                    <div class="row">
                        <div id="careerslinkcontainer-microsite">
                            <a href="<?php echo get_site_url() ?>/job-opportunities/">Job Opportunities</a>
                        </div>
                        <div class="section3">
                            <p class="directions">
                                <a href="https://maps.google.com/?&daddr=<?php echo urlencode( Common\MicrositeAddress() ); ?>" class="btn btn-default">Get Directions</a>
                            </p>
                            <?php get_template_part('template-parts/widgets/widget','give-a-call'); ?>

                            <?php 
                                $template['campaign'] = get_post_meta( get_the_ID(), 'campaign_template', true );           
                                $template['campaignty'] = get_post_meta( get_the_ID(), 'campaignsty_template', true );

                                if (!in_array('landing', $template)):
                            ?>
                            <address class="col-md-6">
                                <?php if( $wp_query->nap->microsite_street) : ?>
                                <a target="_blank" href="https://maps.google.com/maps/dir/?q=<?php echo urlencode( Common\MicrositeAddress() ); ?>">
                                    <i class="fa fa-map-marker"></i><?php echo Common\MicrositeAddress(); ?>
                                </a>
                                <?php else : ?>
                                    <i class="fa fa-map-marker"></i><?php echo Common\MicrositeAddress(); ?>
                                <?php endif; ?>    
                            </address>
                            <?php endif; ?>
                        </div>
                        
                    </div>
                </div>    

                <nav class="navbar navbar-default" id="header_nav">
                  <div class="container-fluid">
                    <div class="row">
                        <div class="navbar-header">
                          <a class="navbar-brand" href="<?php echo site_url('/'); ?>">
                            <img src=" <?php echo Assets\asset_path('images/main-logo-v1.png') ?>" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/> 
                            <em><?php echo $wp_query->nap->microsite_name; ?></em>
                          </a>
                        </div>

                        <p class="nav navbar-right">
                            <a class="btn btn-primary" data-toggle="modal" data-target="<?php echo (get_post_type() == 'campaigns') ? '#start2': '#start1'; ?>">Start a Conversation</a>                         
                        </p>                        
                        
                        <?php if (!in_array('landing', $template)) : ?>
                        <?php 
                            switch_to_blog(1);
                                @wp_nav_menu(
                                    array(
                                        'container_id' => 'navbar', 
                                        'container_class'=> 'main-menu',
                                        'menu_class' => 'nav navbar-nav',
                                        'theme_location' => 'main-menu'
                                    )
                                ); 
                            restore_current_blog();
                        ?> 
                        <?php endif; ?> 
                    </div>    
                  </div><!-- /.container-fluid -->
                </nav>        
                <?php get_template_part('template-parts/partials/header', 'start-conversation-form' ); ?>
                <!--- MICROSITE END-->