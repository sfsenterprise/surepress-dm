<?php use Surepress\Functions\Locations as Locations; ?>

<h2>DreamMaker Bath & Kitchen Locations <strong>Find a location near you </strong></h2>
<form role="search" method="get" class="searchform" action="<?php bloginfo('url'); ?>/locations">
		<input type="hidden" name="search" id="s" placeholder="hi :D">
		<p> 
			<sup>Search by </sup>
			<select class="form-control" id="location_state">
		      <option>State</option>
		      <?php foreach( Locations\active_states() as  $state) : ?>
		      		<option value="<?php echo $state->Nap_State; ?>"><?php echo ( strlen($state->Nap_State) > 2 ) ? $state->Nap_State : $GLOBALS['states_abbr'][$state->Nap_State] ; ?></option>
		      <?php endforeach; ?>	
		    </select>
		</p>
		<p>
			 <button type="button" class="btn btn-primary btn-block myLocation">Use My Location</button>
		</p>
		<p>
			<input type="text" class="form-control location_zip"  placeholder="Enter Zip">
			<button type="submit" class="btn btn-info">Search</button>
		</p>
</form>