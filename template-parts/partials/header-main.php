<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
?>
    <?php get_template_part('template-parts/section/section','announcements'); ?>   
                <!--- MAIN SITE -->
                <div id="topmost_main" class="container-fluid"> 
                    <div class="row">
                        <div id="careerslinkcontainer" style="position: absolute; padding: 11px;">
                            <a href="https://www.dreammakerfranchise.com/" style="color: #fff; font-size: 18px;">Own A Franchise</a> | <a href="<?php echo get_site_url() ?>/job-opportunities/" style="color: #fff; font-size: 18px;">Job Opportunities</a>
                        </div>
                        <form role="search" method="get" class="searchform" action="<?php bloginfo('url'); ?>/locations">
                        <?php if (!is_page_template('template-landing.php')) : ?>    
                            <input type="hidden" name="search" id="t" placeholder="hi :D">
                            <p>
                                <button type="button" class="btn btn-default myLocation">Find My Location</button>
                                <input type="text" class="location_zip" placeholder="Enter Zip Code"/>
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </p>
                        <?php endif; ?>                            
                        </form>
                        <?php get_template_part('template-parts/widgets/widget','give-a-call'); ?>   
                    </div>
                </div>
                <nav class="navbar navbar-default" id="header_nav">
                  <div class="container-fluid">
                    <div class="row">

                        <div class="navbar-header">                           
                          <a class="navbar-brand" href="<?php echo site_url('/'); ?>"><img src=" <?php echo Assets\asset_path('images/main-logo-v1.png') ?>" alt="DreamMaker Bath & Kitchen"/> </a>
                        </div>

                        <p class="nav navbar-right">
                            <a class="btn btn-primary" data-toggle="modal" data-target="<?php echo (get_post_type() == 'campaigns') ? '#start2': '#start1'; ?>">Start a Conversation</a>                          
                        </p>                        
                        <p class="nav navbar-right">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>                      
                        </p>

                        <?php if (!is_page_template('template-landing.php')) : ?>
                            <?php 
                                @wp_nav_menu(
                                    array(
                                        'container_id' => 'navbar', 
                                        'container_class'=> 'main-menu collapse navbar-collapse',
                                        'menu_class' => 'nav navbar-nav',
                                        'theme_location' => 'main-menu'
                                    )
                                ); 
                            ?> 
                        <?php endif; ?>    
                    </div>    
                  </div><!-- /.container-fluid -->
                </nav>
                
                <?php get_template_part('template-parts/partials/header', 'start-conversation-form' ); ?>
                  
                <!--- MAIN SITE END-->    