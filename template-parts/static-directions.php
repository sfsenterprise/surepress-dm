<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Locations as Locations;
$mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($wp_query->nap->microsite_fields)['mongoose_phone']);
?>


<?php the_content(); ?>
<p>&nbsp;</p>
<div class="box">
	<figure>
		<?php if( !empty(get_option('hours_image')) ): 
				echo (wp_get_attachment_image(get_option('hours_image'), 'full'));
			else: ?>
			<img src="<?php echo Assets\asset_path('images/operating-hours.jpg') ?>" alt="Our Location" />
		<?php endif; ?>
		<figcaption>
			 <h6>Hours of Operation </h6>
			<dl>
			<?php if( !empty( get_blog_option($wp_query->nap->microsite_blog_id, "operating_hours") ) ) : ?>
		  	<?php foreach( get_blog_option($wp_query->nap->microsite_blog_id, "operating_hours") as $day => $hours ) : ?>
			  	<dt><?php echo ucwords($day); ?></dt>
			  		<dd>
		  			<?php if( array_key_exists("appointment", $hours) ) : ?>
		  				<a href="tel:<?php echo $wp_query->nap->microsite_phone; ?>"><strong>By Appointment Only</strong></a>
		  			<?php elseif( array_key_exists("closed", $hours) ) : ?>	
		  				CLOSED
		  			<?php else : ?>	
		  				<?php echo date('g:iA', strtotime($hours['opening'])).' - '.date('h:iA', strtotime($hours['closing'])) ; ?>
		  			<?php endif; ?>
			  		</dd><br>
		  	<?php endforeach; ?>
		  	<?php else : ?>
		  		<dt>Nothing found!</dt><dd></dd>
		  	<?php endif; ?>	
		  	</dl>
		</figcaption>
	</figure>
	<?php $mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($wp_query->nap->microsite_fields)['mongoose_phone']);?>
	<div class="address">
		<address>
			<h6>
				<?php if( !Common\is_main() ) : ?>DreamMaker of <?php endif; ?>
				<?php echo $wp_query->nap->microsite_name; ?>
			</h6>
			<i></i><?php echo Common\MicrositeAddress(true); ?><br>
			<i></i><a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $wp_query->nap->microsite_phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $wp_query->nap->microsite_phone); ?></a>
		</address>
		<span>
			<?php if( $wp_query->nap->microsite_street) : ?>
			<p>
				<a target="_blank" class="btn btn-primary direction-url" href="https://maps.google.com/maps/dir/?api=1&destination=<?php echo urlencode( Common\MicrositeAddress() ); ?>" >Directions</a>
			</p>
			<?php endif; ?>
			<p><a class="btn" href="<?php echo get_site_url(); ?>/contact-us/">Contact Us</a></p>
		</span>
	</div>
	<div class="service-areas">
		<h6>Service Areas</h6>
		<?php $areas = unserialize($wp_query->nap->microsite_fields);	?>
		<?php if (!empty($areas["area_served_1"] ) ): ?>
				<p><?php echo $areas["area_served_1"]; ?> </p>
		<?php else: ?>
				<p> No Service Areas found. </p>
		<?php endif; ?> 
	</div>
</div>

<div class="map">
	<?php if ( !empty( get_page_by_title('Areas We Serve') && !Common\is_main() ) ): ?>
		<?php echo do_shortcode('[slmp_geojuice zoomLvl="9" hide_title="true" hide_subtitle="true" excludes="checkins"]'); ?>
		<center><a class="btn btn-primary" href="/<?php echo $wp_query->nap->microsite_slug .'/areas-we-serve/'; ?>">View More Local Remodeling Work</a></center>
	<?php else: ?>
		<div id="sfl_map" style="height: 500px;"
			data-pin="<?php echo Assets\asset_path('images/map-icon.png') ?>"
			data-lat="<?php echo ($wp_query->nap->microsite_lat != "0.000000") ? $wp_query->nap->microsite_lat : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lat'] ; ?>" 
			data-lng="<?php echo ($wp_query->nap->microsite_lng != "0.000000") ? $wp_query->nap->microsite_lng : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lng'] ; ?>"
			data-title="<?php echo 'DreamMaker of '.$wp_query->nap->microsite_name; ?>"
			data-address="<?php echo Common\MicrositeAddress(true); ?>"
			data-phone="<?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $wp_query->nap->microsite_phone); ?>"
		></div>
	<?php endif; ?>
</div>

<div class="directions">

	<?php if (!empty(get_field('direction_text'))):  ?>
		<?php echo get_field('direction_text'); ?>
		<!-- <h6>Directions</h6>
		<dl>
			<dt>From North:</dt>
			<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,</dd>
			<dt>From East:</dt>
			<dd>Nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</dd>
			<dt>From South:</dt>
			<dd>Nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</dd>
			<dt>From West:</dt>
			<dd>Nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</dd>
		</dl> -->
	<?php endif; ?>
</div>

