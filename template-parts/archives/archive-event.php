<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;


$date_now = new DateTime(); 
$date_now = $date_now->format('m/j/y');

$event_schedules = Common\populate_event_schedules($post->ID);
$event_schedules = json_encode($event_schedules);

$schedule = get_field( 'schedule', $post->ID );
if( !empty( $schedule ) ) :

	for( $i = 0; $i < $schedule; $i++ ) :
    	$date[] = date('m/d/y', strtotime(get_field( 'schedule_'.$i.'_date', $post->ID )));
    endfor;

endif;

$cont = false;

foreach( $date as $value ) :
	if( strtotime( $date_now ) <= strtotime( $value ) ) :
		$cont = true;
	endif;
endforeach;

if( $cont == true ) :
?>

<article>
	<figure class='col-sm-4 col-xs-12'>
		<?php// Common\default_thumbnail(); ?>
		<?php ( has_post_thumbnail() ? the_post_thumbnail() :  Common\default_thumbnail() ) ;  ?>
	</figure>
	<div class='col-sm-8  col-xs-12 event-wrap'>
		<div class="event-container">
			<details open>
				<summary>
				<h5><?php the_title(); ?></h5>
					<ul>
						<li><i></i><?php echo get_field("event_location"); ?></li>
						<?php $schedule = get_field( 'schedule' ); ?>
						<?php if( !empty( $schedule ) ) : ?>
							<?php for( $i = 0; $i < $schedule; $i++ ) : ?>
								<?php //if( strtotime(get_field( 'schedule_'.$i.'_date' )) > strtotime(date('m/d/Y')) ) : ?>
									<li>
										<i></i>
										<span><?php echo date('m/d/Y', strtotime(get_field( 'schedule_'.$i.'_date' ))); ?></span>
										<?php $time = get_field('schedule_'.$i.'_time'); ?>
										<?php if( !empty( $time ) ) : ?>
											<?php for( $x = 0; $x < $time; $x++) : ?>
												| <?php echo date('h:i A', strtotime(get_field('schedule_'.$i.'_time_'.$x.'_start'))).' - '.date('h:i A', strtotime(get_field('schedule_'.$i.'_time_'.$x.'_end'))); ?>
											<?php endfor; ?>
										<?php endif; ?>
									</li>
								<?php //endif; ?>
							<?php endfor; ?>
						<?php else : ?>
								No Schedule found.
						<?php endif; ?>
					</ul>
				</summary> 
			</details>
			<span class="details-container">
				<?php the_content(); ?>
				<p class="cont"><a href="<?php echo get_field('event_link'); ?>" class="btn btn-info event-btn btn-<?php echo $post->ID; ?>" data-title="<?php echo get_the_title(); ?>" data-toggle="modal" data-target="#event1" data-eventid="<?php echo $post->ID; ?>" data-schedule='<?php echo $event_schedules; ?>'>Register Now</a></p>
			</span>
		</div>
	</article>
<?php endif; ?>