<?php
	use Surepress\Functions\Common as Common;
	use Surepress\Functions\Assets as Assets;
?>
<?php get_template_part('template-parts/partials/search', 'locations'); ?>

<?php if ( have_posts() ) : ?>
	<div id="sfl_map" style="width: 100%; height: 500px;" data-pin="<?php echo Assets\asset_path('images/map-icon.png') ?>"></div>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part('template-parts/partials/location', 'city'); ?>
	<?php endwhile; ?>	
<?php else : ?>
	No locations found.	
<?php endif; ?>	