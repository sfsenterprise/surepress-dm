<?php  
get_header();
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
?>
<div class="intro">
	<span class="col-sm-push-6 col-sm-6 "><img src="<?php echo Assets\asset_path('images/core-services.jpg') ?>" alt="Remodeling Services" /></span>
	<span class="col-sm-pull-6 col-sm-6 ">
		<?php $service_page = get_page_by_title('Services'); ?>

		<?php if( $service_page->post_content ) : ?>
			<?php echo wpautop($service_page->post_content); ?>
		<?php else : ?>
			<p>Experience a dream come true with DreamMaker® Bath & Kitchen. Our distinctive, full-service interior remodeling approach focuses on serving our clients from design through installation.</p>

			<p>Our company is designed to provide economical solutions for your residential and commercial remodeling needs.</p>

			<p>We're experienced. We're reliable. We're honest. And we're anxious to get started!</p>

			<p>Call our local Design Studio today.</p>
		<?php endif; ?>
	</span> 	
	
</div>
<?php if ( have_posts() ) : ?>
	<div class="services-slicker" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>
	<?php while ( have_posts() ) : the_post(); ?>
       <figure class="col-xs-12 col-sm-6 col-md-4">
			<a href="<?php echo get_the_permalink(); ?>" >       	
			<?php Common\default_thumbnail(get_the_title()); ?>
				<figcaption>
					<span><?php the_title(); ?></span>
				</figcaption>
			</a>
		</figure>
	<?php endwhile; ?>
	</div>
	<div class="arrows">
		<a class="prev"><i></i></a>
		<a class="next"><i></i></a>
	</div>
	
<?php else : ?>
	<?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>
<?php get_footer(); ?>