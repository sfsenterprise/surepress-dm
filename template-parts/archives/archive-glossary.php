<?php 
	get_header(); 

	//EEEEEW SOLUTION FOR GLOBAL DATA -- CLEAN THIS UP J
	switch_to_blog(1);
	$args = array(
	    'post_type'  => $wp_query->query['post_type'],
	    'posts_per_page' => -1,
        'orderby'   => 'post_title',
        'order' => 'ASC',	  	    
	);
	$glossary_query = new WP_Query( $args );

	$terms = get_terms( array( 
	    'taxonomy' => 'glossary-category'
	) );

?>
	<br>
	<h2>Glossary of Home Remodeling</h2>
	<ul class="glossary-categories">
		<?php foreach($terms as $cat){ ?>
			<li>
				<a href="#" data-id="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></a>
			</li>
		<?php } ?>
	</ul>

	<?php if ( $glossary_query->have_posts() ) : ?>
		<div class="glossary-container">
			<div class="panel-group " id="glossary-accordion">
				<?php $category_old = 0; ?>
				<?php 
				while ( $glossary_query->have_posts() ) : $glossary_query->the_post(); 

					$categories = get_the_terms(get_the_ID(), 'glossary-category' ); 
					$next = get_next_post() ? get_next_post()->ID : 0;
					$categories_next = get_the_terms($next, 'glossary-category' );
					$category_id = !empty($categories) ? $categories[0]->term_id : 0; 
					$category_next_id = !empty($categories_next) ? $categories_next[0]->term_id : 0;
					$category_name = !empty($categories) ? $categories[0]->name : 0; 
				?>

					<?php if($category_id !== $category_old): ?>
						<div class="panel panel-default category-<?php echo $category_id; ?> " >
						<h6><?php echo $category_name; ?></h6>
					<?php endif; ?>
						<div class="panel-body">
							<strong><?php echo get_the_title(); ?></strong>
							<p><?php echo get_the_content(); ?></p>
						</div>
					<?php $category_old = $category_id; ?>
					<?php if($category_id !== $category_next_id):?>    
						</div>
					<?php endif; ?>

				<?php endwhile;?>
			</div>
		</div>
	<?php  else : ?>
		<?php get_template_part('template-parts/content', 'none'); ?>
	<?php endif; ?>
	<?php restore_current_blog(); ?>
<?php get_footer(); ?>
