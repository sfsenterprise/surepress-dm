
<div class="main" >
	<h6 id="case-studies-title"> Choose a category</h6>


	<ul class="case-categories" data-id="<?php echo get_current_blog_id(); ?>">
		<li class="fa loading">
		  <center><i class="fa fas fa-spinner fa-spin"></i></center>
		</li>
	</ul>
	<br>
	<br>
	<div class="case-studies-loading"><center><i class="fa fas fa-spinner fa-spin"></i></center></div>
	<div class="case-studies panel-group"></div>
</div>