<?php get_header(); ?>
	<h2>DreamMaker Events</h2>
	<?php $text = get_blog_option(get_current_blog_id(), 'event_copy_text'); ?>
	<p><?php echo ( $text ? $text : '');?></p>
	
	<?php if ( have_posts() ) : ?>
	    <?php while ( have_posts() ) : the_post(); ?>
	        <?php get_template_part('template-parts/archives/archive','event'); ?>
	    <?php endwhile; ?>
		<?php get_template_part('template-parts/archives/archive','events-form'); ?>
	<?php else : ?>
	    <?php get_template_part('template-parts/content', 'none'); ?>
	<?php endif; ?>
<?php get_footer(); ?>