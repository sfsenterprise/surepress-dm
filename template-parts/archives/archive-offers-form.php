<div class="modal fade" id="offer1" tabindex="-1" role="dialog" aria-labelledby="offer1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header"> 
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Close</span>
          </button>
        </div>
        <div class="modal-body">
            <h3></h3>
            <div class="header-text"></div>
    		    <?php  gravity_form( 'Offers',false, false,false, false,true, 100, true );    ?>
        </div>
        <div class="modal-footer">
        		<p><a href="" class="btn btn-info col-sm-4" target="_blank">Download this offer</a>
        		<span class="col-sm-8 offer-text"></span></p>
        </div>
    </div>
  </div>
</div>  