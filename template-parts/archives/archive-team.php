<?php use Surepress\Functions\Common as Common; ?>
<h2>The DreamMaker <?php echo $wp_query->nap->microsite_name; ?> Team</h2>

<?php $content =  get_page_by_path( 'about/our-team', OBJECT, 'page' );  ?>
<?php if( empty($content) || $content->post_content === "" ) : ?>
	<p>With a dedication to superior craftsmanship and ethical excellence, our team of specialists deliver a design that meets your specific needs, while communicating consistently throughout the entire remodeling process. You can feel secure that your project will be completed in the most efficient manner possible with minimal disruption to your life. Plus, our foundation in honest, ethical business practices makes us the professionals you can trust to give you the home you've always wanted.</p>
	<p>Keep scrolling to meet the team.</p>
<?php else : ?>
	<?php echo $content->post_content; ?>
<?php endif; ?>



<?php while ( have_posts() ) : the_post(); ?>
<div class="team-container">
	<figure class="col-sm-4">
		<?php Common\default_thumbnail(); ?>
		<span class="details-wrap">
			<span class="details">
				<span class="middle">
					<a href="tel:<?php echo preg_replace('/[^0-9.]+/', '', get_field('team_phone') ); ?>"><i class="fa fa-phone"></i><?php the_field('team_phone'); ?></a><br>
					<a href="mailto:<?php the_field('team_email'); ?>"><i class="fa fa-envelope"></i><?php the_field('team_email'); ?></a>
				</span>
			</span>
		</span>
		<figcaption>
			<h6><?php the_title(); ?></h6><hr>
			<h6><?php the_field('team_position'); ?></h6>
		</figcaption>
	</figure>
	<div class="col-sm-8 bio-container" >
		<div class="bio">
			<div class="bio-inner">
				<?php the_content(); ?>
			</div>
		</div>
	</div>	
</div>
<div class="clearfix"></div>
<?php endwhile; ?>
