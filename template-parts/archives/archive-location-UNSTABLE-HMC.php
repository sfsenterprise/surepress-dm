<?php 
	use Surepress\Functions\Locations as Locations; 
	use Surepress\Functions\Common as Common;
?>
<h2>DreamMaker Bath & Kitchen Locations</h2>
<strong>Find a location near you </strong>
<form>
	<p> 
		<sup>Search by </sup>
		<select class="form-control" id="State">
	      <option>State</option>
	      <option>2</option>
	      <option>3</option>
	      <option>4</option>
	      <option>5</option>
	    </select>
	</p>
	<p>
		 <button type="submit" class="btn btn-primary">Use My Location</button>
	</p>
	<p>
		<input type="text" class="form-control" id="SearchZip"  placeholder="Enter Zip">
		<button type="submit" class="btn btn-info">Search</button>
	</p>
</form>
	<?php //echo Locations\google_map_data(); ?>
	<div id="sfl_map" data-polygon="<?php //echo Locations\google_map_data(); ?>" style="width: 100%; height: 500px;"></div>

	<p>&nbsp;</p><p>&nbsp;</p>
	<div class="location-states">
		<p align="center">States</p>
		<ul>			
		<?php if ( have_posts() ) : ?>
			<?php $ctr = 1; ?>
			<?php while( have_posts() ) : the_post(); ?>
					<li><h6>
							<a href="<?php echo get_permalink( get_the_ID() ); ?>">
								<?php the_title(); ?> (<?php echo count( $wp_query->locations_nap[get_the_ID()] ); ?>)
							</a>
						</h6>
					</li>

					<?php if( $ctr > ($wp_query->locations_states_list_divider - 1) &&  $ctr%$wp_query->locations_states_list_divider === 0 ) : ?>
						</ul><ul>	
					<?php endif;?>

					<?php $ctr++; ?>
			<?php endwhile;?>
		<?php else : ?>
			 <li>No States found.</li>
		<?php endif; ?>
		</ul>
		<div class="clearfix"></div>
	</div>	



    <script>
	function initMap() {

	  var sfl_container = jQuery("#sfl_map");

	  /*
	  var sfl_lat = parseFloat( sfl_container.attr('data-lat'));
	  var sfl_lng = parseFloat(sfl_container.attr('data-lng'));
	  var sfl_title = sfl_container.attr('data-title');
	  var sfl_address = sfl_container.attr('data-address');
	  var sfl_phone = sfl_container.attr('data-phone');
	  */

	  //var uluru = {lat: 27.994402, lng: -81.760254 };


	  /*
	  var contentString = '<div id="sfl_container_infowindow">'+
						      '<p>'+
					      		'<strong>'+sfl_title+'</strong><br>'+						      
							      sfl_address+'<br>'+
							      sfl_phone+
						      '</p>'+
					      '</div>';


	  var infowindow = new google.maps.InfoWindow({
	    content: contentString
	  });
	  */

	  var map = new google.maps.Map(
	      document.getElementById('sfl_map'), {zoom: 5 }
	  );

	  	/*
	    var coordinates = {
	      "feed1": [
	        [25.774252, -80.190262],
	        [18.466465, -66.118292],
	        [32.321384, -64.75737],
	        [25.774252, -80.190262]
	      ],

	      "feed2": [
	        [26.774252, -81.190262],
	        [19.466465, -67.118292],
	        [33.321384, -65.75737],
	        [26.774252, -81.190262]
	      ],

	      "feed3": [
	        [27.774252, -82.190262],
	        [20.466465, -68.118292],
	        [34.321384, -66.75737],
	        [27.774252, -82.190262]
	      ]
	    };
	    */

	    var coordinates = <?php //echo Locations\google_map_data(); ?>;





	    bounds  = new google.maps.LatLngBounds();//for centering

	    //Loop thru the json and generate map polygon
			for (var i in coordinates) {//array first level (STATE)
				//console.log( coordinates[i] );
				  arr = [];	
			      for (var j = 0; j < coordinates[i].length; j++) { //array first level (lat,lng)
			        arr.push(new google.maps.LatLng(
			          parseFloat(coordinates[i][j][0]),
			          parseFloat(coordinates[i][j][1])
			        ));

			        bounds.extend(arr[arr.length - 1])
			      }

		        var sfl_polygon = new google.maps.Polygon({ //start drawing the map polygon
		          paths: arr,
		          strokeColor: '#FF0000',
		          strokeOpacity: 0.8,
		          strokeWeight: 2,
		          fillColor: '#FF0000',
		          fillOpacity: 0.35
		        });
		        sfl_polygon.setMap(map);	  		      
			}


	  /*
	  var marker = new google.maps.Marker({
	  	position: uluru, 
	  	map: map,
	  	title: sfl_title
	  });

	  infowindow.open(map, marker); //open info by default

	  marker.addListener('click', function() {
	    infowindow.open(map, marker);
	  });
	  */	

	  map.fitBounds(bounds);//center map
	}
    </script>

    <?php /*********Use enqueue to load this ***********/?>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA1i5lA-PWybxpSwDdQuRXc0xct5MQ70I&callback=initMap"></script>