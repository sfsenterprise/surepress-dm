<h2>Take Advantage of a Special Offer Today</h2>
<p>Now is the time to get the beautiful & functional new kitchen or relaxing spa-like bathroom you’ve been thinking about for months or even years. Take advantage of a limited time special offer below and let’s start the conversation.</p>
<table>
<?php	if ( have_posts() ) {?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php 
					$expiration = get_post_meta( $post->ID, 'offers_expiration',true ); 
					$date_now = date("Y/m/d");
				?>

				<?php if( strtotime( $expiration ) >= strtotime( $date_now ) ): ?>	
						<tr>
							<td>
								<span><?php the_title(); ?></span>
								<p><?php echo wp_trim_words( get_the_content(), 100, '' ); ?></p>

								<div class="modal-field field-<?php echo get_the_ID();  ?>">
									<div class="title"><?php the_title(); ?></div>
									<div class="header-text"><?php the_content();  ?></div>
									<div class="offer-text"><?php echo get_field('offer_text'); ?></div>
									<?php 
										$url = "";
										$file =  get_field('offer_link');
										if($file){
											$url = wp_get_attachment_url($file); ?>
											<div class="offer-link"><?php echo $url; ?></div>
										<?php }
									?>
								</div>
							</td>
							<td><a href="#offer1" class="btn btn-info view-offer" data-toggle="modal" data-target="#offer1" data-id="<?php echo get_the_ID(); ?>">Learn More</a></td>
						</tr>
						<?php endif; ?>
	    <?php  endwhile;?>
	    <?php get_template_part('template-parts/archives/archive','offers-form'); ?>
<?php 	} else {
	    get_template_part('template-parts/content', 'none');
	}
?>
</table> 
