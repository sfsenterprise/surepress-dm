<?php use Surepress\Functions\Common as Common; ?>
<?php if ( have_posts() ) : ?>
	<ul class="row boxes">	
		<?php $categories = Common\get_portfolio_categories('portfolio_categories', true);?>
		<?php foreach ($categories as $cat) : ?>
	        <?php $cover_img = Common\get_portfolio_cover($cat->term_id, 'services-archive');?>  
	        <?php if ( get_term_meta( $cat->term_id, 'status', true ) === "deactivated") continue; ?>
	    	<li class="box">
				<div class="img-wrapper">
					<div class="img-container">
						<?php echo $cover_img; ?>
						<div class="inner-container">
							<h6><?php echo $cat->name;?></h6>
							<a class="btn btn-info" href="<?php echo get_category_link($cat->term_id); ?>">VIEW ALL</a>
						</div>
					</div>
				</div>
			</li> 	
	    <?php endforeach; ?>
	</ul>	
<?php else : ?>
	 <?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>