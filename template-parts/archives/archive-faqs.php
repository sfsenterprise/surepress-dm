<?php 
get_header();
use Surepress\Functions\Assets as Assets;       
?>
<div class="intro">
	<h2>FAQs to Help Make Your Remodeling Dreams Come True</h2>
	<span>
		<p>To help you make the best decision for your remodel, see our FAQs below from many of our existing clients. If you have a question that you don’t see below, give us a call today. We look forward to starting the conversation.</p>
		<p>&nbsp;</p>
	</span>		
</div>
<?php 
	//EEEEEW SOLUTION FOR GLOBAL DATA -- CLEAN THIS UP J
	switch_to_blog(1);
	$args = array(
	    'post_type'  => $wp_query->query['post_type'],
	    'posts_per_page' => -1, 
	);
	$faq_query = new WP_Query( $args );
	$terms = get_terms( array('taxonomy' => 'faqs-category') );
?>
<div class="faq-container mobile hidden-sm hidden-md hidden-lg hidden-xl">
	<div class="panel-group" id="faqs-accordion-mobile">
		<div class="panel panel-default">
		</div>
	</div>
</div>

<?php	if ( $faq_query->have_posts() ) {?>
			<div class="faq-container hidden-xs">
				<ul class="nav nav-tabs faq-categories">
						<li class="col-xs-12 col-sm-3 ">
							<a href="#tab-all" data-toggle="tab" data-id="all">All</a>
						</li>
					<?php foreach($terms as $cat){ ?>
						<li class="col-xs-12 col-sm-3 ">
							<a href="#tab-<?php echo $cat->slug; ?>" data-toggle="tab" data-id="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></a>
						</li>
					<?php } ?>
				</ul>
				
				<div class="panel-group " id="faqs-accordion">
			    <?php while ( $faq_query->have_posts() ) : $faq_query->the_post(); 
			    		$categories = get_the_terms(get_the_ID(), 'faqs-category' ); 
			    		$category_class="category-all";
			    		if($categories){
			    			foreach($categories as $category){
				    			$category_class .= " category-".$category->term_id; 
				    		}
			    		}
			    		?>		
						<div class="panel panel-default <?php echo $category_class; ?>"> 
							<a class="collapsed" data-toggle="collapse" data-parent="#faqs-accordion" href="#faq-<?php echo get_the_ID(); ?>">
								<div class="panel-heading">
									<strong class="panel-title"><?php echo get_the_title();  ?></strong>
								</div>
								<i></i>
							</a>             
				            <div id="faq-<?php echo get_the_ID(); ?>" class="panel-collapse collapse" >
				                <div class="panel-body">
				                   <?php echo get_the_content(); ?>
				                </div>
				            </div>
						</div>
	    		<?php endwhile;?>
	    		</div>

	    	</div>
<?php 	} else {
	    get_template_part('template-parts/content', 'none');
	}

	restore_current_blog();
?>

<div class="footer-faq">
	<h6>Tips for maintenance </h6>
	<ul>
		<li>When worn areas appear, recoat with the same finish used on the cabinets or floors</li>
		<li>Use track off mats and furniture leg protector pads to prevent everyday wear and tear on your floors</li>
		<li>Vacuum floors frequently and wipe up spills promptly</li>
	</ul>

	<h6> Things to watch for</h6>
	<ul>
		<li>Avoid excessive and prolonged exposure to sun light, high temperature and humidity</li>
		<li>Heating and cooling vents should be closed or directed away from cabinets</li>
		<li>Fingerprints, cooking fumes and smoking residue may accumulate and should be removed regularly</li>
		<li>Refrain from using ammonia or silicone cleaning products over long period of time</li>
	</ul>

	<h6>Recommended Products</h6>
	<ul>
		<li>Lemon or citrus oil</li>
		<li>A non-wax solution that contains polish or a mild detergent (any excess moisture should be wipe dry)</li>
		<li>Orange cleaner</li>
	</ul>	
</div>
	
<?php get_footer(); ?>