<?php
/* ////////////////////////////////////////////////////////////////////////////////
BE CAREFUL WITH THIS FILE ... SINGLE SERVICES AND SINGLE GALLERY USES THIS LAYOUT
//////////////////////////////////////////////////////////////////////////////// */
?>
<?php  use Surepress\Functions\Common as Common; ?>
<div class="recent-case-studies">
    
    <?php 
        $id = get_the_ID();
        if(get_post_type() == 'portfolio') :
            $category = get_queried_object();
            $id = $category->term_id;
        endif;
        $recent_study = Common\get_related_case($id);
    ?>
    <?php if($recent_study && !empty($recent_study)) { ?>
    <h3>Recent Case Studies</h3>
    <?php foreach(  $recent_study as $c) : ?>
        <section>    
            <div class="case-info">
                <p><strong>Project Name</strong><?php echo get_the_title($c->ID); ?></p>
                <p><strong>Location</strong> <?php echo get_field('case_location',$c->ID); ?></p>
                <p><strong>Project Summary</strong> <?php echo get_the_excerpt($c->ID); ?></p>
                <p><strong>Project Category</strong>
                    <?php if( get_field('case_category', $c->ID) ) : ?> 
                        <?php foreach ( get_field('case_category', $c->ID) as $key => $value ) : ?>
                            <?php $cat = get_term_by('id', $value, 'services-category'); ?>
                            <?php echo $cat->name; ?><?php echo ( $key !== count( get_field('case_category') ) -1 ) ? "," : ""; ?>
                        <?php endforeach; ?>            
                    <?php else : ?>
                            No Category         
                    <?php endif; ?> 
                </p>
                <p><strong>Testimonial</strong> <em><?php echo get_field('case_testimonial',$c->ID); ?></em></p>
            </div>

            <div class="case-image">
            <?php 
                $cat_id = (get_field('case_gallery', $c->ID) ? get_field('case_gallery', $c->ID) : '');
                $cat = get_term_by('id', $cat_id, 'portfolio_categories');
                $portfolios =  Common\get_posts_by_custom_post('portfolio', 'portfolio_categories', (!empty($cat->slug) ? $cat->slug : ''), 6);

                if($portfolios) :
                    foreach ($portfolios as $portfolio) {
                       echo get_the_post_thumbnail($portfolio->ID, 'medium');
                    }
                    $get_after_img = get_field('portfolio_after', $portfolio->ID);
                    if( $get_after_img && !empty($get_after_img)) :
                        echo wp_get_attachment_image( $get_after_img  , 'medium' );
                    endif;
                else:
                   // echo "<center>No gallery found.</center>";
                endif;
            ?>
                <a href="<?php the_permalink($c->ID); ?>" class="btn btn-info">View this Case Study</a>
                
            </div> 
            <div class="clearfix"></div>
        </section>    
    <?php endforeach; ?> 
    <?php } ?>   
</div>