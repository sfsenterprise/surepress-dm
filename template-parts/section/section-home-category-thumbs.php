<?php  use Surepress\Functions\Common as Common;  ?>

<section class="home-category-thumbs">
  <?php 
    if ( get_current_blog_id() == 96 ) {
      echo '<h1>Premier Kitchen & Bath Remodeling in McKinney, TX</h1>';
    } else {
      echo '<h1>What Dream Can We Create For You?</h1>';
    }
  ?>
  
  <?php if( !empty(Common\FeaturedServices() ) ) : ?>
  <ul>
    <?php foreach (  Common\FeaturedServices() as $service) : ?>
      <li>
        <figure>
            <?php 
              $featured_image = get_post_meta($service, 'featured_image', true );
              $img = wp_get_attachment_image ( $featured_image, array(421,461) );
              $service_obj = get_post($service);
             ?>
             

            <?php if( $featured_image && ! empty($img)  ) : ?>
              <?php echo $img; ?>
            <?php else : ?>
              <?php switch_to_blog(1); ?>
                <?php //$service_obj->post_title = ($service_obj->post_title  == 'Bathroom Remodeling' ? 'Bath Remodeling' : $service_obj->post_title ) ?>
                <?php //$service_obj->post_title = ($service_obj->post_title  == 'Safety Mobility' ? 'Safety & Mobility' : $service_obj->post_title ) ?>
                <?php //$service_obj->post_title = ($service_obj->post_title  == 'Cabinet Refacing' ? 'Cabinet Refacing' : $service_obj->post_title ) ?>

                <?php $main_service = get_page_by_title($service_obj->post_title, OBJECT, 'services'); ?>
                <?php $main_featured_image = get_post_meta($main_service->ID, 'featured_image', true ); ?>
                <?php $main_img = wp_get_attachment_image( $main_featured_image, array(421,461) ); ?>
              <?php restore_current_blog(); ?>

              <?php if( $main_img ) : ?>
                <?php echo $main_img; ?>
              <?php else : ?>
                <?php //place holder if Service Thumbnail is not set  ?>
                <img src="<?php echo Surepress\Functions\Assets\asset_path('images/dream-kitchen.jpg') ?>">
              <?php endif; ?>
            <?php endif; ?>
          <figcaption>
            <?php 
              if ( get_current_blog_id() == 96 ) {
                  echo '<strong><h2>' . get_the_title($service) . '</h2></strong>';
              } else {
                  echo '<strong><span>' . get_the_title($service) . '</span></strong>';
              }
            ?>
            <a href="<?php echo get_permalink($service); ?>">Discover More</a>
          </figcaption>
        </figure>
      </li>
    <?php endforeach; ?>  
  </ul>
<?php else : ?> 
  <h6 align="center">No Service Selected.</h6>
<?php endif; ?>
</section>