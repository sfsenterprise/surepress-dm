<section class="home-process">
	<h2>Our Process 
		<small>It's our job, but we never forget it's your home.</small>
	</h2>
	<ul>
		<a href="<?php echo get_site_url(); ?>/about/our-process/#process-1"><li class="process-meet">Meet With a DreamMaker Designer</li></a>
		<a href="<?php echo get_site_url(); ?>/about/our-process/#process-2"><li class="process-discovery">Home Survey</li></a>
		<a href="<?php echo get_site_url(); ?>/about/our-process/#process-3"><li class="process-review">Design Review & Written Proposal</li></a>
		<a href="<?php echo get_site_url(); ?>/about/our-process/#process-4"><li class="process-contract">Sign Agreement</li></a>
		<a href="<?php echo get_site_url(); ?>/about/our-process/#process-5"><li class="process-conference">Pre-Start Conference Meeting</li></a>
		<li class="process-dedicated"><a href="<?php echo get_site_url(); ?>/about/our-process/#process-6">Your Project Manager is Your Contact</a></li>
	</ul>

	<div class="process-message">
		<p><strong>Our clients tell us the best place to start is in our design center</strong>. At our first design center meeting, a DreamMaker designer will listen carefully and discuss your needs so your remodeling dreams are incorporated into your new space. We look forward to you sharing photos of your existing space and encourage you to bring any sketches or inspiration that you have gathered with you. We look forward to getting started!</p>	
	</div>	
</section> 