<?php global $post; ?>
<div class="section-blog-recent-posts">
<h6>Recent Posts</h6> 
<?php 	$args = array( 
			'numberposts' => '2',
			'post_type' => 'post',
			'post_status' => 'publish',
			'post__not_in' =>array( $post->ID )
		);

		$recent_posts = get_posts($args);
		$recent_class = "col-xs-12";
		
		if(count($recent_posts) > 0 ){
			if(count($recent_posts) > 1 ){
				$recent_class = "col-xs-12 col-sm-5";
			}
			foreach($recent_posts as $recent){ ?>
				<div class="<?php echo $recent_class; ?>">
					<h5><?php echo $recent->post_title; ?></h5><hr>
					<p><?php echo wp_trim_words( $recent->post_content, 50, '[...]' ); ?></p>
					<p><a class="btn btn-info" href="<?php echo get_permalink($recent->ID); ?> ">Read More</a></p>
				</div>
			<?php }
		}else{ ?>
			<span class="col-xs-12"><h5>No Recent Posts found! </h5></span>
		<?php } ?>
</div>