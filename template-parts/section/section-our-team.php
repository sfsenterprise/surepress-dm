<?php 	
	use Surepress\Functions\Team as Team;
	use Surepress\Functions\Assets as Assets; 
	$data = Team\get_team_member(); //function will fetch post type team where order = 1
?>

<section class="our-team">
	<h2>Our Team</h2>
	<?php if(count($data) > 0 ) : ?>
		<figure>
				<?php if( has_post_thumbnail($data[0]->ID)  ):?>
					<?php echo get_the_post_thumbnail($data[0]->ID, array(250,375) ); ?>
				<?php else: ?>
					<img src=" <?php echo Assets\asset_path('images/no-profile.jpg') ?>" /> 
				<?php endif; ?>
			<figcaption>
				<strong><?php echo $data[0]->post_title; ?></strong><br>
				<?php echo (!empty(get_post_meta($data[0]->ID, 'team_position',true )) ? get_post_meta($data[0]->ID, 'team_position',true ) : ''); ?>
			</figcaption>
		</figure>

		<div class="our-team-desc">	
			<?php echo (strlen(wpautop($data[0]->post_content)) > 700 ? substr(wpautop($data[0]->post_content), 0, 700).'...' : wpautop($data[0]->post_content)); ?>
			<p><a href="<?php echo get_site_url(); ?>/about/our-team/" class="btn btn-primary">Meet Our Team</a></p>
		</div>	
	<?php else : ?>
		No Team Member Found.
	<?php endif; ?>	
	<div class="clearfix"></div>		
</section>