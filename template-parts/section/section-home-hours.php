<?php use Surepress\Functions\Assets as Assets; ?>
<section class="home-hours">
		<?php if( !empty(get_option('hours_image')) ): 
			echo (wp_get_attachment_image(get_option('hours_image'), 'full'));
		else: ?>
			<img src="<?php echo Assets\asset_path('images/operating-hours.jpg') ?>"  />
		<?php endif; ?>
  	<div>
	  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo" aria-expanded="false">Hours of Operation <i></i></button>
	  <dl id="demo" class="collapse">
	  	<?php if( !empty( get_blog_option($wp_query->nap->microsite_blog_id, "operating_hours") ) ) : ?>
	  	<?php foreach( get_blog_option($wp_query->nap->microsite_blog_id, "operating_hours") as $day => $hours ) : ?>
		  	<dt><?php echo $day; ?></dt>
		  		<dd>
		  			<?php if( array_key_exists("appointment", $hours) ) : ?>
		  				<a href="tel:<?php echo $wp_query->nap->microsite_phone; ?>"><strong>By Appointment Only</strong></a>
		  			<?php elseif( array_key_exists("closed", $hours) ) : ?>	
		  				CLOSED
		  			<?php else : ?>	
		  				<?php echo date('g:iA', strtotime($hours['opening'])).' - '.date('h:iA', strtotime($hours['closing'])) ; ?>
		  			<?php endif; ?>
		  		</dd>
	  	<?php endforeach; ?>
	  	<?php else : ?>
	  		<dt>Nothing found</dt><dd></dd>
	  	<?php endif; ?>	 		  		  		  		  		
	  </dl>	
	</div>  
</section>	