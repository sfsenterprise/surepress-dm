<section class="code-of-values">
	<h2>Code of Values</h2>
	<p>We believe if we treat people with respect and integrity, success will follow. Our Code of Values™ is central to who we are and the way we try to conduct ourselves in business and in life. The Code of Values™ is not just something that hangs on the wall. We have a system for keeping the Code of Values™ in place. It’s a living, breathing philosophy based on timeless principles. We know it is impossible to live these values perfectly, but we can live them with excellence — meaning when we miss one, we are willing to be held accountable in a respectful way.</p>
</section>
