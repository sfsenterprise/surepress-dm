<?php use Surepress\Functions\Common as Common;?>
<section class="home-grey-categories">
	<form>
		<h2>How Can We Make <br>Your Dreams Reality?</h2>
		<?php $services = Common\get_parent_services(4); ?>
		<?php $featured_services = get_option('featured_services'); ?>
		<!-- Dynamically populate checkboxes in sync with the popup form -->
	
		<?php if( is_array($featured_services) && ! empty($featured_services) ) : ?>
			<ul>
				<?php foreach($featured_services as $service): ?>
					<li><label><?php echo get_the_title($service); ?><span><input type="checkbox" value="<?php echo get_the_title($service); ?>"></span></label></li>
				<?php endforeach; ?>
			</ul>
		<?php else : ?>
			<ul>
				<?php foreach($services as $service): ?>
					<li><label><?php echo $service->post_title; ?><span><input type="checkbox" value="<?php echo $service->post_title; ?>"></span></label></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>

		<p><input type="submit" value="Start A Conversation"  data-toggle="modal" data-target="#start1" ></p>
	</form>
</section>