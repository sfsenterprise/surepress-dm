<?php
/* ////////////////////////////////////////////////////////////////////////////////
BE CAREFUL WITH THIS FILE ... SINGLE SERVICES AND SINGLE GALLERY USES THIS LAYOUT
//////////////////////////////////////////////////////////////////////////////// */
?>
<?php  use Surepress\Functions\Common as Common; ?>
<?php  use Surepress\Functions\CaseStudies as CaseStudies; ?>
<div class="recent-case-studies">
    <?php     
        $id = get_the_ID();
        $categoryval = '';
        if(get_post_type() == 'portfolio') :
            $category = get_queried_object();
            
            $categoryname = strtolower($category->name);
            //this is to cover all kitchen galleries
            if(strpos($categoryname, 'kitchen') != FALSE){
            	//echo strpos($categoryname, 'kitchen');
            	$categoryval = "Kitchen Remodeling";
            }else{
                //for querying case studies category the same with gallery
                $categoryval = $categoryname;
            }
            $id = $category->term_id;
        endif;
        //$sites = get_sites( ['site__not_in'=>array(1), 'archived'=>0, 'deleted'=>0, 'fields'=> 'ids'] );
        //replace kitchens with kitchen etc..
        $a = array('kitchens','baths');
        $b = array('kitchen','bath');
        $categoryname = str_replace($a,$b,$categoryname);
        $allcasestudies = array();
        if(get_current_blog_id() == 1){
            $sites = get_sites( ['site__not_in'=>array(1), 'archived'=>0, 'deleted'=>0, 'fields'=> 'ids'] );
            for($i=0; $i<count($sites); $i++){
            	//echo $sites[$i];
            	switch_to_blog($sites[$i]);
            	$args_case = array(
    	                        'posts_per_page '   => 2,
    	                        'numberposts'      => 2,
    	                        'orderby'          => 'date',
    	                        'order'            => 'DESC',
    	                        'post_type'        => 'case-studies',
    	                        'post_status'      => 'publish',
    	                        'meta_query' => array(
    	                            array(
    	                                'key' => 'category',
    	                                'value' => $categoryval,
    	                                'compare' => '='
    	                            )
                                    /*,
                                    array(
                                        'key' => 'job_categories',
                                        'value' => sprintf(':"%s";', ucwords($categoryname)),
                                        'compare' => 'LIKE'
                                    )
                                    */
    	                        )
    	                    );
            	$recent_study = new WP_Query( $args_case );
            	$rsarray = array(); 
            	if ( $recent_study->have_posts() ) {
            		while ( $recent_study->have_posts() ) {
            			$recent_study->the_post();
            			$caseid = get_the_ID();
            			$meta = get_post_meta($caseid);
                        $casestudy = array();

                        $casestudy['project_name'] = get_the_title();
                        $casestudy['category'] = $meta['category'][0];
                        $casestudy['location'] = $meta['city'][0].', '.$meta['state'][0];
                        $casestudy['excerpt'] = get_the_excerpt($caseid, '');
                        $casestudy['testimonials'] = $meta['testimonials'][0];
                        $casestudy['permalink'] = get_the_permalink();
                        $casestudy['post_date'] = get_the_date('Y-m-d H:i:s');
                        //$casestudy['gallery'] = isset($meta['gallery']) ? CaseStudies\format_gallery2(maybe_unserialize($meta['gallery'][0]), $sites[$i]):'';
                        if(!empty($meta['job_categories'])){
                            $casestudy['job_categories'] =  implode(', ', unserialize($meta['job_categories'][0]));
                        }
                        $allcasestudies[] = $casestudy;
            		}
                    wp_reset_postdata();
            	}
            	restore_current_blog();
            }
        }
        // if not corporate site show only case studies for the location
        if(get_current_blog_id() != 1){
            $args_case = array(
                            'posts_per_page '   => 2,
                            'numberposts'      => 2,
                            'orderby'          => 'date',
                            'order'            => 'DESC',
                            'post_type'        => 'case-studies',
                            'post_status'      => 'publish',
                            'meta_query' => array(
                                'relation' => 'OR',
                                array(
                                    'key' => 'category',
                                    'value' => $categoryval,
                                    'compare' => '='
                                )
                                /*,
                                array(
                                    'key' => 'job_categories',
                                    'value' => sprintf(':"%s";', ucwords($categoryname)),
                                    'compare' => 'LIKE'
                                )
                                */
                            )
                        );
            $recent_study = new WP_Query( $args_case );
            $rsarray = array(); 
            if ( $recent_study->have_posts() ) {
                while ( $recent_study->have_posts() ) {
                    $recent_study->the_post();
                    $caseid = get_the_ID();
                    $meta = get_post_meta($caseid);
                    $casestudy = array();

                    $casestudy['project_name'] = get_the_title();
                    $casestudy['category'] = $meta['category'][0];
                    $casestudy['location'] = $meta['city'][0].', '.$meta['state'][0];
                    $casestudy['excerpt'] = get_the_excerpt($caseid, '');
                    $casestudy['testimonials'] = $meta['testimonials'][0];
                    $casestudy['permalink'] = get_the_permalink();
                    $casestudy['post_date'] = get_the_date('Y-m-d H:i:s');
                    //$casestudy['gallery'] = isset($meta['gallery']) ? CaseStudies\format_gallery2(maybe_unserialize($meta['gallery'][0]), $sites[$i]):'';
                    if(!empty($meta['job_categories'])){
                        $casestudy['job_categories'] =  implode(', ', unserialize($meta['job_categories'][0]));
                    }
                    $allcasestudies[] = $casestudy;
                }
                wp_reset_postdata();
            }

        }
        if($allcasestudies && !empty($allcasestudies)){
            
            for($i = 0; $i < count($allcasestudies); $i++):
                if($i == 2){
                    break;
                }
            ?>
            <section>    
                <div class="case-info">
                    <p><strong>Project Name</strong><?php echo $allcasestudies[$i]['project_name']; ?></p>
                    <p><strong>Location</strong> <?php echo $allcasestudies[$i]['location']; ?></p>
                    <p><strong>Project Summary</strong> <?php echo $allcasestudies[$i]['excerpt']; ?></p>
                    <p><strong>Job Category</strong> <?php  echo (isset($allcasestudies[$i]['job_categories'])) ?  $allcasestudies[$i]['job_categories'] : 'Uncategorized'; ?>
                    <p><strong>Project Category</strong>
                        <?php echo $allcasestudies[$i]['category']; ?>
                    </p>
                    <?php if(!empty( $allcasestudies[$i]['testimonials'])): ?>
                    <p><strong>Testimonial</strong> <em><?php echo $allcasestudies[$i]['testimonials']; ?></em></p>
                    <?php endif; ?>
                </div>
                <div class="case-image">
                    <?php if(!empty( $allcasestudies[$i]['gallery'])): ?>
                    <?php echo $allcasestudies[$i]['gallery']; ?>
                    <?php endif; ?>
                    <a href="<?php echo $allcasestudies[$i]['permalink']; ?>" class="btn btn-info">View this Case Study</a>
                    
                </div> 
                <div class="clearfix"></div>
            </section>
            <?php
            endfor;
        }
    ?>
</div>