<?php  	use Surepress\Functions\Common as Common;?>
<?php $option = get_option("dm_settings"); ?>
<section class="home-reviews">
	<?php 
		if ( get_current_blog_id() == 96 ) {
			echo '<h3>Reviews <small>' . (isset($option['guild_quality']['home_review_title']) ? $option['guild_quality']['home_review_title'] : '') . '</small></h3>';
		} else {
			echo '<h2>Reviews <small>' . (isset($option['guild_quality']['home_review_title']) ? $option['guild_quality']['home_review_title'] : '') . '</small></h2>';
		}
	?>

	<?php 
		$featured_reviews = get_option('featured_reviews'); 
	?>
	<?php $counter = 1; ?>
	<div id="homeReviewCarousel" class="carousel slide" <?php echo (!empty($featured_reviews) ? 'data-ride="carousel"' : ''); ?> >
		<ul class="carousel-inner">
			<?php if($featured_reviews): ?>
				<?php foreach($featured_reviews as $review) : ?>
					<?php 
						$entry = GFAPI::get_entry( $review );
					?>
						<?php if (isset($entry->errors)) continue; ?>					
						<li class="item <?php echo ($counter === 1) ?'active':''; ?>">
							<?php if ($entry['status'] !== "publish"): ?>
								<p>
									<?php if(strlen($entry[5]) > 350 ): ?>
										<?php echo substr($entry[5], 0, 350); ?><span class="moreContent"><?php echo substr($entry[5], 350); ?></span>
										<a href="#" class="moreReviewContent">...more</a>										
									<?php else: ?>	
										<?php echo $entry[5]; ?>
									<?php endif; ?>	
								</p>
								<strong><?php echo $entry[6]; ?> | <?php echo $entry[8]; ?>, <?php echo $entry[9]; ?></strong>
							<?php endif; ?>
							<?php $counter++; ?>							
						</li>
				<?php endforeach; ?>
			<?php endif; ?>		

			<?php if ($counter === 1) : ?>
				<li class='active'><p>No reviews found.</p> 
				<strong>Be the first one to <br> <a class="btn btn-info" href="<?php echo get_site_url(); ?>/about/reviews/">Submit A Review</a></strong>
				</li>
			<?php endif; ?>	

		</ul>
		<?php if($featured_reviews && $counter > 1): ?>
			<!-- Left and right controls -->
			  <a class="left carousel-control" href="#homeReviewCarousel" data-slide="prev">
			    <span class="glyphicon glyphicon-menu-left"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#homeReviewCarousel" data-slide="next">
			    <span class="glyphicon glyphicon-menu-right"></span>
			    <span class="sr-only">Next</span>
			  </a>	
		<?php endif; ?>
	</div>
</section>