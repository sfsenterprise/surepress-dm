<?php 
use Surepress\Functions\Common as Common;

$announcement = array();
$fields = maybe_unserialize($wp_query->nap->microsite_fields);


if( !Common\is_main() && isset($fields['microsite_announcement_override']) && $fields['microsite_announcement_override'] == 1) {
	$announcement = $fields;	
} else {
	global $wpdb;
  $global = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE microsite_blog_id = '1'");
	$announcement = maybe_unserialize($global->microsite_fields);
}
?>

<?php if ( isset($announcement['microsite_announcement']) && $announcement['microsite_announcement'] !== "" ) : ?>
	<section class="announcements">
	  <div class="row">
			<a href="#close" id="announcementClose">
				CLOSE <i class="fa fa-times-circle"></i>
			</a>

			<?php echo $announcement['microsite_announcement']; ?>
	  </div>
	</section>
<?php endif; ?>