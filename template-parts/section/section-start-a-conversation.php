		<?php
		if(get_post_type() == 'job-opportunities' || is_page(array('job-opportunities', 'job-opportunities-thank-you'))):

		?>
		<div  id="applyForm"></div>	
		<section class="start-a-conversation">
			<h3>Apply to a Job Opportunity</h3>
			
		
			 <?php  gravity_form( 'Job Opportunity',false, false,false, false,true, 400, true );    ?>
		</section>
		<?php else: ?>
		<section class="start-a-conversation">
			<h3>Design Your Space With the Help of Our Experts.</h3>
			
		
			 <?php  gravity_form( 'Start A Conversation',false, false,false, false,true, 400, true );    ?>
		</section>
		<?php endif; ?>