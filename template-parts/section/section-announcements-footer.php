<?php 
	global $wp_query;
	$microsites = array('fredericksburg', 'springfield');
?>
<?php if ( in_array($wp_query->nap->microsite_slug, $microsites) ) : ?>
<section class="announcementsFooter">
  <div class="row">
  	<a href="#close" id="announcementFooterClose">CLOSE <i class="fa fa-times-circle"></i></a>
	<p>
		A note to our homeowners about the coronavirus.
		<strong>Your health and safety is our first priority.</strong> &nbsp; 
		<a href="/covid-19-policy" class="btn btn-sm btn-secondary">Learn More</a>
	</p>
  </div>
</section>
<?php endif; ?>