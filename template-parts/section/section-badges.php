<?php $option = get_blog_option(get_current_blog_id(), 'dm_settings'); ?>
<section class="badges">
	<ul>
		<?php if( isset($option['badges']) && !empty($option['badges'])) : ?> 
			<?php foreach ($option['badges'] as $badge) : ?>
					<li>
						<?php if( $badge['link'] != ""  ): ?><a href="<?php echo $badge['link']; ?>" target="_blank"><?php endif; ?>
						<img src="<?php echo (isset($badge['image']) ? wp_get_attachment_image_src($badge['image'], 'full')[0] : '')?>" 
							 alt="<?php echo (isset($badge['alt']) ? $badge['alt'] : '');?>" 
							 title="<?php echo (isset($badge['title']) ? $badge['title'] : '');?>"
						/>
						<?php if( $badge['link'] != "" ): ?></a><?php endif; ?>
					</li>
			<?php endforeach; ?>
		<?php else : ?>
			<li class="nkba"><a href="https://nkba.org/" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/nkba.png') ?>" /></a></li>
			<li class="nahb"><a href="https://www.nahb.org/" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/nahb.jpg') ?>" /></a></li>
			<li class="nari"><a href="https://www.nari.org/" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/nari.jpg') ?>" /></a></li>
			<li class="big"><a href="http://www.remodeling.hw.net/benchmarks/big50/the-2017-big50_o" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/big50.png') ?>" /></a></li>
			<li class="design-houzz"><a href="https://www.houzz.com/best-of-houzz-2017" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/houzz-design.jpg') ?>" /></a></li>
			<li class="service-houzz"><a href="https://www.houzz.com/best-of-houzz-2017" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/houzz-service.jpg') ?>" /></a></li>
			<li class="guild"><a href="https://www.guildquality.com/guildmaster" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/guildmaster.jpg') ?>" /></a></li>
			<li class="excel"><a href="https://www.chrysalisawards.com/" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/chrysalis.png') ?>" /></a></li>
		<?php endif; ?>
	</ul>
</section>