<?php 	
	use Surepress\Functions\Common as Common; 
?>
	<?php if( !empty(Common\get_blog_posts(2)) ): ?>
		<section class="blog-posts">
			<?php foreach( Common\get_blog_posts(2) as $blog) : ?>
				<article>
					<h3><?php echo $blog->post_title; ?></h3>
					<p><?php echo ($blog->post_excerpt != "") ? $blog->post_excerpt : substr( strip_tags($blog->post_content), 0, 230).'...' ; ?></p>
					<p align="right"><a href="<?php echo get_permalink($blog->ID); ?>">Read More...</a></p>
				</article>
			<?php endforeach; ?>
			<p class="clearfix">&nbsp;</p>
				<p align="center"><a href="<?php echo get_site_url(); ?>/blog/" class="btn btn-primary">More Posts</a></p>
		</section>	
	<?php endif; ?>
