<div class="review-container">
    <h2>Reviews</h2>
    <span>See what DreamMaker customers are saying</span>
    <div class="review-slider">
        <figure class="review-content">
            <blockquote>They did a very good job. It was done in a timely manner. I felt safe and secure with the people that came to work at my house.</blockquote>
            <figcaption class="author">Terry M</figcaption>
        </figure>
        <figure class="review-content">
            <blockquote>Very courteous, very professional. Cleaned up every time they left.</blockquote>
            <figcaption class="author">Lauren J</figcaption>
        </figure>
        <figure class="review-content">
            <blockquote>DreamMaker just finished replacing our kitchen, dining room, entryway and bathroom floors as well as remodeling our kitchen and parts of the bathroom. They were WONDERFUL!</blockquote>
            <figcaption class="author">Colleen L</figcaption>
        </figure>
        <figure class="review-content">
            <blockquote>I've found DreamMaker to be highly professional, creative and ethical bath and kitchen remodelers. Highly recommended!</blockquote>
            <figcaption class="author">Brad V</figcaption>
        </figure>
        <figure class="review-content">
            <blockquote>They did a fantastic job with my kitchen.  I go into my kitchen and can't believe it is mine.  They were fabulous to work with and I recommended them to all my friends.</blockquote>
            <figcaption class="author">Max & Lori A</figcaption>
        </figure>
        <figure class="review-content">
            <blockquote>DreamMaker made our experience easier than expected. They did not fall short of my expectations. We will use them again for additional remodeling in the future. They did wonderful tile work and were very respectful to us and our home.</blockquote>
            <figcaption class="author">Scott & Rene T</figcaption>
        </figure>
        <figure class="review-content">
            <blockquote>DreamMaker Bath & Kitchen went above and beyond with my project. We added to it once they got started and all was done in a very timely manner.</blockquote>
            <figcaption class="author">Janice R</figcaption>
        </figure>
    </div>
</div>