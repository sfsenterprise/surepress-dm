<div class="container">
	<div class="row">
		<h2>Search results for <em>'<?php echo $_GET['s']; ?>'</em></h2>
		
		<?php if( have_posts() ) : ?>	
			<ul class="search-wrapper">
			<?php while ( have_posts() ) : the_post(); ?>
				<li>
					<article>
						<h6><?php the_title(); ?></h6>
						<hr>
						<p><?php echo wp_trim_words( get_the_content(), 70,'[...]' ); ?></p>
						<a href="<?php echo get_the_permalink();?>" class="btn btn-primary">Read More </a>
					</article>
					<figure><?php the_post_thumbnail(array(400,400)); ?></figure>
					<div class="clearfix"></div>
				</li>
			<?php endwhile; ?>
			</ul>
			<div class="clearfix"></div>
			<div align="center"><?php echo paginate_links(); ?></div>
		<?php else : ?>
			<h3>Nothing found.</h3>
		<?php endif; ?>
		
	</div>
</div>

