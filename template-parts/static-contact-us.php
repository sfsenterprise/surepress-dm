
<?php 
	use Surepress\Functions\Common as Common; 
	use Surepress\Functions\Locations as Locations; 
	use Surepress\Functions\Assets as Assets;
	$mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($wp_query->nap->microsite_fields)['mongoose_phone']);
?>
<h2>Start a Conversation</h2>
<p class="intro">Our remodeling team wants to provide the absolute best experience for each of our clients. Whether you need more information about a remodeling project, are interested in becoming a franchisee, or just want to give feedback in general... please feel free to contact us by clicking on the links below. We look forward to hearing from you!</p>

<div class="contact-form-wrapper">
	<h2> How Can We Help You? </h2>
	 <?php  gravity_form( 'Contact Us',false, false,false, false,true, 100, true );    ?>
</div>

<address class="col-xs-12 col-lg-5">
	<?php if( Common\is_main() ) : ?>
		<div class="title">Corporate Headquarters</div>
	<?php else : ?>	
		<div class="title">Design Center Location</div>
	<?php endif; ?>	

	<?php echo Common\MicrositeAddress(true); ?><br><br>
	<!--<a class="btn btn-primary">Location of your nearest franchise</a><br><br>-->
	<div class="title">Email and telephone</div>
	For Residential Inquiries:<br>
	<a class="phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $wp_query->nap->microsite_phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $wp_query->nap->microsite_phone); ?></a><br>
	<?php if ( !empty(unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']) ): ?>
		<a href="mailto:<?php echo unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']; ?>"><?php echo unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']; ?></a>
	<?php endif; ?>
	<br><br>
	<div class="title">Hours of Operation:</div>
	  	<?php if( !empty( get_blog_option($wp_query->nap->microsite_blog_id, "operating_hours") ) ) : ?>
	  	<dl>	
	  	<?php foreach( get_blog_option($wp_query->nap->microsite_blog_id, "operating_hours") as $day => $hours ) : ?>
		  	<dt><?php echo $day; ?></dt>
		  		<dd>
		  			<?php if( array_key_exists("appointment", $hours) ) : ?>
		  				<a href="tel:<?php echo $wp_query->nap->microsite_phone; ?>"><strong>By Appointment Only</strong></a>
		  			<?php elseif( array_key_exists("closed", $hours) ) : ?>	
		  				CLOSED
		  			<?php else : ?>	
		  				<?php echo date('g:iA', strtotime($hours['opening'])).' - '.date('h:iA', strtotime($hours['closing'])) ; ?>
		  			<?php endif; ?>		  			
		  		</dd>
	  	<?php endforeach; ?>
	  	<?php else : ?>
	  		<dt>Coming Soon!</dt><dd></dd>
	  	<?php endif; ?>	 	
	  	</dl>
</address>
	
<div class="col-xs-12 col-lg-7">	
	<div id="sfl_map"
		 data-pin="<?php echo Assets\asset_path('images/map-icon.png') ?>" 
         data-lat="<?php echo ($wp_query->nap->microsite_lat != "0.000000") ? $wp_query->nap->microsite_lat : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lat'] ; ?>" 
         data-lng="<?php echo ($wp_query->nap->microsite_lng != "0.000000") ? $wp_query->nap->microsite_lng : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lng'] ; ?>"
	     data-title="<?php echo ( Common\is_main() ) ? 'DreamMaker Bath & Kitchen Corporate Headquarters' : 'DreamMaker of '.$wp_query->nap->microsite_name; ?>"
	     data-address="<?php echo Common\MicrositeAddress(true); ?>"
	     data-phone="<?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $wp_query->nap->microsite_phone); ?>"
	></div>	
</div>

<div class="clearfix"></div>
