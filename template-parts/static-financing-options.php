<?php
	use Surepress\Functions\Assets as Assets;

	$default_disclaimer = '* DreamMaker Bath and Kitchen ("DreamMaker") and its independently owned and operated franchisees are neither brokers nor lenders. Financing is provided by third-party lenders unaffiliated with DreamMaker or its franchisees, under terms and conditions arranged directly between the customer and such lender, all subject to credit requirements and satisfactory completion of finance documents. DreamMaker does not assist with, counsel or negotiate financing, other than providing customers an introduction to lenders interested in financing DreamMaker customers. Not all buyers may qualify. Offer valid only for participating dealers in geographical areas served by DreamMaker or its designees. Some conditions may apply; see www.dreammaker-remodel.com or your local DreamMaker franchisee for more details. Each franchise location is independently owned and operated.';

	switch_to_blog(1);
	$finance_page = get_page_by_path('financing-options');
	$disclaimer = (get_field('disclaimer', $finance_page->ID) ? get_field('disclaimer', $finance_page->ID) : $default_disclaimer );
	restore_current_blog();	
?> 

<h2>Financing Options For Your Dream Remodel</h2>
<p>Remodeling is a big decision and it's important you get what you want. We offer third party financing to give you the convenience to purchase the remodel of your dreams the way that works for you.</p>
<p>&nbsp;</p>
<?php $financing_options = get_posts(['post_type'=>'financial-options', 'post_status'=>'any', 'posts_per_page'=>-1]); ?>
<?php $corporate_includes = get_option('corporate_financing'); ?>

<div class="offer-wrapper offer-categories hidden-xs">

	<ul class="nav nav-tabs">
		<?php $count = 0; ?>
		<?php $today = new DateTime(); ?>
		<?php $today = $today->format('m/d/Y'); ?>

		<?php foreach($corporate_includes as $key => $option): ?>
			<?php switch_to_blog(1); ?>
			<?php $corporate_option = get_post($option); ?>
			<?php $options = get_post_meta($option, 'options', true); ?>
			<?php restore_current_blog(); ?>
			
			<?php if(!empty( $options['show']) &&  $options['show'] == 1 && strtotime($options['expiration']) >= strtotime($today)) : ?>

				<li class="<?php echo $count == 0 ? 'active' : ''; $count++; ?>">
					<a href="#tab-<?php echo $key; ?>" data-toggle="tab" data-id="<?php echo $key; ?>">
						<?php echo $corporate_option->post_title; ?>
					</a>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>

		<?php foreach($financing_options as $key => $option): ?>
			<?php $options = get_post_meta($option->ID, 'options', true); ?>

			<?php if(!empty( $options['show']) &&  $options['show'] == 1 && strtotime($options['expiration']) >= strtotime($today)) : ?>

				<li class="<?php echo $count == 0 ? 'active' : ''; $count++; ?>">
					<a href="#tab-<?php echo count($corporate_includes) + $key; ?>" data-toggle="tab" data-id="<?php echo count($corporate_includes) + $key; ?>">
						<?php echo $option->post_title; ?>
					</a>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>

	<div class="tab-content">
		<?php $count = 0; ?>
		<?php if(is_array($corporate_includes) && !empty($corporate_includes)) : ?>
			<?php foreach($corporate_includes as $key => $option): ?>
				<?php switch_to_blog(1); ?>
				<?php $corporate_option = get_post($option); ?>
				<?php $options = get_post_meta($corporate_option->ID, 'options', true); ?>
				<?php $thumb = (!empty($options['script']) ? $options['script'] : wp_get_attachment_image($options['image'], 'full', '',['class'=> 'col-md-7 img-responsive'] )); ?>
				<?php restore_current_blog(); ?>
				
				<?php if(!empty( $options['show']) &&  $options['show'] == 1 && strtotime($options['expiration']) >= strtotime($today)) : ?>

					<div class="tab-pane <?php echo $count == 0 ? 'active' : ''; $count++; ?>" id="tab-<?php echo $key;?>">
						<figure>
							<?php echo $thumb; ?>

							<figcaption class="col-md-5">
								<h5><?php echo $corporate_option->post_title; ?></h5>

								<?php if(!empty($options['details'])) : ?>
									<p><?php echo stripslashes_deep($options['details']); ?></p>

									<hr>

									<?php if ( !empty($options['view-details']) ): ?>
										<a class="btn btn-default" href="<?php echo $options['view-details']; ?>" target="_blank">View Offer</a>
									<?php endif; ?>
								<?php endif; ?>
							</figcaption>
						</figure>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>

		<?php if(is_array($financing_options) && !empty($financing_options)) : ?>
			<?php foreach($financing_options as $key => $option) : ?>
				<?php $options = get_post_meta($option->ID, 'options', true); ?>

				<?php if(!empty( $options['show']) &&  $options['show'] == 1 && strtotime($options['expiration']) >= strtotime($today)) : ?>

					<div class="tab-pane <?php echo $count == 0 ? 'active' : ''; $count++; ?>" id="tab-<?php echo count($corporate_includes) + $key;?>">
						<figure>
							<?php echo (!empty($options['script']) ? $options['script'] : wp_get_attachment_image($options['image'], 'full', '',['class'=> 'col-md-7 img-responsive'] )); ?>

							<figcaption class="col-md-5">
								<h5><?php echo $option->post_title; ?></h5>

								<?php if(!empty($options['details'])) : ?>
									<p><?php echo stripslashes_deep($options['details']); ?></p>

									<hr>

									<?php if ( !empty($options['view-details']) ): ?>
										<a class="btn btn-default" href="<?php echo $options['view-details']; ?>" target="_blank">Apply Now</a>
									<?php endif; ?>
									<?php if($option->post_title === 'Fast & Easy Home Improvement Financing'): ?>
										<p style="font-size: 10px;">* We May be Compensated by LightStream through this Link</p>
									<?php endif; ?>
								<?php endif; ?>
							</figcaption>
						</figure>
					</div> 
				<?php endif;?>
			<?php endforeach;?>
		<?php endif;?>
	</div>
</div>

<div class="financing-offers-container mobile hidden-sm hidden-md hidden-lg hidden-xl">
	<div class="panel-group" id="financing-offers-accordion-mobile">
		<div class="panel panel-default">
		</div>
	</div>
</div>

<small><?php echo $disclaimer; ?></small>
