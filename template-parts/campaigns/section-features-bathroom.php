<?php 
	use Surepress\Functions\Assets as Assets;
?>

<section class="campaign-features">
	<h2>Get the Perfect Bathroom Remodel for Your Home</h2>
	<h4>Bathroom Remodeling Made Easy With DreamMaker <?php echo $wp_query->nap->microsite_name; ?></h4>
	
	<div class="container">
		<?php if ( get_the_content(get_the_ID()) === ""): ?>
		<ul>
			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/bath-1.png') ?>" />
					<figcaption>
						<h5>Full-Service Remodeling</h5>

						By handling all aspects of the design and the remodeling, DreamMaker can ensure consistency and efficiency throughout all stages of the process.					
					</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/bath-2.png') ?>" />
					<figcaption>
						<h5>Personalized Care</h5>

						While planning the remodel, DreamMaker will work closely with you to help you select the products that will best reflect your vision for your home. 					
					</figcaption>
				</figure>	
			</li>	

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/bath-3.png') ?>" />
					<figcaption>
						<h5>Creative Design Ideas</h5>

						If you want some inspiration for your bathroom, you can find it at DreamMaker. Backed by remarkable creativity and experience.					
					</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/bath-4.png') ?>" />
					<figcaption>
						<h5>Bathroom Design Experts</h5>

						Our design experts will provide you with everything from start to finish when updating your bathroom. Offering everything even minor details.					
					</figcaption>
				</figure>	
			</li>						
		</ul>	
		<?php else : ?>
			<?php the_content(); ?>
		<?php endif; ?>
	</div>

	<p><a href="#campaignsanchor" class="btn btn-primary">START A CONVERSATION</a></p>
</section>	