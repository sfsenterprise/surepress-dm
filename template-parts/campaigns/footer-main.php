<?php
	use Surepress\Functions\Assets as Assets;
	use Surepress\Functions\Common as Common;
	use Surepress\Functions\Services as Services;

	$template =  get_post_meta( get_the_ID(), 'campaign_template', true ); 
?>	
		<?php if ($template === "careers") : ?>
			<div  id="applyForm"></div>	
			<section class="start-a-conversation">
				<h3>Apply to a Job Opportunity</h3>
				 <?php  gravity_form( 'Job Opportunity',false, false,false, false,true, 400, true );    ?>
			</section>
		<?php endif; ?>			

		
			<?php get_template_part('template-parts/campaigns/section', 'badges' ); ?>
			<?php $option = get_blog_option(get_current_blog_ID(), 'dm_settings'); ?>
			<?php $ccorp_text = (isset($option['footer_ccorp'][0]) && !empty($option['footer_ccorp'][0])) ? $option['footer_ccorp'][0] : "&copy; 2005-" . date('Y') . " Worldwide Refinishing Systems Inc. dba DreamMaker Bath & Kitchen. Independently Owned and Operated Franchises."; ?>
			<footer id="colophon" class="site-footer campaigns-footer" role="contentinfo">
				<div class="container-fluid">
					<div class="row">	
						<?php $id = (get_post_meta(get_the_ID(), 'campaign_footerlogo', TRUE )); ?>   
						<?php $image_url = Common\is_main() ? Assets\asset_path('images/footer-plural.png') : Assets\asset_path('images/footer-singular.png'); ?>

						<?php 
						if(!empty($id)): 
								$img = wp_get_attachment_image_src($id); 
								if( isset($img) && !empty($img) ){
									$image_url = wp_get_attachment_image_src($id)[0];
								}
						endif; 
						?>

						<figure>
							<a href="<?php echo site_url('/'); ?>"><img src="<?php echo $image_url;?>" height="50" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/></a>
							
							<figcaption>
								<div><?php echo $wp_query->nap->microsite_name; ?></div>

								<?php 
								$removed_entity_names = array("Southern Rhode Island", "The Woodlands", "Northwest Arkansas");
								$entity_loc = $wp_query->nap->microsite_name;

								if (!in_array($entity_loc, $removed_entity_names)) : ?>
									<div class="ccorp"><?php echo $wp_query->nap->microsite_owner; ?></div>
								<?php endif; ?>

							</figcaption>			
						</figure>
						
						<address>
							<?php 
								$street = get_post_meta(get_the_ID(), 'campaign_street', TRUE );
								$street2 = get_post_meta(get_the_ID(), 'campaign_street2', TRUE );
								$city = get_post_meta(get_the_ID(), 'campaign_city', TRUE );
								$state = get_post_meta(get_the_ID(), 'campaign_state', TRUE );
								$zip = get_post_meta(get_the_ID(), 'campaign_zip', TRUE );
								$email = get_post_meta(get_the_ID(), 'campaign_email', TRUE );
								$phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
								$phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
							?>
								<p>
									<?php echo $street . (!empty($street2) ? ", ".$street2 : ""); ?><br>
									<?php echo $city .", ". $state.", ".$zip; ?><br>
									<?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
									<strong class="phone"><i class="fa fa-phone"></i><a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a></strong>
									
								</p>	
						</address>
					</div>
				</div>			

				<p class="copyright"><?php echo $ccorp_text; ?></p>			
			</footer>