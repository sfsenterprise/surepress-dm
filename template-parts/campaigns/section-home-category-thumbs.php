<?php  use Surepress\Functions\Common as Common;  
global $cta_option;
$whydmhl = get_post_meta(get_the_ID(), 'why_dm_headline', true)
?>

<section class="campaign-home-category-thumbs home-category-thumbs container">
<?php $keyword = get_post_meta( get_the_ID(), 'campaign_template', TRUE );
  if(isset($whydmhl) && $whydmhl !== '') :
?>
<h2><?php echo $whydmhl;  ?></h2>
<?php
  else:
?>
  <h2>Why Work With DreamMaker For Your Remodeling Project? </h2>
<?php endif; ?>
  <ul>
    <li>
      <figure>
        <img height=100 src="<?php echo Surepress\Functions\Assets\asset_path('images/campaign-thumb1.png') ?>">
      </figure>
      <figcaption>
        <h3>100% Recommendation Rate*</h3>
        <p>Of the hundreds of DreammMaker clients surveyed around the country, 100% told Guild Quality they would recommend us to remodel your home.</p>
      </figcaption>
    </li>
    <li>
      <figure>
      <img src="<?php echo Surepress\Functions\Assets\asset_path('images/campaign-thumb2.png') ?>">
      </figure>
      <figcaption>
        <h3>Code of Values &trade;</h3>
        <p>We  believe if we treat people with respect and integrity, success will follow. Our code of values is central to who we are in business and in life. We hope you will see the difference.</p>
      </figcaption>
    </li>
    <li>
      <figure>
      <img src="<?php echo Surepress\Functions\Assets\asset_path('images/campaign-thumb3.png') ?>">
      </figure>
      <figcaption>
        <h3>It's About You</h3>
        <p>Our distinctive remodeling specialists are committed to delivering a customized design that will be completed in the most efficient manner possible with minimal disruption to your life.</p>
      </figcaption>
    </li>
  </ul>

  <!-- <center><a class="btn btn-info btn-lg" href="#campaignsanchor">Start a Conversation</a></center> -->
  <center><a class="btn btn-info btn-lg" href="#campaignsanchor"><?php echo (isset($cta_option) && $cta_option != '' ? $cta_option : 'START A CONVERSATION');  ?></a></center>
</section>