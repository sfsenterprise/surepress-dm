<?php 
$button_label = get_post_meta( get_the_ID() , 'campaign_cta_btn', true);
$options = get_post_meta(get_the_ID(), 'campaign_slider');
$option['slider'] = isset($options[0]) ?$options[0] : array() ; 
?>

<section class="home-slider campaign-slider">
	<div id="homeTopCarousel" class="carousel slide" data-ride="carousel">

		<?php if(isset($option['slider']) && !empty($option['slider'])) :  ?>

			<ol class="carousel-indicators">
				<?php for($i= 0; $i < count($option['slider']); $i++) : ?>
					<li data-target="#homeTopCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i ==0 ? 'active' : '')?>"></li>
				<?php endfor; ?>
			</ol>
			<div class="carousel-inner">
				<?php for($i= 0; $i < count($option['slider']); $i++){ ?>
					<figure class="item <?php echo ($i ==0 ? 'active' : '')?>">
						<img src="<?php echo wp_get_attachment_image_src($option['slider'][$i]['image'], 'full')[0];?>"alt="<?php echo $option['slider'][$i]['alt'];?>" title="<?php echo $option['slider'][$i]['title'];?>">
						<figcaption>
							<p class="<?php echo(empty($option['slider'][$i]['slide_title']) &&  empty($option['slider'][$i]['slide_caption'])) ? 'hidden' : '';  ?>">
								<em><?php echo (isset($option['slider'][$i]['slide_caption']) ? strip_tags($option['slider'][$i]['slide_caption'], '<br>') : '');?></em>
						      	<strong><?php echo (isset($option['slider'][$i]['slide_title']) ? strip_tags($option['slider'][$i]['slide_title'],'<br>') : '');?></strong>
					      	</p>

					      	<p><a href="#applyForm"><?php echo (isset($button_label) && $button_label != '' ? $button_label : 'Apply for this job');  ?></a></p>
						</figcaption>
					</figure>
				<?php } ?>
			</div>

	<?php endif; ?>

	</div>
</section>
