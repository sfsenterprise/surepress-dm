<?php 
	use Surepress\Functions\Assets as Assets;
?>

<section class="campaign-features">
	<h2>Get the Perfect Kitchen Remodel With DreamMaker <?php echo $wp_query->nap->microsite_name; ?></h2>
	<h4>Our <?php echo $wp_query->nap->microsite_name; ?> Design Team Will Help You Every Step of the Way</h4>
	
	<div class="container">
		<?php if ( get_the_content(get_the_ID()) === ""): ?>
		<ul>
			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/kitchen-1.png') ?>" />
					<figcaption>
						<h5>Expert Craftsmanship in Every Detail</h5>

						With DreamMaker <?php echo $wp_query->nap->microsite_name; ?>, you can be sure of a cooking space that has been designed with the utmost skill and precision. Transform your kitchen today!					
					</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/kitchen-2.png') ?>" />
					<figcaption>
						<h5>Timeless Transitional Kitchen Designs</h5>

						Transform your kitchen into a timeless space. Whether you’re looking for modern sophistication or classic charm, let their transitional designs bring any vision to life!				
					</figcaption>
				</figure>	
			</li>	

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/kitchen-3.png') ?>" />
					<figcaption>
						<h5>Contemporary Kitchens That Will Inspire</h5>

						DreamMaker <?php echo $wp_query->nap->microsite_name; ?> designs kitchens that will ignite your creative spark! From modern to traditional, these contemporary spaces are sure to inspire.				
					</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/kitchen-4.png') ?>" />
					<figcaption>
						<h5>Traditional Kitchens for Modern Homes</h5>

						DreamMaker <?php echo $wp_query->nap->microsite_name; ?> helps homeowners bring classic charm into their homes with stylish and timeless kitchens that will last for decades.					
					</figcaption>
				</figure>	
			</li>						
		</ul>	
		<?php else : ?>
			<?php the_content(); ?>
		<?php endif; ?>
	</div>

	<p><a href="#campaignsanchor" class="btn btn-primary">START A CONVERSATION</a></p>
</section>	