<?php 
	use Surepress\Functions\Assets as Assets;
?>

<section class="campaign-steps">
	<h2>Our Easy 6 Step Process for Kitchen and Bathroom Remodeling</h2>
	<h4><?php echo $wp_query->nap->microsite_name; ?>'s Favorite Kitchen and Bathroom Remodeling Process</h4>
	<div class="container">
		<ul>
			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/campaign-v2-01.png') ?>" />
					<figcaption>Meet With a DreamMaker Designer</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/campaign-v2-02.png') ?>" />
					<figcaption>Home Survey</figcaption>
				</figure>	
			</li>	

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/campaign-v2-03.png') ?>" />
					<figcaption>Design Review & Written Proposal</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/campaign-v2-04.png') ?>" />
					<figcaption>Sign Agreement</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/campaign-v2-05.png') ?>" />
					<figcaption>Pre-Start Conference Meeting</figcaption>
				</figure>	
			</li>

			<li>
				<figure>
					<img src="<?php echo Assets\asset_path('images/campaign-v2-06.png') ?>" />
					<figcaption>Your Project Manager is Your Contact</figcaption>
				</figure>	
			</li>									
		</ul>	
	</div>	
	<p><a href="#campaignsanchor" class="btn btn-primary">START A CONVERSATION</a></p>
</section>	