<?php
	$yt_id = get_post_meta(get_the_ID(), 'campaign_video', TRUE);
?>

<?php if($yt_id && $yt_id !== "") : ?>
<section class="campaign-video" style="text-align: center; margin: -85px -15px 0; padding: 0; background: linear-gradient(180deg, rgba(2,0,36,1) 0%, rgba(67,42,113,1) 0%, rgba(60,38,101,1) 64.8%, rgba(52,33,88,0) 65%, rgba(0,0,0,0) 100%);">
	<div class="container">
		<h2 style="color: #fff; margin: 30px 15px">Learn More About the DreamMaker Remodeling Process</h2>		
		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $yt_id; ?>?modestbranding=1&rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	  
		</div>		
	</div>	
</section>
<?php endif; ?>