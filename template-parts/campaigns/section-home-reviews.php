<?php  	use Surepress\Functions\Common as Common;?>
<?php $option = get_option("dm_settings"); ?>
<section class="home-reviews campaign-reviews">
	<h2>Reviews</h2>
	<p><?php echo isset($option['guild_quality']['home_review_title']) ? $option['guild_quality']['home_review_title'] : '' ;?></p>
	<?php $featured_reviews = get_post_meta(get_the_ID(), 'campaign_reviews', TRUE); ?>
	<?php $counter = 1; ?>
	<div id="homeReviewCarousel" class="carousel slide" <?php echo (!empty($featured_reviews) ? 'data-ride="carousel"' : ''); ?> >
		<ul class="carousel-inner">
			<?php if($featured_reviews): ?>
				<?php foreach($featured_reviews as $review) : ?>
					<?php $entry = GFAPI::get_entry( $review ); ?>
					<?php if (!is_wp_error($entry)): ?>
						<li class="item <?php echo ($counter === 1) ?'active':''; ?>">
							<p><?php echo $entry[5]; ?></p>
							<strong><?php echo $entry[6]; ?> | <?php echo $entry[8]; ?>, <?php echo $entry[9]; ?></strong>
						</li>
					<?php endif; ?>
						<!-- <li class="item <?php echo ($counter === 1) ?'active':''; ?>">
							<?php if (!is_wp_error($entry)): ?>
								<p><?php echo $entry[5]; ?></p>
								<strong><?php echo $entry[6]; ?> | <?php echo $entry[8]; ?>, <?php echo $entry[9]; ?></strong>
							<?php else: ?>
								<p>No reviews found.</p> 
								<strong>Be the first one to <br> <a class="btn btn-info" href="<?php echo get_site_url(); ?>/about/reviews/">Submit A Review</a></strong>
							<?php endif; ?>
						</li> -->
						<?php $counter++; ?>
				<?php endforeach; ?>
			<?php else: ?>	
				<li class='active'><p>No reviews found.</p> 
				<strong>Be the first one to <br> <a class="btn btn-info" href="<?php echo get_site_url(); ?>/about/reviews/">Submit A Review</a></strong>
				</li>
			<?php endif; ?>		
		</ul>
		<?php if($featured_reviews): ?>
			<!-- Left and right controls -->
			  <a class="left carousel-control" href="#homeReviewCarousel" data-slide="prev">
			    <span class="glyphicon glyphicon-menu-left"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#homeReviewCarousel" data-slide="next">
			    <span class="glyphicon glyphicon-menu-right"></span>
			    <span class="sr-only">Next</span>
			  </a>	
		<?php endif; ?>
	</div>
</section>