<?php 
	use Surepress\Functions\Assets as Assets;
	use Surepress\Functions\Common as Common;

	$yt_id = get_post_meta(get_the_ID(), 'campaign_video', TRUE);
	$phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
	$phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
?>

<section id="about" class="kb-about">
	<div class="container">
		<?php if ( get_the_content(get_the_ID()) === ""): ?>
			<div class="about-wrapper">
				<div class="about-video">
					<div class="embed-responsive embed-responsive-16by9">
						<?php if($yt_id && $yt_id !== "") : ?>
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $yt_id; ?>?modestbranding=1&rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	  
						<?php else: ?>
							<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
								<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
									<iframe src="https://fast.wistia.net/embed/iframe/z6b6oi88jy?videoFoam=true " title="Y2Mate.is < https://y2mate.is/ > - DreamMaker Approach 30--2YtcNtv-X8-1080p-1657149860548 Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" msallowfullscreen width="100%" height="100%"></iframe>
								</div>
							</div>
							<script src=" https://fast.wistia.net/assets/external/E-v1.js " async></script>
						<?php endif; ?>
					</div>
				</div>
				<div class="about-content">
					<h4>ABOUT DREAMMAKER BATH & KITCHEN</h4>
					<h2>Make Your Dream Kitchen or Bathroom a Reality Today</h2>
					<p>Ready to turn your remodeling dreams into a reality? DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?> is here to help! Our talented team offers full-service remodeling, from design to installation, with a focus on meeting your unique needs. We prioritize communication and superior craftsmanship to ensure the smoothest possible process for you. Trust us to complete your project efficiently and with minimal disruption to your daily life. Want to learn more? Schedule a free consultation today!</p>
					<p><a href="#contact" class="btn btn-info btn-lg">Click Here to Book a Meeting</a></p>
					<?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
					<a class="phone-wrapper" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
					<i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
				</div>
			</div>
		<?php else : ?>
			<?php the_content(); ?>
		<?php endif; ?>
	</div>
</section>	