<?php 
    use Surepress\Functions\Common as Common; 
    use Surepress\Functions\Locations as Locations;
    use Surepress\Functions\Assets as Assets;
    $mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($wp_query->nap->microsite_fields)['mongoose_phone']);
?>
<section class="home-map">
    <?php //print_r($wp_query->nap); ?>
    <div id="sfl_map" 
         data-lat="<?php echo ($wp_query->nap->microsite_lat != "0.000000") ? $wp_query->nap->microsite_lat : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lat'] ; ?>" 
         data-lng="<?php echo ($wp_query->nap->microsite_lng != "0.000000") ? $wp_query->nap->microsite_lng : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lng'] ; ?>"
         data-title="<?php echo 'DreamMaker of '.$wp_query->nap->microsite_name; ?>"
         data-address="<?php echo Common\MicrositeAddress(true); ?>"
         data-phone="<?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $wp_query->nap->microsite_phone); ?>"
         data-pin="<?php echo Assets\asset_path('images/map-icon.png') ?>"
    ></div>
    <p class="map_buttons">
        <?php if( $wp_query->nap->microsite_street) : ?>
        <a class=" btn btn-primary direction-url" target="_blank" href="https://maps.google.com/maps/dir/?api=1&destination=<?php  echo urlencode( Common\MicrositeAddress() ); ?>" class="btn btn-primary" >Directions</a>

        <?php endif; ?>
        <a href="<?php echo get_site_url(); ?>/contact-us/" class="btn btn-link">Contact Us</a>
    </p>
</section>