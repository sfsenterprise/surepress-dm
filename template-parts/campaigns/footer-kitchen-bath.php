<?php
    use Surepress\Functions\Assets as Assets;

    $phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
    $phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
?>

<?php get_template_part('template-parts/campaigns/section', 'kb-badges' ); ?>
<section class="campaign-before-footer">
    <div class="campaign-before-footer-wrap">
        <h4>GET PROFESSIONAL DESIGN & REMODELING ADVICE TODAY FOR FREE</h4>
        <h2>Schedule a Free Consultation Today and Start Planning Your Dream Space</h2>
        <a class="btn btn-info btn-lg" href="#contact">Click Here to Book a Meeting</a>
        <?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
        <a class="before-footer-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
        <i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
    </div>
</section>
<footer id="colophon" class="site-footer campaigns-footer" role="contentinfo">
    <div class="footer-wrap" align="center">
        <a class="campaign-footer-logo" href="<?php echo site_url('/'); ?>">
            <img src=" <?php echo Assets\asset_path('images/main-logo-v1.png') ?>" title="DreamMaker Bath & Kitchen" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/> 
            <em><?php echo $wp_query->nap->microsite_name; ?></em>
        </a>   
    </div>
    <ul class="footer-menu">
        <li><a href="#about">About Us</a></li>
        <li><a href="#services">Our Services</a></li>
        <li><a href="#gallery">Photo Gallery</a></li>
        <li><a href="#contact">Contact Us</a></li>
    </ul>
    <p class="copyright">©2005-<?php echo date('Y'); ?> Worldwide Refinishing Systems Inc. dba DreamMaker Bath & Kitchen. This location is independently owned and locally operated by Rocky Point Remodeling under license from DreamMaker Bath & Kitchen.</p>		
</footer>