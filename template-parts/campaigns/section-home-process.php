<section class="campaign-process">
	<div class="container">
	<h2>Our Process 
		<small><?php echo get_post_meta(get_the_ID(), 'our_process_subheadline', TRUE); ?></small>
	</h2>
	<ol>
		<li class="process-meet">Meet With a <br>DreamMaker Designer</li>
		<li class="process-discovery">Home Survey & <br>Project Discovery</li>
		<li class="process-review">Review/Refine Designs <br>& Written Proposal</li>
		<li class="process-contract">Contract Signing</li>
		<li class="process-conference">Pre-Start<br> Conference Meeting</li>
		<li class="process-dedicated">Production With Your <br>Dedicated Project Manager</li>
</ol>	
	</div>
</section> 