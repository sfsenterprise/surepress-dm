<?php 
	use Surepress\Functions\Assets as Assets;

  $phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
  $phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
  $galleries = get_post_meta(get_the_ID(), 'campaign_kbgallery', TRUE );
?>

<section id="gallery" class="kb-gallery">	
	<div class="container">
    <h4>SEE BEAUTIFUL REMODELING PROJECTS</h4>
    <h2>Get Inspired With Our Gallery of Past Projects</h2>
		<div class="gallery-wrapper">
      <?php if(!empty($galleries)): ?>
        <?php foreach($galleries as $gallery): ?>
          <a href="<?php echo $gallery; ?>" data-fancybox="galleries">
            <img src="<?php echo $gallery; ?>"/>
          </a>
        <?php endforeach; ?>
      <?php else: ?>
        <center><p>No Galleries Found.</p></center>
      <?php endif; ?>
		</div>
    <div class="gallery-btn-wrap">
      <a class="btn btn-info btn-lg" href="#contact">Click Here to Book a Meeting</a>
      <?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
      <a class="gallery-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
      <i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
    </div>
	</div>
</section>	