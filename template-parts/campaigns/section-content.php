
<?php $img = get_post_meta( get_the_ID(), 'campaign_subphoto', TRUE ); ?>

<section class="campaign-content" style="background-image:url(<?php echo isset($img) ? wp_get_attachment_url($img) : ""; ?> )">
	<div class="container">
		<?php if( !empty( get_the_content() ) ): ?>
					<?php  the_content(); ?>
		<?php else: ?>
			<h2>It's Our Job, But We Never <br>Forget It's Your Home</h2>
			<p>Here at DreamMaker, we believe our bathroom remodeling projects do more for our clients than simply improve the look of their homes. Our work enhances our customers lives by providing them with a home that's more comfortable and better suited to their functional need both now and for many years to come.</p>
			<p>So, if you're interested in partnering with the home improvement experts at DreamMaker Bath & Kitchen to provide you with everything you need to turn your bathroom into a fully functional, modern space or even the spa of your dreams, <strong><i>contact us today</i></strong>.
		<?php endif; ?>
	</div>
</section>