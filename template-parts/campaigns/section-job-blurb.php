<?php  use Surepress\Functions\Common as Common;  
global $cta_option;
$whydmhl = get_post_meta(get_the_ID(), 'why_dm_headline', true)
?>

<section class="campaign-home-category-thumbs home-category-thumbs container">
  <h2>Why Work at DreamMaker Bath and Kitchen? </h2>

  <ul>
    <li>
      <figure>
        <img height=100 src="<?php echo Surepress\Functions\Assets\asset_path('images/campaign-thumb1.png') ?>">
      </figure>
      <figcaption>
        <h3>An excellent team</h3>
        <p>Of the hundreds of DreammMaker clients surveyed around the country, 96% told Guild Quality they would recommend us to remodel their home.</p>
      </figcaption>
    </li>
    <li>
      <figure>
      <img src="<?php echo Surepress\Functions\Assets\asset_path('images/campaign-thumb2.png') ?>">
      </figure>
      <figcaption>
        <h3>Code of Values &trade;</h3>
        <p>We  believe if we treat people with respect and integrity, success will follow. Our code of values is central to who we are in business and in life. We hope you will see the difference.</p>
      </figcaption>
    </li>
    <li>
      <figure>
      <img src="<?php echo Surepress\Functions\Assets\asset_path('images/campaign-thumb3.png') ?>">
      </figure>
      <figcaption>
        <h3>Our remodeling process</h3>
        <p>Our distinctive full service remodeling approach focuses on serving our clients from design through installation, with a dedication superior craftsmanship and ethical excellence.</p>
      </figcaption>
    </li>
  </ul>

  <center><a class="btn btn-info btn-lg" href="#applyForm">Apply for Job Here</a></center>
</section>