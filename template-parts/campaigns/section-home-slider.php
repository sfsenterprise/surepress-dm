<?php 
global $cta_option;
$options = get_post_meta(get_the_ID(), 'campaign_slider');
$option['slider'] = isset($options[0]) ?$options[0] : array() ; 
?>

<section class="home-slider campaign-slider">
	<div id="homeTopCarousel" class="carousel slide" data-ride="carousel">

		<?php if(isset($option['slider']) && !empty($option['slider'])) :  ?>

			<ol class="carousel-indicators">
				<?php for($i= 0; $i < count($option['slider']); $i++) : ?>
					<li data-target="#homeTopCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i ==0 ? 'active' : '')?>"></li>
				<?php endfor; ?>
			</ol>
			<div class="carousel-inner">
				<?php for($i= 0; $i < count($option['slider']); $i++){ ?>
					<figure class="item <?php echo ($i ==0 ? 'active' : '')?>">
						<img src="<?php echo wp_get_attachment_image_src($option['slider'][$i]['image'], 'full')[0];?>"alt="<?php echo $option['slider'][$i]['alt'];?>" title="<?php echo $option['slider'][$i]['title'];?>">
						<figcaption>
							<p class="<?php echo(empty($option['slider'][$i]['slide_title']) &&  empty($option['slider'][$i]['slide_caption'])) ? 'hidden' : '';  ?>">
								<em><?php echo (isset($option['slider'][$i]['slide_caption']) ? strip_tags($option['slider'][$i]['slide_caption'], '<br>') : '');?></em>
						      	<strong><?php echo (isset($option['slider'][$i]['slide_title']) ? strip_tags($option['slider'][$i]['slide_title'],'<br>') : '');?></strong>
					      	</p>
					      	<!-- <p><a href="#campaignsanchor">START A CONVERSATION</a></p> -->
					      	<p><a href="#campaignsanchor"><?php echo (isset($cta_option) && $cta_option != '' ? $cta_option : 'START A CONVERSATION');  ?></a></p>
						</figcaption>
					</figure>
				<?php } ?>
			</div>

		<?php else : ?>

			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#homeTopCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#homeTopCarousel" data-slide-to="1"></li>
				<li data-target="#homeTopCarousel" data-slide-to="2"></li>
				<li data-target="#homeTopCarousel" data-slide-to="3"></li>
				<li data-target="#homeTopCarousel" data-slide-to="4"></li>
				<li data-target="#homeTopCarousel" data-slide-to="5"></li>
				<li data-target="#homeTopCarousel" data-slide-to="6"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">

			    <figure class="item active">
			      <img src="<?php echo home_url('wp-content/uploads/sites/4/2018/07/Kitchen-Remodeling-1.jpg'); ?>">
					<figcaption>
				      	<p>
					      	<strong>Create the Kitchen</strong>
					      	<em>That Houses Your Greatest Memories</em>
				      	</p>
				      	<?php $kitchens = get_term_by( 'slug', 'traditional-kitchens', 'portfolio_categories' ) ?>

				      	<?php if($kitchens) : ?>
					     	<p><a href="<?php echo home_url('galleries/traditional-kitchens'); ?>">SEE MORE KITCHENS</a></p>
					    <?php endif; ?>
			      </figcaption>
			    </figure>

			    <figure class="item">
			      <img src="<?php echo home_url('wp-content/uploads/sites/4/2018/07/Bathroom-Remodeling-1.jpg'); ?>">
					<figcaption>
				      	<p>
					      	<strong>Design the Bathroom</strong>
					      	<em>That Focuses On You</em>
				      	</p>
				      	<?php $bath = get_term_by( 'slug', 'master-bath', 'portfolio_categories' ) ?>

				      	<?php if($bath) : ?>
				      		<p><a href="<?php echo home_url('galleries/master-bath'); ?>">SEE MORE BATHROOMS</a></p>
				      	<?php endif; ?>
			      </figcaption>
			    </figure>

			    <figure class="item">
			      <img src="<?php echo home_url('wp-content/uploads/sites/4/2018/07/Cabinet-Refacing.jpg'); ?>">
					<figcaption>
				      	<p>
					      	<strong>Choose the Cabinets</strong>
					      	<em>That Transform Your Kitchen & Bathrooms</em>
				      	</p>
				      	<?php $cabinet = get_term_by( 'slug', 'cabinet-refacing', 'portfolio_categories' ) ?>

				      	<?php if($cabinet) : ?>
				      		<p><a href="<?php echo home_url('galleries/cabinet-refacing'); ?>">SEE MORE CABINETRY</a></p>
				      	<?php endif; ?>
			      </figcaption>
			    </figure>

			    <figure class="item">
			      <img src="<?php echo home_url('wp-content/uploads/sites/4/2018/07/Interior-Remodeling-2.jpg'); ?>">
					<figcaption>
				      	<p>
					      	<strong>Transform Your Home</strong>
					      	<em>Into the One You’ve Always Wanted</em>
				      	</p>
				      	<?php $interior = get_term_by( 'slug', 'interior-remodeling', 'portfolio_categories' ) ?>

				      	<?php if($interior) : ?>
				      		<p><a href="<?php echo home_url('galleries/interior-remodeling'); ?>">SEE MORE INTERIORS</a></p>
				      	<?php endif; ?>
			      </figcaption>
			    </figure>

			    <figure class="item">
			      <img src="<?php echo home_url('wp-content/uploads/sites/4/2018/07/Safety-Mobility-1.jpg'); ?>">
					<figcaption>
				      	<p>
					      	<strong>Function & Style</strong>
					      	<em>That Creates Your Forever Home</em>
				      	</p>
				      	<?php $safety = get_term_by( 'slug', 'safety-mobility', 'portfolio_categories' ) ?>

				      	<?php if($safety) : ?>
				      		<p><a href="<?php echo home_url('galleries/safety-mobility'); ?>">SEE MORE OPTIONS</a></p>
				      	<?php endif; ?>
			      </figcaption>
			    </figure>

			    <figure class="item">
			      <img src="<?php echo home_url('wp-content/uploads/sites/4/2018/07/General-Contracting.jpg'); ?>">
					<figcaption>
				      	<p>
					      	<strong>Trust & Professionalism</strong>
					      	<em>That Gets the Job Done</em>
				      	</p>
				      	<?php $general = get_page_by_title( 'General Contracting', OBJECT, 'services' ) ?>

				      	<?php if($general) : ?>
				      		<p><a href="<?php echo home_url('services/general-contracting'); ?>">LEARN MORE</a></p>
				      	<?php endif; ?>
			      </figcaption>
			    </figure>

			    <figure class="item">
			      <img src="<?php echo home_url('wp-content/uploads/sites/4/2018/07/Commercial-Remodeling.jpg'); ?>">
					<figcaption>
				      	<p>
					      	<strong>Skilled & Dependable</strong>
					      	<em>For the Scope and <br/>Scale Your Project Requires</em>
				      	</p>
				      	<?php $commercial = get_page_by_title( 'Commercial Remodeling', OBJECT, 'services' ) ?>

				      	<?php if($commercial) : ?>
				      		<p><a href="<?php echo home_url('services/commercial-remodeling'); ?>">LEARN MORE</a></p>
				      	<?php endif; ?>
			      </figcaption>
			    </figure>

			  </div>
		  <?php endif; ?>

	</div>
</section>
