<?php $option = get_blog_option(1, 'dm_settings'); ?>
<?php $selected_badges  = get_post_meta(get_the_ID(), 'campaign_badge', true); ?>

<section class="kb-badges">
	<ul>
		<?php if( isset($selected_badges ) && !empty($selected_badges)) : ?> 
			<?php foreach ($option['badges'] as $badge) : ?>		
				<?php if( in_array($badge['image'], $selected_badges) ): ?>
					<?php switch_to_blog(1); ?>
						<li>
							<?php if( $badge['link'] != ""  ): ?><a href="<?php echo $badge['link']; ?>" target="_blank"><?php endif; ?>
							<img src="<?php echo (isset($badge['image']) ? wp_get_attachment_image_src($badge['image'], 'full')[0] : '')?>" 
								alt="<?php echo (isset($badge['alt']) ? $badge['alt'] : '');?>" 
								title="<?php echo (isset($badge['title']) ? $badge['title'] : '');?>"
							/>
							<?php if( $badge['link'] != "" ): ?></a><?php endif; ?>
						</li>
					<?php restore_current_blog(); ?>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php else : ?>
			<li class="nkba"><a href="https://nkba.org/" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/nkba.png') ?>" /></a></li>
			<li class="nahb"><a href="https://www.nahb.org/" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/nahb.jpg') ?>" /></a></li>
			<li class="nari"><a href="https://www.nari.org/" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/nari.jpg') ?>" /></a></li>
			<li class="big"><a href="http://www.remodeling.hw.net/benchmarks/big50/the-2017-big50_o" target="_blank"><img src="<?php echo Surepress\Functions\Assets\asset_path('images/big50.png') ?>" /></a></li>
		<?php endif; ?>
	</ul>
</section>