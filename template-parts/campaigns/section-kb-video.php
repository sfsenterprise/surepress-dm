<?php
    use Surepress\Functions\Assets as Assets;

    $phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
    $phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
?>

<section class="campaign-kb-video">
    <div class="video-background-wrap">
        <video class="video-background" src="<?php echo Assets\asset_path('images/dm_bg_video.mp4') ?>" title="Dreammaker Background Video" autoplay="" muted="" playsinline="" loop=""></video>	  
    </div>
    <div class="campaign-after-header-wrap">
        <h4>Upgrade Your Home Quickly and Easily With DreamMaker Bath & Kitchen</h4>
        <h1>Create the Perfect Kitchen and Bathroom With DreamMaker Bath & Kitchen</h1>
        <a class="btn btn-info btn-lg" href="#contact">Click Here to Book a Meeting</a>
        <?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
        <a class="after-header-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
        <i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
    </div>				
</section>