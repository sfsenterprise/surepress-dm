<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;

$street = get_post_meta(get_the_ID(), 'campaign_street', TRUE );
$street2 = get_post_meta(get_the_ID(), 'campaign_street2', TRUE );
$city = get_post_meta(get_the_ID(), 'campaign_city', TRUE );
$state = get_post_meta(get_the_ID(), 'campaign_state', TRUE );
$zip = get_post_meta(get_the_ID(), 'campaign_zip', TRUE );
$email = get_post_meta(get_the_ID(), 'campaign_email', TRUE );
$phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
$phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
?>   
                <nav class="navbar navbar-default campaign-navbar-adv" id="header_nav">
                  <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-sm-6 text-sm-center">
							<?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
	                        <a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
	                        	Give Us a Call! <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
                        </div>

                        <div class="col-12 col-sm-6 text-right text-sm-center">
                        	<?php echo $street . (!empty($street2) ? ", ".$street2 : ""); ?> <?php echo $city .", ". $state.", ".$zip; ?>							
                        </div>                        

                    </div>    
                  </div><!-- /.container-fluid -->     
                <div align="center">
	              <a class="campaign-navbar-adv-logo" href="<?php echo site_url('/'); ?>">
	                <img src=" <?php echo Assets\asset_path('images/main-logo-v1.png') ?>" title="DreamMaker Bath & Kitchen" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/> 
	                <em><?php echo $wp_query->nap->microsite_name; ?></em>
	              </a>   
	            </div>                               
                </nav>                       
                <?php get_template_part('template-parts/partials/header', 'campaign-start-conversation-form' ); ?>
                <!--- MICROSITE END-->