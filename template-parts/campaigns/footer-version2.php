<?php
	use Surepress\Functions\Assets as Assets;
	use Surepress\Functions\Common as Common;
	use Surepress\Functions\Services as Services;

	$template =  get_post_meta( get_the_ID(), 'campaign_template', true ); 
?>	 	
		<?php get_template_part('template-parts/campaigns/section', 'badges' ); ?>
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container-fluid">
				
				<div class="row">	
						<figure>
							<a href="<?php echo site_url('/'); ?>">

								<?php if( Common\is_main() ) : ?>
									<img src="<?php echo Assets\asset_path('images/dreammaker-logo-franchises.png')?>" alt="DreamMaker Bath & Kitchen"/>
								<?php else : ?>
									<img src="<?php echo Assets\asset_path('images/dreammaker-logo-franchise.png')?>" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/>
								<?php endif; ?>	
							</a>
							<?php if( !Common\is_main() ) : ?>	
								<figcaption><?php echo $wp_query->nap->microsite_name; ?></figcaption>			
							<?php endif; ?>		
						</figure>

						<div class="newsletter">
							<?php if( Common\is_main() ) : ?>
								<p>DreamMaker's bottom line is people. We believe if we treat people with respect and integrity, success will follow.</p>
								<p><a href="https://www.private-eclub.com/subscribe/dreammakerbath" target="_blank" class="btn btn-link btn-lg">Get Our Newsletter</a></p>
							<?php else : ?>
								<p>The Code of Values informs the way DreamMaker treats you, our customer. We hope you will see the difference.</p>
								<p><a href="https://www.private-eclub.com/subscribe/dreammakerbath" target="_blank" class="btn btn-link btn-lg">Get Our Newsletter</a></p>
							<?php endif; ?>	
						</div>	


						<div class="new-grid">
							<div class="footer-nav">
								<h4 class="title">Services</h4>
								<ul>
								<?php foreach( Services\get_services(0) as $service ) :  ?>
									<li><a href="<?php echo get_permalink($service->ID); ?>"><?php echo $service->post_title; ?></a></li>
								<?php endforeach; ?>
								</ul>
							</div>							
							<div class="footer-nav">
								<h4 class="title">Resources</h4>
								<ul id="menu-resources-menu">
									<?php if( !Common\is_main() ) : ?>
										<li><a href="<?php echo get_site_url(); ?>/directions/">Directions</a></li>
										<li><a href="<?php echo get_site_url(); ?>/faq/">FAQ</a></li>
										<li><a href="<?php echo get_site_url(); ?>/glossary/">Glossary</a></li>
										<li><a href="<?php echo get_site_url(); ?>/blog/">Remodeling Tips</a></li>
										<li><a href="<?php echo get_site_url(); ?>/sitemap/">Sitemap</a></li>
										<li><a href="<?php echo get_site_url(); ?>/privacy-policy/">Privacy Policy</a></li>
									<?php else: ?>
										<li><a href="/locations/">Locations</a></li>
										<li><a href="/faq/">FAQ</a></li>
										<li><a href="/glossary/">Glossary</a></li>
										<li><a href="/blog/">Remodeling Tips</a></li>
										<li><a href="/sitemap/">Sitemap</a></li>
										<li><a href="/privacy-policy/">Privacy Policy</a></li>
									<?php endif; ?>
								</ul>
								
							</div>

							<address>
								<h4>
									<?php echo "<a href='".get_site_url()."/contact-us/'>Contact Us</a> ";	?>
								</h4>
								<p>
									<?php $mongoose_num = preg_replace('/[^0-9]/', '', maybe_unserialize($wp_query->nap->microsite_fields)['mongoose_phone']);?>
									<strong><a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $wp_query->nap->microsite_phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $wp_query->nap->microsite_phone); ?></a></strong><br>
									<?php if ( !empty(unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']) ): ?>
										<a href="mailto:<?php echo unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']; ?>"><?php echo unserialize($wp_query->nap->microsite_fields)['microsite_contact_email']; ?></a>
									<?php endif; ?>
									<?php if( !Common\is_main() ) : ?>
										<?php if ( !empty(($wp_query->nap->microsite_owner)) ): ?>
											<p>This location is independently owned and locally operated by <?php echo $wp_query->nap->microsite_owner; ?> under license from DreamMaker Bath & Kitchen.</p>
										<?php endif; ?>
									<?php endif; ?>	
								</p>						
							</address>
						</div>	

						<div class="footer-social">
							<h4>Connect with Us</h4>
							<?php if( !Common\is_main() ) : ?>
								<a href="<?php echo get_site_url(get_current_blog_ID()); ?>/about/our-team/">Meet The Team</a>
							<?php endif; ?>
							<?php get_template_part('template-parts/widgets/widget','social-media');  ?>
							<p><a href="https://www.dreammakerfranchise.com/" target="_blank" class="btn btn-link btn-lg smaller">Own a Franchise</a></p>
						</div>
						<div class="clearfix"></div>

				</div>
			<!-- end row -->

			</div>			
			<?php $option = get_blog_option(get_current_blog_ID(), 'dm_settings');
				$ccorp_text = (isset($option['footer_ccorp'][0]) && !empty($option['footer_ccorp'][0])) ? $option['footer_ccorp'][0] : "&copy; 2005-" . date('Y') . " Worldwide Refinishing Systems Inc. dba DreamMaker Bath & Kitchen. Independently Owned and Operated Franchises."; ?>
			<p class="copyright"><?php echo $ccorp_text; ?></p>			
		</footer>
		<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0110/2302.js" async="async" ></script>