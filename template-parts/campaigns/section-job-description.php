<section class="campaign-process">
	<div class="container">
		<h2><?php the_title(); ?>
			<small>Essential Duties and Responsibilities</small>
		</h2>
		<ol style="column-count: 1">
			<?php $duties = get_post_meta(get_the_ID(), 'career_duties', true);  ?>
			<?php if(!empty($duties) ) : ?>
				<?php foreach($duties as $duty) : ?>
					<li><?php echo $duty; ?></li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ol>	
	</div>
</section> 