<?php 
	use Surepress\Functions\Assets as Assets;

  $phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
  $phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
?>

<section id="services" class="kb-services">	
	<div class="container">
    <h4>OUR SERVICES AT DREAMMAKER</h4>
    <h2>Kitchen and Bathroom Remodeling Services Made Easy</h2>
		<div class="services-wrapper">
			<div class="services-item">
				<figure>
					<img src="<?php echo Assets\asset_path('images/kb-kitchens.jpg') ?>" />
					<figcaption>
            <h4>EXPERT KITCHEN REMODELING</h4>
						<h2>Kitchen Remodeling</h2>

						Transform your kitchen from ordinary to extraordinary and take advantage of our expertise in customizing your perfect remodel. Every project is made unique at DreamMaker Bath & Kitchen, taking you through the entire design process with one of our experienced professionals.					
					</figcaption>
				</figure>
				<div class="service-btn-wrap hide-wrap-desktop">
					<a class="btn btn-info btn-lg" href="#contact">Click Here to Book a Meeting</a>
					<?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
					<a class="service-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
					<i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
				</div>	
			</div>
			<div class="services-item">
				<figure>
					<img src="<?php echo Assets\asset_path('images/kb-bathrooms.jpg') ?>" />
					<figcaption>
            <h4>TRANSFORM YOUR BATHROOM</h4>
						<h2>Bathroom Remodeling</h2>

						If you’ve been dreaming of a bathroom makeover, DreamMaker Bath & Kitchen can make it happen. Our team of remodelers can transform any room into the bath of your dreams. Whether you want to modernize, update or start from scratch, our experienced designers will make it come to life. 					
					</figcaption>
				</figure>
				<div class="service-btn-wrap">
					<a class="btn btn-info btn-lg" href="#contact">Click Here to Book a Meeting</a>
					<?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
					<a class="service-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
					<i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
				</div>
			</div>	
			<div class="services-item">
				<figure>
					<img src="<?php echo Assets\asset_path('images/kb-interior.jpg') ?>" />
					<figcaption>
            <h4>INTERIOR OF YOUR DREAMS</h4>
						<h2>Interior Remodeling</h2>

						At DreamMaker Bath & Kitchen, we believe a beautiful home starts with smartly designed spaces. Through interior remodeling, we help you reevaluate how your home functions and can provide creative solutions to make it work better. Start living your dream space today! 					
					</figcaption>
				</figure>
				<div class="service-btn-wrap hide-wrap-desktop">
					<a class="btn btn-info btn-lg" href="#contact">Click Here to Book a Meeting</a>
					<?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
					<a class="service-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
					<i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
				</div>
			</div>					
		</div>
    <!-- <div class="service-btn-wrap">
      <a class="btn btn-info btn-lg" href="#contact">Click Here to Book a Meeting</a>
      <?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
      <a class="service-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
      <i aria-hidden="true" class="fa fa-phone"></i> Click Here to Call Us <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
    </div> -->
	</div>
</section>	