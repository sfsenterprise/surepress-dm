<?php 	
	use Surepress\Functions\Common as Common; 
	$data = Common\get_events(); //function will fetch post type team where order = 1
	$now = strtotime( date('Y-m-d'));
	$cnt = 0;
?>

<section class="events-section <?php echo (count($data) <= 0) ? 'hidden' : ''; ?>" >
	<h2>Events</h2>
		<div class="row">
		<?php foreach ($data as $event ): ?>
			<?php $event_schedules = Common\populate_event_schedules($event->ID); ?>
			<?php $event_schedules = json_encode($event_schedules); ?>
			<?php $show = false; ?>
			<?php $date = get_field('event_date', $event->ID);?>

			<?php foreach( $date as $key => $value ): ?>
				<?php if( strtotime( $value ) >= $now ) $show = true; ?>
			<?php endforeach; ?>

			<?php if( $cnt < 3 && $show != false ) : ?>
				<article class="col-xs-12 col-sm-4">
					<h3><?php echo $event->post_title; ?></h3>
						<?php foreach( $date as $key => $value ): ?>
							<?php if( strtotime( $value ) >= $now ) : ?>
								<p class="date">
									<?php echo (date_create($value) ? date_format(date_create($value),"d F Y") : "" ); ?>	
								</p>
							<?php endif; ?>
						<?php endforeach; ?>
					<p><?php echo wp_trim_words( $event->post_content, 55, '...'); ?></p>
					<p class="button">
						<a href="<?php echo get_field('event_link'); ?>" class="btn btn-info" data-title="<?php echo $event->post_title; ?>" data-toggle="modal" data-target="#event1" data-eventid="<?php echo $event->ID; ?>" data-schedule='<?php echo $event_schedules; ?>'>Register Now</a>
					</p>
				</article>
				<?php $cnt++; ?>
			<?php endif; ?>

		<?php endforeach; ?>
		</div>
</section>	
<?php if( $show != false ): ?>
	<div class="event-modal">
		<?php get_template_part('template-parts/archives/archive','events-form'); ?>
	</div>
<?php endif; ?>