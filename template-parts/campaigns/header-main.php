<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
?>   
                <nav class="navbar navbar-default campaign-navbar" id="header_nav">
                  <div class="container-fluid">
                    <div class="row">
                        <div class="navbar-header">
                          <a class="navbar-brand" href="<?php echo site_url('/'); ?>">
                            <img src=" <?php echo Assets\asset_path('images/main-logo-v1.png') ?>" title="DreamMaker Bath & Kitchen" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/> 
                            <em><?php echo $wp_query->nap->microsite_name; ?></em>
                          </a>
                        </div>

                        <p class="nav navbar-right">
                        <?php 
                            $street = get_post_meta(get_the_ID(), 'campaign_street', TRUE );
                            $street2 = get_post_meta(get_the_ID(), 'campaign_street2', TRUE );
                            $city = get_post_meta(get_the_ID(), 'campaign_city', TRUE );
                            $state = get_post_meta(get_the_ID(), 'campaign_state', TRUE );
                            $zip = get_post_meta(get_the_ID(), 'campaign_zip', TRUE );
                            $email = get_post_meta(get_the_ID(), 'campaign_email', TRUE );
                            $phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
                            $phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
                        ?>
                        <?php echo $street . (!empty($street2) ? ", ".$street2 : ""); ?><br>
                        <?php echo $city .", ". $state.", ".$zip; ?><br>
                        
                        <?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
                        <strong><i class="fa fa-phone"></i><a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a></strong>
								
                        </p>                        

                    </div>    
                  </div><!-- /.container-fluid -->
                </nav>        
                <?php get_template_part('template-parts/partials/header', 'campaign-start-conversation-form' ); ?>
                <!--- MICROSITE END-->