<?php 
	use Surepress\Functions\Assets as Assets;
  use Surepress\Functions\Locations as Locations;
  use Surepress\Functions\Common as Common;

  $street = get_post_meta(get_the_ID(), 'campaign_street', TRUE );
  $street2 = get_post_meta(get_the_ID(), 'campaign_street2', TRUE );
  $city = get_post_meta(get_the_ID(), 'campaign_city', TRUE );
  $state = get_post_meta(get_the_ID(), 'campaign_state', TRUE );
  $zip = get_post_meta(get_the_ID(), 'campaign_zip', TRUE );
  $email = get_post_meta(get_the_ID(), 'campaign_email', TRUE );
  $phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
  $phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
?>

<section id="contact" class="kb-contactus">	
	<div class="container">
		<div class="contactus-wrapper">
			<div class="contactus-item">
        <h4>SCHEDULE A FREE CONSULTATION NOW</h4>
        <h2>Fill Out This Form to Schedule a No Hassle Meeting Today</h2>
        <div class="form-wrapper" style="background: url('<?php echo Assets\asset_path('images/kb-contact-bkg.jpg') ?>');">
          <?php gravity_form( 'Perfect Kitchen and Bathroom',false, false,false, false,true, 400, true ); ?>
        </div>
			</div>
			<div class="contactus-item">
        <h4>CONTACT DREAMMAKER TODAY</h4>
        <h2>Contact Us Today for the Best Remodeling Customer Service</h2>
        <div class="map-wrapper">
          <?php $lat = ($wp_query->nap->microsite_lat != "0.000000") ? $wp_query->nap->microsite_lat : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lat']; ?>
          <?php $lng = ($wp_query->nap->microsite_lng != "0.000000") ? $wp_query->nap->microsite_lng : Locations\get_latlng( $wp_query->nap->microsite_zip_postal )['results'][0]['geometry']['location']['lng']; ?>
          <?php $title = ( Common\is_main() ) ? 'DreamMaker Bath & Kitchen Corporate Headquarters' : 'DreamMaker of '.$wp_query->nap->microsite_name; ?>
          <iframe src = "https://maps.google.com/maps?q=<?php echo $title; ?>&z=10&output=embed" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          <!-- <iframe src = "https://maps.google.com/maps?q=<?php //echo $lat.','.$lng.'&title='.$title; ?>&z=10&output=embed" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe> -->
        </div>
			</div>					
		</div>
    <div class="contact-details-wrap">
      <div class="contact-box">
        <h4>PHONE NUMBER</h4>
        <?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
        <p><a class="service-phone" href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>"><?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a></p>
      </div>
      <div class="contact-box">
        <h4>EMAIL ADDRESS</h4>
        <p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
      </div>
      <div class="contact-box">
        <h4>STREET ADDRESS</h4>
        <p><?php echo $street . (!empty($street2) ? ", ".$street2 : ""); ?> <?php echo $city .", ". $state.", ".$zip; ?></p>
      </div>
    </div>
	</div>
</section>	