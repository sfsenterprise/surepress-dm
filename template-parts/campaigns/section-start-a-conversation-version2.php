<?php
global $cta_option;
?>
		<section class="campaign-form-version2" >
			<div class="container">
			<div id="campaignsanchor"></div>
			<h2>Talk to Us Today About Your Remodeling Dreams</h2>
			 <?php  gravity_form( 'Talk to Us Today About Your Remodeling Dreams',false, false,false, false,true, 400, true );    ?>
			</div>
		</section>
		<!-- for changing button label on forms -->
<script type="text/javascript">
	jQuery('.gform_button').val("<?php echo (isset($cta_option) && $cta_option != '' ? $cta_option : 'START A CONVERSATION');  ?>");
</script>