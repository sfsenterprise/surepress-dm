<?php use Surepress\Functions\Assets as Assets; ?>
<section class="campaign-content" style="margin:  0 -15px; background-size: cover; background-image:url(<?php echo Assets\asset_path('images/contact-us-form-bg.jpg') ?>)">
	<div class="container">
		<h1 style="font-family: 'Open Sans';">What we are looking for?</h1>
		<h2>Education, Experience and Other Qualifications</h2>
		<?php 
			$experience = get_post_meta( get_the_ID() , 'career_educ_exp', true); 
			echo $experience;
		?>

		<?php
			$qualifications = get_post_meta( get_the_ID(), 'career_qualifications', true); 
			if (!empty($qualifications) ) :
		?>
			<p>&nbsp;</p>
			<ul class="mt-3 mb-3">
				<?php foreach($qualifications as $qualification): ?>
					<li><?php echo $qualification; ?></li>
				<?php endforeach;?>	
			</ul>
			<p>&nbsp;</p>	
		<?php endif; ?>

		<p><a class="btn btn-info btn-lg" href="#applyForm">Apply Today</a></p>
	</div>
</section>