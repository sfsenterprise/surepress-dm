<?php 
	use Surepress\Functions\Common as Common;
	global $cta_option;
	$cta_option = get_post_meta(get_the_ID(), 'campaign_cta_btn', true); 
?>
	<div class="container-fluid">
			<?php get_template_part('template-parts/campaigns/section', 'video-version2' ); ?>
			<?php get_template_part('template-parts/campaigns/section', 'features-kitchen' ); ?>			
		<?php get_template_part('template-parts/campaigns/section', 'steps' ); ?>
	</div>
	<?php get_template_part('template-parts/campaigns/section', 'start-a-conversation-version2' ); ?>