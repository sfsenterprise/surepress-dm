<?php 
	use Surepress\Functions\Common as Common;
	global $cta_option;
	$cta_option = get_post_meta(get_the_ID(), 'campaign_cta_btn', true); 
?>
	<div class="container-fluid">
		<?php get_template_part('template-parts/campaigns/section', 'kb-video' ); ?>
		<?php get_template_part('template-parts/campaigns/section', 'kb-badges' ); ?>
		<?php get_template_part('template-parts/campaigns/section', 'kb-about' ); ?>			
		<?php get_template_part('template-parts/campaigns/section', 'kb-services' ); ?>
		<?php get_template_part('template-parts/campaigns/section', 'kb-gallery' ); ?>
		<?php get_template_part('template-parts/campaigns/section', 'kb-contact' ); ?>
	</div>