<?php 
	use Surepress\Functions\Common as Common;
	global $cta_option;
	$cta_option = get_post_meta(get_the_ID(), 'campaign_cta_btn', true); 
?>
	<div class="container-fluid">
		<div class="home_top_slide">
			<?php get_template_part('template-parts/campaigns/section', 'home-slider'); ?>	
		</div>	
		<?php get_template_part('template-parts/campaigns/section', 'video' ); ?>				
		<?php get_template_part('template-parts/campaigns/section', 'home-category-thumbs' ); ?>
	</div>
	<?php get_template_part('template-parts/campaigns/section', 'home-process' ); ?>

	<?php get_template_part('template-parts/campaigns/section', 'content' ); ?>

	<?php get_template_part('template-parts/campaigns/section', 'start-a-conversation' ); ?>

	<?php get_template_part('template-parts/campaigns/section', 'home-reviews' ); ?>