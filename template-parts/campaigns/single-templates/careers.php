
	<div class="container-fluid">
		<div class="home_top_slide">
			<input type="hidden" id="site-id" name="site-id" value="<?php echo get_current_blog_id(); ?>">
			<?php get_template_part('template-parts/campaigns/section', 'job-slider'); ?>
		</div>	

        <?php get_template_part('template-parts/campaigns/section', 'job-blurb' ); ?>
        <?php get_template_part('template-parts/campaigns/section', 'job-description' ); ?>
        <?php get_template_part('template-parts/campaigns/section', 'job-qualification' ); ?>		
	</div>