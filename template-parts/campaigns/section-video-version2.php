<?php
	$yt_id = get_post_meta(get_the_ID(), 'campaign_video', TRUE);
	$template =  get_post_meta( get_the_ID(), 'campaign_template', true );
?>

<?php if($yt_id && $yt_id !== "") : ?>
<section class="campaign-video-version2">
	<div class="container">
		<h2>Get the <?php echo ($template === 'version2_b' ? 'Bathroom' : 'Kitchen'); ?> of Your Dreams</h2>		
		<h4>DreamMaker <?php echo $wp_query->nap->microsite_name; ?> : Trusted <?php echo ($template === 'version2_b' ? 'Bathroom' : 'Kitchen'); ?> Remodelers</h4>
		
		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $yt_id; ?>?modestbranding=1&rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	  
		</div>

		<p><a href="#campaignsanchor" class="btn btn-primary">START A CONVERSATION</a></p>				
	</div>	
</section>
<?php else : ?>
<section class="campaign-video-version2">
	<div class="container">
		<h2>Get the <?php echo ($template === 'version2_b' ? 'Bathroom' : 'Kitchen'); ?> of Your Dreams</h2>		
		<h4>DreamMaker <?php echo $wp_query->nap->microsite_name; ?> : Trusted <?php echo ($template === 'version2_b' ? 'Bathroom' : 'Kitchen'); ?> Remodelers</h4>
		
		<div class="embed-responsive embed-responsive-16by9">
			<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
				<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
					<iframe src="https://fast.wistia.net/embed/iframe/z6b6oi88jy?videoFoam=true " title="Y2Mate.is < https://y2mate.is/ > - DreamMaker Approach 30--2YtcNtv-X8-1080p-1657149860548 Video" allow="autoplay; fullscreen" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" msallowfullscreen width="100%" height="100%"></iframe>
				</div>
			</div>
			<script src=" https://fast.wistia.net/assets/external/E-v1.js " async></script>
		</div>
		
		<p><a href="#campaignsanchor" class="btn btn-primary">START A CONVERSATION</a></p>			
	</div>	
</section>
<?php endif; ?>