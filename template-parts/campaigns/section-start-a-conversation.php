<?php
global $cta_option;
?>
		<section class="start-a-conversation campaigns" >
			<div id="campaignsanchor"></div>
			<h2>Schedule Your Free Consultation Today! </h2>
			 <?php  gravity_form( 'Start A Conversation - Campaign',false, false,false, false,true, 400, true );    ?>
			 <div class="sub">By submitting this request, you consent to receive from DreamMaker Bath and Kitchen emails to the provided email address and text messages with marketing updates sent through an automatic telephone dialing system to the provided mobile phone number. Your consent to these terms is not a condition of purchase.</div>
		</section>
		<!-- for changing button label on forms -->
<script type="text/javascript">
	jQuery('.gform_button').val("<?php echo (isset($cta_option) && $cta_option != '' ? $cta_option : 'START A CONVERSATION');  ?>");
</script>