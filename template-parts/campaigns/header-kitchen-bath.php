<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;

$street = get_post_meta(get_the_ID(), 'campaign_street', TRUE );
$street2 = get_post_meta(get_the_ID(), 'campaign_street2', TRUE );
$city = get_post_meta(get_the_ID(), 'campaign_city', TRUE );
$state = get_post_meta(get_the_ID(), 'campaign_state', TRUE );
$zip = get_post_meta(get_the_ID(), 'campaign_zip', TRUE );
$phone = get_post_meta(get_the_ID(), 'campaign_phone', TRUE );
$phonetracking = get_post_meta(get_the_ID(), 'campaign_phonetracking', TRUE );
?>   
<nav class="navbar navbar-default campaign-navbar-adv" id="header_nav">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 text-sm-center">
                <?php $mongoose_num = preg_replace('/[^0-9]/', '', $phonetracking);?>
                <a href="tel:<?php echo (isset($mongoose_num) && !empty($mongoose_num) ? $mongoose_num : $phone); ?>">
                <i aria-hidden="true" class="fa fa-phone"></i> Give Us a Call Today: <?php echo (isset($mongoose_num) && !empty($mongoose_num) ?  Common\formatPhone($mongoose_num) : $phone); ?></a>
            </div>
            <div class="col-12 col-sm-6 text-right text-sm-center">
                <i aria-hidden="true" class="fa fa-home"></i> <?php echo $street . (!empty($street2) ? ", ".$street2 : ""); ?> <?php echo $city .", ". $state.", ".$zip; ?>							
            </div>                        
        </div>    
    </div><!-- /.container-fluid -->     
    <div class="campaign-header-wrap" align="center">
        <ul class="header-menu">
            <li><a href="#about">About Us</a></li>
            <li><a href="#services">Our Services</a></li>
        </ul>
        <a class="campaign-header-logo" href="<?php echo site_url('/'); ?>">
            <img src=" <?php echo Assets\asset_path('images/main-logo-v1.png') ?>" title="DreamMaker Bath & Kitchen" alt="DreamMaker Bath & Kitchen of <?php echo $wp_query->nap->microsite_name; ?>"/> 
            <em><?php echo $wp_query->nap->microsite_name; ?></em>
        </a>
        <ul class="header-menu">
            <li><a href="#gallery">Photo Gallery</a></li>
            <li><a href="#contact">Contact Us</a></li>
        </ul>
        <div class="mobile-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="navbar" class="mobile-main-menu navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                <ul class="mobile-header-menu nav navbar-nav">
                    <li><a href="#about">About Us</a></li>
                    <li><a href="#services">Our Services</a></li>
                    <li><a href="#gallery">Photo Gallery</a></li>
                    <li><a href="#contact">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>                               
</nav>                       