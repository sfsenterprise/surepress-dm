<?php
use Surepress\Functions\Assets as Assets;
?>


<!-- custom process for reno -->
<?php if( get_current_blog_id() == 90 ): ?>
	<div class="intro container">
		<h2>DreamMaker’s Remodeling Process</h2>
		<span  class="intro-txt col-lg-6">
			<h6>It’s our job, but we never forget it’s your home.</h6>
			<p>That’s why DreamMaker Bath & Kitchen stands behind our ethical, honest, professional and trustworthy team members. They are working in your home to make your remodeling dreams come true.</p>
			<p>Our distinctive, full-service remodeling approach focuses on serving our clients from design through installation. With a dedication to superior craftsmanship and ethical excellence, our specialists are committed to delivering a design that meets your specific needs, while communicating consistently throughout the entire process. You can feel secure that your project will be completed in the most efficient manner possible with minimal disruption to your life.</p>
		</span>	
		<span class="col-lg-6 processlist" >
			<a href="#process-1">Meet With a DreamMaker Designer</a>
			<a href="#process-2">Home Survey</a>
			<a href="#process-3">Budget Review</a>
			<a href="#process-4">Design Review & Written Proposal</a>
			<a href="#process-5">Sign Agreement</a>
			<a href="#process-6">Pre-Start Conference Meeting</a>
			<a href="#process-7">Your Project Manager is Your Contact</a>
		</span> 		
	</div>
	<div class="process-list">
		<a class="hidden-anchor" id="process-1"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-1.png') ?>" alt="Your Project Manager Is Your Contact" />
				<font>1</font>
				<figcaption><h6>Meet With a DreamMaker Designer</h6>
				<p>Meet with us in the design center or by webinar. During our first meeting together, a DreamMaker designer will listen carefully and discuss your goals so your remodeling dreams are incorporated into your new space. We look forward to you sharing photos of your existing space and encourage you to bring or show any sketches or inspiration that you have. After this meeting, we will schedule your home survey and design review meeting.</p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-2"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-2.png') ?>" alt="Home Survey" />
				<font>2</font>
				<figcaption><h6>Home Survey</h6>
					<p>Following protocols, your DreamMaker designer will come to your home and complete a survey which includes taking measurements, photos and looking for any structural, plumbing or electrical issues that may need to be resolved or incorporated into the design. The more information your designer has, the more detailed and precise your initial estimate is going to be. Once your home survey is complete, your DreamMaker designer will set the next meeting.</p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-3"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-4.png') ?>" alt="Budget Review" />
				<font>3</font>
				<figcaption><h6>Budget Review</h6>
				<p>At the budget review meeting, the Sales/Design team will give you a range for what your project is likely to cost, based on the details discussed so far. If you are agreeable and want to continue with the process, DreamMaker will take a small retainer (which can be applied to the contract) and you will move into the Design phase. </p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-4"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-3.png') ?>" alt="Design Review & Written Proposal" />
				<font>4</font>
				<figcaption><h6>Design Review & Written Proposal</h6>
				<p>During this phase, we will review the designs we have crafted for you and together, land on a final design that includes every aspect of your dream remodel. As we present designs, you should also expect a few proposal options to match the designs to give you a more focused lens on what your desired remodel will cost. </p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-5"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-4.png') ?>" alt="Sign Agreement" />
				<font>5</font>
				<figcaption><h6>Sign Agreement</h6>
					<p>For us, a detailed contract sets the tone for excellent communication and clearly maps our expectations of the project. Your agreement will include the exact week we’ll start your project, how many days we expect the work to take and all of the drawings and documents. </p>
					<p>We are serious about delivering on our promises.</p>

				</figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-6"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-5.png') ?>" alt="Meet With a Dreammaker Designer"   />
				<font>6</font>
				<figcaption><h6>Pre-Start Conference Meeting</h6>
					<p>After the finalized plans and specifications are signed and processed, the project manager will arrange a pre-start conference meeting to review the scope of schedule, construction and installation process. </p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-7"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-6.png') ?>" alt="Pre-start Conference Meeting"   />
				<font>7</font>
				<figcaption><h6>Your Project Manager is Your Contact</h6>
					<p>Your project manager will be your dedicated point of contact throughout your entire project and will also coordinate any extra pros we bring in to get things done expertly. Should you experience any concerns or have any questions, call your project manager immediately. We will ensure your home gets quality installation and the dream remodel you envisioned. </p></figcaption>
			</figure>
		</div>
	</div>

<!-- process for other microsites except ann-arbor -->
<?php elseif( get_current_blog_id() !== 23 ): ?>
	<div class="intro container">
		<h2>DreamMaker’s Remodeling Process</h2>
		<span  class="intro-txt col-lg-6">
			<h6>It’s our job, but we never forget it’s your home.</h6>
			<p>That’s why DreamMaker Bath & Kitchen stands behind our ethical, honest, professional and trustworthy team members. They are working in your home to make your remodeling dreams come true.</p>
			<p>Our distinctive, full-service remodeling approach focuses on serving our clients from design through installation. With a dedication to superior craftsmanship and ethical excellence, our specialists are committed to delivering a design that meets your specific needs, while communicating consistently throughout the entire process. You can feel secure that your project will be completed in the most efficient manner possible with minimal disruption to your life.</p>
		</span>	
		<span class="col-lg-6 processlist" >
			<a href="#process-1">Meet With a DreamMaker Designer</a>
			<a href="#process-2">Home Survey</a>
			<a href="#process-3">Design Review & Written Proposal</a>
			<a href="#process-4">Sign Agreement</a>
			<a href="#process-5">Pre-Start Conference Meeting</a>
			<a href="#process-6">Your Project Manager is Your Contact</a>
		</span> 		
	</div>
	<div class="process-list">
		<a class="hidden-anchor" id="process-1"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-1.png') ?>" alt="Your Project Manager Is Your Contact" />
				<font>1</font>
				<figcaption><h6>Meet With a DreamMaker Designer</h6>
				<p>Meet with us in the design center or by webinar. During our first meeting together, a DreamMaker designer will listen carefully and discuss your goals so your remodeling dreams are incorporated into your new space. We look forward to you sharing photos of your existing space and encourage you to bring or show any sketches or inspiration that you have. After this meeting, we will schedule your home survey and design review meeting.</p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-2"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-2.png') ?>" alt="Home Survey" />
				<font>2</font>
				<figcaption><h6>Home Survey</h6>
					<p>Following protocols, your DreamMaker designer will come to your home and complete a survey which includes taking measurements, photos and looking for any structural, plumbing or electrical issues that may need to be resolved or incorporated into the design. The more information your designer has, the more detailed and precise your initial estimate is going to be. Once your home survey is complete, your DreamMaker designer will set the next meeting.</p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-3"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-3.png') ?>" alt="Sign Agreement" />
				<font>3</font>
				<figcaption><h6>Design Review & Written Proposal</h6>
				<p>During this phase, we will review the designs we have crafted for you and together, land on a final design that includes every aspect of your dream remodel. As we present designs, you should also expect a few proposal options to match the designs to give you a more focused lens on what your desired remodel will cost. </p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-4"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-4.png') ?>" alt="Design Review & Written Proposal" />
				<font>4</font>
				<figcaption><h6>Sign Agreement</h6>
					<p>For us, a detailed contract sets the tone for excellent communication and clearly maps our expectations of the project. Your agreement will include the exact week we’ll start your project, how many days we expect the work to take and all of the drawings and documents. </p>
					<p>We are serious about delivering on our promises.</p>

				</figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-5"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-5.png') ?>" alt="Meet With a Dreammaker Designer"   />
				<font>5</font>
				<figcaption><h6>Pre-Start Conference Meeting</h6>
					<p>After the finalized plans and specifications are signed and processed, the project manager will arrange a pre-start conference meeting to review the scope of schedule, construction and installation process. </p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-6"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-6.png') ?>" alt="Pre-start Conference Meeting"   />
				<font>6</font>
				<figcaption><h6>Your Project Manager is Your Contact</h6>
					<p>Your project manager will be your dedicated point of contact throughout your entire project and will also coordinate any extra pros we bring in to get things done expertly. Should you experience any concerns or have any questions, call your project manager immediately. We will ensure your home gets quality installation and the dream remodel you envisioned. </p></figcaption>
			</figure>
		</div>
	</div>

<?php else: ?>
	<div class="intro container">
		<h2>DreamMaker’s Safe Remodeling Process</h2>
		<span  class="intro-txt col-lg-6">
			<h6>It’s our job, but we never forget it’s your home.</h6>
			<p>That’s why DreamMaker Bath & Kitchen stands behind our safe, ethical, honest, professional and trustworthy team members. They are working in your home to make your remodeling dreams come true.</p>
			<p>Our distinctive, full-service remodeling approach focuses on serving our clients safely from design through installation. With a dedication to superior craftsmanship and ethical excellence, our specialists are committed to delivering a design that meets your specific needs, while communicating consistently throughout the entire process. You can feel secure that your project will be completed in the most efficient and safe manner possible with minimal disruption to your life.</p>
		</span>	
		<span class="col-lg-6 processlist" >
			<a href="#process-1">Safely Meet With a DreamMaker Designer</a>
			<a href="#process-2">Home Visit</a>
			<a href="#process-3">Design Process and Concept Review</a>
			<a href="#process-4">Sign Agreement</a>
			<a href="#process-5">Pre-Production Meeting</a>
			<a href="#process-6">Your Field Supervisor is Your Contact</a>
		</span> 		
	</div>
	<div class="process-list">
		<a class="hidden-anchor" id="process-1"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-1.png') ?>" alt="Your Field Supervisor Is Your Contact" />
				<font>1</font>
				<figcaption><h6>Safely Meet With a DreamMaker Designer</h6>
				<p>Meet with us in the design center. During our first meeting together, a DreamMaker designer will listen carefully and discuss your goals so your remodeling dreams are incorporated into your new space. We look forward to you sharing photos of your existing space and encourage you to bring or show any sketches or inspiration that you have. After this meeting, we will schedule your home visit and design review meeting.</p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-2"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-2.png') ?>" alt="Home Visit" />
				<font>2</font>
				<figcaption><h6>Home Visit</h6>
					<p>Following safe protocols, your DreamMaker designer will come to your home and complete a survey which includes taking measurements, photos and looking for any structural, plumbing or electrical issues that may need to be resolved or incorporated into the design. The more information your designer has, the more detailed and precise your initial estimate is going to be. Once your home visit is complete, your DreamMaker designer will set the next meeting.</p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-3"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-3.png') ?>" alt="Sign Agreement" />
				<font>3</font>
				<figcaption><h6>Design Process and Concept Review</h6>
				<p>During this phase, we will review the designs we have crafted for you and together, land on a final design that includes every aspect of your dream remodel. As we present designs, you should also expect a few proposal options to match the designs to give you a more focused lens on what your desired remodel will cost. </p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-4"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-4.png') ?>" alt="Design Process and Concept Review" />
				<font>4</font>
				<figcaption><h6>Sign Agreement</h6>
					<p>For us, a detailed contract sets the tone for excellent communication and clearly maps our expectations of the project. Your agreement will include the anticipated date range we plan to start your project, how many days we expect the work to take and all of the drawings and documents. </p>
					<p>We are serious about delivering on our promises.</p>

				</figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-5"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-5.png') ?>" alt="Meet With a Dreammaker Designer"   />
				<font>5</font>
				<figcaption><h6>Pre-Production Meeting</h6>
					<p>After the finalized plans and specifications are signed and processed, the field supervisor will arrange a pre-production meeting to review the scope of schedule, construction and installation process. </p></figcaption>
			</figure>
		</div>
		<a class="hidden-anchor" id="process-6"></a>
		<div>
			<figure>
				<img src="<?php echo Assets\asset_path('images/our-process-6.png') ?>" alt="Pre-Production Meeting"   />
				<font>6</font>
				<figcaption><h6>Your Field Supervisor is Your Contact</h6>
					<p>Your field supervisor will be your dedicated point of contact throughout your entire project and will also coordinate any trade partners we bring in to get things done expertly. Should you experience any concerns or have any questions, call your field supervisor immediately. We will ensure your home gets quality installation and the dream remodel you envisioned. </p></figcaption>
			</figure>
		</div>
	</div>
<?php endif; ?>


