<?php
    get_header(); 
?>
<section class="hero-image">
    <h1>Helpful Videos</h1>

</section>
<div class="container">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >   
        <?php
            // use Surepress\Functions\Common as Common;
            use Surepress\Functions\Videos as Videos;
            $videos = Videos\get();
            $video_limit = 9;
        ?>

        <?php echo Videos\content()['video_page_content']; ?>

        <?php if ( count($videos) > 0 ) : ?>
        	<ul class="video_list" id="yt_video_list">
        	<?php $ctr	= 1; ?>
        	<?php foreach($videos as $video) : ?>
        		<?php if($ctr > $video_limit) break; ?>
        		<li>
				<div class="embed-responsive embed-responsive-16by9">
				  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allow="encrypted-media; gyroscope; modestbranding" allowfullscreen></iframe>				  
				</div>        			
        		</li>
        		<?php $ctr++; ?>
        	<?php endforeach; ?>	
        	</ul>

        	<?php if( count($videos) > $video_limit) : ?>
        	<div align="center" class="clearfix">
        		<button class="btn" id="load_more_ytvids" data-page="2" data-limit="<?php echo $video_limit; ?>">Load More <i class="fa fa-plus-square"></i></button>
        	</div>	
        	<?php endif; ?>	

        <?php else : ?>
            No videos found. 
        <?php endif; ?> 
    </div>
</div>     
<?php get_footer(); ?>