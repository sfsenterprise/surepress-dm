<?php
use Surepress\Functions\Assets as Assets;
use Surepress\Functions\Common as Common;
use Surepress\Functions\Google as Google;
use Surepress\Functions\Scripts as Scripts;
?>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="<?php echo Assets\asset_path('images/favicon.png') ?>" type="image/x-icon" />
      <link rel="shortcut icon" href="<?php echo Assets\asset_path('images/favicon.png') ?>" type="image/x-icon" />      
        <?php wp_head(); ?> 
        
        <script type="application/ld+json">
          <?php echo Google\json_ld(); ?>
        </script>

      <?php $leads =  get_blog_option(get_current_blog_ID(), 'dm_settings'); ?>
      <?php if( isset($leads['slmcleads_key'][0]) && !empty($leads['slmcleads_key'][0]) ):?>
          <script>var pAuth = '<?php echo $leads['slmcleads_key'][0]; ?>';</script>
          <script type='text/javascript' src='https://libs.sfs.io/public/lead-script-gf.js?v=2.1.00'></script>
      <?php endif; ?>
      
      <?php 
        if( isset($leads['yext_key']['ktags']) && !empty($leads['yext_key']['ktags']) ):
            echo $leads['yext_key']['ktags'];
        endif; 
      ?>
      <!-- Google Tag Manager -->
      <?php if (!empty($wp_query->nap->microsite_gtm_ga)): ?>
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','<?php echo $wp_query->nap->microsite_gtm_ga; ?>');</script>
      <?php endif; ?>
      <!-- End Google Tag Manager -->
       <!-- DRS-660 -->
       <style>
        .home-category-thumbs ul li figure h2 {
          transition: all 0.5s ease !important;
          display: flex !important;
          flex-direction: column-reverse !important;
          height: 33% !important;
          margin: -20px -20px 0 !important;
          font-family: 'Open Sans' !important;
          font-weight: 700 !important;
          font-size: 1em !important;
          justify-content: center !important;
          color: #fff !important;
          margin-top: 0px !important;
        }

        section.our-team h3 {
          text-align: center;
          color: #432a71;
          margin-bottom: 25px;
          font-family: 'Neuton';
          font-weight: 700;
          font-size: 2.188em;
        }

        .home-reviews h3 {
          color: #fff;
          margin-bottom: 25px;
          font-family: 'Neuton';
          font-weight: 700;
          font-size: 2.188em;
        }

        .home-reviews h3 small {
          font-size: 0.594em;
          color: #fff;
          font-family: 'Open Sans';
          display: block;
          font-weight: 700;
          margin-top: 15px;
        }

        section.call-to-action.animate h3 {
          margin: 0 0 20px;
          color: #fff;
          font-size: 2.188em;
          font-family: 'Neuton';
          font-weight: 700;
        }

        .home-process h3 {
          margin: 0;
          padding: 0;
          font-size: 2.2em;
          font-family: 'Neuton';
          font-weight: 700;
          color: #432a71;
        }

        .home-process h3 small {
          display: block;
          font-size: 0.657em;
          font-family: 'Open Sans';
          font-weight: 700;
          color: #332e30;
          margin-top: 15px;
        }
      </style>
    </head>

    <body <?php body_class(); ?>>       
        <?php if (!empty($wp_query->nap->microsite_gtm_ga)): ?>
          <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $wp_query->nap->microsite_gtm_ga; ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
          <!-- End Google Tag Manager (noscript) -->
        <?php endif; ?>
        <?php echo Scripts\body_scripts(); ?>

        <header id="masthead" class="site-header" role="banner">
            <?php if( Common\is_main() ) : ?>
                <?php get_template_part('template-parts/partials/header', 'main'); ?>
            <?php else : ?>
                <?php get_template_part('template-parts/partials/header', 'microsite'); ?>
            <?php endif; ?>    
        </header>

        <main>